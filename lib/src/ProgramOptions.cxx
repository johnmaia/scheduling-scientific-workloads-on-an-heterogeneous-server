#include "ProgramOptions.h"

using namespace std;
namespace po = boost::program_options;

// E PRECISO DEFINIR AS OPTS PADRAO
// read files, etc, em que se verifica a existencia dos ficheiros

ProgramOptions::ProgramOptions (void){
	desc.add_options()
		("help,h", "produces the help message")
		("file,f", po::value<string>(), "input root file")
		("directory,d", po::value<string>(), "directory with a set of input files")
		("output,o", po::value<string>(), "output event file name")
		("record,r", po::value<string>(), "output recorded variables file name")
		("pdf,p", po::value<string>(), "PDF histograms output file name")
		("background,b", po::value<string>(), "Background PDF file name")
		("signal,s", po::value<string>(), "Singal PDF file name")
	;
}

template<typename T>
void ProgramOptions::add (string name, string short_name, string description, bool required) {
	stringstream opt;

	//options[name] = required;
	opt << name << "," << short_name;
	desc.add_options(opt.str().c_str(), po::value<T>(), description.c_str());

}

template<typename T>
void ProgramOptions::add (string name, string description, bool required) {

	//options[name] = required;
	desc.add_options(name.c_str(), po::value<T>(), description.c_str());

}

bool ProgramOptions::getOptions (int argc, char *argv[], map<string, string> &options, vector<string> &file_list) {
	po::store(po::parse_command_line(argc, argv, desc), vm);
	po::notify(vm);

	// get the options
	if (vm.count("help")) {
		cout << desc << endl;
		return false;
	}

	if (vm.count("file")) {
		options["file"] = vm["file"].as<string>();
		struct stat buffer;

		if (stat (vm["file"].as<string>().c_str(), &buffer) != 0) {
			cerr << "Input root file does not exist" << endl;
			return false;
		}

		file_list.push_back(vm["file"].as<string>());
	} else {
		if (vm.count("directory")) {
			options["directory"] = vm["directory"].as<string>();
			struct stat buffer;

			stat (vm["directory"].as<string>().c_str(), &buffer);

			if (!S_ISDIR(buffer.st_mode)) {
				cerr << "Input directory does not exist" << endl;
				return false;
			}

			DIR *dir;
			struct dirent *ent;
			if ((dir = opendir (vm["directory"].as<string>().c_str())) != NULL) {
				/* print all the files and directories within directory */
				while ((ent = readdir (dir)) != NULL) {
					string file = ent->d_name;
					if (file[0] != '.') {
						stringstream sfile;
						sfile << vm["directory"].as<string>() << "/" << file;
						file_list.push_back(sfile.str());
					}
				}

				closedir (dir);
			} else {
				/* could not open directory */
				cerr << "Could not open directory" << endl;
				return false;
			}
		} else {
			cerr << "No input directory or file specified" << endl;
			return false;
		}
	}

	if (vm.count("background")) {
		options["background"] = vm["background"].as<string>();
		struct stat buffer;

		if (stat (vm["background"].as<string>().c_str(), &buffer) != 0) {
			cerr << "Background PDF file does not exist" << endl;
			return false;
		}
	} else {
		options["background"] = "unset";
	}

	if (vm.count("signal")) {
		options["signal"] =  vm["signal"].as<string>();
		struct stat buffer;

		if (stat (vm["signal"].as<string>().c_str(), &buffer) != 0) {
			cerr << "Signal PDF file does not exist" << endl;
			return false;
		}
	} else {
		options["signal"] = "unset";
	}

	if (vm.count("record")) {
		options["record"] = vm["record"].as<string>();
	} else {
		cerr << "Output variables record file not set, will not record variables" << endl;
		options["record"] = "unset";
	}

	if (vm.count("output")) {
		options["output"] = vm["output"].as<string>();
	} else {
		cerr << "Output event file name not set, won't save events" << endl;
	}

	if (vm.count("pdf")) {
		stringstream ofile;
		ofile << vm["pdf"].as<string>() << ".root" << endl;
		options["pdf"] = ofile.str();
	} else {
		//cerr << "PDF histograms output file name not set" << endl;
		//return false;
	}

	if (vm.count("likelihood")) {
		stringstream ofile;
		ofile << vm["likelihood"].as<string>() << ".root" << endl;
		options["likelihood"] = ofile.str();
	} else {
		//cerr << "Likelihood histograms output file name not set" << endl;
		//return false;
	}

	return true;
}