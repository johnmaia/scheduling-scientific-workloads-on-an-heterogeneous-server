#include "CutOrganiser.h"

using namespace std;

CutOrganiser::CutOrganiser (void) {

}

CutOrganiser::CutOrganiser (unsigned _number_of_cuts) {
	initialize(_number_of_cuts);
}

CutOrganiser::~CutOrganiser (void) {
	delete[] cut_weight;
	delete[] cut_order;
	delete[] cut_execution_time;
	delete[] events_passed_per_cut;
	delete[] events_failed_per_cut;

	for (unsigned i = 0; i <= number_of_cuts; i++)
		delete[] cut_dependencies[i];
}

void CutOrganiser::initialize (unsigned _number_of_cuts) {
	number_of_cuts = _number_of_cuts;

	events_passed_per_cut = new long unsigned [number_of_cuts];
	events_failed_per_cut = new long unsigned [number_of_cuts];
	cut_execution_time    = new long unsigned [number_of_cuts];

	cut_order     = new unsigned [number_of_cuts + 1];
	cut_weight    = new double [number_of_cuts + 1];

	cut_dependencies = new unsigned*[number_of_cuts + 1];

	cut_weight[0] = 0;
	cut_order[0]  = 0;
	longest_cut   = 0;


	for (unsigned i = 0; i < number_of_cuts; i++) {

		cut_dependencies[i] = new unsigned[number_of_cuts + 1];

		events_passed_per_cut[i] = 0;
		events_failed_per_cut[i] = 0;
		cut_execution_time[i]    = 0;
		cut_order[i + 1]		 = i + 1;
		cut_weight[i + 1]		 = DEFAULT_CUT_WEIGHT;
	}

	// add the last line of the matrix
	cut_dependencies[number_of_cuts] = new unsigned[number_of_cuts + 1];
}

// Apply the formula to calculate the cut weight here
void CutOrganiser::updateCutWeight (unsigned cut_number) {
	// lets test at 30% cut exec time + 70% filter score
	double filter_ratio, filter_score, time_ratio, time_score;

	if (events_failed_per_cut[cut_number] != 0) {
		filter_ratio = (double) events_passed_per_cut[cut_number] /
					   (double) (events_passed_per_cut[cut_number] + events_failed_per_cut[cut_number]);

		filter_score = filter_ratio * filter_ratio * MAX_CUT_WEIGHT;
	} else {
		filter_ratio = 1;
		filter_score = MAX_CUT_WEIGHT;
	}

	if (cut_execution_time[cut_number] != 0)
		time_ratio = (double) cut_execution_time[cut_number] / (double) longest_cut;
	else
		time_ratio = 0;

	time_score = time_ratio * MAX_CUT_TIME;

	cut_weight[cut_number] = 0.3 * time_score + 0.7 * filter_score + 1;

	//cout << "filter ratio for cut " << cut_number << ": " << time_score << " - " << filter_score << " - " << filter_ratio << " - " << cut_execution_time[cut_number] << " - " << longest_cut << " - " << cut_weight[cut_number] << endl;

	//cut_dependencies
	for (unsigned i = 1; i <= number_of_cuts; i++)
		if (cut_dependencies[cut_number + 1][i] != 0 && cut_dependencies[cut_number + 1][i] != INF_CUT_WEIGHT)
			cut_dependencies[cut_number + 1][i] = cut_weight[cut_number];

	// reset execution time for the cuts?
	//events_failed_per_cut[cut_number] = 0;
	//events_passed_per_cut[cut_number] = 0;
}


void CutOrganiser::cutInitialTime (void) {
	initial_time = chrono::high_resolution_clock::now();
}

void CutOrganiser::cutEndTime (unsigned cut_number) {
	auto end_time = chrono::high_resolution_clock::now();

	chrono::duration<double> elapsed = end_time - initial_time;

	long unsigned elps = elapsed.count() * 1000000;	// to nanosseconds and microsseconds

	if(cut_execution_time[cut_number - 1] != 0)
		cut_execution_time[cut_number - 1] = (cut_execution_time[cut_number - 1] + elps)/* / 2*/;
	else
		cut_execution_time[cut_number - 1] = elps;

	if (cut_execution_time[cut_number - 1] > longest_cut)
		longest_cut = cut_execution_time[cut_number - 1];
}

void CutOrganiser::reorderCuts (void) {
	vector<unsigned> path;

	path = route (0, path, 0);

	//cout << "Cost: " << pathCost(path) << endl;
	//for (unsigned i = 0; i < path.size(); i++)
	//	cout << "Node: " << path[i] << endl;

	for (unsigned i = 0; i < number_of_cuts; i++) {
		cout << path[i + 1] << "(" << cut_weight[i] << ") -> ";
	//	cut_order[i + 1] = path[i + 1];
	}
}

void CutOrganiser::computeCutOrder (void) {
	// cuts start at id 1!!!
	for (unsigned cut_number = 0; cut_number < number_of_cuts; cut_number++) {
		updateCutWeight(cut_number);
	}

	//reorderCuts();
}

bool CutOrganiser::exists (unsigned vertex, vector<unsigned> current_path) {
	for (unsigned i = 0; i < current_path.size(); i++)
		if (vertex == current_path[i])
			return true;

	return false;
}

bool CutOrganiser::existsDep (unsigned dep_vertex, unsigned source_vertex, vector<unsigned> current_path) {
	for (unsigned i = 0; (i < current_path.size()) && (source_vertex != current_path[i]); i++)
		if (dep_vertex == current_path[i])
			return true;

	return false;
}

bool CutOrganiser::checkDependencies (unsigned vertex, vector<unsigned> current_path) {
	for (unsigned i = 0; i <= number_of_cuts; i++) {
		if ((cut_dependencies[vertex][i] == INF_CUT_WEIGHT) && (!existsDep(i, vertex, current_path))) {
			return false;
		}
	}

	return true;
}

unsigned CutOrganiser::pathCost (vector<unsigned> current_path) {
	unsigned cost = 0;

	if (current_path.size() == 0)
		return 0;

	if (current_path.size() == 1)
		return current_path[0];

	for (unsigned i = 0; i < current_path.size() - 1; i++) {
		if (!checkDependencies(current_path[i], current_path))
			return INF_CUT_WEIGHT;
		else
			cost += cut_dependencies[current_path[i]][current_path[i + 1]];
	}

	return cost;
}

vector<unsigned> CutOrganiser::route (unsigned start, vector<unsigned> current_path, unsigned level) {
	unsigned cost = INF_CUT_WEIGHT,i;
	bool new_path = false;
	vector<unsigned> partial_path;

	current_path.push_back(start);

	// iterates through all possible paths to take next
	if (!(current_path.size() == number_of_cuts + 1)) {
		for (/*unsigned*/ i = 0; i <= number_of_cuts; i++) {
			// check if the vertex is already in the
			if (!(exists(i, current_path))) {
				vector<unsigned> aux_path;

				// cost of visiting next node
				aux_path = route(i, current_path, level+1);
				unsigned current_cost = pathCost(aux_path);

				if ((cost > current_cost) && (aux_path.size() == number_of_cuts + 1)) {
					cost = current_cost;
					partial_path = aux_path;
					new_path = true;
				}
			}
		}
	}
	//cout << current_path.size() << endl;
	//if (level == number_of_cuts)
	//cout << cost << " - " << i << " - " << level << endl;

	if (new_path)
		return partial_path;
	else
		return current_path;
}

void CutOrganiser::readOrder (string filename) {
	ifstream file (filename);
	string line;
	unsigned i = 0;

	while (getline(file, line) && i <= number_of_cuts) {
		cut_order[i] = stoul(line, NULL, 0);

		i++;
	}

	file.close();
}

void CutOrganiser::writeOrder (string filename) {
	ofstream file (filename);
		stringstream ss;

	for (unsigned i = 0; i <= number_of_cuts; i++)
		ss << cut_order[i] << "\n";

	file << ss.str();

	file.close();
}

void CutOrganiser::addCutDependency (unsigned cut1, unsigned cut2) {
	cut_dependencies[cut2][cut1] = DEFAULT_CUT_WEIGHT;
	cut_dependencies[cut1][cut2] = INF_CUT_WEIGHT;
}

// to call preferably after addCutDep
void CutOrganiser::initCutDependencies (void) {

	for (unsigned i = 0; i < number_of_cuts + 1; i++) {
		for (unsigned j = 0; j < number_of_cuts + 1; j++) {
			if (i == j)
				cut_dependencies[i][j] = 0;
			else
				if (j == 0) {
					cut_dependencies[i][j] = INF_CUT_WEIGHT;
				} else {
					if (cut_dependencies[i][j] != INF_CUT_WEIGHT)
						cut_dependencies[i][j] = cut_weight[i];
				}
		}
	}
}
