#include "DataAnalysis.h"

extern vector<HEPEvent> events;

#include <TTH_STLorentzVector.h>
#include <TLorentzVectorWFlags.h>
#include <TTH_Event_offload.h>
#include <TTH_Event_pack.h>
#include <TTH_offload.h>
#include <TTH_offload_aux.h>

// #include <TTH_Event_offload_gpu.h>
// #include <TTH_Event_pack_gpu.h>
// #include <TTH_offload_aux_gpu.h>
// #include <TTH_offload_gpu.h>

// #include <cuda.h>

#include <iostream>
#include <fstream>

#include <boost/lockfree/queue.hpp>

long unsigned event_counter;
#pragma omp threadprivate(event_counter)
long unsigned event_counter_process;
unsigned thread_id;
#pragma omp threadprivate(thread_id)
unsigned num_threads;
ofstream outputFile;

boost::lockfree::queue<int> qEventSteps(0);

//boost::thread *filereader_thread;

/*
 * DISCLAIMER => cuts begin with id 1 to number_of_cuts (including this value)
 */

DataAnalysis::DataAnalysis (vector<string> _rootfile_list, string _tree_filename, string _output_root_file, string _signal_file, string _background_file, string _tree_name, unsigned ncuts) {
	#ifdef D_MPI
		MPI_Comm_rank(MPI_COMM_WORLD, &process_id);
		MPI_Comm_size(MPI_COMM_WORLD, &number_of_processes);
	#else
		process_id = 0;
		number_of_processes = 1;
	#endif

	event_counter_process = 0;
	event_counter = 0;
	tree_name = _tree_name;
	root_file_list = _rootfile_list;
	stringstream ts1;
	ts1 << _output_root_file;
	ts1 << "_" << process_id << ".root";
	output_root_file = ts1.str();
	stringstream ts2;
	ts2 << _tree_filename;
	ts2 << "_" << process_id << ".root";
	tree_filename = ts2.str();
	signal_file = _signal_file;
	background_file = _background_file;
	pdfs = false;
	number_of_cuts = ncuts;

	initCuts();
}

DataAnalysis::DataAnalysis (vector<string> _rootfile_list, string _tree_filename, string _output_root_file, string _signal_file, string _background_file, unsigned ncuts) {
	#ifdef D_MPI
		MPI_Comm_rank(MPI_COMM_WORLD, &process_id);
		MPI_Comm_size(MPI_COMM_WORLD, &number_of_processes);
	#else
		process_id = 0;
		number_of_processes = 1;
	#endif

	event_counter_process = 0;
	event_counter = 0;
	tree_name = "unset";
	root_file_list = _rootfile_list;
	stringstream ts1;
	ts1 << _output_root_file;
	ts1 << "_" << process_id << ".root";
	output_root_file = ts1.str();
	stringstream ts2;
	ts2 << _tree_filename;
	ts2 << "_" << process_id << ".root";
	tree_filename = ts2.str();
	signal_file = _signal_file;
	background_file = _background_file;
	pdfs = false;
	number_of_cuts = ncuts;

	initCuts();
}

DataAnalysis::~DataAnalysis (void) {
	delete[] tree_rec_vars;
}

// Init output file
void initOutputFile(void)
{
  outputFile.open("misc/output.dat");
	outputFile << "# \t WLP1 \t WET1 \t WLP2 \t WET2 \t SCHE \t SPEED \t tET \t tWSL" << endl;
}

// Close output file
void closeOutputFile(void)
{
  outputFile.close();
}

void DataAnalysis::initCuts (void) {
	cut_counter   = 1;

	cut_organiser.initialize(number_of_cuts);

	tree_rec_vars = new TTree*[number_of_cuts];

	for (unsigned i = 0; i < number_of_cuts; i++){
		stringstream s1, s2;
		s1 << "Recorded_Vars_";
		s1 << i;
		s2 << "Variables recorded for cut ";
		s2 << i;

		tree_rec_vars[i] = new TTree (s1.str().c_str(), s2.str().c_str());
	}
}

void processCutsMIC0thread (int setSize, int& evComputed, double& timeMIC) {
  double t1, t2;
  GET_TIME(t1);

	int begin = 0;
	while(qEventSteps.pop(begin)){
		int end = begin + setSize;

		evComputed+= setSize;

	  int nevents = end - begin;

	  // Copy info from event to new offload struct
	  EventPack * packArray;
		int * cutReport;

	  packArray = (EventPack *) malloc(sizeof(EventPack) * nevents);
		cutReport = (int *) malloc(sizeof(int) * nevents);

		int eventCounter = 0;
	  for(unsigned i = begin; i < end; ++i) {
			packArray[eventCounter] = PackEvent(i);
			eventCounter++;
		}

	  processCutsMIC0(packArray, cutReport, nevents);

	  for(unsigned i = 0; i < nevents; ++i)	UnpackEvent(packArray[i]);

		// cout << "[MIC] Number of events that passed each cut:" << endl;
		// for(unsigned i = 0; i <= 17; ++i){
		// 	int passed = 0;
		// 	for(unsigned j = 0; j < nevents; ++j){
		// 		if(cutReport[j] >= i) ++passed;
		// 	}
		// 	cout << "\t=> " << i+1 << ": " << passed << endl;
		// }

		free(cutReport);
	  free(packArray);
	}
	GET_TIME(t2);
	timeMIC = t2 - t1;
}

void processCutsMIC1thread (int setSize, int& evComputed, double& timeMIC) {
  double t1, t2;
  GET_TIME(t1);

	int begin = 0;
	while(qEventSteps.pop(begin)){
		int end = begin + setSize;

		evComputed+= setSize;

	  int nevents = end - begin;

	  // Copy info from event to new offload struct
	  EventPack * packArray;
		int * cutReport;

	  packArray = (EventPack *) malloc(sizeof(EventPack) * nevents);
		cutReport = (int *) malloc(sizeof(int) * nevents);

		int eventCounter = 0;
	  for(unsigned i = begin; i < end; ++i) {
			packArray[eventCounter] = PackEvent(i);
			eventCounter++;
		}

	  processCutsMIC1(packArray, cutReport, nevents);

	  for(unsigned i = 0; i < nevents; ++i)	UnpackEvent(packArray[i]);

		// cout << "[MIC] Number of events that passed each cut:" << endl;
		// for(unsigned i = 0; i <= 17; ++i){
		// 	int passed = 0;
		// 	for(unsigned j = 0; j < nevents; ++j){
		// 		if(cutReport[j] >= i) ++passed;
		// 	}
		// 	cout << "\t=> " << i+1 << ": " << passed << endl;
		// }

		free(cutReport);
	  free(packArray);
	}
	GET_TIME(t2);
	timeMIC = t2 - t1;
}

// void processCutsGPUthread (int begin, int end, double& timeGPU) {
//   double t1, t2;
//   GET_TIME(t1);
//
//   int nevents = end - begin;
//
//   // Copy info from event to new offload struct
//   EventPackGPU * packArray;
// 	int * cutReport;
//
//   packArray = (EventPackGPU *) malloc(sizeof(EventPackGPU) * nevents);
// 	cutReport = (int *) malloc(sizeof(int) * nevents);
//
// 	for(unsigned i = 0; i < nevents; ++i){
// 		cutReport[i] = -1;
// 	}
//
// 	int eventCounter = 0;
//   for(unsigned i = begin; i < end; ++i) {
// 		packArray[eventCounter] = PackEventGPU(i);
// 		eventCounter++;
// 	}
//
//   processGPU(packArray, cutReport, nevents);
//
//   for(unsigned i = 0; i < nevents; ++i)	UnpackEventGPU(packArray[i]);
//
//   GET_TIME(t2);
//
// 	cout << "[GPU] Number of events that passed each cut:" << endl;
// 	for(unsigned i = 0; i <= 17; ++i){
// 		int passed = 0;
// 		int error = 0;
// 		for(unsigned j = 0; j < nevents; ++j){
// 			if(cutReport[j] >= i) ++passed;
// 			if(cutReport[j] < 0) ++error;
// 		}
// 		cout << "\t=> " << i+1 << ": " << passed << "\t Error: " << error << endl;
// 	}
//
// 	free(cutReport);
//   free(packArray);
//
// 	timeGPU = t2 - t1;
// }

void DataAnalysis::processCutsCPU (int setSize, int& evComputed, double &timeCPU){
  double t1, t2;
  GET_TIME(t1);

	int begin = 0;
	while(qEventSteps.pop(begin)){
		int end = begin + setSize;
		evComputed+= setSize;

	  #pragma omp parallel
	  {
	    #ifdef D_MULTICORE
	    thread_id = omp_get_thread_num();
	    num_threads = omp_get_num_threads();
	    #else
	    thread_id = 0;
	    num_threads = 1;
	    #endif

	    #pragma omp single
	    initRecord();

	    #pragma omp for schedule(dynamic)
	    for (unsigned ev = begin; ev < end; ev++) {
	      event_counter = ev;
	      processCuts();
	    }
	  }
	}

  GET_TIME(t2);
  timeCPU = t2 - t1;
}

// void observerMIC () {
// 	usleep(1000000);
//   for (unsigned j = 0; j < 2; ++j){
//     cout << "[OBS][MIC][" << j <<"] workState: " << getWorkStatus() << endl;
//     usleep(2000000);
//   }
//   return;
// }

void DataAnalysis::loop (void) {
	initOutputFile();

	//scheduler environment variables
	int physicalDev		= atoi(getenv("SCHE_PD"));
	int computingRes	= atoi(getenv("SCHE_CR"));
	int initSetSize		= atoi(getenv("SCHE_SS"));

  bool firstFile = true;

	int CPU = 0;
	int MIC0 = 1;
	int MIC1 = 2;

	string workerName[] = {"CPU", "MIC0", "MIC1"};

	int iter = 0;

	int nWorkers = computingRes;
	int currentEvent = 0;
	int stablePercentage = 10;
	int stableStep = 0;

	int	* wrkTaskSize;
	double * wrkWorkloadPercentage;
	double * wrkPreviousWorkloadPercentage;
	double * wrkElapsedTime;
	double * wrkWaitTimePercentage;

	long unsigned number_events = events.size();
	event_counter_process += events.size();

  // int totalWorkSizeLimit = event_counter_process*0.1;
  // if(totalWorkSizeLimit>2000) totalWorkSizeLimit = 3000;
	int totalWorkSizeLimit = initSetSize;

	if(firstFile){
    wrkTaskSize = (int *) malloc(sizeof(int) * nWorkers);
    wrkWorkloadPercentage = (double *) malloc(sizeof(double) * nWorkers);
		wrkPreviousWorkloadPercentage = (double *) malloc(sizeof(double) * nWorkers);
    wrkElapsedTime = (double *) malloc(sizeof(double) * nWorkers);
    wrkWaitTimePercentage = (double *) malloc(sizeof(double) * nWorkers);

		// wrkWorkloadPercentage[MIC] = atof(getenv("MIC_LOAD"));
		// wrkWorkloadPercentage[CPU] = atof(getenv("CPU_LOAD"));
		int j = 0;
		for(unsigned int i = 0; i < physicalDev; ++i){
			if(j >= computingRes) j = 0;
			wrkWorkloadPercentage[j] += 100/physicalDev;
			j++;
	  }

		// wrkWorkloadPercentage[CPU] = 100/nWorkers;
    // wrkWorkloadPercentage[MIC0] = 100/nWorkers;
		// wrkWorkloadPercentage[MIC1] = 100/nWorkers;

		firstFile = false;
	} else currentEvent = 0;
  int workAssigned = 0;
  for(unsigned i = 0; i < nWorkers; ++i){
    wrkTaskSize[i] = totalWorkSizeLimit * (wrkWorkloadPercentage[i]/100.f);
    workAssigned += wrkTaskSize[i];
  }

  if(workAssigned < totalWorkSizeLimit) wrkTaskSize[nWorkers-1] += totalWorkSizeLimit - workAssigned;


	int nSteps = events.size()/totalWorkSizeLimit;
	qEventSteps.reserve(nSteps);

	for(unsigned i = 0; i < nSteps; ++i){
		qEventSteps.push(i * totalWorkSizeLimit);
	}

	cout << "[#][#] Scheduler report:" << endl;
	cout << "[#][#] nWorkers: " << nWorkers << endl;
	cout << "[#][#] physicalDev: " << physicalDev << endl;
	cout << "[#][#] Set size: " << totalWorkSizeLimit << endl;
	cout << "[#][#] Number of Steps: " << nSteps << endl;
	// for(unsigned i = 0; i < nWorkers; ++i){
	// 	cout << "[#][#] Worker: " << i << " | Load Percentage: " << wrkWorkloadPercentage[i] << " | Number of events: " << wrkTaskSize[i] << endl;
	// }

	// for(unsigned int i = 0; i < nWorkers; ++i){
  //   cout << "[i] Iter " << iter << " | Thread " << i << " task size: " << wrkTaskSize[i] << " | wrkWorkloadPercentage: " << wrkWorkloadPercentage[i] << endl;
  // }

  double start, finish, elapsed, schedulerElapsed;

  double timeMIC;
  double timeCPU;

  GET_TIME(start);

  boost::thread threadMIC0(&processCutsMIC0thread, totalWorkSizeLimit, boost::ref(wrkTaskSize[MIC0]), boost::ref(wrkElapsedTime[MIC0]));
	boost::thread threadMIC1(&processCutsMIC1thread, totalWorkSizeLimit, boost::ref(wrkTaskSize[MIC1]), boost::ref(wrkElapsedTime[MIC1]));
	processCutsCPU(totalWorkSizeLimit, wrkTaskSize[CPU], wrkElapsedTime[CPU]);

  threadMIC0.join();
	threadMIC1.join();

	// threadGPU.join();
	// threadObserverMIC.join();

  GET_TIME(finish);

  // Calculate elapsed time
  elapsed = finish - start;

	cout << "[#][#] Scheduler ended!" << endl;
	cout << "[#][#] Total Elapsed: " << elapsed << " seconds" << endl;
	cout << "[#][#] Speed: " <<  events.size()/elapsed << " ev/second " << endl;
	for(unsigned int i = 0; i < nWorkers; ++i){
	  cout << "[#][#] Worker: " << i << " | processed: " << wrkTaskSize[i] << " | took: " << wrkElapsedTime[i] << " seconds | Speed: " << wrkTaskSize[i]/wrkElapsedTime[i] << " ev/second | Waited: " << ((elapsed - wrkElapsedTime[i])/elapsed)*100 << " seconds " << endl;
	}

		// GET_TIME(start);
    // for(unsigned int i = 0; i < nWorkers; ++i){
    //   cout << "[+][Thread: " << i << "] processed " << wrkWorkloadPercentage[i] << " of the events in: " << wrkElapsedTime[i] << " seconds" << endl;
		// 	wrkWaitTimePercentage[i] = ((elapsed - wrkElapsedTime[i])/elapsed)*100;
		// 	wrkPreviousWorkloadPercentage[i] = wrkWorkloadPercentage[i];
    //   totalWorkPrevious += wrkTaskSize[i];
    // }
		//
    // cout << "[+] Elapsed time: " << elapsed << " s " << endl;
    // cout << "[+] Speed: " << (float) totalWorkPrevious/elapsed << " E/s" << endl;
    // cout << "[+] Remaining events: " << number_events - currentEvent << " | Total: " << number_events << endl;
		//
    // if(currentEvent < number_events){
    //   if ((currentEvent + totalWorkSizeLimit) <= number_events) totalWorkNextIter = totalWorkSizeLimit;
    //   else totalWorkNextIter = number_events - currentEvent;
		//
    //   // Calculate new workload
    //   for(unsigned int i = 0; i < nWorkers; ++i){
    //     wrkWorkloadPercentage[i] = wrkWorkloadPercentage[i] + (wrkWaitTimePercentage[i] * (wrkWorkloadPercentage[i]/100.f));
    //     sumNWL += wrkWorkloadPercentage[i];
    //   }
		//
    //   // Normalize
    //   for(unsigned int i = 0; i < nWorkers; ++i){
    //     wrkWorkloadPercentage[i] /= sumNWL;
    //     wrkWorkloadPercentage[i] *= 100.f;
    //   }
		//
    //   // Atribute tasks to each worker based on their workload
    //   for(unsigned int i = 0; i < nWorkers; ++i){
    //     wrkTaskSize[i] = totalWorkNextIter * ((float) wrkWorkloadPercentage[i]/100.f);
    //     totalWorkNextIterAux += wrkTaskSize[i];
    //   }
		//
    //   // Atribute the extra tasks
    //   while(totalWorkNextIterAux < totalWorkNextIter){
    //     for(unsigned int i = 0; i < nWorkers && (totalWorkNextIterAux < totalWorkNextIter); ++i){
    //       wrkTaskSize[i] += 1;
    //       ++totalWorkNextIterAux;
    //     }
    //   }
		//
    //   // Calculate workload after extras
    //   for(unsigned int i = 0; i < nWorkers; ++i){
    //     wrkWorkloadPercentage[i] = ((float) wrkTaskSize[i]/ (float) totalWorkNextIter)*100.f;
    //   }
		//
		// 	if((iter > 0) && (stableStep < 4)){
		// 		float percentageDiferential = 0;
		// 		for(unsigned int i = 0; i < nWorkers; ++i){
		// 			float percentageDiferentialTemp =  wrkPreviousWorkloadPercentage[i] - wrkWorkloadPercentage[i];
		// 			if(percentageDiferentialTemp < 0) percentageDiferentialTemp *=-1;
		// 			percentageDiferential += percentageDiferentialTemp;
		// 		}
		// 		if(percentageDiferential <= stablePercentage) {
		// 			totalWorkSizeLimit *= 2;
		// 			cout << "[!] Doubling totalWorkSizeLimit: " << totalWorkSizeLimit << " | percentageDiferential: " << percentageDiferential << endl;
		// 			stableStep++;
		// 		}
		// 	}
		// 	GET_TIME(finish);
		//
		// 	schedulerElapsed = finish - start;
		//
		// 	outputFile <<  iter << " \t ";
		// 	for(unsigned int i = 0; i < nWorkers; ++i)  outputFile << (float) wrkPreviousWorkloadPercentage[i] << " \t " << wrkElapsedTime[i] << "\t";
		// 	outputFile << schedulerElapsed << "\t" << (double) totalWorkPrevious/elapsed << " \t " << elapsed << "\t" << totalWorkSizeLimit << endl;
		//
    //   iter++;
    //   // Print info related to the next Iteration
    //   for(unsigned int i = 0; i < nWorkers; ++i){
    //     cout << "[i] Iter " << iter << " | Thread " << i << " | Number of events: " << wrkTaskSize[i] << endl;
    //   }
    // } else {
		// 	outputFile <<  iter << " \t ";
    //   for(unsigned int i = 0; i < nWorkers; ++i)  outputFile << (double) wrkWorkloadPercentage[i] << " \t " << wrkElapsedTime[i] << "\t";
		// 	outputFile << schedulerElapsed << "\t" <<  (double) totalWorkPrevious/elapsed << " \t " << elapsed << "\t" << totalWorkSizeLimit << endl;
		// }

	event_counter++;

	writeVariables();
	writePdfs();

	free(wrkTaskSize);
	free(wrkWorkloadPercentage);
	free(wrkPreviousWorkloadPercentage);
	free(wrkElapsedTime);
	free(wrkWaitTimePercentage);
	closeOutputFile();
}

void DataAnalysis::loopNoRecord (void) {
	// initOutputFile();
	//
  // bool firstFile = true;
	//
	// int CPU = 0;
	// int MIC0 = 1;
	// int MIC1 = 2;
	//
	// int iter = 0;
	//
	// int nWorkers = 3;
	// int currentEvent = 0;
	// int stablePercentage = 10;
	// int stableStep = 0;
	//
	// int * wrkTaskSize;
	// double * wrkWorkloadPercentage;
	// double * wrkPreviousWorkloadPercentage;
	// double * wrkElapsedTime;
	// double * wrkWaitTimePercentage;
	//
	// long unsigned number_events = events.size();
	// event_counter_process += events.size();
	//
  // int totalWorkSizeLimit = event_counter_process*0.1;
  // if(totalWorkSizeLimit>2000) totalWorkSizeLimit = 3000;
	//
	// if(firstFile){
  //   wrkTaskSize = (int *) malloc(sizeof(int) * nWorkers);
  //   wrkWorkloadPercentage = (double *) malloc(sizeof(double) * nWorkers);
	// 	wrkPreviousWorkloadPercentage = (double *) malloc(sizeof(double) * nWorkers);
  //   wrkElapsedTime = (double *) malloc(sizeof(double) * nWorkers);
  //   wrkWaitTimePercentage = (double *) malloc(sizeof(double) * nWorkers);
	//
	// 	// wrkWorkloadPercentage[MIC] = atof(getenv("MIC_LOAD"));
	// 	// wrkWorkloadPercentage[CPU] = atof(getenv("CPU_LOAD"));
	//
	// 	wrkWorkloadPercentage[CPU] = 100/nWorkers;
  //   wrkWorkloadPercentage[MIC0] = 100/nWorkers;
	// 	wrkWorkloadPercentage[MIC1] = 100/nWorkers;
	//
	// 	firstFile = false;
	// } else currentEvent = 0;
	//
  // int workAssigned = 0;
  // for(unsigned i = 0; i < nWorkers; ++i){
  //   wrkTaskSize[i] = totalWorkSizeLimit * (wrkWorkloadPercentage[i]/100.f);
  //   workAssigned += wrkTaskSize[i];
  // }
	//
  // if(workAssigned < totalWorkSizeLimit) wrkTaskSize[nWorkers-1] = totalWorkSizeLimit - workAssigned;
	//
  // for(unsigned int i = 0; i < nWorkers; ++i){
  //   cout << "[i] Iter " << iter << " | Thread " << i << " task size: " << wrkTaskSize[i] << endl;
  // }
	//
	//
  // while(currentEvent < number_events){
	//
  //   double start, finish, elapsed, schedulerElapsed;
	//
  //   double timeMIC;
  //   double timeCPU;
  //   double sumNWL = 0.f;
	//
  //   int totalWorkNextIter = 0;
  //   int totalWorkNextIterAux = 0;
  //   int totalWorkPrevious = 0;
	//
  //   GET_TIME(start);
  //   boost::thread threadMIC0(&processCutsMIC0thread, currentEvent, currentEvent+wrkTaskSize[MIC0], boost::ref(wrkElapsedTime[MIC0]));
	// 	// boost::thread threadObserverMIC(&observerMIC);
  //   currentEvent += wrkTaskSize[MIC0];
	//
	// 	boost::thread threadMIC1(&processCutsMIC1thread, currentEvent, currentEvent+wrkTaskSize[MIC1], boost::ref(wrkElapsedTime[MIC1]));
	// 	// boost::thread threadObserverMIC(&observerMIC);
	// 	currentEvent += wrkTaskSize[MIC1];
	//
	// 	// boost::thread threadGPU(&processCutsGPUthread, currentEvent, currentEvent+wrkTaskSize[GPU], boost::ref(wrkElapsedTime[GPU]));
	// 	// currentEvent += wrkTaskSize[GPU];
	//
  //   processCutsCPU(currentEvent, currentEvent+wrkTaskSize[CPU], wrkElapsedTime[CPU]);
  //   currentEvent += wrkTaskSize[CPU];
	//
  //   threadMIC0.join();
	// 	threadMIC1.join();
	//
	// 	// threadGPU.join();
	// 	// threadObserverMIC.join();
	//
  //   GET_TIME(finish);
	//
  //   // Calculate elapsed time
  //   elapsed = finish - start;
	//
  //   for(unsigned int i = 0; i < nWorkers; ++i){
  //     cout << "[+][Thread: " << i << "] processed " << wrkWorkloadPercentage[i] << " of the events in: " << wrkElapsedTime[i] << " seconds" << endl;
	// 		wrkWaitTimePercentage[i] = ((elapsed - wrkElapsedTime[i])/elapsed)*100;
	// 		wrkPreviousWorkloadPercentage[i] = wrkWorkloadPercentage[i];
  //     totalWorkPrevious += wrkTaskSize[i];
  //   }
	//
  //   cout << "[+] Elapsed time: " << elapsed << " s " << endl;
  //   cout << "[+] Speed: " << (float) totalWorkPrevious/elapsed << " E/s" << endl;
  //   cout << "[+] Remaining events: " << number_events - currentEvent << " | Total: " << number_events << endl;
	//
  //   if(currentEvent < number_events){
  //     if ((currentEvent + totalWorkSizeLimit) <= number_events) totalWorkNextIter = totalWorkSizeLimit;
  //     else totalWorkNextIter = number_events - currentEvent;
	//
  //     // Calculate new workload
  //     for(unsigned int i = 0; i < nWorkers; ++i){
  //       wrkWorkloadPercentage[i] = wrkWorkloadPercentage[i] + (wrkWaitTimePercentage[i] * (wrkWorkloadPercentage[i]/100.f));
  //       sumNWL += wrkWorkloadPercentage[i];
  //     }
	//
  //     // Normalize
  //     for(unsigned int i = 0; i < nWorkers; ++i){
  //       wrkWorkloadPercentage[i] /= sumNWL;
  //       wrkWorkloadPercentage[i] *= 100.f;
  //     }
	//
  //     // Atribute tasks to each worker based on their workload
  //     for(unsigned int i = 0; i < nWorkers; ++i){
  //       wrkTaskSize[i] = totalWorkNextIter * ((float) wrkWorkloadPercentage[i]/100.f);
  //       totalWorkNextIterAux += wrkTaskSize[i];
  //     }
	//
  //     // Atribute the extra tasks
  //     while(totalWorkNextIterAux < totalWorkNextIter){
  //       for(unsigned int i = 0; i < nWorkers && (totalWorkNextIterAux < totalWorkNextIter); ++i){
  //         wrkTaskSize[i] += 1;
  //         ++totalWorkNextIterAux;
  //       }
  //     }
	//
  //     // Calculate workload after extras
  //     for(unsigned int i = 0; i < nWorkers; ++i){
  //       wrkWorkloadPercentage[i] = ((float) wrkTaskSize[i]/ (float) totalWorkNextIter)*100.f;
  //     }
	//
	// 		if((iter > 0) && (stableStep < 4)){
	// 			float percentageDiferential = 0;
	// 			for(unsigned int i = 0; i < nWorkers; ++i){
	// 				float percentageDiferentialTemp =  wrkPreviousWorkloadPercentage[i] - wrkWorkloadPercentage[i];
	// 				if(percentageDiferentialTemp < 0) percentageDiferentialTemp *=-1;
	// 				percentageDiferential += percentageDiferentialTemp;
	// 			}
	// 			if(percentageDiferential <= stablePercentage) {
	// 				totalWorkSizeLimit *= 2;
	// 				cout << "[!] Doubling totalWorkSizeLimit: " << totalWorkSizeLimit << " | percentageDiferential: " << percentageDiferential << endl;
	// 				stableStep++;
	// 			}
	// 		}
	//
	// 		outputFile <<  iter << " \t ";
	// 		for(unsigned int i = 0; i < nWorkers; ++i)  outputFile << (float) wrkPreviousWorkloadPercentage[i] << " \t " << wrkElapsedTime[i] << "\t";
	// 		outputFile <<  (double) totalWorkPrevious/elapsed << " \t " << elapsed << "\t" << totalWorkSizeLimit << endl;
	//
  //     iter++;
  //     // Print info related to the next Iteration
  //     for(unsigned int i = 0; i < nWorkers; ++i){
  //       cout << "[i] Iter " << iter << " | Thread " << i << " | Number of events: " << wrkTaskSize[i] << endl;
  //     }
  //   } else {
	// 		outputFile <<  iter << " \t ";
  //     for(unsigned int i = 0; i < nWorkers; ++i)  outputFile << (double) wrkWorkloadPercentage[i] << " \t " << wrkElapsedTime[i] << "\t";
	// 		outputFile <<  (double) totalWorkPrevious/elapsed << " \t " << elapsed << "\t" << totalWorkSizeLimit << endl;
	// 	}
  // }
	//
	// event_counter++;
	//
	// free(wrkTaskSize);
	// free(wrkWorkloadPercentage);
	// free(wrkPreviousWorkloadPercentage);
	// free(wrkElapsedTime);
	// free(wrkWaitTimePercentage);
	// closeOutputFile();
}


void DataAnalysis::smFileListQ (void) {

	// size_t const max_msg_size = 0x100;
	//
	// //Erase previous message queue
	// message_queue::remove("filelist_queue");
	//
	// //Create a message_queue.
	// message_queue mq
	// 								 (create_only               //only create
	// 								 ,"filelist_queue"          //name
	// 								 ,root_file_list.size()     //max message number
	// 								 ,max_msg_size              //max message size
	// 								 );
	//
	//
	// // reads a specific amount of files allocated for each process
	// for (int i = 0; i < root_file_list.size(); i++) {
	// 	std::string s(root_file_list[i]);
	// 	mq.send(s.data(), s.size(), 0);
	// }
}

void DataAnalysis::run (void) {

  t.start();

  //cut_organiser.initCutDependencies();

  int excess_files 	= root_file_list.size() % number_of_processes;
  float excess_factor = (float) excess_files / (float) number_of_processes;
  int files		 	= (float) root_file_list.size() / (float) number_of_processes - excess_factor;

  if (process_id < excess_files)
    files++;

  // reads a specific amount of files allocated for each process
  for (int i = 0; i < files; i++) {
    if (tree_name == "unset") {
      DataReader root_reader (root_file_list[process_id * files + i]);
      root_reader.loadEvents();
    } else {
      DataReader root_reader (root_file_list[process_id * files + i], tree_name);
      root_reader.loadEvents();
    }
  }

  initialize();

  stringstream err;
  err << "unset_" << process_id << ".root";

  if (tree_filename != err.str())
    loop();
  else
    loopNoRecord();

  finalize();

  if (save_events)
    writeEvents();

  #ifdef D_MPI
    MPI_Barrier(MPI_COMM_WORLD);
  #endif

  t.stop();

  #ifdef D_REPORT
    report();
  #endif

  t.report(Report::Verbose);
}

//inline
void DataAnalysis::processCuts (void) {
	bool pass;
	unsigned cut_number;
	unsigned cut_id;
	Cut c;

	// unsigned firstEvent = areaPerThread[thread_id][0];
	// unsigned lastEvent = areaPerThread[thread_id][1];
	//
	// //cout << "[P] Thread: " << thread_id << " | firstEvent: " << firstEvent << " | lastEvent: " << lastEvent << endl;
	//
	// for (unsigned ev = firstEvent; ev <= lastEvent; ev++) {
	// 	event_counter = ev;
		pass = true;
		cut_number = 0;

		for (unsigned i = 1; i <= number_of_cuts && pass; i++) {
			cut_id = cut_organiser.getCutOrder(i);
			c = cuts[cut_id];
			//	cut_organiser.cutInitialTime();
			pass = c();
			//	cut_organiser.cutEndTime(cut_id);

			if (pass) {
				if (i == number_of_cuts)
					recordPdfs();

				// #pragma omp critical
				// recordVariables(cut_number);

				#ifdef D_REPORT
					cut_organiser.incPassedEvent(cut_id);
				#endif
			} else {
				#ifdef D_REPORT
					#pragma omp critical
					cut_organiser.incFailedEvent(cut_id);
				#endif
			}
			cut_number++;
		}
	// }
}

//inline
void DataAnalysis::processCutsNoRecord (void) {
	bool pass = true;
	unsigned cut_number = 0;
	unsigned cut_id;
	Cut c;

	for (unsigned i = 1; i <= number_of_cuts && pass; i++) {
		cut_id = cut_organiser.getCutOrder(i);
		c = cuts[cut_id];

	//	cut_organiser.cutInitialTime();

		pass = c();

	//	cut_organiser.cutEndTime(cut_id);

		if (pass) {

			#ifdef D_REPORT
				#pragma omp critical
				cut_organiser.incPassedEvent(cut_id);
			#endif
		} else {
			#ifdef D_REPORT
				#pragma omp critical
				cut_organiser.incFailedEvent(cut_id);
			#endif
		}
		cut_number++;
	}
}

void DataAnalysis::initialize (void) {
}

void DataAnalysis::finalize (void) {
}

void DataAnalysis::initRecord (void) {
}

void DataAnalysis::report (void) {

	#ifdef D_MPI
	long unsigned local_events = events.size();
	long unsigned global_events;

	MPI_Reduce(&event_counter_process, &global_events, 1, MPI_UNSIGNED_LONG, MPI_SUM, 0, MPI_COMM_WORLD);

	if (process_id == 0)
		cout << event_counter_process << " events analysed by " << number_of_processes << " processes with " << num_threads << " threads each" << endl << endl;
	#else
	cout << event_counter_process << " events analysed by process " << process_id << " with " << num_threads << " threads" << endl << endl;
	#endif

	cout << "Number of events that passed each cut: " << endl;

	for (unsigned i = 1; i <= number_of_cuts; i++) {
	//	for (unsigned j  cut_table)
				#ifdef D_MPI
				local_events = cut_organiser.getPassedEvents(cut_organiser.getCutOrder(i) - 1);

				MPI_Reduce(&local_events, &global_events, 1, MPI_UNSIGNED_LONG, MPI_SUM, 0, MPI_COMM_WORLD);

				if(process_id == 0)
					cout << "\t=> " << i << ": " << global_events << endl;
				#else
				cout << "\t=> " << i << ": " << cut_organiser.getPassedEvents(cut_organiser.getCutOrder(i) - 1) << endl;
				#endif
			}

	cout << endl;
	//printCuts();
}

unsigned DataAnalysis::addCut (string name, Cut c) {
	cuts.insert(make_pair(cut_counter, c));
	cut_table.insert(make_pair(name, cut_counter));

	cut_counter++;

	return cuts.size();
}

void DataAnalysis::printCuts (void) {
/*	for (unsigned i = 0; i < number_of_cuts + 1; i++) {
		if (!i) {
			cout << "\tstart\t";

			for (auto &ct : cut_table)
				cout << ct.first << "\t";

			cout << endl;
		}

		for (unsigned j = 0; j < number_of_cuts + 1; j++) {
			if (!j) {
				if(!i)
					cout << "start\t";

				unsigned b = 0;
				for (auto &ct : cut_table) {
					if (b == (i - 1)) {
						cout << ct.first << "\t";
						break;
					}

					b++;
				}
			}

			cout << cut_organiser.getCutDependency(i, j) << "\t";
		}
		cout << endl;
	}
	cout << endl << endl;

	printCutsOrder();*/
}

void DataAnalysis::printCutsOrder (void) {

/*	cout << "== Cut order ==" << endl;
	for (unsigned i = 1; i < number_of_cuts + 1; i++)
		for (auto &a : cut_table)
			if (a.second == cut_organiser.getCutOrder(i))
				cout << "\t" << a.first << endl;

	cout << endl;*/
}

bool DataAnalysis::addCutDependency (string cut1, string cut2) {
	// check if the cut exists on the lookup table
	if (cut_table.find(cut1) == cut_table.end() || cut_table.find(cut2) == cut_table.end() )
		return false;

	// checks if it's a duplicate
	if (cut1 == cut2)
		return false;

	// add the dependency
	cut_organiser.addCutDependency(cut_table.find(cut1)->second, cut_table.find(cut2)->second);

	return true;
}

void DataAnalysis::writeEvents (void) {
	TFile out_file (output_root_file.c_str(), "recreate");
	TTree *to_write = new TTree ("Final_Events", "Events that passed all cuts");

	for (long unsigned i = 0; i < cut_organiser.getPassedEvents(number_of_cuts - 1); i++) {
	//	events[events_passed_per_cut[number_of_cuts - 1].at(i)].write(to_write);
	}
}

void DataAnalysis::recordVariables (unsigned cut_number) {
}

void DataAnalysis::writeVariables (void) {
}

void DataAnalysis::recordPdfs (void) {
}

void DataAnalysis::writePdfs (void) {
}

void DataAnalysis::readPdf (vector<string> signals, vector<string> backgrounds) {
	// Read signal pdf
	if (signal_file != "unset") {
		TFile file (signal_file.c_str());

		for (unsigned i = 0; i < signals.size(); i++) {
			TH1D *p = (TH1D*) file.Get(signals[i].c_str());
			signal_pdf.push_back(*p);
		}
	}

	// Read background pdf
	if (background_file != "unset") {
		TFile file (background_file.c_str());

		for (unsigned i = 0; i < backgrounds.size(); i++) {
			TH1D *p = (TH1D*) file.Get(backgrounds[i].c_str());
			background_pdf.push_back(*p);
		}
	}

	pdfs = true;
}

void DataAnalysis::writeAllHistograms (void) {
}

long unsigned DataAnalysis::currentEvent (void) {
	return event_counter;
}
