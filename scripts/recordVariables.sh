#!/bin/bash

# Parses the DataAnalysis file of the user to create
# the variables and expressions to be stored for each
# cut.
#
# $1 -> Just the name of the class
# $2 -> Number of cuts in the analysis

SUFFIX="scripts"
SUFFIX2=".cxx"

HEP_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
HEP_DIR=${HEP_DIR%$SUFFIX}

if [ $# -eq 0 ]
  then
    echo "Not enough arguments supplied"
fi

F="$HEP_DIR/Analysis/$1/src/$1_cfg.cxx"
EV="$HEP_DIR/Analysis/$1/src/$1_Event.h"

ODIR="$HEP_DIR/Analysis/$1/src/"


#mv $FS"."* $HEP_DIR"tools/record_parser/bin/"

# ./recordParser -f /Users/andre/LIP-Minho/HEP/Analysis/Test/src/Test_cfg.cxx -e /Users/andre/LIP-Minho/HEP/Analysis/Test/src/Test_Event.h -a Test -c 4
cd $HEP_DIR"tools/record_parser/bin/"

./recordParser -f $F -e $EV -a $1 -c $2 -t $3

# moves the analysis file back to place
rm $ODIR"$1_cfg.cxx"
rm $ODIR"$1.h"

mv $HEP_DIR"tools/record_parser/bin/$1_final.cxx" $ODIR"$1_cfg.cxx"
mv $HEP_DIR"tools/record_parser/bin/$1_final.h" $ODIR"$1.h"

#mv $HEP_DIR"tools/record_parser/bin/$1_temp.cxx" $ODIR"$1_cfg.cxx"
#mv $HEP_DIR"tools/record_parser/bin/$1_temp.h" $ODIR"$1.h"

rm $HEP_DIR"tools/record_parser/bin/$1_temp.cxx"
rm $HEP_DIR"tools/record_parser/bin/$1_temp.h"