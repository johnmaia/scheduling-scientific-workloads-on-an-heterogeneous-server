#!/bin/bash

SUFFIX="scripts"
HEP_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
HEP_DIR=${HEP_DIR%$SUFFIX}

cd $HEP_DIR/lib

echo "#ifndef EVENT_h" > $HEP_DIR/lib/src/Event.h
echo "#define EVENT_h" >> $HEP_DIR/lib/src/Event.h
echo "" >> $HEP_DIR/lib/src/Event.h
echo "#include \"$HEP_DIR/Analysis/$1/src/$1_Event.h\"" >> $HEP_DIR/lib/src/Event.h
echo "" >> $HEP_DIR/lib/src/Event.h
echo "#endif" >> $HEP_DIR/lib/src/Event.h
