#!/bin/sh

# $1 Analysis Name


SUFFIX="scripts"
HEP_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
CLASS_NAME="$1_Event"
HEP_DIR=${HEP_DIR%$SUFFIX}

# creates the event data interface
cd $HEP_DIR/tools/interface_generator/bin
./EDGen $HEP_DIR/Analysis/$1/src/ $CLASS_NAME.h