#!/bin/bash

SUFFIX="scripts"
HEP_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
HEP_DIR=${HEP_DIR%$SUFFIX}

cd $HEP_DIR/lib

make clean
make
