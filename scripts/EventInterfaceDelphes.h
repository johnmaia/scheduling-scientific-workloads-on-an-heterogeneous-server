// ###################################################################################
//
//							!!Disclaimer!!
//
// ###################################################################################
//
// Add variables in the EventData class, between the public and constructor statements
//
// ###################################################################################

#include <vector>

extern std::vector<HEPEvent> events;
extern long unsigned event_counter;
#pragma omp threadprivate(event_counter)

/*
 *	 defines below
 */

#define photons events[event_counter].photons
#define electrons events[event_counter].electrons
#define muons events[event_counter].muons
#define taus events[event_counter].taus
#define jets events[event_counter].jets
#define tracks events[event_counter].tracks
#define MET events[event_counter].MET
#define MHT events[event_counter].MHT
#define TET events[event_counter].TET
#define THT events[event_counter].THT
#define nparts events[event_counter].nparts
#define processId events[event_counter].processId
#define weight events[event_counter].weight
#define scale events[event_counter].scale
#define alphaQED events[event_counter].alphaQED
#define alphaQCD events[event_counter].alphaQCD
#define PDFscale events[event_counter].PDFscale
#define particles events[event_counter].particles
