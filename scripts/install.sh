#!/bin/bash
#
# Script that installs the HEP-Frame tools
#
# $1 Specific boost lib dir, if any

SUFFIX="scripts"
HEP_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
HEP_DIR=${HEP_DIR%$SUFFIX}


realpath() {
    [[ $1 = /* ]] && echo "$1" || echo "$PWD/${1#./}"
}

if [ "$#" -eq 1 ]
  then
  	BOOST_DIR==$(realpath $1)
  else
  	echo "Assuming that Boost is installed on system root"
fi

mv generic_Makefile generic_Makefile_temp
line=$(head -n 1 generic_Makefile_temp)
if [[ "$line" == BOOST* ]]; then
echo "$(tail -n +2 generic_Makefile_temp)" > generic_Makefile_temp
fi
echo "BOOST_DIR=$1" | cat - generic_Makefile_temp > generic_Makefile
rm generic_Makefile_temp

mv generic_Makefile_delphes generic_Makefile_delphes_temp
line=$(head -n 1 generic_Makefile_delphes_temp)
if [[ "$line" == BOOST* ]]; then
echo "$(tail -n +2 generic_Makefile_delphes_temp)" > generic_Makefile_delphes_temp
fi
echo "BOOST_DIR=$1" | cat - generic_Makefile_delphes_temp > generic_Makefile_delphes
rm generic_Makefile_delphes_temp

mv ../lib/Makefile ../lib/Makefile_temp
line=$(head -n 1 ../lib/Makefile_temp)
if [[ "$line" == BOOST* ]]; then
echo "$(tail -n +2 ../lib/Makefile_temp)" > ../lib/Makefile_temp
fi
echo "BOOST_DIR=$1" | cat - ../lib/Makefile_temp > ../lib/Makefile
rm ../lib/Makefile_temp

echo ""
echo "---------------------------------------------------"
echo "HEP-Frame :: Compiling the Analysis Generator tool"
echo "---------------------------------------------------"
echo ""

# compiles analysis generator
cd $HEP_DIR/tools/analysis_generator
mv Makefile Makefile_temp
line=$(head -n 1 Makefile_temp)
if [[ "$line" == BOOST* ]]; then
echo "$(tail -n +2 Makefile_temp)" > Makefile_temp
fi
echo "BOOST_DIR=$1" | cat - Makefile_temp > Makefile
rm Makefile_temp

make clean
make

echo ""
echo "---------------------------------------------------"
echo "HEP-Frame :: Compiling the Class Generator tool"
echo "---------------------------------------------------"
echo ""

# compiles class generator
cd $HEP_DIR/tools/class_generator
mv Makefile Makefile_temp
line=$(head -n 1 Makefile_temp)
if [[ "$line" == BOOST* ]]; then
echo "$(tail -n +2 Makefile_temp)" > Makefile_temp
fi
echo "BOOST_DIR=$1" | cat - Makefile_temp > Makefile
rm Makefile_temp

make clean
make

echo ""
echo "---------------------------------------------------"
echo "HEP-Frame :: Compiling the Interface Generator tool"
echo "---------------------------------------------------"
echo ""

# compiles the interface generator
cd $HEP_DIR/tools/interface_generator

make clean
make

echo ""
echo "---------------------------------------------------"
echo "HEP-Frame :: Compiling the Record Parser tool"
echo "---------------------------------------------------"
echo ""

# compiles record parser
cd $HEP_DIR/tools/record_parser
mv Makefile Makefile_temp
line=$(head -n 1 Makefile_temp)
if [[ "$line" == BOOST* ]]; then
echo "$(tail -n +2 Makefile_temp)" > Makefile_temp
fi
echo "BOOST_DIR=$1" | cat - Makefile_temp > Makefile
rm Makefile_temp

make clean
make

echo ""
echo "---------------------------------------------------"
echo "HEP-Frame :: Compiling ExRootAnalysis"
echo "---------------------------------------------------"
echo ""

# compiles record parser
cd $HEP_DIR/tools/ExRootAnalysis

make clean
make

echo ""
echo "---------------------------------------------------"
echo "HEP-Frame :: Compiling Delphes"
echo "---------------------------------------------------"
echo ""

# compiles record parser
cd $HEP_DIR/tools/Delphes-3.3.2

make clean
make
