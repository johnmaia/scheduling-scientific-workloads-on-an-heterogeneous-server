#include <TCanvas.h>
#include "TTH.h"
#include "EventInterface.h"
#include "myvector.h"
#include <TRandom3.h>
#include <TFile.h>
#include "TLorentzVectorWFlags.h"

using namespace std;

extern vector<HEPEvent> events;
extern long unsigned event_counter;
#pragma omp threadprivate(event_counter)

#define GeV 1000
#define Luminosity 2.034
#define TPI 3.14159265358979312
#define mc_lum 2.034	//mc_aux e sempre 2

float EtaCutJet = 999;
//vector<TMonteCarlo> MonteCarlo;

float mW = 80.40*GeV;
float mt = 172.5*GeV;
float mH_UserValue = 125.00*GeV;
unsigned ttDKF_njet_UserValue = 4;
unsigned ttDKF_njets;
unsigned dilep_iterations = 128;
static TRandom3 rnd;

// pdfs
double LowerEdge[100];
double UpperEdge[100];
double Scale[100];
int NBins[100];
double *pdfKinFitVec[100];
TH1D *pdfKinFit[100];

TCanvas *mydummycanvas;

TTH::~TTH (void) {
}

// Use this constructor if you don't specify a ttree in the input file
TTH::TTH (vector<string> _root_file, string _tree_filename, string _output_root_file, string _signal, string _background, unsigned ncuts) : DataAnalysis (_root_file, _tree_filename, _output_root_file, _signal, _background, ncuts) {
	// Set to true if you want to save events that pass all cuts
	save_events = false;
	mydummycanvas=new TCanvas();
	// Initialise your class variables here
	// Set to true if you want to save events that pass all cuts
	save_events = false;
	// Initialise your class variables here
	TFile *fTruth = new TFile("main_ttH_8TeV.root");
	double IntPDF;

	for (unsigned n = 0; n < 2; n++) {
		if ( n == 0 ) pdfKinFit[n] = (TH1D*)fTruth->Get("sel00_pdf_pt__n1"); // 1st pdf: pT neutrino 1
		if ( n == 1 ) pdfKinFit[n] = (TH1D*)fTruth->Get("sel00_pdf_pt__n2"); // 2nd pdf: pT neutrino 2

		// normalize to unity and save histos
		IntPDF = pdfKinFit[n]->Integral();
		if ( IntPDF != 0 ) pdfKinFit[n]->Scale(1./IntPDF);

		// smooth histos and save them
  		pdfKinFit[n]->Smooth(3);

		NBins[n] = pdfKinFit[n]->GetNbinsX();
		pdfKinFitVec[n] = new double[NBins[n]+2];
		LowerEdge[n] = pdfKinFit[n]->GetBinLowEdge(1);
		UpperEdge[n] = pdfKinFit[n]->GetBinLowEdge(NBins[n]+1);
		Scale[n] = double(NBins[n])/(UpperEdge[n]-LowerEdge[n]);

		// Input pdfKinFitVec
		for (int bin = 0; bin < NBins[n]+2; bin++){
			pdfKinFitVec[n][bin] = pdfKinFit[n]->GetBinContent(bin);
		}
	}
	delete fTruth;
}

// Use this constructor if you specify a ttree in the input file
TTH::TTH (vector<string> _root_file, string _tree_filename, string _output_root_file, string _signal, string _background, string _tree_name, unsigned ncuts) : DataAnalysis (_root_file, _tree_filename, _output_root_file, _signal, _background, _tree_name, ncuts) {
	// Set to true if you want to save events that pass all cuts
	save_events = false;
	mydummycanvas=new TCanvas();
	// Initialise your class variables here
	// Set to true if you want to save events that pass all cuts
	save_events = false;
	// Initialise your class variables here
	TFile *fTruth = new TFile("main_ttH_8TeV.root");
	double IntPDF;

	for (unsigned n = 0; n < 2; n++) {
		if ( n == 0 ) pdfKinFit[n] = (TH1D*)fTruth->Get("sel00_pdf_pt__n1"); // 1st pdf: pT neutrino 1
		if ( n == 1 ) pdfKinFit[n] = (TH1D*)fTruth->Get("sel00_pdf_pt__n2"); // 2nd pdf: pT neutrino 2

		// normalize to unity and save histos
		IntPDF = pdfKinFit[n]->Integral();
		if ( IntPDF != 0 ) pdfKinFit[n]->Scale(1./IntPDF);

		// smooth histos and save them
  		pdfKinFit[n]->Smooth(3);

		NBins[n] = pdfKinFit[n]->GetNbinsX();
		pdfKinFitVec[n] = new double[NBins[n]+2];
		LowerEdge[n] = pdfKinFit[n]->GetBinLowEdge(1);
		UpperEdge[n] = pdfKinFit[n]->GetBinLowEdge(NBins[n]+1);
		Scale[n] = double(NBins[n])/(UpperEdge[n]-LowerEdge[n]);

		// Input pdfKinFitVec
		for (int bin = 0; bin < NBins[n]+2; bin++){
			pdfKinFitVec[n][bin] = pdfKinFit[n]->GetBinContent(bin);
		}
	}
	delete fTruth;
}

// Initialize any variables of the event
void TTH::initialize (void) {

}

// Finalize any calculations with variables of the event after the cut processing
void TTH::finalize (void) {

}


//myrunnumber != 105200
void Calculations(void){
	NbtagJet=0;

	MyGoodJetVec.clear();
	// __AO 18 Outubro_______________________________
	MyGoodBtaggedJetVec.clear();
	MyGoodNonBtaggedJetVec.clear();
	// __AO 18 Outubro_______________________________

	for( unsigned i =0; i<JetVec.size(); i++){
		if( JetVec[i].Pt()>PtCutJet && fabs(JetVec[i].Eta())<EtaCutJet ){
			MyGoodJetVec.push_back(JetVec[i]);
			if(abs(JetVec[i].isb) == 5) {
				NbtagJet++;
				MyGoodBtaggedJetVec.push_back(JetVec[i]);
			}
			if(abs(JetVec[i].isb) != 5) {
				MyGoodNonBtaggedJetVec.push_back(JetVec[i]);
			}
		}
	}


	// get number of truth leptons
	ntruthlep    = 0;
	ntruthele    = 0;
	ntruthmu     = 0;
	ntruthtau    = 0;
	ntrutheletau = 0;
	ntruthmutau  = 0;
	ntruthleptau = 0;


	ntruthele    = TruthEleNumber; 	// nTuple Variable
	ntruthmu     = TruthMuonNumber; // nTuple Variable

	ntruthlep = ntruthele + ntruthmu + ntruthleptau;

	// Ht from Minintuple
	Ht = Ht_Mini;

	// Lepton Lorentz vectors reconstruction
	if(LeptonVec.size() > 1)
	{
		ll = LeptonVec[0] + LeptonVec[1];

		llmiss.SetPxPyPzE(ll.Px() + MissPx, ll.Py() + MissPy, 0., ll.E() + MissPt);
	}
	else
	{
		ll.SetPxPyPzE(0., 0., 0., 0.);

		llmiss.SetPxPyPzE(0., 0., 0., 0.);
	}


	// Hz calculation
	Hz = 0.;
	for(unsigned i = 0; i<LeptonVec.size(); i++)     Hz = Hz+LeptonVec[i].Pz();
	for (unsigned i = 0; i<MyGoodJetVec.size(); i++) Hz = Hz+MyGoodJetVec[i].Pz();
}

bool LorentzVecComp (TLorentzVectorWFlags a, TLorentzVectorWFlags b) {
	return a.Pt() > b.Pt();
}

void LeptonIsolationDeltaR(void){
	double IsoDeltaR;
	double tmp;

	for(unsigned i = 0; i < LeptonVec.size(); ++i){
		IsoDeltaR = 999.;
		tmp = 999.;

		for(unsigned j = 0; j < JetVec.size(); j++) {
			tmp = JetVec[j].DeltaR(LeptonVec[i]);
			if( tmp < IsoDeltaR )
				IsoDeltaR = tmp;
		}

		for(unsigned j = 0; j < LeptonVec.size(); j++) {
			tmp = LeptonVec[j].DeltaR(LeptonVec[i]);
			if( tmp < IsoDeltaR && j != i )
				IsoDeltaR = tmp;
		}
	}
}

void JetIsolationDeltaR(void){
	double IsoDeltaR;
	double tmp;

	for (unsigned i = 0; i < JetVec.size(); ++i){
		IsoDeltaR = 999.;
		tmp = 999.;

		for (unsigned j = 0; j < JetVec.size(); j++) {
			tmp = JetVec[j].DeltaR(JetVec[i]);
			if (tmp < IsoDeltaR && j != i)
				IsoDeltaR = tmp;
		}

		for (unsigned j = 0; j < LeptonVec.size(); j++) {
			tmp = LeptonVec[j].DeltaR(JetVec[i]);
			if (tmp < IsoDeltaR)
				IsoDeltaR = tmp;
		}

		JetVec[i].SetIsoDeltaR(IsoDeltaR);
	}
}

void FillObjects(void) {
	bool FillJets      = true;
	bool FillMuons     = true;
	bool FillElectrons = true;


	// reset output
	std::vector<TLorentzVectorWFlags> use_Photon_Vec;
	std::vector<TLorentzVectorWFlags> use_Electron_Vec;
	std::vector<TLorentzVectorWFlags> use_Muon_Vec;
	std::vector<TLorentzVectorWFlags> use_Jet_Vec;

	// usefull variables
	int jets25inevent=0;

	// auxiliar stuff
	Int_t kf = 0;
	TLorentzVector Good;

	// ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	// electrons
	// ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	//For 2011:  https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/TopCommonObjects2011#Electrons
	Double_t elpT, elEta, elPhi, elEne;
	Int_t elTruthMatch, elTrigMatch;

	// Loop over all electrons.
	if (FillElectrons) for (unsigned k=0; k<lep_n; ++k){
		// ------------------------------check it is an electron
		if ( fabs(lep_type[k])  != 11 )  continue;
		// ------------------------------accept electron
		elpT   = lep_pt[k];
		elEta  = lep_eta[k];
		elPhi  = lep_phi[k];
		elEne  = lep_E[k];
		Good.SetPtEtaPhiE(elpT, elEta, elPhi, elEne);
		if ( lep_charge[k]>0 ) kf = -11; else kf = 11;
		// truth and trigger match
		elTruthMatch = 0;
		if ( lep_truthMatched[k] ) elTruthMatch = 1 ;
		elTrigMatch  = 0;
		if ( lep_trigMatched[k] )  elTrigMatch  = 1 ;
		// creat TLorentzWFlags vector
		TLorentzVectorWFlags GoodWFlags(Good, k, kf, 999., elTruthMatch, elTrigMatch);
		use_Electron_Vec.push_back(GoodWFlags);
	}  // Fill Electrons done

	// ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	// take vertex z information (assumed to be ok)
	// ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	if( hasGoodVertex == 1 ){
		// ------------------------------accept vertex
		TVector3 v( -999., -999., vxp_z);
		Vtx.push_back(v);
	}


	// ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	// jets
	// ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	// for jet energy scale and resolution uncertainties:
	//Double_t jetET;
	Double_t jetpT, jetEta, jetPhi, jetEne;
	Int_t jetTruthMatch;

	// Loop over all jets
	if (FillJets) for (unsigned k=0; k<jet_n; ++k)  {
		// ------------------------------if jets have MV1>0.795
		if ( BTagCut != 999  &&  jet_MV1[k] > BTagCut  ) kf = 5; else kf = 1;

		// ------------------------------accept jet
		jetpT   = jet_pt[k];
		jetEta  = jet_eta[k];
		jetPhi  = jet_phi[k];
		jetEne  = jet_E[k];
		Good.SetPtEtaPhiE(jetpT, jetEta, jetPhi, jetEne );
		// truth and trigger match
		jetTruthMatch = 0;
		if ( jet_truthMatched[k] == 1 ) jetTruthMatch = 1 ;
		// creat TLorentzWFlags vector
		TLorentzVectorWFlags GoodWFlags(Good, k, kf, 999., jetTruthMatch, -1);
		use_Jet_Vec.push_back(GoodWFlags);
	}  // Fill jets done


	// ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	// muons
	// ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	Double_t mupT, muEta, muPhi, muEne;
	Int_t muTruthMatch, muTrigMatch;

	// loop over all muons
	if (FillMuons) for (unsigned k=0; k<lep_n; ++k){

		// ------------------------------check it is an electron
		if ( fabs(lep_type[k])  != 13 )  continue;

		// ------------------------------accept muon
		mupT   = lep_pt[k];
		muEta  = lep_eta[k];
		muPhi  = lep_phi[k];
		muEne  = lep_E[k];
		if ( lep_charge[k]>0 ) kf = -13; else kf = 13;
		Good.SetPtEtaPhiE(mupT, muEta, muPhi, muEne);
		// truth and trigger match
		muTruthMatch = 0;
		if ( lep_truthMatched[k] ) muTruthMatch = 1 ;
		muTrigMatch  = 0;
		if ( lep_trigMatched[k] )  muTrigMatch  = 1 ;
		// creat TLorentzWFlags vector
		TLorentzVectorWFlags GoodWFlags(Good, k, kf, 999., muTruthMatch, muTrigMatch);
		use_Muon_Vec.push_back(GoodWFlags);

	}  // Fill muons done


	// ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	// Fill objects
	// ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

	 // Fill jets
	 for (unsigned k=0; k< use_Jet_Vec.size(); ++k) {
				JetVec.push_back(use_Jet_Vec[k]);
				if ( use_Jet_Vec[k].Pt() > 25*GeV ) jets25inevent++;
	 }

	 // Fill electrons
	 for (unsigned k=0; k< use_Electron_Vec.size(); ++k) {
			LeptonVec.push_back(use_Electron_Vec[k]);
	 }
	 // Fill muons
	 for (unsigned k=0; k< use_Muon_Vec.size();     ++k) {
	LeptonVec.push_back(use_Muon_Vec[k]);
	 }
}

void FillAllVectors (void) {

	// fill vectors with particles

	FillObjects();

	// sort particles inside the vectors
	sort(LeptonVec.begin(), LeptonVec.end(), LorentzVecComp);
	sort(JetVec.begin(), JetVec.end(), LorentzVecComp);

	// isolation angles
	LeptonIsolationDeltaR();
	JetIsolationDeltaR();

	// other stuff to be filled only after nTuple->FillVectors is called
	Isub = channelNumber;
	LumiBlock = -999;
	RunNumber = runNumber;
	EveNumber = eventNumber;
	TruthEleNumber = truE;
	TruthMuonNumber = truM;
	if (cosmicEvent)
		Cosmic = 1;
	else
		Cosmic = 0;
	HforFlag = 0;
	massInv_LL_Mini = massInv_LL;
	jet_n_Mini = jet_n;
	EleMuoOverlap = 0;
	JetCleanning = 0;
	if ( passGRL )
		GoodRL = 1;
	else
		GoodRL = 0;
	// MissPt() must be called before MissPx() and MissPy()
	MissPt = met_et;
	MissPx = met_et * cos(met_phi) / GeV;
	MissPy = met_et * sin(met_phi) / GeV;
	Sphericity = 0;
	Aplanarity = 0;
	Planarity  = 0;

}

void Csqrt(double _ar, double _ai, double _my[]) {
	///// complex sqrt
	double x,y,r,w;
	if  (  (_ar == 0.0) && (_ai == 0.0) ) {
		_my[0]=0.0;
		_my[1]=0.0;
		return;
	} else {
		x=fabs(_ar);
		y=fabs(_ai);
		if (x >= y) {
			r=y/x;
			w=sqrt(x)*sqrt(0.5*(1.0+sqrt(1.0+r*r)));
		} else {
			r=x/y;
			w=sqrt(y)*sqrt(0.5*(r+sqrt(1.0+r*r)));
		}
		if (_ar>= 0.0) {
			_my[0]=w;
			_my[1]=_ai/(2.0*w);
		} else {
			_my[1]=(_ai >= 0) ? w : -w;
			_my[0]=_ai/(2.0*_my[1]);
		}
		return;
	}
}

void cubic(double a[], double rr[], double ri[]) {
	int i;
	double a0, a1, a2, a3;
	double g, h, y1, sh, theta, xy1, xy2, xy3;
	double y2, z1, z2, z3, z4;
	//// initialize the results
	for (i = 0; i < 3; i ++)
	{
		rr[i] = 0.0;
		ri[i] = 0.0;
	}

	a0 = a[0];
	a1 = a[1]/3.0;
	a2 = a[2]/3.0;
	a3 = a[3];

	g = (a0 * a0) * a3 - 3.0 * a0 * a1 * a2 + 2.0 * pow(a1, 3);
	h = a0 * a2 - a1 * a1;
	y1 = g * g + 4.0 * pow(h, 3);

	if (y1 < 0.0){
		sh = sqrt(-h);
		theta = acos(g / (2.0 * h * sh)) / 3.0;
		xy1 = 2.0 * sh * cos(theta);
		xy2 = 2.0 * sh * cos(theta + (2.0 * TPI / 3.0));
		xy3 = 2.0 * sh * cos(theta + (4.0 * TPI / 3.0));
		rr[0] = (xy1 - a1) / a0;
		rr[1] = (xy2 - a1) / a0;
		rr[2] = (xy3 - a1) / a0;
		return;
	} else {
		y2 = sqrt(y1);
		z1 = (g + y2) / 2.0;
		z2 = (g - y2) / 2.0;
		if (z1 < 0.0){
			z3 = pow(-z1, 1.0/3.0);
			z3 = -z3;
		} else  z3 = pow(z1, 1.0/3.0);
		if (z2 < 0.0){
			z4 = pow(-z2, 1.0/3.0);
			z4 = - z4;
		}
		else  z4 = pow(z2, 1.0/3.0);

		rr[0] = -(a1 + z3 + z4) / a0;
		rr[1] = (-2.0 * a1 + z3 + z4) / (2.0 * a0);
		ri[1] = sqrt(3.0) * (z4 - z3) / (2.0 * a0);
		rr[2] = rr[1];
		ri[2] = -ri[1];

		return;

	}
}

void toz(double k[], double l[], double g[]){
	//// checked !!
	///////////////////////////////////////////////////////////////////////////
	///// bring z=A+Bx+Cy to 2*D*sqrt(x**2+y**2+z**2)-2(ax+by+dz) = E
	///// simplify it to g1*x^2 + g2*y^2 + 2*g3*x + 2*g4*y + 2*g5*xy + g6 = 0
	///////////////////////////////////////////////////////////////////////////
	double A = k[0];
	double B = k[1];
	double C = k[2];
	double D = k[3];
	double E = k[4];
	double a = l[0];
	double b = l[1];
	double d = l[2];
	g[0] = 4*pow(D,2)*( 1 + pow(B,2)) - 4*pow(a,2) -4*pow(d,2)*pow(B,2) - 8*a*d*B;
	if ( g[0]!=0 ) {
		g[1] = ( 4*pow(D,2)*( 1 + pow(C,2)) - 4*pow(b,2) -4*pow(d,2)*pow(C,2) - 8*b*d*C )/g[0] ;
		g[2] = ( (4*pow(D,2)-4*d*d)*A*B - 4*a*d*A - 2*E*a - 2*E*d*B )/g[0];
		g[3] = ( (4*pow(D,2)-4*d*d)*A*C - 4*b*d*A - 2*E*b - 2*E*d*C )/g[0];
		g[4] = ( (4*pow(D,2)-4*d*d)*B*C - 4*a*b   - 4*a*d*C - 4*b*d*B )/g[0];
		g[5] = ( (4*pow(D,2)-4*d*d)*A*A - E*E - 4*E*d*A )/g[0];
		g[0] = 1.0;
	} else {
		g[1] = ( 4*pow(D,2)*( 1 + pow(C,2)) - 4*pow(b,2) -4*pow(d,2)*pow(C,2) - 8*b*d*C ) ;
		g[2] = ( (4*pow(D,2)-4*d*d)*A*B - 4*a*d*A - 2*E*a - 2*E*d*B );
		g[3] = ( (4*pow(D,2)-4*d*d)*A*C - 4*b*d*A - 2*E*b - 2*E*d*C );
		g[4] = ( (4*pow(D,2)-4*d*d)*B*C - 4*a*b   - 4*a*d*C - 4*b*d*B );
		g[5] = ( (4*pow(D,2)-4*d*d)*A*A - E*E - 4*E*d*A );
		g[0] = 0.;
	}
	return;
}

void my_qu( double my_in[], double my_val[]) {

	///////////////////////////////////////////
	/////  find the solution /////////////////
	/////  ax^4+bx^3+cx^2+dx+e=0
	//////////////////////////////////////////
	double a=my_in[0];
	double b=my_in[1];
	double c=my_in[2];
	double d=my_in[3];
	double e=my_in[4];

	double real[3]={0,0,0};
	double img[3]={0,0,0};
	double x1_r = 0; double x1_i = 0;
	double x2_r = 0; double x2_i = 0;
	double x3_r = 0; double x3_i = 0;
	double x4_r = 0; double x4_i = 0;


	/////////////////////////////////////////////
	///// in case of a==0, simplify to cubic
	///// bx^3+cx^2+dx+e=0
	/////////////////////////////////////////////

	if ( a ==0 && b!=0 ){
		double input[4]={b,c,d,e};
		cubic(input,real,img);
		x1_r = real[0];	x1_i = img[0];
		x2_r = real[1];	x2_i = img[1];
		x3_r = real[2];	x3_i = img[2];
		my_val[0] = x1_r;
		my_val[1] = x1_i;
		my_val[2] = x2_r;
		my_val[3] = x2_i;
		my_val[4] = x3_r;
		my_val[5] = x3_i;
		my_val[6] = x4_r;
		my_val[7] = x4_i;

		return;
	}


	/////////////////////////////////////////////
	///// in case of a==0 && b==0,
	///// simplify to quadratic
	///// cx*2 + d*x + e =0
	/////////////////////////////////////////////

	if ( a ==0 && b==0 && c!=0){
		double alpha = pow(d,2)-4*c*e;
		if (alpha>=0) {
			x1_r = (-1*d + sqrt(alpha))/2/c;	x1_i = 0;
			x2_r = (-1*d - sqrt(alpha))/2/c;	x2_i = 0;
		} else {
			x1_r = (-1*d )/2/c;	x1_i = sqrt(-alpha)/2/c;
			x2_r = (-1*d )/2/c;	x2_i = -1*sqrt(-alpha)/2/c;
		}
		my_val[0] = x1_r;
		my_val[1] = x1_i;
		my_val[2] = x2_r;
		my_val[3] = x2_i;
		my_val[4] = x3_r;
		my_val[5] = x3_i;
		my_val[6] = x4_r;
		my_val[7] = x4_i;

		return;
	}


	/////////////////////////////////////////////
	///// in case of a==0 && b==0 && c==0,
	///// simplify to linear equation dx + e =0
	/////////////////////////////////////////////

	if (a ==0 && b==0 && c==0 && d!=0){
		x1_r = -e/d;	x1_i = 0;
		my_val[0] = x1_r;
		my_val[1] = x1_i;
		my_val[2] = x2_r;
		my_val[3] = x2_i;
		my_val[4] = x3_r;
		my_val[5] = x3_i;
		my_val[6] = x4_r;
		my_val[7] = x4_i;

		return;
	}


	//////////////////////////////////////////////
	////  (1)the common expression
	///////////////////////////////////////////////
	double bb=b/a; double cc=c/a;
	double dd=d/a; double ee=e/a;

	//////////////////////////////////////////////
	////  (2) the equation changes to
	////	x^4 + bb*x^3 + cc*x^2 + dd*x + ee=0
	////
	////  (3) substitude x=y-aa/4, then we get
	////    y^4 + f*y^2 + g*y + h =0; where
	///////////////////////////////////////////

	double _f = cc - 3*pow(bb,2)/8;
	double _g = dd + (pow(bb,3)/8) - (bb*cc/2);
	double _h = ee - (3*pow(bb,4)/256) + (pow(bb,2)*cc/16) - (bb*dd/4);

	////////////////////////////////////////////////////////////////////////////
	///  (4) the normal situation is f, g and h are non-zero; then		////
	//// 	the related cubic equation is					////
	//// 	z^3 + (f/2) z^2 + ( (f^2-4h)/16 )*z -g^2/64 =0;			////
	////	 it has three "squared" roots, for example, p,q and l, 		////
	//// 	then p^2, q^2 and l^2 are the root of equation above		////
	//// 	set r=-g/8(pq), then the four roots of the original quartic are	////
	//// 		x = p + q + r -bb/4;					////
	//// 		x = p - q - r -bb/4;					////
	//// 		x = -p + q - r -bb/4;					////
	//// 		x = -p - q + r -bb/4;					////
	////////////////////////////////////////////////////////////////////////////

	double c_1 = 1.;
	double c_2 = _f/2;
	double c_3 = (pow(_f,2)-4*_h)/16.;
	double c_4 = -1*pow(_g,2)/64.;

	double input[4]={c_1,c_2,c_3,c_4};
	cubic(input,real,img);

	////////////////////////////////////////////////
	////// (5) sqrt root of the cubic equation solutions
	////////////////////////////////////////////////

	double out_r[3]={0,0,0};
	double out_i[3]={0,0,0};
	bool _img[3]={false,false,false};
	int img_index =0;
	double my[2];

	for (int ii=0; ii<3; ii++){
		Csqrt(real[ii],img[ii],my);
		out_r[ii] = my[0];
		out_i[ii] = my[1];
		if ( my[1]!=0 ) {
			_img[ii]=true;
			img_index++;
		}
	}


	///////////////////////////////////////////////
	////(x + yi)(u + vi) = (xu - yv) + (xv + yu)i
	//// calculating r = -g/(8pq)
	////////////////////////////////////////////////
	double r_r;	double r_i;
	double p_r;	double p_i;
	double q_r;	double q_i;

	if (_img[0]==_img[1] && out_r[0]*out_r[1]>0 ){
		r_r	= out_r[0]*out_r[1] - out_i[0]*out_i[1];
		r_i	= out_r[0]*out_i[1] + out_r[1]*out_i[0];
		p_r	= out_r[0]; p_i = out_i[0];
		q_r	= out_r[1]; q_i = out_i[1];
	} else {
		for (int kk=0; kk<2; kk++) {
			for (int k=kk+1; k<3; k++) {
				if (_img[kk]==_img[k]){
					r_r	= out_r[kk]*out_r[k] - out_i[kk]*out_i[k];
					r_i	= out_r[kk]*out_i[k] + out_r[k]*out_i[kk];
					p_r	= out_r[kk]; p_i = out_i[kk];
					q_r	= out_r[k]; q_i = out_i[k];
				}
			}
		}
	}


	if( r_r !=0 )	{
		r_r 	= -1.*_g/8/r_r;
	} else {
		r_r 	= 0;
	}
	if( r_i !=0 )	{
		r_r 	= -1.*_g/8/r_i;
	} else {
		r_i 	= 0;
	}

	x1_r = p_r + q_r + r_r -b/a/4;
	x1_i = p_i + q_i + r_i;
	x2_r = p_r - q_r - r_r -b/a/4;
	x2_i = p_i - q_i - r_i;
	x3_r = -1*p_r + q_r - r_r -b/a/4;
	x3_i = -1*p_i + q_i - r_i;
	x4_r = -1*p_r - q_r + r_r -b/a/4;
	x4_i = -1*p_i - q_i + r_i;

	my_val[0] = x1_r;
	my_val[1] = x1_i;
	my_val[2] = x2_r;
	my_val[3] = x2_i;
	my_val[4] = x3_r;
	my_val[5] = x3_i;
	my_val[6] = x4_r;
	my_val[7] = x4_i;
}
vector<myvector>* calc_dilep(double t_mass[], double w_mass[],
						double in_mpx[], double in_mpy[], double in_mpz[],
						TLorentzVector* lep_a, TLorentzVector* lep_b,
						TLorentzVector* bl_a, TLorentzVector* bl_b)
{

	double mpx, mpy, G_1, G_2, G_3, G_4, _d, _a, _f, _b, _e, _g;


  mpx = in_mpx[0];
  mpy = in_mpy[0];

  const double WMass_a = w_mass[0];
  const double tMass_a = t_mass[0];
  const double WMass_b = w_mass[1];
  const double tMass_b = t_mass[1];

   G_1 = WMass_a*WMass_a - ( lep_a->M() )*( lep_a->M() );
   G_3 = WMass_b*WMass_b - ( lep_b->M() )*( lep_b->M() );
   G_2 = tMass_a*tMass_a - ( bl_a->M()  )*( bl_a->M()  );
   G_4 = tMass_b*tMass_b - ( bl_b->M()  )*( bl_b->M()  );

  double S=mpx;
  double T=mpy;


   double G_5 =	( bl_a->Px()/bl_a->E() - lep_a->Px()/lep_a->E() );
   double G_6 =	( bl_a->Py()/bl_a->E() - lep_a->Py()/lep_a->E() );
   double G_7 =	( bl_a->Pz()/bl_a->E() - lep_a->Pz()/lep_a->E() );
   double G_8 =	( G_1/lep_a->E() - G_2/bl_a->E() )/2.;

   double G_9 =	( bl_b->Px()/bl_b->E() - lep_b->Px()/lep_b->E() );
   double G_10 =	( bl_b->Py()/bl_b->E() - lep_b->Py()/lep_b->E() );
   double G_11 =	( bl_b->Pz()/bl_b->E() - lep_b->Pz()/lep_b->E() );
   double G_12 =	( G_3/lep_b->E() - G_4/bl_b->E() )/2.;


  double in_a[5],on_a[3],out_a[6];
  in_a[0] = G_8/G_7;
  in_a[1] = -1.0*G_5/G_7;
  in_a[2] = -1.0*G_6/G_7;
  in_a[3] = lep_a->E();
  in_a[4] = G_1;

  on_a[0] = lep_a->Px();
  on_a[1] = lep_a->Py();
  on_a[2] = lep_a->Pz();
  toz(in_a, on_a, out_a);

  double in_c[5],on_c[3],out_c[6];
  in_c[0] = G_12/G_11;
  in_c[1] = -1*G_9/G_11;
  in_c[2] = -1*G_10/G_11;
  in_c[3] = lep_b->E();
  in_c[4] = G_3;

  on_c[0] = lep_b->Px();
  on_c[1] = lep_b->Py();
  on_c[2] = lep_b->Pz();
  toz(in_c, on_c, out_c);

  double out_e[6];
  out_e[0] = out_c[0];
  out_e[1] = out_c[1];
  out_e[2] = -1*( out_c[0]*mpx + out_c[2] + out_c[4]*mpy);
  out_e[3] = -1*( out_c[1]*mpy + out_c[3] + out_c[4]*mpx);
  out_e[4] = out_c[4];
  out_e[5] =( out_c[0]*mpx*mpx + out_c[1]*mpy*mpy + 2*out_c[2]*mpx + 2*out_c[3]*mpy + out_c[5] + 2*out_c[4]*mpx*mpy);

	double _A = out_e[0];
	double _B = out_e[1];
	double _D = out_e[2];
	double _E = out_e[3];
	double _F = out_e[4];
	double _G = out_e[5];

	if ( out_a[0]!=0  ){
	  _a = out_a[0];
	  _b = out_a[1];
	  _d = out_a[2];
	  _e = out_a[3];
	  _f = out_a[4];
	  _g = out_a[5];
	}
	if (out_a[0]==0 && _A!=0 ) {
	  _a = 1.;
	  _b = out_a[1] + _B/_A;
	  _d = out_a[2] + _D/_A;
	  _e = out_a[3] + _E/_A;
	  _f = out_a[4] + _F/_A;
	  _g = out_a[5] + _G/_A;
	}
	if ( out_a[0]==0 && _A==0){
	return NULL;
	}

	double fx_1 = 2.*(_A*_d - _a*_D);
	double fx_2 = 2.*(_A*_f - _a*_F);
	double fx_3 = _a*_B - _A*_b;
	double fx_4 = 2.*(_E*_a - _A*_e);
	double fx_5 = _a*_G - _g*_A;

	double k_1 = ( _f*_f - _a*_b )/_a/_a;
	double k_2 = ( 2.*_d*_f - 2.*_a*_e )/_a/_a;
	double k_3 = ( _d*_d-_a*_g )/_a/_a;
	double k_4 = -1.*_d/_a;
	double k_5 = -1.*_f/_a;




	double g_1 = 4.*_A*_A*k_5*k_5 + 4.*_F*_F + 8.*_A*_F*k_5;
	double g_2 = 8.*_A*_A*k_4*k_5 + 8.*_A*_D*k_5 + 8.*_A*_F*k_4 + 8.*_D*_F;
	double g_3 = 4.*_A*_A*k_4*k_4 + 4.*_D*_D + 8.*_A*_D*k_4;
	double g_4 = _A*k_1 + _A*k_5*k_5;
	double g_5 = _A*k_2 + 2.*_A*k_4*k_5 + 2.*_D*k_5;
	double g_6 = _A*k_3 + _A*k_4*k_4 + 2.*_D*k_4 + _G;

	double m_1 = g_1*k_1;
	double m_2 = g_1*k_2 + g_2*k_1;
	double m_3 = g_1*k_3 + g_2*k_2 + g_3*k_1;
	double m_4 = g_2*k_3 + g_3*k_2;
	double m_5 = g_3*k_3;

	double m_6  = _B*_B + 4.*_F*_F*k_5*k_5 + 4.*_B*_F*k_5;
	double m_7  = 4.*_B*_E + 8.*_F*_F*k_4*k_5 + 4.*_B*_F*k_4 + 8.*_E*_F*k_5;
	double m_8  = 4.*_E*_E + 4.*_F*_F*k_4*k_4 + 8.*_E*_F*k_4;
	double m_80 = pow(g_4,2);
	double m_81 = 2*g_4*g_5;
	double m_9  = pow(g_5,2) + 2.*g_4*g_6;
	double m_10 = 2.*g_5*g_6;
	double m_11 = g_6*g_6;

	double m_12 = 	2.*_A*_B*k_1 + 2.*_A*_B*k_5*k_5 + 4.*_A*_F*k_1*k_5 + 4.*_A*_F*pow(k_5,3);
	double m_13 = 	2.*_A*_B*k_2 + 4.*_A*_B*k_4*k_5 + 4.*_B*_D*k_5 +
			4.*_A*(_E*k_1 + _E*k_5*k_5 + _F*k_1*k_4 + _F*k_2*k_5) +
			12.*_A*_F*k_4*k_5*k_5 + 8.*_D*_F*k_5*k_5;
	double m_14 = 	2.*_A*_B*k_3 + 2.*_A*_B*k_4*k_4 + 4.*_D*_B*k_4 + 2.*_B*_G + 4.*_A*_E*k_2 +
			8.*_A*_E*k_4*k_5 + 8.*_E*_D*k_5 + 4.*_A*_F*k_2*k_4 + 4.*_A*_F*k_3*k_5 +
			12.*_A*_F*k_4*k_4*k_5 + 16.*_D*_F*k_4*k_5 + 4.*_F*_G*k_5;
	double m_15 = 	4.*_A*_E*(k_3 + k_4*k_4) + 8.*_E*_D*k_4 + 4.*_E*_G + 4.*_A*_F*(k_3*k_4 + pow(k_4,3)) +
			8.*_D*_F*k_4*k_4 + 4.*_F*_G*k_4;

	double  re[5];
		re[0] = m_1 - m_6 - m_12 - m_80;
		re[1] = m_2 - m_7 - m_13 - m_81;
		re[2] = m_3 - m_8 - m_9 - m_14;
		re[3] = m_4 - m_10 - m_15;
		re[4] = m_5 - m_11;



	double output[8];
		my_qu(re,output);

	double delta;
	int ncand(0);


	vector<myvector> *neutrinoContainer = new std::vector<myvector>; //// must use new



	double rec_x1;
	double rec_y1;
	double rec_z1;
	double rec_e1;
	double rec_x2;
	double rec_y2;
	double rec_z2;
	double rec_e2;

	double m_delta_mass;

	for (int j=0; j<8; j+=2){
	   delta = k_1*output[j]*output[j] + k_2*output[j] + k_3;
		if ( output[j+1]==0 && delta>=0) {
		  //cout<<"check one: "<<j<<" "<<fx_1 + fx_2*output[j]<<endl;
		  if ( (fx_1 + fx_2*output[j])!=0 ) {
		       	rec_x1 = (fx_3*pow(output[j],2) + fx_4*output[j] + fx_5)/(fx_1 + fx_2*output[j]);
		  } else {
		        rec_x1 = sqrt(delta)+k_4+k_5*output[j];
		  }

		  rec_y1 = output[j];
		  rec_z1 = G_8/G_7 - G_5*rec_x1/G_7 - G_6*rec_y1/G_7;
		  rec_e1 = sqrt(rec_x1*rec_x1 + rec_y1*rec_y1 + rec_z1*rec_z1);
		  rec_x2 = S - rec_x1;
		  rec_y2 = T - rec_y1;
		  rec_z2 = G_12/G_11 - G_9*rec_x2/G_11 - G_10*rec_y2/G_11;
		  rec_e2 = sqrt(rec_x2*rec_x2 + rec_y2*rec_y2 + rec_z2*rec_z2);

		  //myvector sol(rec_x1, rec_y1, rec_z1, rec_z2 );
		  //neutrinoContainer->push_back( sol );


	         // self-consistence check and control of the solutions
	         TLorentzVector *m_w1 = new TLorentzVector(rec_x1+lep_a->Px(), rec_y1+lep_a->Py(), rec_z1+lep_a->Pz(), rec_e1+lep_a->E());
	         TLorentzVector *m_w2 = new TLorentzVector(rec_x2+lep_b->Px(), rec_y2+lep_b->Py(), rec_z2+lep_b->Pz(), rec_e2+lep_b->E());
	         TLorentzVector *m_t1 = new TLorentzVector(rec_x1+ bl_a->Px(), rec_y1+ bl_a->Py(), rec_z1+ bl_a->Pz(), rec_e1+ bl_a->E());
	         TLorentzVector *m_t2 = new TLorentzVector(rec_x2+ bl_b->Px(), rec_y2+ bl_b->Py(), rec_z2+ bl_b->Pz(), rec_e2+ bl_b->E());

		 m_delta_mass = 1000.0; // allow mass variation range for reco W and tops..
	         bool m_good_eq1 = ( fabs(S -(rec_x1+rec_x2)) 	  <=0.01 )?true:false;
	         bool m_good_eq2 = ( fabs(T -(rec_y1+rec_y2)) 	  <=0.01 )?true:false;
		  bool m_good_eq3 = ( fabs(m_w1->M() - w_mass[0]) <= m_delta_mass )?true:false;
		  bool m_good_eq4 = ( fabs(m_w2->M() - w_mass[1]) <= m_delta_mass )?true:false;
		  bool m_good_eq5 = ( fabs(m_t1->M() - t_mass[0]) <= m_delta_mass )?true:false;
		  bool m_good_eq6 = ( fabs(m_t2->M() - t_mass[1]) <= m_delta_mass )?true:false;
		  //cout<<" S: "<<S<<" "<<rec_x1<<" "<<rec_x2 <<endl;
		  //cout<<" T: "<<T<<" "<<rec_y1<<" "<<rec_y2 <<endl;
	         //cout<< m_w1->M()<<" "<<m_w2->M()<<" "<< m_t1->M() <<" "<< m_t2->M() <<endl;

	         if ( m_good_eq1 && m_good_eq2 && m_good_eq3 && m_good_eq4 && m_good_eq5 && m_good_eq6 ) {
		    neutrinoContainer->push_back( myvector(rec_x1, rec_y1, rec_z1, rec_z2) );
	           ncand++;
	         } else {
		//    cout<<" One solution is found to be out of range: "<< m_w1->M()<<" "<<m_w2->M()<<" "<< m_t1->M() <<" "<< m_t2->M()<<endl;
		    //cout<<m_good_eq1<<m_good_eq2<<m_good_eq3<<m_good_eq4<<m_good_eq5<<m_good_eq6<<endl;
		 }
	    }
	}

	return neutrinoContainer;
}

void ttDilepKinFit(){
	// #############################################################################
	//
	//  Description:Make Kinematic Fit of ttH Dileptonic Events  (ttH->bWbWbb->blnublnubb)
	//
	//  Input:	1) =2 leptons
	//              2) >=N jets:    N>=4 jets for ttbar + H->bbbar
	//		3) missing energy
	//
	//  Output:	Best solution for (2)neutrinos 4-vectors according to method:
	//		i)  ttDKF_SolutionChoice = 1  Lowest nupT1*nupT2 value
	//		ii) ttDKF_SolutionChoice = 2  nupT1,nupT2 p.d.fs
	//
	//  Coded by:   A.Onofre
	//  Date:       20/12/2011 (1st version)
	//  Last Changed by:   S.Amor
	//  Date:       13/12/2012 (2nd version)
	//
	// #############################################################################


 	// =================================================================================================================
 	// =================================================================================================================
	//          I N P U T     F I T    P A R A M E T E R S
 	// =================================================================================================================
 	// =================================================================================================================
	// Define Jets Combination Method:
	//	ttDKF_JetCombChoice = 1  	(use N jets (ttDKF_njets) to test combinations)
	//				 	(ttDKF_njets chosen by user; <= total number of jets)
	//				 	(deal with ttbar and H->bb at the same time)
	int	ttDKF_JetCombChoice  	= 1;
	// ------------------------------------------------------------------------------------------------------------------
	// Define method used to get the best solution of the Neutrino and Anti-neutrino:
	// 	ttDKF_SolutionChoice = 1  	Lowest nupT1*nupT2 value
	//	ttDKF_SolutionChoice = 2  	nupT1,nupT2 p.d.fs used
	// ttDKF_SolutionChoice = 1;
	int	ttDKF_SolutionChoice 	= 2;
	// ------------------------------------------------------------------------------------------------------------------
	// Define method used to chose best Higgs:
	// 	ttDKF_HiggsChoice = 1		Mass Constraint 		(mj1j2 closest to mH_UserValue)
	//	ttDKF_HiggsChoice = 2  		Transverse Momentum Constraint 	(pT_Higgs = - pT_ttbar)
	//	ttDKF_HiggsChoice = 3    	Mass from Angle Constraint 	(mj1 = mj2)
	int	ttDKF_HiggsChoice 	= 3;
 	// =================================================================================================================
 	// =================================================================================================================


 	// =================================================================================================================
	// Define usefull variables
 	// =================================================================================================================
	int  nTSol =  0;			// initialize Total number of solutions counter
	double    t_m[2] = {mt, mt};		// initialize top quarks masses
        double    w_m[2] = {mW, mW};		// initialize W bosons masses
 	double in_mpx[2] = {MissPx, MissPx};    // initialize miss(Px_neutrino1, Px_neutrino2)
 	double in_mpy[2] = {MissPy, MissPy};    // initialize miss(Py_neutrino1, Py_neutrino2)
        double in_mpz[2] = {0., 0.};		// initialize neutrinos Pz to zero
	// auxiliar variables TLorentzVectors and extended TLorentzVectors
	TLorentzVector 		z_bl,       c_bl;
	TLorentzVector 	    	z_bj      , c_bj      , z_lep      , c_lep    ;
	TLorentzVectorWFlags    z_bjWFlags, c_bjWFlags, z_lepWFlags, c_lepWFlags;
	TLorentzVectorWFlags    jet1_HiggsWFlags, jet2_HiggsWFlags;
	// result of kinematic fit
	std::vector<myvector> *result;

 	// =================================================================
	// Initialize Solutions Flag
 	// =================================================================
	int	HasSolution = 0;

 	// =================================================================
	// Reset all solutions for tt Dileptonic Kinematical Fit:
 	// If solutions exist:	-There are 4 of them per combination
	//			 (solutions from quartic equations due to
	//			 momentum-energy conservation + mW + mT)
 	// =================================================================
	// top quark 1
    vector<TLorentzVectorWFlags>     b1_ttDKF;
    vector<TLorentzVectorWFlags>     l1_ttDKF;
    vector<TLorentzVectorWFlags>     n1_ttDKF;
    vector<TLorentzVectorWFlags>     W1_ttDKF;
    vector<TLorentzVectorWFlags>     t1_ttDKF;
	// top quark 2
    vector<TLorentzVectorWFlags>     b2_ttDKF;
    vector<TLorentzVectorWFlags>     l2_ttDKF;
    vector<TLorentzVectorWFlags>     n2_ttDKF;
    vector<TLorentzVectorWFlags>     W2_ttDKF;
    vector<TLorentzVectorWFlags>     t2_ttDKF;
	// ttbar system
	vector<TLorentzVectorWFlags> ttbar_ttDKF;
	// Higgs Boson
    vector<TLorentzVectorWFlags>     b1_Higgs_ttDKF;
    vector<TLorentzVectorWFlags>     b2_Higgs_ttDKF;
    vector<TLorentzVectorWFlags>     Higgs_ttDKF;
    vector<double>     mHiggsJet1_ttDKF;
    vector<double>     mHiggsJet2_ttDKF;

	// Output Probabilities:
    vector<double>     ProbHiggs_ttDKF;
  	vector<double> ProbTTbar_ttDKF;
	vector<double> ProbTotal_ttDKF;

 	// =================================================================
	// Identify Leptons: z_lep=Highest pT lepton, c_lep=Lowest pT lepton
 	// =================================================================
	z_lep       = LeptonVec[0]; // to be use ONLY as TLorentzVector
	z_lepWFlags = LeptonVec[0]; // extended TLorentzVector
	c_lep       = LeptonVec[1]; // to be use ONLY as TLorentzVector
	c_lepWFlags = LeptonVec[1]; // extended TLorentzVector


 	// =================================================================
	// Identify Jets candidates (MyChoiceJetVec,ordered by pT)
	//  i) ttDKF_JetCombChoice = 1: Use N jets (ttDKF_njets) to test
	//				combinations deal with ttbar and
        //				H->bbbar at the same time
 	// =================================================================
	vector<TLorentzVectorWFlags> MyChoiceJetVec;

	// -----------------------------------------------------------------
	//  ttDKF_JetCombChoice = 1  Use N jets, b and non-b
        //			     NOTE: pass MyChoiceJetVec  to the tool
        //			           pass ttDKF_njets = number of jets
	//			     (do nothing but pass the vectors)
	//  by: S.Amor 13.Dez.2012
	// -----------------------------------------------------------------
	if ( ttDKF_JetCombChoice == 1 ){
		for ( unsigned jetID=0; jetID<MyGoodJetVec.size();  ++jetID){
			MyChoiceJetVec.push_back(MyGoodJetVec[jetID]);
		}
		// -----------------------------------------------------------------
		// USER INPUT NUMBER OF JETS PER EVENT FOR PERMUTATIONS :
		// -----------------------------------------------------------------
		ttDKF_njets = ttDKF_njet_UserValue; 	// value range: [4; MyGoodJetVec.size()]

		if ( ttDKF_njets > MyGoodJetVec.size() ) {
			//cout << "WARNING: Number of Jets Higher than the Maximum Number of Jets in the Event." << endl;
			//cout << "         Setting ttDKF_njets = Total Number of Jets" << endl;
			ttDKF_njets = MyGoodJetVec.size(); 	// value range: [2; MyGoodJetVec.size()]
		}
        	if (ttDKF_njets < 4){
	//			cout << "WARNING: NUMBER OF JETS INSUFFICIENT FOR KINEMATIC RECONSTRUCTION" << endl;
			ttDKF_JetCombChoice = 0; // does not compute kinematic fit
		}

	}


	// =================================================================  \\
	//              	Kinematic Fit to tt System	              \\
	// -----------------------------------------------------------------  \\
        // 	Assumptions:	-Reconst. Top quark mass fixed to mt   	      \\
	//			-Reconst.  W  boson mass fixed to mW   	      \\
	//			-Try all Permutations for leptons and bjets   \\
	//			 (do not use Chuenlei criteria)        	      \\
	//			-Vary (E,pt) within Resolution	       	      \\
	//      Output:		-Best solution of top's  according to method: \\
	//			 ttDKF_SolutionChoice = 1 Lowest nupT1*nupT2  \\
	//			 ttDKF_SolutionChoice = 2 nupT1,nupT2 p.d.fs  \\
	// =================================================================  \\
	// initialize Best Solution Methods (ttDKF_SolutionChoice = 1 and = 2)
	// index of best solution (if any)
	int n_ttDKF_Best = -999;

	// H->bbar Probability Factors
        double higgs_sele_ang  	= -10e+15;

	// ttH->lnublnubbbar Probability Factors
	double MaxTotalProb = -10e+15;

	// ttbar variables
	double myttbar_px;
	double myttbar_py;
	double myttbar_pz;
	double myttbar_E;

	// Higgs helpfull variables
	double theta_jet1_HiggsFromTTbar;
	double theta_jet2_HiggsFromTTbar;
	double fac_j1j2H_ttbar;
	double mass_j1H_ttbar;
	double mass_j2H_ttbar;

	// ---------------------------------------
	// by: S.Amor 13.Dez.2012
	//
	// ttDKF_JetCombChoice = 1
	//			     N jets = ttDKF_njets
	//			     2 jets for ttbar
	//			     2 jet for H->bbbar
	// ---------------------------------------

        if ( ttDKF_JetCombChoice == 1 ){
	  for ( unsigned j1=0; j1 < ttDKF_njets ; j1++){
           for ( unsigned j2=0; j2 < ttDKF_njets ; j2++){
	    if (j1!=j2){ 				 // no repetition of jets
	     // ---------------------------------------
	     // Initialize top quark and W boson masses
             // ---------------------------------------
             t_m[0]      = mt;
             t_m[1]      = mt;
             w_m[0]	 = mW;
             w_m[1]	 = mW;

	     // ---------------------------------------
	     // Initialize Jet Permutations
	     //     Note: z_bj is associated with z_lep
	     //           c_bj is associated with c_lep
             // ---------------------------------------
	     z_bj       = MyChoiceJetVec[j1]; // to be use ONLY as TLorentz Vector
   	     z_bjWFlags = MyChoiceJetVec[j1]; // extended TLorentzVector
	     c_bj       = MyChoiceJetVec[j2]; // to be use ONLY as TLorentz Vector
	     c_bjWFlags = MyChoiceJetVec[j2]; // extended TLorentzVector

	     for ( unsigned j3=0; j3 < ttDKF_njets-1 ; j3++){
	      if (( j3!=j1) && ( j3!=j2)){  		// no repetition of jets
	       for ( unsigned j4=j3+1; j4 < ttDKF_njets ; j4++){
	        if (( j4!=j1) && ( j4!=j2)){  		// no repetition of jets

		 jet1_HiggsWFlags = MyChoiceJetVec[j3]; // Jet from Higgs Decay (H->bbbar)
		 jet2_HiggsWFlags = MyChoiceJetVec[j4]; // Jet from Higgs Decay (H->bbbar)

		 // ###################################################################
		 //   C H A N G E   O B J E C T S   W I T H I N   R E S O L U T I O N #
		 // ###################################################################
		 // WARNING:  myNumResTest  = 1 => no resolution study apllied
		 // 				     Normal Running Mode
		 //
		 //	      myNumResTest >= 1 => samples the resolution distributions
		 //				of objects and calls  reconstruction
		 //				routine for each Jet combination;
		 //				Here, Resolution values MUST BE !=0
		 //				(ONLY EXAMPLES ARE SHOWN; USER CHOICES!)
		 // ###################################################################
		 // Define number of experiments for resolution
		 unsigned myNumResTest = dilep_iterations;

		 // Resolution values
		 double Sx_e=0.02;  double Sy_e=0.02;  double Sz_e=0.02;  double St_e=0.02; 	double Se_e=0.02;  // electrons
		 double Sx_m=0.02;  double Sy_m=0.02;  double Sz_m=0.02;  double St_m=0.02; 	double Se_m=0.02;  // muons
		 double Sx_j=0.02;  double Sy_j=0.02;  double Sz_j=0.02;  double St_j=0.02; 	double Se_j=0.02;  // jets


		 // loop over several resolution experiments
		 for ( unsigned iRes=0; iRes< myNumResTest ; iRes++){
         // Initialize random number seed

			// new four-vectors
			double n_Px; double n_Py; double n_Pz;	double n_Pt; double n_E;
			double delPx; double delPy;

			// _______________________________
			// _______z_lep___________________
			// _______________________________
			if (  abs(  z_lepWFlags.isb  )  ==  11  ){ //___electrons____
				n_Px = z_lepWFlags.Px() * ( 1. + rnd.Gaus( 0., Sx_e ) );
				n_Py = z_lepWFlags.Py() * ( 1. + rnd.Gaus( 0., Sy_e ) );
				n_Pz = z_lepWFlags.Pz() * ( 1. + rnd.Gaus( 0., Sz_e ) );
				n_Pt = z_lepWFlags.Pt() * ( 1. + rnd.Gaus( 0., St_e ) );
				n_E  = z_lepWFlags.E()  * ( 1. + rnd.Gaus( 0., Se_e ) );
			} else if (  abs(z_lepWFlags.isb) == 13 ){ //_____muons______
				n_Px = z_lepWFlags.Px() * ( 1. + rnd.Gaus( 0., Sx_m ) );
				n_Py = z_lepWFlags.Py() * ( 1. + rnd.Gaus( 0., Sy_m ) );
				n_Pz = z_lepWFlags.Pz() * ( 1. + rnd.Gaus( 0., Sz_m ) );
				n_Pt = z_lepWFlags.Pt() * ( 1. + rnd.Gaus( 0., St_m ) );
				n_E  = z_lepWFlags.E()  * ( 1. + rnd.Gaus( 0., Se_m ) );
			}
			// Recalculate z_lep
			n_E = sqrt ( n_Px*n_Px + n_Py*n_Py + n_Pz*n_Pz + z_lepWFlags.M()*z_lepWFlags.M() );
			z_lep.SetPx( n_Px );	// Change Px
			z_lep.SetPy( n_Py ); 	// Change Py
			z_lep.SetPz( n_Pz ); 	// Change Pz
			z_lep.SetE(  n_E  ); 	// Change E
			// Propagate to MissPx and MissPy
			delPx = z_lepWFlags.Px() - n_Px;
			delPy = z_lepWFlags.Py() - n_Py;
			in_mpx[0] = MissPx + delPx; in_mpx[1] = MissPx + delPx; // initialize miss(Px,Py) neutrino 1
			in_mpy[0] = MissPy + delPy; in_mpy[1] = MissPy + delPy; // initialize miss(Px,Py) neutrino 2
			in_mpz[0] = 0.            ; in_mpz[1] = 0.;		// initialize neutrinos Pz to zero


			// _______________________________
			// _______c_lep___________________
			// _______________________________
			if (  abs(  c_lepWFlags.isb  )  ==  11  ){ //___electrons____
				n_Px = c_lepWFlags.Px() * ( 1. + rnd.Gaus( 0., Sx_e ) );
				n_Py = c_lepWFlags.Py() * ( 1. + rnd.Gaus( 0., Sy_e ) );
				n_Pz = c_lepWFlags.Pz() * ( 1. + rnd.Gaus( 0., Sz_e ) );
				n_Pt = c_lepWFlags.Pt() * ( 1. + rnd.Gaus( 0., St_e ) );
				n_E  = c_lepWFlags.E()  * ( 1. + rnd.Gaus( 0., Se_e ) );
			} else if (  abs(c_lepWFlags.isb) == 13 ){ //_____muons______
				n_Px = c_lepWFlags.Px() * ( 1. + rnd.Gaus( 0., Sx_m ) );
				n_Py = c_lepWFlags.Py() * ( 1. + rnd.Gaus( 0., Sy_m ) );
				n_Pz = c_lepWFlags.Pz() * ( 1. + rnd.Gaus( 0., Sz_m ) );
				n_Pt = c_lepWFlags.Pt() * ( 1. + rnd.Gaus( 0., St_m ) );
				n_E  = c_lepWFlags.E()  * ( 1. + rnd.Gaus( 0., Se_m ) );
			}
			// Recalculate c_lep
			n_E = sqrt ( n_Px*n_Px + n_Py*n_Py + n_Pz*n_Pz + c_lepWFlags.M()*c_lepWFlags.M() );
			c_lep.SetPx( n_Px );	// Change Px
			c_lep.SetPy( n_Py ); 	// Change Py
			c_lep.SetPz( n_Pz ); 	// Change Pz
			c_lep.SetE(  n_E  ); 	// Change E
			// Propagate to MissPx and MissPy
			delPx = c_lepWFlags.Px() - n_Px;
			delPy = c_lepWFlags.Py() - n_Py;
			in_mpx[0] += delPx; in_mpx[1] += delPx; // correct miss(Px,Py) neutrino 1
			in_mpy[0] += delPy; in_mpy[1] += delPy; // correct miss(Px,Py) neutrino 2
			in_mpz[0] += 0.   ; in_mpz[1] += 0.;	// initialize neutrinos Pz to zero

			// _______________________________
			// _______z_bj____________________
			// _______________________________
			n_Px = z_bjWFlags.Px() * ( 1. + rnd.Gaus( 0., Sx_j ) );
			n_Py = z_bjWFlags.Py() * ( 1. + rnd.Gaus( 0., Sy_j ) );
			n_Pz = z_bjWFlags.Pz() * ( 1. + rnd.Gaus( 0., Sz_j ) );
			n_Pt = z_bjWFlags.Pt() * ( 1. + rnd.Gaus( 0., St_j ) );
			n_E  = z_bjWFlags.E()  * ( 1. + rnd.Gaus( 0., Se_j ) );
			// Recalculate z_bj
			n_E = sqrt ( n_Px*n_Px + n_Py*n_Py + n_Pz*n_Pz + z_bjWFlags.M()*z_bjWFlags.M() );
			z_bj.SetPx( n_Px );	// Change Px
			z_bj.SetPy( n_Py ); 	// Change Py
			z_bj.SetPz( n_Pz ); 	// Change Pz
			z_bj.SetE(  n_E  ); 	// Change E
			// Propagate to MissPx and MissPy
			delPx = z_bjWFlags.Px() - n_Px;
			delPy = z_bjWFlags.Py() - n_Py;
			in_mpx[0] += delPx; in_mpx[1] += delPx; // correct miss(Px,Py) neutrino 1
			in_mpy[0] += delPy; in_mpy[1] += delPy; // correct miss(Px,Py) neutrino 2
			in_mpz[0] += 0.   ; in_mpz[1] += 0.;	// initialize neutrinos Pz to zero


			// _______________________________
			// _______c_bj____________________
			// _______________________________
			n_Px = c_bjWFlags.Px() * ( 1. + rnd.Gaus( 0., Sx_j ) );
			n_Py = c_bjWFlags.Py() * ( 1. + rnd.Gaus( 0., Sy_j ) );
			n_Pz = c_bjWFlags.Pz() * ( 1. + rnd.Gaus( 0., Sz_j ) );
			n_Pt = c_bjWFlags.Pt() * ( 1. + rnd.Gaus( 0., St_j ) );
			n_E  = c_bjWFlags.E()  * ( 1. + rnd.Gaus( 0., Se_j ) );
			// Recalculate c_bj
			n_E = sqrt ( n_Px*n_Px + n_Py*n_Py + n_Pz*n_Pz + c_bjWFlags.M()*c_bjWFlags.M() );
			c_bj.SetPx( n_Px );	// Change Px
			c_bj.SetPy( n_Py ); 	// Change Py
			c_bj.SetPz( n_Pz ); 	// Change Pz
			c_bj.SetE(  n_E  ); 	// Change E
			// Propagate to MissPx and MissPy
			delPx = c_bjWFlags.Px() - n_Px;
			delPy = c_bjWFlags.Py() - n_Py;
			in_mpx[0] += delPx; in_mpx[1] += delPx; // correct miss(Px,Py) neutrino 1
			in_mpy[0] += delPy; in_mpy[1] += delPy; // correct miss(Px,Py) neutrino 2
			in_mpz[0] += 0.   ; in_mpz[1] += 0.;	// initialize neutrinos Pz to zero


			// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
			// %      Code to Evaluate Solutions     %
			// %    All Needed Objects Initialised   %
			// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
			// ---------------------------------------
			// Define TLorentzVectors for (b,l) system
			// ---------------------------------------
			z_bl = z_bj + z_lep;
			c_bl = c_bj + c_lep;

			// ---------------------------------------
			// Find tt dileptonic solutions
			// ---------------------------------------
			result = calc_dilep(t_m, w_m, in_mpx, in_mpy, in_mpz, &z_lep, &c_lep, &z_bl, &c_bl);
	                if ( result->size() > 0 ) HasSolution++;  // increment solution counter

			// ---------------------------------------
			// Get info from all possible solutions
			// ---------------------------------------
	                std::vector<myvector>::iterator pp;
	                for ( pp = result->begin(); pp < result->end(); pp++) {

	                	double   px,  py,  pz,  E,
					apx, apy, apz, aE;
				int     iPDGnu1, iPDGW1, iPDGt1;
				int     iPDGnu2, iPDGW2, iPDGt2;

				// -------------------------------
				//  1st top quark Reconstruction
				// -------------------------------
				// b-quark 1
	        		b1_ttDKF.push_back(z_bjWFlags);
				// lepton 1
	        		l1_ttDKF.push_back(z_lepWFlags);
				if ( z_lepWFlags.isb ==  11 ) { iPDGnu1 = -12; iPDGW1 = -24; iPDGt1 = -6; }
				if ( z_lepWFlags.isb == -11 ) { iPDGnu1 = +12; iPDGW1 = +24; iPDGt1 = +6; }
				if ( z_lepWFlags.isb ==  13 ) { iPDGnu1 = -14; iPDGW1 = -24; iPDGt1 = -6; }
				if ( z_lepWFlags.isb == -13 ) { iPDGnu1 = +14; iPDGW1 = +24; iPDGt1 = +6; }
				// neutrino 1
	                	px = pp->Px();
	                	py = pp->Py();
	                	pz = pp->Pz();
	                	E  = sqrt(px*px + py*py + pz*pz);
				TLorentzVector n1;
			        n1.SetPxPyPzE(  px,   py,   pz,  E);
				TLorentzVectorWFlags nu1(n1,0,iPDGnu1,999.,-1,-1);
	        		n1_ttDKF.push_back(nu1);
				// W boson 1
				TLorentzVector w1;
			        w1.SetPxPyPzE(	px + z_lepWFlags.Px(),
						py + z_lepWFlags.Py(),
						pz + z_lepWFlags.Pz(),
						E  + z_lepWFlags.E()   );
				TLorentzVectorWFlags ww1(w1,0,iPDGW1,999.,-1,-1);
	        		W1_ttDKF.push_back(ww1);
				// top quark 1
				TLorentzVector t1;
			        t1.SetPxPyPzE(	px + z_lepWFlags.Px() + z_bjWFlags.Px(),
						py + z_lepWFlags.Py() + z_bjWFlags.Py(),
						pz + z_lepWFlags.Pz() + z_bjWFlags.Pz(),
						E  + z_lepWFlags.E()  + z_bjWFlags.E() );
				TLorentzVectorWFlags tt1(t1,0,iPDGt1,999.,-1,-1);
	        		t1_ttDKF.push_back(tt1);

				// -------------------------------
				//  2nd top quark reconstruction
				// -------------------------------
				// b-quark 2
	        		b2_ttDKF.push_back(c_bjWFlags);
				// lepton 2
	        		l2_ttDKF.push_back(c_lepWFlags);
				if ( c_lepWFlags.isb ==  11 ) { iPDGnu2 = -12; iPDGW2 = -24; iPDGt2 = -6; }
				if ( c_lepWFlags.isb == -11 ) { iPDGnu2 = +12; iPDGW2 = +24; iPDGt2 = +6; }
				if ( c_lepWFlags.isb ==  13 ) { iPDGnu2 = -14; iPDGW2 = -24; iPDGt2 = -6; }
				if ( c_lepWFlags.isb == -13 ) { iPDGnu2 = +14; iPDGW2 = +24; iPDGt2 = +6; }
				// neutrino 2
	                	apx = MissPx-px;
	                	apy = MissPy-py;
	                	apz = pp->aPz();
	                	aE  = sqrt(apx*apx + apy*apy + apz*apz);
				TLorentzVector n2;
			        n2.SetPxPyPzE( apx,  apy,  apz, aE);
				TLorentzVectorWFlags nu2(n2,0,iPDGnu2,999.,-1,-1);
	        		n2_ttDKF.push_back(nu2);
				// W boson 2
				TLorentzVector w2;
			        w2.SetPxPyPzE(	apx + c_lepWFlags.Px(),
						apy + c_lepWFlags.Py(),
						apz + c_lepWFlags.Pz(),
						aE  + c_lepWFlags.E()   );
				TLorentzVectorWFlags ww2(w2,0,iPDGW2,999.,-1,-1);
	        		W2_ttDKF.push_back(ww2);
				// top quark 2
				TLorentzVector t2;
			        t2.SetPxPyPzE(	apx + c_lepWFlags.Px() + c_bjWFlags.Px(),
						apy + c_lepWFlags.Py() + c_bjWFlags.Py(),
						apz + c_lepWFlags.Pz() + c_bjWFlags.Pz(),
						aE  + c_lepWFlags.E()  + c_bjWFlags.E() );
				TLorentzVectorWFlags tt2(t2,0,iPDGt2,999.,-1,-1);
	        		t2_ttDKF.push_back(tt2);

				// -------------------------------
				//  (t,tbar) system reconstruction
				// -------------------------------
				TLorentzVector ttbar;
			        myttbar_px = px + z_lepWFlags.Px() + z_bjWFlags.Px() + apx + c_lepWFlags.Px() + c_bjWFlags.Px();
				myttbar_py = py + z_lepWFlags.Py() + z_bjWFlags.Py() + apy + c_lepWFlags.Py() + c_bjWFlags.Py();
				myttbar_pz = pz + z_lepWFlags.Pz() + z_bjWFlags.Pz() + apz + c_lepWFlags.Pz() + c_bjWFlags.Pz();
				myttbar_E  = E  + z_lepWFlags.E()  + z_bjWFlags.E()  + aE  + c_lepWFlags.E()  + c_bjWFlags.E();
			        ttbar.SetPxPyPzE( myttbar_px, myttbar_py, myttbar_pz, myttbar_E);
				TLorentzVectorWFlags ttbar2(ttbar,0, 999,999.,-1,-1);
	        		ttbar_ttDKF.push_back(ttbar2);

				// -------------------------------
				//   Higgs system reconstruction
				// -------------------------------
				// jet 1 from Higgs
	        		b1_Higgs_ttDKF.push_back( jet1_HiggsWFlags );
				// jet 2 from Higgs
	        		b2_Higgs_ttDKF.push_back( jet2_HiggsWFlags );
				// Higgs itself
				TLorentzVector myHiggs;
			        myHiggs.SetPxPyPzE(	jet1_HiggsWFlags.Px() + jet2_HiggsWFlags.Px(),
							jet1_HiggsWFlags.Py() + jet2_HiggsWFlags.Py(),
							jet1_HiggsWFlags.Pz() + jet2_HiggsWFlags.Pz(),
							jet1_HiggsWFlags.E()  + jet2_HiggsWFlags.E() );
				TLorentzVectorWFlags Higgs(myHiggs,0, 25 ,999.,-1,-1);
				Higgs_ttDKF.push_back( Higgs );


				// -----------------------------------------------------------------------------
				// Compute best solution for H->bb
				// -----------------------------------------------------------------------------
				// Three methods: 1) Use Mass Constraint          (mj1j2 closest to mH_UserValue)
				// 		  2) Use Transverse Momentum Constraint ( pT_Higgs = - pT_ttbar )
				// 		  3) Use Mass from Angle Constraint
				// -----------------------------------------------------------------------------

				// -----------------------------------------------------------------------------
				// Method 1:
				// -----------------------------------------------------------------------------
				// Mass Constraint: mj1j2 = mH_UserValue
				// -----------------------------------------------------------------------------
				double myHiggs_MassDiff = fabs( myHiggs.M() - mH_UserValue );

				// -----------------------------------------------------------------------------
				// Method 2:
				// -----------------------------------------------------------------------------
				// Transverse Momentum Constraint: pT_Higgs = - pT_ttbar
				// -----------------------------------------------------------------------------
				double myHiggs_PxDiff = fabs( myHiggs.Px() - ttbar.Px() );
				double myHiggs_PyDiff = fabs( myHiggs.Py() - ttbar.Py() );
				double myHiggs_pTDiff = sqrt( myHiggs_PxDiff*myHiggs_PxDiff + myHiggs_PyDiff*myHiggs_PyDiff );

				// -----------------------------------------------------------------------------
				// Method 3 :
				// -----------------------------------------------------------------------------
				// Mass from Angle Constraint: 	mj1 = mj2 (from the hard process)
				// 				Compute Hard Process Kinematics for H->bb
				// -----------------------------------------------------------------------------
				// Mass Initialization
				mass_j1H_ttbar = -999.;
				mass_j2H_ttbar = -999.;

				// Higgs Momenta from ttbar system
				//TVector3 HiggsFromTTbar( - ttbar.Px(), - ttbar.Py(), (Hz - ttbar.Pz()) );
				// Try to compute ttbar. without NU !! CHECK!!
				TVector3 HiggsFromTTbar( - ttbar.Px(), - ttbar.Py(), (Hz + n1.Pz() + n2.Pz() - ttbar.Pz() ) );
				// Test jets for Higgs
	 	   		TVector3  jet1_vec( jet1_HiggsWFlags.Px(), jet1_HiggsWFlags.Py(), jet1_HiggsWFlags.Pz() );
	 	   		TVector3  jet2_vec( jet2_HiggsWFlags.Px(), jet2_HiggsWFlags.Py(), jet2_HiggsWFlags.Pz() );
				// check jet angle with respect to ttbar direction
                   		theta_jet1_HiggsFromTTbar = jet1_vec.Angle( HiggsFromTTbar );
                   		theta_jet2_HiggsFromTTbar = jet2_vec.Angle( HiggsFromTTbar );

				if ( sin(theta_jet1_HiggsFromTTbar)*sin(theta_jet2_HiggsFromTTbar) ) {
					fac_j1j2H_ttbar	= 1. + ( 1. - cos(theta_jet1_HiggsFromTTbar)*cos(theta_jet2_HiggsFromTTbar) )
							          / ( sin(theta_jet1_HiggsFromTTbar)*sin(theta_jet2_HiggsFromTTbar) ) ;
					mass_j1H_ttbar	= sqrt( 2. * fac_j1j2H_ttbar ) * sin( theta_jet1_HiggsFromTTbar ) * jet1_HiggsWFlags.P()  ;
					mass_j2H_ttbar	= sqrt( 2. * fac_j1j2H_ttbar ) * sin( theta_jet2_HiggsFromTTbar ) * jet2_HiggsWFlags.P()  ;

        				higgs_sele_ang  = fabs( ( mass_j1H_ttbar + mass_j2H_ttbar ) / ( mass_j1H_ttbar - mass_j2H_ttbar ) ) ;
				}

				//Save Higgs Mass from Angular Kinematic Equations of bjet 1 and bjet 2
				mHiggsJet1_ttDKF.push_back(mass_j1H_ttbar);
				mHiggsJet2_ttDKF.push_back(mass_j2H_ttbar);


				// -----------------------------------------------------------------------------
				// Higgs Probability :
				// -----------------------------------------------------------------------------
				// Method 1:  Use Mass Constraint  (mj1j2 closest to mH_UserValue)
				if ( ttDKF_HiggsChoice == 1 ) ProbHiggs_ttDKF.push_back(   1./myHiggs_MassDiff 	);
				// -----------------------------------------------------------------------------
				// Method 2:  Use Transverse Momentum Constraint (pT_Higgs = - pT_ttbar)
				if ( ttDKF_HiggsChoice == 2 ) ProbHiggs_ttDKF.push_back(   1./myHiggs_pTDiff 	);
				// -----------------------------------------------------------------------------
				// Method 3: Use Mass from Angle Constraint
				if ( ttDKF_HiggsChoice == 3 ) ProbHiggs_ttDKF.push_back(   higgs_sele_ang 	);


				// -------------------------------
				// Test Best Solution Now:
				// (i)  ttDKF_SolutionChoice = 1
				//	 (Lowest nupT1*nupT2)
				// (ii) ttDKF_SolutionChoice = 2
				//	 (nupT1,nupT2 p.d.fs)
				// -------------------------------
				// (i) Lowest nupT1*nupT2
				if ( ttDKF_SolutionChoice == 1 ) {
		                   	double nu_pt_cand = 	sqrt( n1_ttDKF[nTSol].Px() * n1_ttDKF[nTSol].Px() +
								      n1_ttDKF[nTSol].Py() * n1_ttDKF[nTSol].Py() ) *
								sqrt( n2_ttDKF[nTSol].Px() * n2_ttDKF[nTSol].Px() +
								      n2_ttDKF[nTSol].Py() * n2_ttDKF[nTSol].Py() );

					// ------------------------------------------
					// ttbar System Probability :
					// ------------------------------------------
					ProbTTbar_ttDKF.push_back( 1./ nu_pt_cand);

					// before checking ttbar take Higgs into account also
					// nu_pt_cand *= 1./ProbHiggs_ttDKF(nTSol);
					// decide here !!!!!
	                   		//if ( nu_pt_cand < nu_sele_pt ) { nu_sele_pt = nu_pt_cand; n_ttDKF_Best = nTSol;}
				}

				// (ii) nupT1,nupT2 from p.d.fs
				if ( ttDKF_SolutionChoice == 2 ) {

					// Define used pdf variables (make sure the range of variables meets histos)
					std::vector<double> Xpdf;
					Xpdf.push_back(n1_ttDKF[nTSol].Pt()/GeV); // 1st pdf: pT neutrino 1
					Xpdf.push_back(n2_ttDKF[nTSol].Pt()/GeV); // 2nd pdf: pT neutrino 2

					// Loop over all pdf available and evaluate the pdf product (if it is possible)
					double myProdXpdf    = 1.;
					for ( unsigned i_pdf = 0; i_pdf < Xpdf.size() ; ++i_pdf){
						// get bin for Xpdf[i_pdf]
						int xBin = int( ( Xpdf[i_pdf] - LowerEdge[i_pdf] ) * Scale[i_pdf] ) + 1;
						if (  ( xBin >= 1 )  && ( xBin <= NBins[i_pdf] ) ){
							myProdXpdf   *= pdfKinFitVec[i_pdf][xBin];
						} else {
							myProdXpdf   *= 0.;
						}
					}

					// ------------------------------------------
					// ttbar System Probability :
					// ------------------------------------------
					ProbTTbar_ttDKF.push_back( myProdXpdf );


					// before checking ttbar take Higgs into account also
					// myProdXpdf *= ProbHiggs_ttDKF(nTSol);
					// decide here !!!!!
                   			//if ( ( myProdXpdf > nu_sele_pdf ) && ( myProdXpdf != 0. ) ) { nu_sele_pdf = myProdXpdf ; n_ttDKF_Best = nTSol;}

				}
				// -------------------------------
				// Last Action Before Exit:
				//   Increment Solutions Counter
				//  (its also the index vectors)
				// -------------------------------
                                nTSol++;

			}
			// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
			// %      Code to Evaluate Solutions     %
			// %      Solutions Found Are Stored     %
			// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

		 }
		 // ###################################################################
		 //   C H A N G E   O B J E C T S   W I T H I N   R E S O L U T I O N #
		 // ###################################################################

	 	}
	       }  // for over j4
	      }
	     }  // for over j3
	    }  // j1!=j2
	   }  // for over j2
	  }  // for over j1
	}   // JET COMBINATION CHOICE


	// ==================================================================
	// Solutions Cycle
	// ==================================================================


	for ( int iSol = 0; iSol < nTSol; iSol++) {

		ProbTotal_ttDKF.push_back( ProbHiggs_ttDKF[iSol]*ProbTTbar_ttDKF[iSol] );
                if ( ( ProbTotal_ttDKF[iSol] > MaxTotalProb ) && ( ProbTotal_ttDKF[iSol] != 0. ) ) { MaxTotalProb = ProbTotal_ttDKF[iSol] ; n_ttDKF_Best = iSol;}

		//cout << "ProbHiggs_ttDKF[iSol] " << ProbHiggs_ttDKF[iSol] << "; ProbTTbar_ttDKF[iSol] " << ProbTTbar_ttDKF[iSol] << "; ProbTotal_ttDKF[iSol] " << ProbTotal_ttDKF[iSol] << endl;

                //if ( ( ProbHiggs_ttDKF[iSol] > MaxHiggsProb ) && ( ProbHiggs_ttDKF[iSol] != 0. ) ) { MaxHiggsProb = ProbHiggs_ttDKF[iSol] ; n_ttDKF_Best = iSol;}

	} // Solutions Cicle

	// -------------------------------------------------------------------
	// Redefine HasSolution if no other reconstruction criteria met
	// -------------------------------------------------------------------
	HasSolution = (n_ttDKF_Best >= 0) ? HasSolution : 0;

	// -------------------------------------------------------------------
	// Make sure backward compatibility is preserved + Few Calculations
	// -------------------------------------------------------------------
	if(  HasSolution > 0  )
	{

		// -------------------------------------------------------------------
		Neutrino     = n1_ttDKF[n_ttDKF_Best];  	// Neutrino 1
		Antineutrino = n2_ttDKF[n_ttDKF_Best];  	// Neutrino 2
		// ###  leptons  ###
		RecLepP 	= l1_ttDKF[n_ttDKF_Best];
		RecLepN 	= l2_ttDKF[n_ttDKF_Best];
		// ###  b-quarks ###
		RecB    	= b1_ttDKF[n_ttDKF_Best];
		RecBbar 	= b2_ttDKF[n_ttDKF_Best];
		// ### Neutrinos ###
		RecNeu    	= n1_ttDKF[n_ttDKF_Best];
		RecNeubar 	= n2_ttDKF[n_ttDKF_Best];
		// ###  W bosons ###
		RecWp    	= W1_ttDKF[n_ttDKF_Best];
		RecWn    	= W2_ttDKF[n_ttDKF_Best];
		// ###  t-quarks ###
		RecT    	= t1_ttDKF[n_ttDKF_Best];
		RecTbar 	= t2_ttDKF[n_ttDKF_Best];
		// ###  ttbar system ###
		RecTTbar    	= ttbar_ttDKF[n_ttDKF_Best];
		// ###  Higgs system ###
		RecHiggs    	  = Higgs_ttDKF[n_ttDKF_Best];
		RecHiggsB1	  = b1_Higgs_ttDKF[n_ttDKF_Best];
		RecHiggsB2	  = b2_Higgs_ttDKF[n_ttDKF_Best];
		RecMassHiggsJet1  = mHiggsJet1_ttDKF[n_ttDKF_Best]; //samor 16.Dec.2012
		RecMassHiggsJet2  = mHiggsJet2_ttDKF[n_ttDKF_Best];

		RecProbTotal_ttH  = ProbTotal_ttDKF[n_ttDKF_Best];


                //...t/tbar...
		TVector3       t_boost, tb_boost, tt_boost;

		//...get top boosts................
		t_boost  =  -(RecT).BoostVector();
		tb_boost =  -(RecTbar).BoostVector();
		tt_boost =  -(RecT + RecTbar).BoostVector();

                //.................................
		//...make boost  to t..............
                //.................................
                //___b____
		RecB_BoostedtoT    = RecB;
		RecB_BoostedtoT.Boost(t_boost);
                //___W+___
		RecWp_BoostedtoT   = RecWp;
		RecWp_BoostedtoT.Boost(t_boost);
                //___l+___
		RecLepP_BoostedtoT = RecLepP;
		RecLepP_BoostedtoT.Boost(t_boost);
                //___neu__
		RecNeu_BoostedtoT  = RecNeu;
		RecNeu_BoostedtoT.Boost(t_boost);


                //.................................
		//...make boost  to tbar...........
                //.................................
                //___bbar___
		RecBbar_BoostedtoTbar   = RecBbar;
		RecBbar_BoostedtoTbar.Boost(tb_boost);
                //____W-____
		RecWn_BoostedtoTbar     = RecWn;
		RecWn_BoostedtoTbar.Boost(tb_boost);
                //____l-____
		RecLepN_BoostedtoTbar   = RecLepN;
		RecLepN_BoostedtoTbar.Boost(tb_boost);
                //__neubar__
		RecNeubar_BoostedtoTbar = RecNeubar;
		RecNeubar_BoostedtoTbar.Boost(tb_boost);


                //.................................
		//...make boost to ttbar...........
                //.................................
                //___t____
		RecT_Boostedtottbar   =  RecT;
		RecT_Boostedtottbar.Boost(tt_boost);
                //__tbar__
		RecTbar_Boostedtottbar  =  RecTbar;
		RecTbar_Boostedtottbar.Boost(tt_boost);


                //.................................
		//....Spin Correlations............
                //.................................
	        //_____l+__in_t__________
		RecCos_LepP_T_BoostedtoT = cos(  RecLepP_BoostedtoT   .Angle (    RecT_Boostedtottbar.Vect()));
	        //_____nu__in_t__________
		RecCos_Neu_T_BoostedtoT  = cos(   RecNeu_BoostedtoT   .Angle (    RecT_Boostedtottbar.Vect()));
	        //_____b__in_t___________
		RecCos_B_T_BoostedtoT    = cos(     RecB_BoostedtoT   .Angle (    RecT_Boostedtottbar.Vect()));


	        //_____l-__in_tbar_______
		RecCos_LepN_Tbar_BoostedtoTbar    = cos(  RecLepN_BoostedtoTbar   .Angle ( RecTbar_Boostedtottbar.Vect()));
	        //_____nu__in_t__________
		RecCos_Neubar_Tbar_BoostedtoTbar  = cos(RecNeubar_BoostedtoTbar   .Angle ( RecTbar_Boostedtottbar.Vect()));
	        //_____b__in_t___________
		RecCos_Bbar_Tbar_BoostedtoTbar    = cos(  RecBbar_BoostedtoTbar   .Angle ( RecTbar_Boostedtottbar.Vect()));


                // ################################
                // ##     W+/- c.m. systems	 ##
                // ################################
                //...W+/-...
                TVector3       Wp_boost, Wn_boost;

                //...get W+/- boosts................
                Wp_boost  =  -(RecWp).BoostVector();
                Wn_boost  =  -(RecWn).BoostVector();

                //.................................
                //...make boost  to W+.............
                //.................................
                //___l+___
                RecLepP_BoostedtoWp = RecLepP;
                RecLepP_BoostedtoWp.Boost(Wp_boost);
                //___b____
                RecB_BoostedtoWp    = RecB;
                RecB_BoostedtoWp.Boost(Wp_boost);
                //__neu___
                RecNeu_BoostedtoWp = RecNeu;
                RecNeu_BoostedtoWp.Boost(Wp_boost);

                //.................................
                //...make boost  to W-.............
                //.................................
                //____l-____
                RecLepN_BoostedtoWn   = RecLepN;
                RecLepN_BoostedtoWn.Boost(Wn_boost);
                //__bbar____
                RecBbar_BoostedtoWn   = RecBbar;
                RecBbar_BoostedtoWn.Boost(Wn_boost);
                //__neu___
                RecNeubar_BoostedtoWn = RecNeubar;
                RecNeubar_BoostedtoWn.Boost(Wn_boost);

                //.................................
                //....W Polarizations..............
                //.................................
                //_____(l+,b)__in_W+__________
                RecCos_LepP_B_BoostedtoWp =  -cos(  RecLepP_BoostedtoWp   .Angle (  RecB_BoostedtoWp.Vect()));
                //_____(l-,bbar)__in_W-_______
                RecCos_LepN_Bbar_BoostedtoWn =  -cos(  RecLepN_BoostedtoWn   .Angle (  RecBbar_BoostedtoWn.Vect()));

	}

}

void initEvent (void) {
	FillAllVectors();
	PtCutJet  = -999.*GeV;
	TruthEleNumber = truE;
	TruthMuonNumber = truM;
	Ht_Mini = ht;
	Weight = mcWeight * Luminosity / mc_lum;
	BTagCut = 0.8119;

	if ( trigE )
		ElectronTrigger = 1;

	if ( trigM )
		MuonTrigger = 1;


	/*
	TMonteCarlo mc_tot(0, -1, 1, 1, "Total MC");
	MonteCarlo.push_back(mc_tot);

	// coisas montecarlo

	THisto histos;
	MonteCarlo[0].histo.push_back(histos);
	BookHistograms(MonteCarlo[0].histo[i_syst].histo);
	// prepare counters of events (all MC/data samples)
	for (Int_t mc=0; mc<MonteCarlo.size(); ++mc) {
		MonteCarlo[mc].p_nSelEvt.push_back(std::vector<Double_t>());
		MonteCarlo[mc].p_nSelWeightedEvt.push_back(std::vector<Double_t>());
		MonteCarlo[mc].p_nSelWeightedEvtErr.push_back(std::vector<Double_t>());
		for (int i=0; i <= MaxCuts; ++i) {
			MonteCarlo[mc].p_nSelEvt[i_syst].push_back(0.);
			MonteCarlo[mc].p_nSelWeightedEvt[i_syst].push_back(0.);
			MonteCarlo[mc].p_nSelWeightedEvtErr[i_syst].push_back(0.);
		}
	}*/
}


bool cut0 (void) {
	initEvent();
	Calculations();

	// _cut17 = false;

	// nao faz o calc2 porque lepsample == 23 e leptonsep == 0
	if(Weight == 0)
		return false;
	else
		return true;
}

bool cut1 (void) {
	// isdata == 0 por isso nao se considera o cut 0

	if (ElectronTrigger != 1 && MuonTrigger != 1)
		return false;
	else
		return true;
}

bool cut2 (void) {
	if(Cosmic)
		return false;
	else
		return true;
}

bool cut3 (void) {
	if (Vtx.size() == 0)
		return false;
	else
		return true;
}

bool cut4 (void) {
	if (LeptonVec.size() < 2)
		return false;

	int nele = 0;
	int nmuo = 0;
	for(unsigned i=0; i < LeptonVec.size(); i++){
		if (abs(LeptonVec[i].isb) == 11) nele = nele + 1;
		if (abs(LeptonVec[i].isb) == 13) nmuo = nmuo + 1;
	}
	if (nele < 1) return false;
	if (nmuo < 1) return false;

	return true;
}

bool cut5 (void) {
	if (LeptonVec.size() != 2)
		return false;

	if((abs(LeptonVec[0].isb) != 11 || abs(LeptonVec[1].isb) != 13) && (abs(LeptonVec[0].isb) != 13 || abs(LeptonVec[1].isb) != 11) )
		return false;

	return true;
}

bool cut6 (void) {
	if (LeptonVec[0].Pt() < 25.*GeV)
		return false;
	else
		return true;
}

bool cut7 (void) {
	int HasElectronMatchingTrigger = 0;
	int HasMuonMatchingTrigger = 0;

	for (unsigned i=0; i < LeptonVec.size(); ++i) {
		int ii = LeptonVec[i].idx;
		// check leptons are trigger matched
		if (abs(LeptonVec[ii].isb) == 11 && LeptonVec[ii].itrigMatch == 1 ) HasElectronMatchingTrigger = 1;
		if (abs(LeptonVec[ii].isb) == 13 && LeptonVec[ii].itrigMatch == 1 ) HasMuonMatchingTrigger = 1;
	}

	bool ElectronTriggerOK = ( ElectronTrigger != 0 ) && ( HasElectronMatchingTrigger != 0 );
	bool     MuonTriggerOK = (     MuonTrigger != 0 ) && (     HasMuonMatchingTrigger != 0 );

	if ( !(ElectronTriggerOK || MuonTriggerOK) )
		return false;

	return true;

}

bool cut8 (void) {
	if (EleMuoOverlap !=0)
		return false;
	else
		return true;
}

bool cut9 (void) {
	if (JetCleanning != 0)
		return false;
	else
		return true;

}

bool cut10 (void) {
	if(Ht <= 130.*GeV)
		return false;
	else
		return true;
}

bool cut11 (void) {
	if (jet_n_Mini <= 2 )
		return false;
	else
		return true;

}

bool cut12 (void) {
	if (LeptonVec[0].isb * LeptonVec[1].isb >= 0.)
		return false;
	else
		return true;
}

bool cut13 (void) {
	if(ll.M()/GeV <= 15.)
		return false;
	else
		return true;
}

bool cut14 (void) {
	if (LeptonVec[0].itruthMatch != 1)
		return false;
	if (LeptonVec[1].itruthMatch != 1)
		return false;

	return true;
}

bool cut15 (void) {
	// cut 18 nao testa nada porque lepsample == 23
	if (NbtagJet < 1)
		return false;
	else
		return true;
}

bool cut16 (void) {
	if (NbtagJet != 2 && rnd.Rndm() < 0.4)
		return false;
	else
		return true;

}

bool cut17 (void) {
	// REBENTA AQUI????
	// o 21 tambem nao faz nada
//	if (!_cut17) {
//		_cut17 = true;
		ttDilepKinFit();
//	}

	return true;
}

int main (int argc, char *argv[]) {
	map<string, string> options;
	ProgramOptions opts;
	vector<string> file_list;

	#ifdef D_MPI
		MPI_Init(&argc, &argv);
	#endif

	if (!opts.getOptions(argc, argv, options, file_list))
		return 0;

	unsigned number_of_cuts = 18;

	TTH anl (file_list, options["record"], options["output"], options["signal"], options["background"], number_of_cuts);
	// Add the cuts using the addCut method
	anl.addCut("cut0",  cut0);
	anl.addCut("cut1",  cut1);
	anl.addCut("cut2",  cut2);
	anl.addCut("cut3",  cut3);
	anl.addCut("cut4",  cut4);
	anl.addCut("cut5",  cut5);
	anl.addCut("cut6",  cut6);
	anl.addCut("cut7",  cut7);
	anl.addCut("cut8",  cut8);
	anl.addCut("cut9",  cut9);
	anl.addCut("cut10", cut10);
	anl.addCut("cut11", cut11);
	anl.addCut("cut12", cut12);
	anl.addCut("cut13", cut13);
	anl.addCut("cut14", cut14);
	anl.addCut("cut15", cut15);
	anl.addCut("cut16", cut16);
	anl.addCut("cut17", cut17);

	anl.addCutDependency("cut4", "cut5");
	anl.addCutDependency("cut4", "cut6");
	anl.addCutDependency("cut4", "cut7");
	anl.addCutDependency("cut4", "cut12");
	anl.addCutDependency("cut4", "cut14");

	anl.addCutDependency("cut0", "cut17");
	anl.addCutDependency("cut1", "cut17");
	anl.addCutDependency("cut2", "cut17");
	anl.addCutDependency("cut3", "cut17");
	anl.addCutDependency("cut4", "cut17");
	anl.addCutDependency("cut5", "cut17");
	anl.addCutDependency("cut6", "cut17");
	anl.addCutDependency("cut7", "cut17");
	anl.addCutDependency("cut8", "cut17");
	anl.addCutDependency("cut9", "cut17");
	anl.addCutDependency("cut10", "cut17");
	anl.addCutDependency("cut11", "cut17");
	anl.addCutDependency("cut12", "cut17");
	anl.addCutDependency("cut13", "cut17");
	anl.addCutDependency("cut14", "cut17");
	anl.addCutDependency("cut15", "cut17");
	anl.addCutDependency("cut16", "cut17");


	anl.addCutDependency("cut0", "cut1");
	anl.addCutDependency("cut0", "cut2");
	anl.addCutDependency("cut0", "cut3");
	anl.addCutDependency("cut0", "cut4");
	anl.addCutDependency("cut0", "cut8");
	anl.addCutDependency("cut0", "cut9");
	anl.addCutDependency("cut0", "cut10");
	anl.addCutDependency("cut0", "cut11");
	anl.addCutDependency("cut0", "cut13");
	anl.addCutDependency("cut0", "cut15");
	anl.addCutDependency("cut0", "cut16");


/*	anl.addCutDependency("cut0", "cut1");
	anl.addCutDependency("cut1", "cut2");
	anl.addCutDependency("cut2", "cut3");
	anl.addCutDependency("cut3", "cut4");
	anl.addCutDependency("cut4", "cut5");
	anl.addCutDependency("cut5", "cut6");
	anl.addCutDependency("cut6", "cut7");
	anl.addCutDependency("cut7", "cut8");
	anl.addCutDependency("cut8", "cut9");
	anl.addCutDependency("cut9", "cut10");
	anl.addCutDependency("cut10", "cut11");
	anl.addCutDependency("cut11", "cut12");
	anl.addCutDependency("cut12", "cut13");
	anl.addCutDependency("cut13", "cut14");
	anl.addCutDependency("cut14", "cut15");
	anl.addCutDependency("cut15", "cut16");
	anl.addCutDependency("cut16", "cut17");*/



	// Processes the analysis
	anl.run();
	#ifdef D_MPI
		MPI_Finalize();
	#endif

	return 0;
}
