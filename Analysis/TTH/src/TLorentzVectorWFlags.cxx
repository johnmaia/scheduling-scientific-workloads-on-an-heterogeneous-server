

#define TLorentzVectorWFlags_cxx

#include "TLorentzVectorWFlags.h"

// #############################################################################
TLorentzVectorWFlags::TLorentzVectorWFlags(): TLorentzVector(), idx(-1), isb(-1), IsoDeltaR(999), itruthMatch(-1), itrigMatch(-1){
// #############################################################################

}

// #############################################################################
TLorentzVectorWFlags::TLorentzVectorWFlags(Double_t px, Double_t py, Double_t pz, Double_t E, Int_t index, Int_t index2, double p_IsoDeltaR, Int_t index3, Int_t index4) :
  TLorentzVector(px,py,pz,E), idx(index), isb(index2), IsoDeltaR(p_IsoDeltaR), itruthMatch(index3), itrigMatch(index4) {
// #############################################################################

}

// #############################################################################
TLorentzVectorWFlags::TLorentzVectorWFlags(TLorentzVector v, int index, int index2, double p_IsoDeltaR, int index3, int index4) :
  TLorentzVector(v), idx(index), isb(index2), IsoDeltaR(p_IsoDeltaR), itruthMatch(index3), itrigMatch(index4) {
// #############################################################################

}

// #############################################################################
TLorentzVectorWFlags::TLorentzVectorWFlags(const TLorentzVectorWFlags& other) :
  TLorentzVector(other), idx(other.idx), isb(other.isb), IsoDeltaR(other.IsoDeltaR), itruthMatch(other.itruthMatch), itrigMatch(other.itrigMatch) {
// #############################################################################

}

// #############################################################################
TLorentzVectorWFlags& TLorentzVectorWFlags::operator=(const TLorentzVectorWFlags& other) {
// #############################################################################

  if (&other==this) {
    return *this ;
  }
  TLorentzVector::operator=(other) ;
  idx = other.idx ;
  isb = other.isb ;
  IsoDeltaR = other.IsoDeltaR;
  itruthMatch = other.itruthMatch ;
  itrigMatch  = other.itrigMatch ;
  return *this ;
}

// #############################################################################
TLorentzVectorWFlags::~TLorentzVectorWFlags() {
// #############################################################################

}
