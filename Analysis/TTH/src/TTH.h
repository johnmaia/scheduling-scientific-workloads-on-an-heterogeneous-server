#ifndef Ordem_h
#define Ordem_h

#include <cstdlib>
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <DataAnalysis.h>

using namespace std;


class TTH : public DataAnalysis {
	// Variables to record per cut
	vector< Int_t > *record_eventNumber;
	vector< Float_t > *record_vxp_z;
	vector< Float_t > *record_scaleFactor_MUON;
	vector< Float_t > *record_massInv_LL;
	vector< Float_t > *record_met_et;
	vector< UInt_t > *record_jet_n;
	vector< UInt_t > *record_alljet_n;
	vector< UInt_t > *record_channel_DL;
	vector< Float_t > *record_scaleFactor_WJETSSHAPE;
	vector< Float_t > *record_mcWeight;
	vector< Int_t > *record_dataPeriod;
// Pdfs to record
	// Insert your class variables here


	void recordVariables (unsigned cut_number);
	void writeVariables (void);
	void recordPdfs (void);
	void writePdfs (void);
	void initialize (void);
	void finalize (void);

	void initRecord (void);

public:
	~TTH(void);
	TTH (vector<string> _root_file, string _tree_filename, string _output_root_file, string _signal, string _background, unsigned ncuts);
	TTH (vector<string> _root_file, string _tree_filename, string _output_root_file, string _signal, string _background, string _tree_name, unsigned ncuts);
};

#endif
