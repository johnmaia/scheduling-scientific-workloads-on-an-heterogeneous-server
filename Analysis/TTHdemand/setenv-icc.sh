#!/bin/bash

cd $HOME/tools/root/5.34.34/icc.16.0.0/
. bin/thisroot.sh
cd /home/a51752/tools/hep-frame-development/Analysis/TTHoffload/
module add gnu/4.9.0
module add gnu/4.9.3
source /share/apps/intel/compilers_and_libraries/linux/bin/compilervars.sh intel64
export INTEL=yes
export LD_LIBRARY_PATH=/home/a51752/tools/boost/1.59-icc/lib:$LD_LIBRARY_PATH
