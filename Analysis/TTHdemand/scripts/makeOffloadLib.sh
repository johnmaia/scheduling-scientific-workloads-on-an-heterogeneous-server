# Remove old files
cd offload/mic/
rm -f build/*
rm -f lib/*

mkdir -p build/
mkdir -p lib/

# Compile new lib
icpc -fopenmp -c -Wall -std=c++11 src/TTH_offload.cxx -o build/TTH_offload.o
ar -r lib/libOffload.a build/TTH_offload.o
cd ../..
