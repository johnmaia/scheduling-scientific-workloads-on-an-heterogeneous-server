#ifndef TLorentzVectorWFlags_h
#define TLorentzVectorWFlags_h

#include <TLorentzVector.h>
#include <vector>

// #############################################################################
class TLorentzVectorWFlags : public TLorentzVector {

 public:
  //
  TLorentzVectorWFlags() ;
  TLorentzVectorWFlags(Double_t px, Double_t py, Double_t pz, Double_t E, int idx, int isb, double IsoDeltaR, int itruthMatch, int itrigMatch);
  TLorentzVectorWFlags(TLorentzVector v, int idx, int isb, double IsoDeltaR, int itruthMatch, int itrigMatch);
  TLorentzVectorWFlags(const TLorentzVectorWFlags& other);
  //
  ~TLorentzVectorWFlags();
  TLorentzVectorWFlags& operator=(const TLorentzVectorWFlags& other) ;

  inline void SetIsoDeltaR(double p_IsoDeltaR) {IsoDeltaR = p_IsoDeltaR;};

  int idx ;
  int isb ;
  double IsoDeltaR;
  int itruthMatch;
  int itrigMatch;

 private:

  //  ClassDef(TLorentzVectorWFlags,0)
} ;

#endif
