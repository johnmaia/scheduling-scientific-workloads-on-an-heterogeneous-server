#include "TTH.h"

#define THIS_THREAD(x) thread_id * num_threads + x
#define THIS_CUT(t,x) t * num_threads + x

using namespace std;

extern vector<HEPEvent> events;
extern unsigned num_threads;
extern unsigned thread_id;
#pragma omp threadprivate(thread_id)
extern long unsigned event_counter;
#pragma omp threadprivate(event_counter)


// ***********************************************
// Method to record variables, do not edit
void TTH::initRecord (void) {
		record_eventNumber = new vector< Int_t >[number_of_cuts];
		record_vxp_z = new vector< Float_t >[number_of_cuts];
		record_scaleFactor_MUON = new vector< Float_t >[number_of_cuts];
		record_massInv_LL = new vector< Float_t >[number_of_cuts];
		record_met_et = new vector< Float_t >[number_of_cuts];
		record_jet_n = new vector< UInt_t >[number_of_cuts];
		record_alljet_n = new vector< UInt_t >[number_of_cuts];
		record_channel_DL = new vector< UInt_t >[number_of_cuts];
		record_scaleFactor_WJETSSHAPE = new vector< Float_t >[number_of_cuts];
		record_mcWeight = new vector< Float_t >[number_of_cuts];
		record_dataPeriod = new vector< Int_t >[number_of_cuts];

}

void TTH::recordVariables (unsigned cut_number) {

	record_eventNumber[cut_number].push_back(events[event_counter].eventNumber);
	record_vxp_z[cut_number].push_back(events[event_counter].vxp_z);
	record_scaleFactor_MUON[cut_number].push_back(events[event_counter].scaleFactor_MUON);
	record_massInv_LL[cut_number].push_back(events[event_counter].massInv_LL);
	record_met_et[cut_number].push_back(events[event_counter].met_et);
	record_jet_n[cut_number].push_back(events[event_counter].jet_n);
	record_alljet_n[cut_number].push_back(events[event_counter].alljet_n);
	record_channel_DL[cut_number].push_back(events[event_counter].channel_DL);
	record_scaleFactor_WJETSSHAPE[cut_number].push_back(events[event_counter].scaleFactor_WJETSSHAPE);
	record_mcWeight[cut_number].push_back(events[event_counter].mcWeight);
	record_dataPeriod[cut_number].push_back(events[event_counter].dataPeriod);

}

void TTH::writeVariables (void) {
	TFile *tree_file = new TFile (tree_filename.c_str(), "recreate");

	tree_file->cd();

	for (unsigned i = 0; i < number_of_cuts; i++) {
		if (record_dataPeriod[i].size()) {
			Int_t aux_eventNumber;
			tree_rec_vars[i]->Branch("eventNumber", &aux_eventNumber);
			Float_t aux_vxp_z;
			tree_rec_vars[i]->Branch("vxp_z", &aux_vxp_z);
			Float_t aux_scaleFactor_MUON;
			tree_rec_vars[i]->Branch("scaleFactor_MUON", &aux_scaleFactor_MUON);
			Float_t aux_massInv_LL;
			tree_rec_vars[i]->Branch("massInv_LL", &aux_massInv_LL);
			Float_t aux_met_et;
			tree_rec_vars[i]->Branch("met_et", &aux_met_et);
			UInt_t aux_jet_n;
			tree_rec_vars[i]->Branch("jet_n", &aux_jet_n);
			UInt_t aux_alljet_n;
			tree_rec_vars[i]->Branch("alljet_n", &aux_alljet_n);
			UInt_t aux_channel_DL;
			tree_rec_vars[i]->Branch("channel_DL", &aux_channel_DL);
			Float_t aux_scaleFactor_WJETSSHAPE;
			tree_rec_vars[i]->Branch("scaleFactor_WJETSSHAPE", &aux_scaleFactor_WJETSSHAPE);
			Float_t aux_mcWeight;
			tree_rec_vars[i]->Branch("mcWeight", &aux_mcWeight);
			Int_t aux_dataPeriod;
			tree_rec_vars[i]->Branch("dataPeriod", &aux_dataPeriod);

			for (unsigned j = 0; j < events.size(); j++)
				if (j < record_dataPeriod[i].size()) {
					aux_eventNumber = record_eventNumber[i].at(j);
					aux_vxp_z = record_vxp_z[i].at(j);
					aux_scaleFactor_MUON = record_scaleFactor_MUON[i].at(j);
					aux_massInv_LL = record_massInv_LL[i].at(j);
					aux_met_et = record_met_et[i].at(j);
					aux_jet_n = record_jet_n[i].at(j);
					aux_alljet_n = record_alljet_n[i].at(j);
					aux_channel_DL = record_channel_DL[i].at(j);
					aux_scaleFactor_WJETSSHAPE = record_scaleFactor_WJETSSHAPE[i].at(j);
					aux_mcWeight = record_mcWeight[i].at(j);
					aux_dataPeriod = record_dataPeriod[i].at(j);
					tree_rec_vars[i]->Fill();
				}

			tree_rec_vars[i]->Write();
		}
	}
	delete tree_file;
}

// Write here the variables and expressions to record per cut
#ifdef RecordVariables
eventNumber
vxp_z
scaleFactor_MUON
massInv_LL
met_et
jet_n
alljet_n
channel_DL
scaleFactor_WJETSSHAPE
mcWeight
dataPeriod
#endif
// ***********************************************


// ***********************************************
// Method to record pdfs, do not edit
void TTH::recordPdfs (void) {

}

void TTH::writePdfs (void) {

}

// Write here the variables to record as pdf
#ifdef PdfVariables

#endif
// ***********************************************
