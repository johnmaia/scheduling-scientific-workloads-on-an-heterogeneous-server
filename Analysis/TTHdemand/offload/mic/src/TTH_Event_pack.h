#ifndef hepevpack_h
#define hepevpack

#include "TTH_STLorentzVectorWFlags.h"
#include "TTH_STVector3.h"

typedef struct eventPack {
  int eventID;
  HEPEvent_Offload ev;
} EventPack;

#endif
