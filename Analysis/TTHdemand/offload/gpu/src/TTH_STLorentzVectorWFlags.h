#ifndef stlorentzwflags_h
#define stlorentzwflags_h

#include <cuda.h>

// double const  kPI        = atan(1)*4;
// double const  kTWOPI     = 2.*atan(1)*4;

typedef struct stlorentzwflags {
  double Fx;
  double Fy;
  double Fz;
  double Fe;

  int idx = -1;
  int isb = -1;
  double IsoDeltaR = 999;
  int itruthMatch = -1;
  int itrigMatch = -1;

  __device__ double Px();
  __device__ double Py();
  __device__ double Pz();
  __device__ double Pt();
  __device__ double E();
  __device__ double T();

  __device__ double M();

  __device__ double P();

  __device__ double Perp();
  __device__ double Perp2();

  __device__ double Mag();
  __device__ double Mag2();

  __device__ void SetPx(double x);
  __device__ void SetPy(double y);
  __device__ void SetPz(double z);
  __device__ void SetE(double e);
  __device__ void SetPxPyPzE(double px, double py, double pz, double e);
  __device__ void SetRest(int pidx, int pisb, double pIsoDeltaR, int pitruthMatch, int pitrigMatch);
  __device__ void SetIsoDeltaR(double p_IsoDeltaR);

  __device__ double CosTheta();
  __device__ double Phi();
  __device__ double Phi_mpi_pi(double x);
  __device__ double PseudoRapidity();
  __device__ double Eta();
  __device__ double DeltaR(const stlorentzwflags & v);
} STLorentzVectorWFlags;

inline __device__ double STLorentzVectorWFlags::Px(){ return Fx; }
inline __device__ double STLorentzVectorWFlags::Py(){ return Fy; }
inline __device__ double STLorentzVectorWFlags::Pz(){ return Fz; }
inline __device__ double STLorentzVectorWFlags::Pt(){ return Perp(); }
inline __device__ double STLorentzVectorWFlags::E(){ return Fe; }
inline __device__ double STLorentzVectorWFlags::T(){ return Fe; }

inline __device__ double STLorentzVectorWFlags::M(){ return Mag(); }

inline __device__ double STLorentzVectorWFlags::P(){ return Mag(); }

inline __device__ double STLorentzVectorWFlags::CosTheta(){
   double ptot = Mag();
   return ptot == 0.0 ? 1.0 : Fz/ptot;
}

inline __device__ double STLorentzVectorWFlags::Phi()
{
   // return vector phi
   return (atan(1.0f)*4)+atan2(-Fy,-Fx);
}


inline __device__ double STLorentzVectorWFlags::Phi_mpi_pi(double x) {
   if(isnan(x)){
      return x;
   }
   while (x >= atan(1.0f)*4) x -= 2.*atan(1.0f)*4;
   while (x < -atan(1.0f)*4) x += 2.*atan(1.0f)*4;
   return x;
}

inline __device__ double STLorentzVectorWFlags::PseudoRapidity(){
   double cosTheta = CosTheta();
   if (cosTheta*cosTheta < 1) return -0.5* log( (1.0-cosTheta)/(1.0+cosTheta) );
   if (Fz == 0) return 0;
   //Warning("PseudoRapidity","transvers momentum = 0! return +/- 10e10");
   if (Fz > 0) return 10e10;
   else        return -10e10;
}

inline __device__ double STLorentzVectorWFlags::Eta(){
   return PseudoRapidity();
}

inline __device__ double STLorentzVectorWFlags::DeltaR(const stlorentzwflags& v) {
   STLorentzVectorWFlags a;
   a = v;
   double deta = Eta()-a.Eta();
   double dphi = Phi_mpi_pi(Phi()-a.Phi());
   return sqrt( deta*deta+dphi*dphi );
}

inline __device__ double STLorentzVectorWFlags::Perp(){ return sqrt(Perp2()); }
inline __device__ double STLorentzVectorWFlags::Perp2(){ return Fx*Fx + Fy*Fy;; }

inline __device__ double STLorentzVectorWFlags::Mag(){
   double mm = Mag2();
   return mm < 0.0 ? -sqrt(-mm) : sqrt(mm);
}

inline __device__ double STLorentzVectorWFlags::Mag2(){
  return Fx*Fx + Fy*Fy + Fz*Fz;
}

inline __device__ void STLorentzVectorWFlags::SetPx(double x){ Fx = x; }
inline __device__ void STLorentzVectorWFlags::SetPy(double y){ Fy = y; }
inline __device__ void STLorentzVectorWFlags::SetPz(double z){ Fz = z; }
inline __device__ void STLorentzVectorWFlags::SetE(double e){ Fe = e; }
inline __device__ void STLorentzVectorWFlags::SetPxPyPzE(double px, double py, double pz, double e){
  Fx = px; Fy =py; Fz = pz; Fe = e;
}
inline __device__ void STLorentzVectorWFlags::SetRest(int pidx, int pisb, double pIsoDeltaR, int pitruthMatch, int pitrigMatch){
  idx = pidx;
  isb = pisb;
  IsoDeltaR = pIsoDeltaR;
  itruthMatch = pitruthMatch;
  itrigMatch = pitrigMatch;
}

inline __device__ void STLorentzVectorWFlags::SetIsoDeltaR(double p_IsoDeltaR){ IsoDeltaR = p_IsoDeltaR; };

#endif
