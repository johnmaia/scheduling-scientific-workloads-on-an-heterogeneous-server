#include <vector>

// Copy information from a TLorentzVectorWFlags to a STLorentzVectorWFlags
void TLorentzVectorWFToSTLWF_GPU(TLorentzVectorWFlags tlvwf, STLorentzVectorWFlags * stlwf){
  stlwf->Fx = tlvwf.X();
  stlwf->Fy = tlvwf.Y();
  stlwf->Fz = tlvwf.Z();
  stlwf->Fe = tlvwf.T();

  stlwf->idx = tlvwf.idx;
  stlwf->isb = tlvwf.isb;
  stlwf->IsoDeltaR = tlvwf.IsoDeltaR;
  stlwf->itruthMatch = tlvwf.itruthMatch;
  stlwf->itrigMatch = tlvwf.itrigMatch;
}

// Copy information from a TLorentzVector to a STLorentzVector
void TLorentzVectorToSTLRTZ_GPU(TLorentzVector tlv, STLorentzVector * stlrtz){
  stlrtz->Fx = tlv.X();
  stlrtz->Fy = tlv.Y();
  stlrtz->Fz = tlv.Z();
  stlrtz->Fe = tlv.T();
}

void TVector3ToSTVector3_GPU(TVector3 tv, STVector3 * stvect3){
  stvect3->Fx = tv.X();
  stvect3->Fy = tv.Y();
  stvect3->Fz = tv.Z();
}

// Copy information back from the STLorentzVector to the TLorentzVector
void STLRTZToTLorentzVector_GPU(STLorentzVector stlrtz, TLorentzVector tlv){
  tlv.SetPxPyPzE(stlrtz.Fx, stlrtz.Fy, stlrtz.Fz, stlrtz.Fe);
}
// Copy information back from the STLorentzVector to the TLorentzVector
void STLWFToTLorentzVectorWF_GPU(STLorentzVectorWFlags stlwf, TLorentzVectorWFlags tlvwf){
  tlvwf.SetX(stlwf.Fx);
  tlvwf.SetY(stlwf.Fy);
  tlvwf.SetZ(stlwf.Fz);
  tlvwf.SetT(stlwf.Fe);

  tlvwf.idx = stlwf.idx;
  tlvwf.isb = stlwf.isb;
  tlvwf.IsoDeltaR = stlwf.IsoDeltaR;
  tlvwf.itruthMatch = stlwf.itruthMatch;
  tlvwf.itrigMatch = stlwf.itrigMatch;
}

void STVector3ToTVector3_GPU(STVector3 stvect3, TVector3 tvect3){
  tvect3.SetX(stvect3.Fx);
  tvect3.SetY(stvect3.Fy);
  tvect3.SetZ(stvect3.Fz);
}

void TVector3VectorToArray_GPU(std::vector<TVector3> tvect3Vector, STVector3 * stvec3Array){
  for(unsigned i = 0; i < tvect3Vector.size(); ++i){
    STVector3 stvect3;
    TVector3ToSTVector3_GPU(tvect3Vector[i], &stvect3);
    stvec3Array[i] = stvect3;
  }
}

void TLorentzVectorWFVectorToArray_GPU(std::vector<TLorentzVectorWFlags> tlvwfVector, STLorentzVectorWFlags * stlWFArray){
  for(unsigned i = 0; i < tlvwfVector.size(); ++i){
    STLorentzVectorWFlags stlwf;
    TLorentzVectorWFToSTLWF_GPU(tlvwfVector[i], &stlwf);
    stlWFArray[i] = stlwf;
  }
}

void ArrayToTVector3Vector_GPU(STVector3 * stvec3Array, std::vector<TVector3> tvect3Vector, int size){
  for(unsigned i = 0; i < size; ++i){
    TVector3 tvect3;
    STVector3ToTVector3_GPU(stvec3Array[i], tvect3);
    tvect3Vector.push_back(tvect3);
  }
}

void ArrayToTLorentzVectorWFVector_GPU(STLorentzVectorWFlags * stlWFArray, std::vector<TLorentzVectorWFlags> tlWFVector, int size){
  for(unsigned i = 0; i < size; ++i){
    TLorentzVectorWFlags tlvwf;
    STLWFToTLorentzVectorWF_GPU(stlWFArray[i], tlvwf);
    tlWFVector.push_back(tlvwf);
  }
}

// Copy information from the event to the offload structure for the cut17
HEPEvent_Offload_GPU HEPEventToHEPEvent_Offload_GPU(int eventID){
  HEPEvent_Offload_GPU ev;

  ev.I_ElectronTrigger = events[eventID].ElectronTrigger;
  ev.I_MuonTrigger = events[eventID].MuonTrigger;
  ev.I_channelNumber = events[eventID].channelNumber;
  ev.I_Isub = events[eventID].Isub;

  ev.I_LumiBlock = events[eventID].LumiBlock;
  ev.I_RunNumber = events[eventID].RunNumber;
  ev.I_EveNumber = events[eventID].EveNumber;
  ev.I_runNumber = events[eventID].runNumber;
  ev.I_eventNumber = events[eventID].eventNumber;

  ev.I_ntruthlep = events[eventID].ntruthlep;
  ev.I_ntruthele = events[eventID].ntruthele;
  ev.I_ntruthmu = events[eventID].ntruthmu;
  ev.I_ntrutheletau = events[eventID].ntrutheletau;
  ev.I_ntruthmutau = events[eventID].ntruthmutau;
  ev.I_ntruthtau = events[eventID].ntruthtau;
  ev.I_ntruthleptau = events[eventID].ntruthleptau;
  ev.I_truE = events[eventID].truE;
  ev.I_truM = events[eventID].truM;
  ev.I_TruthEleNumber = events[eventID].TruthEleNumber;
  ev.I_TruthMuonNumber = events[eventID].TruthMuonNumber;

  ev.I_HforFlag = events[eventID].HforFlag;
  ev.I_Cosmic = events[eventID].Cosmic;
  ev.I_EleMuoOverlap = events[eventID].EleMuoOverlap;
  ev.I_jet_n_Mini = events[eventID].jet_n_Mini;
  ev.I_NbtagJet = events[eventID].NbtagJet;
  ev.I_JetCleanning = events[eventID].JetCleanning;
  ev.I_GoodRL = events[eventID].GoodRL;

  ev.UI_lep_n = events[eventID].lep_n;
  ev.UI_jet_n = events[eventID].jet_n;

  for(unsigned i = 0; i < 3; ++i){
    ev.UIA_lep_type[i] = events[eventID].lep_type[i];
    ev.FA_lep_pt[i] = events[eventID].lep_pt[i];
    ev.FA_lep_eta[i] = events[eventID].lep_eta[i];
    ev.FA_lep_phi[i] = events[eventID].lep_phi[i];
    ev.FA_lep_E[i] = events[eventID].lep_E[i];
    ev.FA_lep_charge[i] = events[eventID].lep_charge[i];
    ev.BA_lep_truthMatched[i] = events[eventID].lep_truthMatched[i];
    ev.BA_lep_trigMatched[i] = events[eventID].lep_trigMatched[i];
  }

  for(unsigned i = 0; i < 14; ++i){
    ev.FA_jet_MV1[i] = events[eventID].jet_MV1[i];
    ev.FA_jet_pt[i] = events[eventID].jet_pt[i];
    ev.FA_jet_eta[i] = events[eventID].jet_eta[i];
    ev.FA_jet_phi[i] = events[eventID].jet_phi[i];
    ev.FA_jet_E[i] = events[eventID].jet_E[i];
    ev.FA_jet_truthMatched[i] = events[eventID].jet_truthMatched[i];
  }

  ev.F_ht = events[eventID].ht;
  ev.F_vxp_z = events[eventID].vxp_z;
  ev.F_massInv_LL = events[eventID].massInv_LL;
  ev.F_met_et = events[eventID].met_et;
  ev.F_met_phi = events[eventID].met_phi;
  ev.F_mcWeight = events[eventID].mcWeight;

  ev.B_hasGoodVertex = events[eventID].hasGoodVertex;
  ev.B_cosmicEvent = events[eventID].cosmicEvent;
  ev.B_passGRL = events[eventID].passGRL;
  ev.B_trigE = events[eventID].trigE;
  ev.B_trigM = events[eventID].trigM;

  ev.D_massInv_LL_Mini = events[eventID].massInv_LL_Mini;
  ev.D_PtCutJet = events[eventID].PtCutJet;
  ev.D_Ht_Mini = events[eventID].Ht_Mini;
  ev.D_Weight = events[eventID].Weight;
  ev.D_Ht = events[eventID].Ht;
  ev.D_Hz = events[eventID].Hz;
	ev.D_MissPx = events[eventID].MissPx;
	ev.D_MissPy = events[eventID].MissPy;
  ev.D_MissPt = events[eventID].MissPt;
  ev.D_BTagCut = events[eventID].BTagCut;
  ev.D_Sphericity = events[eventID].Sphericity;
  ev.D_Aplanarity = events[eventID].Aplanarity;
  ev.D_Planarity = events[eventID].Planarity;

  TLorentzVectorToSTLRTZ_GPU(events[eventID].ll, &ev.TL_ll);
  TLorentzVectorToSTLRTZ_GPU(events[eventID].llmiss, &ev.TL_llmiss);
  TLorentzVectorToSTLRTZ_GPU(events[eventID].RecT, &ev.TL_RecT);
  TLorentzVectorToSTLRTZ_GPU(events[eventID].RecWp, &ev.TL_RecWp);
  TLorentzVectorToSTLRTZ_GPU(events[eventID].RecLepP, &ev.TL_RecLepP);
  TLorentzVectorToSTLRTZ_GPU(events[eventID].RecNeu, &ev.TL_RecNeu);
  TLorentzVectorToSTLRTZ_GPU(events[eventID].RecTbar, &ev.TL_RecTbar);
  TLorentzVectorToSTLRTZ_GPU(events[eventID].RecBbar, &ev.TL_RecBbar);
  TLorentzVectorToSTLRTZ_GPU(events[eventID].RecWn, &ev.TL_RecWn);
  TLorentzVectorToSTLRTZ_GPU(events[eventID].RecNeubar, &ev.TL_RecNeubar);
  TLorentzVectorToSTLRTZ_GPU(events[eventID].RecTTbar, &ev.TL_RecTTbar);
  TLorentzVectorToSTLRTZ_GPU(events[eventID].RecHiggsB1, &ev.TL_RecHiggsB1);
  TLorentzVectorToSTLRTZ_GPU(events[eventID].RecHiggsB2, &ev.TL_RecHiggsB2);
  TLorentzVectorToSTLRTZ_GPU(events[eventID].Neutrino, &ev.TL_Neutrino);
  TLorentzVectorToSTLRTZ_GPU(events[eventID].Antineutrino, &ev.TL_Antineutrino);

  ev.D_RecMassHiggsJet1 = events[eventID].RecMassHiggsJet1;
	ev.D_RecMassHiggsJet2 = events[eventID].RecMassHiggsJet2;
	ev.D_RecProbTotal_ttH = events[eventID].RecProbTotal_ttH;

  TLorentzVectorToSTLRTZ_GPU(events[eventID].RecB_BoostedtoT, &ev.TL_RecB_BoostedtoT);
  TLorentzVectorToSTLRTZ_GPU(events[eventID].RecWp_BoostedtoT, &ev.TL_RecWp_BoostedtoT);
  TLorentzVectorToSTLRTZ_GPU(events[eventID].RecLepP_BoostedtoT, &ev.TL_RecLepP_BoostedtoT);
  TLorentzVectorToSTLRTZ_GPU(events[eventID].RecNeu_BoostedtoT, &ev.TL_RecNeu_BoostedtoT);
  TLorentzVectorToSTLRTZ_GPU(events[eventID].RecBbar_BoostedtoTbar, &ev.TL_RecBbar_BoostedtoTbar);
  TLorentzVectorToSTLRTZ_GPU(events[eventID].RecWn_BoostedtoTbar, &ev.TL_RecWn_BoostedtoTbar);
  TLorentzVectorToSTLRTZ_GPU(events[eventID].RecLepN_BoostedtoTbar, &ev.TL_RecLepN_BoostedtoTbar);
  TLorentzVectorToSTLRTZ_GPU(events[eventID].RecNeubar_BoostedtoTbar, &ev.TL_RecNeubar_BoostedtoTbar);

  TLorentzVectorToSTLRTZ_GPU(events[eventID].RecT_Boostedtottbar, &ev.TL_RecT_Boostedtottbar);
  TLorentzVectorToSTLRTZ_GPU(events[eventID].RecTbar_Boostedtottbar, &ev.TL_RecTbar_Boostedtottbar);

  ev.D_RecCos_LepP_T_BoostedtoT = events[eventID].RecCos_LepP_T_BoostedtoT;
  ev.D_RecCos_Neu_T_BoostedtoT = events[eventID].RecCos_Neu_T_BoostedtoT;
  ev.D_RecCos_B_T_BoostedtoT = events[eventID].RecCos_B_T_BoostedtoT;
  ev.D_RecCos_LepN_Tbar_BoostedtoTbar = events[eventID].RecCos_LepN_Tbar_BoostedtoTbar;
  ev.D_RecCos_Neubar_Tbar_BoostedtoTbar = events[eventID].RecCos_Neubar_Tbar_BoostedtoTbar;
  ev.D_RecCos_Bbar_Tbar_BoostedtoTbar = events[eventID].RecCos_Bbar_Tbar_BoostedtoTbar;
  ev.D_RecCos_LepP_B_BoostedtoWp = events[eventID].RecCos_LepP_B_BoostedtoWp;
  ev.D_RecCos_LepN_Bbar_BoostedtoWn = events[eventID].RecCos_LepN_Bbar_BoostedtoWn;

  TLorentzVectorToSTLRTZ_GPU(events[eventID].RecB_BoostedtoWp, &ev.TL_RecB_BoostedtoWp);
  TLorentzVectorToSTLRTZ_GPU(events[eventID].RecLepP_BoostedtoWp, &ev.TL_RecLepP_BoostedtoWp);
  TLorentzVectorToSTLRTZ_GPU(events[eventID].RecNeu_BoostedtoWp, &ev.TL_RecNeu_BoostedtoWp);
  TLorentzVectorToSTLRTZ_GPU(events[eventID].RecBbar_BoostedtoWn, &ev.TL_RecBbar_BoostedtoWn);
  TLorentzVectorToSTLRTZ_GPU(events[eventID].RecLepN_BoostedtoWn, &ev.TL_RecLepN_BoostedtoWn);
  TLorentzVectorToSTLRTZ_GPU(events[eventID].RecNeubar_BoostedtoWn, &ev.TL_RecNeubar_BoostedtoWn);

  return ev;
}

// Copy information back from the structure to the event
void HEPEvent_OffloadToHEPEvent_GPU(HEPEvent_Offload_GPU &ev, int eventID){
  events[eventID].ElectronTrigger = ev.I_ElectronTrigger;
  events[eventID].MuonTrigger = ev.I_MuonTrigger;
  events[eventID].channelNumber = ev.I_channelNumber;
  events[eventID].Isub = ev.I_Isub;

  events[eventID].LumiBlock = ev.I_LumiBlock;
  events[eventID].RunNumber = ev.I_RunNumber;
  events[eventID].EveNumber = ev.I_EveNumber;
  events[eventID].runNumber = ev.I_runNumber;
  events[eventID].eventNumber = ev.I_eventNumber;

  events[eventID].ntruthlep = ev.I_ntruthlep;
  events[eventID].ntruthele = ev.I_ntruthele;
  events[eventID].ntruthmu = ev.I_ntruthmu;
  events[eventID].ntrutheletau = ev.I_ntrutheletau;
  events[eventID].ntruthmutau = ev.I_ntruthmutau;
  events[eventID].ntruthtau = ev.I_ntruthtau;
  events[eventID].ntruthleptau = ev.I_ntruthleptau;
  events[eventID].truE = ev.I_truE;
  events[eventID].truM = ev.I_truM;
  events[eventID].TruthEleNumber = ev.I_TruthEleNumber;
  events[eventID].TruthMuonNumber = ev.I_TruthMuonNumber;

  events[eventID].HforFlag = ev.I_HforFlag;
  events[eventID].Cosmic = ev.I_Cosmic;
  events[eventID].EleMuoOverlap = ev.I_EleMuoOverlap;
  events[eventID].jet_n_Mini = ev.I_jet_n_Mini;
  events[eventID].NbtagJet = ev.I_NbtagJet;
  events[eventID].JetCleanning = ev.I_JetCleanning;
  events[eventID].GoodRL = ev.I_GoodRL;

  events[eventID].lep_n = ev.UI_lep_n;
  events[eventID].jet_n = ev.UI_jet_n;


  for(unsigned i = 0; i < 3; ++i){
    events[eventID].lep_type[i] = ev.UIA_lep_type[i];
    events[eventID].lep_pt[i] = ev.FA_lep_pt[i];
    events[eventID].lep_eta[i] = ev.FA_lep_eta[i];
    events[eventID].lep_phi[i] = ev.FA_lep_phi[i];
    events[eventID].lep_E[i] = ev.FA_lep_E[i];
    events[eventID].lep_charge[i] = ev.FA_lep_charge[i];
    events[eventID].lep_truthMatched[i] = ev.BA_lep_truthMatched[i];
    events[eventID].lep_trigMatched[i] = ev.BA_lep_trigMatched[i];
  }

  for(unsigned i = 0; i < 14; ++i){
    events[eventID].jet_MV1[i] = ev.FA_jet_MV1[i];
    events[eventID].jet_pt[i] = ev.FA_jet_pt[i];
    events[eventID].jet_eta[i] = ev.FA_jet_eta[i];
    events[eventID].jet_phi[i] = ev.FA_jet_phi[i];
    events[eventID].jet_E[i] = ev.FA_jet_E[i];
    events[eventID].jet_truthMatched[i] = ev.FA_jet_truthMatched[i];
  }

  events[eventID].ht = ev.F_ht;
  events[eventID].vxp_z = ev.F_vxp_z;
  events[eventID].massInv_LL = ev.F_massInv_LL;
  events[eventID].met_et = ev.F_met_et;
  events[eventID].met_phi = ev.F_met_phi;
  events[eventID].mcWeight = ev.F_mcWeight;

  events[eventID].hasGoodVertex = ev.B_hasGoodVertex;
  events[eventID].cosmicEvent = ev.B_cosmicEvent;
  events[eventID].passGRL = ev.B_passGRL;
  events[eventID].trigE = ev.B_trigE;
  events[eventID].trigM = ev.B_trigM;

  events[eventID].massInv_LL_Mini = ev.D_massInv_LL_Mini;
  events[eventID].PtCutJet = ev.D_PtCutJet;
  events[eventID].Ht_Mini = ev.D_Ht_Mini;
  events[eventID].Weight = ev.D_Weight;
  events[eventID].Ht = ev.D_Ht;
  events[eventID].Hz = ev.D_Hz;
	events[eventID].MissPx = ev.D_MissPx;
	events[eventID].MissPy = ev.D_MissPy;
  events[eventID].MissPt = ev.D_MissPt;
  events[eventID].BTagCut = ev.D_BTagCut;
  events[eventID].Sphericity = ev.D_Sphericity;
  events[eventID].Aplanarity = ev.D_Aplanarity;
  events[eventID].Planarity = ev.D_Planarity;

  STLRTZToTLorentzVector_GPU(ev.TL_ll, events[eventID].ll);
  STLRTZToTLorentzVector_GPU(ev.TL_llmiss, events[eventID].llmiss);
  STLRTZToTLorentzVector_GPU(ev.TL_RecT, events[eventID].RecT);
  STLRTZToTLorentzVector_GPU(ev.TL_RecWp, events[eventID].RecWp);
  STLRTZToTLorentzVector_GPU(ev.TL_RecLepP, events[eventID].RecLepP);
  STLRTZToTLorentzVector_GPU(ev.TL_RecNeu, events[eventID].RecNeu);
  STLRTZToTLorentzVector_GPU(ev.TL_RecTbar, events[eventID].RecTbar);
  STLRTZToTLorentzVector_GPU(ev.TL_RecBbar, events[eventID].RecBbar);
  STLRTZToTLorentzVector_GPU(ev.TL_RecWn, events[eventID].RecWn);
  STLRTZToTLorentzVector_GPU(ev.TL_RecNeubar, events[eventID].RecNeubar);
  STLRTZToTLorentzVector_GPU(ev.TL_RecTTbar, events[eventID].RecTTbar);
  STLRTZToTLorentzVector_GPU(ev.TL_RecHiggsB1, events[eventID].RecHiggsB1);
  STLRTZToTLorentzVector_GPU(ev.TL_RecHiggsB2, events[eventID].RecHiggsB2);
  STLRTZToTLorentzVector_GPU(ev.TL_Neutrino, events[eventID].Neutrino);
  STLRTZToTLorentzVector_GPU(ev.TL_Antineutrino, events[eventID].Antineutrino);

  events[eventID].RecMassHiggsJet1 = ev.D_RecMassHiggsJet1;
	events[eventID].RecMassHiggsJet2 = ev.D_RecMassHiggsJet2;
	events[eventID].RecProbTotal_ttH = ev.D_RecProbTotal_ttH;

  STLRTZToTLorentzVector_GPU(ev.TL_RecB_BoostedtoT, events[eventID].RecB_BoostedtoT);
  STLRTZToTLorentzVector_GPU(ev.TL_RecWp_BoostedtoT, events[eventID].RecWp_BoostedtoT);
  STLRTZToTLorentzVector_GPU(ev.TL_RecLepP_BoostedtoT, events[eventID].RecLepP_BoostedtoT);
  STLRTZToTLorentzVector_GPU(ev.TL_RecNeu_BoostedtoT, events[eventID].RecNeu_BoostedtoT);
  STLRTZToTLorentzVector_GPU(ev.TL_RecBbar_BoostedtoTbar, events[eventID].RecBbar_BoostedtoTbar);
  STLRTZToTLorentzVector_GPU(ev.TL_RecWn_BoostedtoTbar, events[eventID].RecWn_BoostedtoTbar);
  STLRTZToTLorentzVector_GPU(ev.TL_RecLepN_BoostedtoTbar, events[eventID].RecLepN_BoostedtoTbar);
  STLRTZToTLorentzVector_GPU(ev.TL_RecNeubar_BoostedtoTbar, events[eventID].RecNeubar_BoostedtoTbar);

  STLRTZToTLorentzVector_GPU(ev.TL_RecT_Boostedtottbar, events[eventID].RecT_Boostedtottbar);
  STLRTZToTLorentzVector_GPU(ev.TL_RecTbar_Boostedtottbar, events[eventID].RecTbar_Boostedtottbar);

  events[eventID].RecCos_LepP_T_BoostedtoT = ev.D_RecCos_LepP_T_BoostedtoT;
  events[eventID].RecCos_Neu_T_BoostedtoT = ev.D_RecCos_Neu_T_BoostedtoT;
  events[eventID].RecCos_B_T_BoostedtoT = ev.D_RecCos_B_T_BoostedtoT;
  events[eventID].RecCos_LepN_Tbar_BoostedtoTbar = ev.D_RecCos_LepN_Tbar_BoostedtoTbar;
  events[eventID].RecCos_Neubar_Tbar_BoostedtoTbar = ev.D_RecCos_Neubar_Tbar_BoostedtoTbar;
  events[eventID].RecCos_Bbar_Tbar_BoostedtoTbar = ev.D_RecCos_Bbar_Tbar_BoostedtoTbar;
  events[eventID].RecCos_LepP_B_BoostedtoWp = ev.D_RecCos_LepP_B_BoostedtoWp;
  events[eventID].RecCos_LepN_Bbar_BoostedtoWn = ev.D_RecCos_LepN_Bbar_BoostedtoWn;

  STLRTZToTLorentzVector_GPU(ev.TL_RecB_BoostedtoWp, events[eventID].RecB_BoostedtoWp);
  STLRTZToTLorentzVector_GPU(ev.TL_RecLepP_BoostedtoWp, events[eventID].RecLepP_BoostedtoWp);
  STLRTZToTLorentzVector_GPU(ev.TL_RecNeu_BoostedtoWp, events[eventID].RecNeu_BoostedtoWp);
  STLRTZToTLorentzVector_GPU(ev.TL_RecBbar_BoostedtoWn, events[eventID].RecBbar_BoostedtoWn);
  STLRTZToTLorentzVector_GPU(ev.TL_RecLepN_BoostedtoWn, events[eventID].RecLepN_BoostedtoWn);
  STLRTZToTLorentzVector_GPU(ev.TL_RecNeubar_BoostedtoWn, events[eventID].RecNeubar_BoostedtoWn);
}

// Pack event in one single structure
EventPackGPU PackEventGPU(int eventID){
  EventPackGPU pack;
  pack.eventID = eventID;
  pack.ev = HEPEventToHEPEvent_Offload_GPU(eventID);
  return pack;
}

// Unpack event
void UnpackEventGPU(EventPackGPU pack){
  HEPEvent_OffloadToHEPEvent_GPU(pack.ev, pack.eventID);
}
