#ifndef hepevoff_gpu_h
#define hepevoff_gpu

typedef struct hepevoff_gpu {

  int I_ElectronTrigger;
  int I_MuonTrigger;
  int I_channelNumber;
  int I_Isub;

  int I_LumiBlock;
  int I_RunNumber;
  int I_EveNumber;

  int I_ntruthlep;
  int I_ntruthele;
  int I_ntruthmu;
  int I_ntrutheletau;
  int I_ntruthmutau;
  int I_ntruthtau;
  int I_ntruthleptau;
  int I_truE;
  int I_truM;
  int I_TruthEleNumber;
  int I_TruthMuonNumber;

  int I_runNumber;
  int I_eventNumber;

  int I_HforFlag;
  int I_Cosmic;
  int I_EleMuoOverlap;
  int I_NbtagJet;
  int I_jet_n_Mini;
  int I_JetCleanning;
  int I_GoodRL;

  unsigned int UI_lep_n;
  unsigned int UI_jet_n;

  unsigned int UIA_lep_type[3];

  float FA_lep_pt[3];
  float FA_lep_eta[3];
  float FA_lep_phi[3];
  float FA_lep_E[3];
  float FA_lep_charge[3];

  float FA_jet_MV1[14];
  float FA_jet_pt[14];
  float FA_jet_eta[14];
  float FA_jet_phi[14];
  float FA_jet_E[14];
  float FA_jet_truthMatched[14];


  float F_ht;
  float F_vxp_z;
  float F_massInv_LL;
  float F_met_et;
  float F_met_phi;
  float F_mcWeight;


  bool BA_lep_truthMatched[3];
  bool BA_lep_trigMatched[3];

  bool B_hasGoodVertex;
  bool B_cosmicEvent;
  bool B_passGRL;
  bool B_trigE;
  bool B_trigM;


  double D_massInv_LL_Mini;


  double D_PtCutJet;
  double D_Ht_Mini;
  double D_Weight;
  double D_Ht;
  double D_Hz;
  double D_MissPx;
  double D_MissPy;
  double D_MissPt;
  double D_BTagCut;
  double D_Sphericity;
  double D_Aplanarity;
  double D_Planarity;

  STLorentzVector TL_ll;
  STLorentzVector TL_llmiss;


  //_____objects_____ ttDilepKinFit
  STLorentzVector TL_RecT;
  STLorentzVector TL_RecB;
  STLorentzVector TL_RecWp;
  STLorentzVector TL_RecLepP;
  STLorentzVector TL_RecNeu;
  STLorentzVector TL_RecTbar;
  STLorentzVector TL_RecBbar;
  STLorentzVector TL_RecWn;
  STLorentzVector TL_RecLepN;
  STLorentzVector TL_RecNeubar;
  STLorentzVector TL_RecTTbar;
  STLorentzVector TL_RecHiggs;
  STLorentzVector TL_RecHiggsB1;
  STLorentzVector TL_RecHiggsB2;
  STLorentzVector TL_Neutrino;
  STLorentzVector TL_Antineutrino;

  double D_RecMassHiggsJet1;
  double D_RecMassHiggsJet2;
  double D_RecProbTotal_ttH;

  //_____Boost to top_____
  STLorentzVector TL_RecB_BoostedtoT;
  STLorentzVector TL_RecWp_BoostedtoT;
  STLorentzVector TL_RecLepP_BoostedtoT;
  STLorentzVector TL_RecNeu_BoostedtoT;
  STLorentzVector TL_RecBbar_BoostedtoTbar;
  STLorentzVector TL_RecWn_BoostedtoTbar;
  STLorentzVector TL_RecLepN_BoostedtoTbar;
  STLorentzVector TL_RecNeubar_BoostedtoTbar;

  //_____Boost to ttbar_____
  STLorentzVector TL_RecT_Boostedtottbar;
  STLorentzVector TL_RecTbar_Boostedtottbar;

  //_____angles______
  double D_RecCos_LepP_T_BoostedtoT;
  double D_RecCos_Neu_T_BoostedtoT;
  double D_RecCos_B_T_BoostedtoT;
  double D_RecCos_LepN_Tbar_BoostedtoTbar;
  double D_RecCos_Neubar_Tbar_BoostedtoTbar;
  double D_RecCos_Bbar_Tbar_BoostedtoTbar;
  double D_RecCos_LepP_B_BoostedtoWp;
  double D_RecCos_LepN_Bbar_BoostedtoWn;

   //_____Boost to W+-_____
  STLorentzVector TL_RecB_BoostedtoWp;
  STLorentzVector TL_RecLepP_BoostedtoWp;
  STLorentzVector TL_RecNeu_BoostedtoWp;
  STLorentzVector TL_RecBbar_BoostedtoWn;
  STLorentzVector TL_RecLepN_BoostedtoWn;
  STLorentzVector TL_RecNeubar_BoostedtoWn;

  // TLorentzVectorWFlags elements - AINDA FALTA IMPLEMENTAR ISTO
  // double TLWF_LeptonVec[5] = {-1, -1, 999, -1, -1};
  // double TLWF_MyGoodJetVec[5] = {-1, -1, 999, -1, -1};

  STVector3 TLWF_Vtx[20];
  int TLWF_Vtx_size;

  STLorentzVectorWFlags TLWF_JetVec[20];
  int TLWF_JetVec_size;

  STLorentzVectorWFlags TLWF_LeptonVec[20];
  int TLWF_LeptonVec_size;

  STLorentzVectorWFlags TLWF_MyGoodJetVec[20];
  int TLWF_MyGoodJetVec_size;

  STLorentzVectorWFlags TLWF_MyGoodBtaggedJetVec[20];
  int TLWF_MyGoodBtaggedJetVec_size;

  STLorentzVectorWFlags TLWF_MyGoodNonBtaggedJetVec[20];
  int TLWF_MyGoodNonBtaggedJetVec_size;
} HEPEvent_Offload_GPU;

#endif
