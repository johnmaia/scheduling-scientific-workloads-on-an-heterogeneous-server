#ifndef myvector_h
#define myvector_h

typedef struct myvect {
  double bpz;
  double apx;
  double apy;
  double apz;

  __device__ void SetaPzPxPyPz(double px, double py, double pz, double npz){apx = px; apy = py; apz = pz; bpz = npz;}

	inline __device__ double aPz() const {return bpz;};
	inline __device__ double Px() const {return apx;};
	inline __device__ double Py() const {return apy;};
	inline __device__ double Pz() const {return apz;};
} myvector;

#endif
