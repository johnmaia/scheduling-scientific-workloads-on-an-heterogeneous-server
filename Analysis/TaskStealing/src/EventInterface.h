// ###################################################################################
//
//							!!Disclaimer!!
//
// ###################################################################################
//
// Add variables in the EventData class, between the public and constructor statements
//
// ###################################################################################

#include <vector>
#include "TaskStealing_Event.h"

extern std::vector<HEPEvent> events;
extern long unsigned event_counter;

/*
 *	 defines below
 */


#define id events[event_counter].id
#define mcWeight events[event_counter].mcWeight
#define pileupWeight events[event_counter].pileupWeight
#define weight_btag_77 events[event_counter].weight_btag_77
#define weight_lept_eff events[event_counter].weight_lept_eff
#define eventNumber events[event_counter].eventNumber
#define runNumber events[event_counter].runNumber
#define mcChannelNumber events[event_counter].mcChannelNumber
#define mu events[event_counter].mu
#define el_pt events[event_counter].el_pt
#define el_eta events[event_counter].el_eta
#define el_phi events[event_counter].el_phi
#define el_e events[event_counter].el_e
#define el_charge events[event_counter].el_charge
#define el_miniiso events[event_counter].el_miniiso
#define mu_pt events[event_counter].mu_pt
#define mu_eta events[event_counter].mu_eta
#define mu_phi events[event_counter].mu_phi
#define mu_e events[event_counter].mu_e
#define mu_charge events[event_counter].mu_charge
#define mu_miniiso events[event_counter].mu_miniiso
#define jet_pt events[event_counter].jet_pt
#define jet_eta events[event_counter].jet_eta
#define jet_phi events[event_counter].jet_phi
#define jet_e events[event_counter].jet_e
#define jet_mv1 events[event_counter].jet_mv1
#define jet_mvb events[event_counter].jet_mvb
#define jet_mv1c events[event_counter].jet_mv1c
#define jet_mv2c00 events[event_counter].jet_mv2c00
#define jet_mv2c10 events[event_counter].jet_mv2c10
#define jet_mv2c20 events[event_counter].jet_mv2c20
#define jet_ip3dsv1 events[event_counter].jet_ip3dsv1
#define jet_jvt events[event_counter].jet_jvt
#define met_met events[event_counter].met_met
#define met_phi events[event_counter].met_phi
#define ee events[event_counter].ee
#define mumu events[event_counter].mumu
#define emu events[event_counter].emu
