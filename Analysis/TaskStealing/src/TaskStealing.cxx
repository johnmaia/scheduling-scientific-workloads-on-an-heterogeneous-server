#include "TaskStealing.h"
#include "EventInterface.h"
#include <boost/interprocess/ipc/message_queue.hpp>

using namespace std;
using namespace boost::interprocess;

extern vector<HEPEvent> events;
extern long unsigned event_counter;
#pragma omp threadprivate(event_counter)


// Use this constructor if you don't specify a ttree in the input file
TaskStealing::TaskStealing (vector<string> _root_file, string _tree_filename, string _output_root_file, string _signal, string _background, unsigned ncuts) : DataAnalysis (_root_file, _tree_filename, _output_root_file, _signal, _background, ncuts) {
	// Set to true if you want to save events that pass all cuts
	save_events = false;

	// Initialise your class variables here

}

// Use this constructor if you specify a ttree in the input file
TaskStealing::TaskStealing (vector<string> _root_file, string _tree_filename, string _output_root_file, string _signal, string _background, string _tree_name, unsigned ncuts) : DataAnalysis (_root_file, _tree_filename, _output_root_file, _signal, _background, _tree_name, ncuts) {
	// Set to true if you want to save events that pass all cuts
	save_events = false;

	// Initialise your class variables here

}


// Initialize any information before processing the events
void TaskStealing::initialize (void) {

}

// Finalize any calculations after processing all events
void TaskStealing::finalize (void) {

}

// Sample cut
bool cut1 (void) {
    float a = 2;

    for (unsigned i = 0; i < 1000000; i++)
        a = a * 1.5 + i;

    return true;
}

// Sample cut
bool cut2 (void) {
    float a = 2;
    for (unsigned i = 0; i < 1000000; i++)
        a = a * 1.5 + i;

    if (el_pt->size() > 1)
        return a > 2;
    else
        return a < 2;
}

// Sample cut
bool cut3 (void) {

    if (el_pt->at(0) > 70000)
        return true;
    else
        return false;
}

// Sample cut
bool cut4 (void) {
    float a = 2;
    for (unsigned i = 0; i < 1000000; i++)
        a = a * 1.5 + i;

    if (jet_pt->size() > 2)
        return true;
    else
        return false;
}

// Sample cut
bool cut5 (void) {
    if (jet_pt->at(0) > 50000)
        return true;
    else
        return false;
}

// Sample cut
bool cut6 (void) {
    if (jet_pt->at(1) > 50000)
        return true;
    else
        return false;
}

int main (int argc, char *argv[]) {
	map<string, string> options;
	ProgramOptions opts;
	vector<string> file_list;

	if (!opts.getOptions(argc, argv, options, file_list))
		return 0;

	size_t const max_msg_size = 0x100;

	try{
		//Erase previous message queue
		message_queue::remove("filelist_queue");

		//Create a message_queue.
		message_queue mq
										 (create_only               //only create
										 ,"filelist_queue"          //name
										 ,file_list.size()     //max message number
										 ,max_msg_size              //max message size
										 );


		// reads a specific amount of files allocated for each process
		for (int i = 0; i < file_list.size(); i++) {
			std::string s(file_list[i]);
			mq.send(s.data(), s.size(), 0);
		}
	}
	catch(interprocess_exception &ex){
    std::cout << ex.what() << std::endl;
  }



	#ifdef D_MPI
		MPI_Init(&argc, &argv);
	#endif

	unsigned number_of_cuts = 6;

	TaskStealing anl (file_list, options["record"], options["output"], options["signal"], options["background"], "nominal", number_of_cuts);

	srand(0);

	// Add the cuts using the addCut method
	anl.addCut("cut1", cut1);
	anl.addCut("cut2", cut2);
	anl.addCut("cut3", cut3);
	anl.addCut("cut4", cut4);
	anl.addCut("cut5", cut5);
	anl.addCut("cut6", cut6);

	anl.addCutDependency("cut1", "cut3");
	anl.addCutDependency("cut2", "cut3");
	anl.addCutDependency("cut3", "cut4");
	anl.addCutDependency("cut4", "cut5");
	anl.addCutDependency("cut5", "cut6");

	// Processes the analysis
	anl.run();

	#ifdef D_MPI
		MPI_Finalize();
	#endif

	boost::interprocess::message_queue::remove("filelist_queue");
	return 0;
}
