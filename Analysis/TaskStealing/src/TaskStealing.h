#ifndef TaskStealing_h
#define TaskStealing_h

#include <cstdlib>
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <omp.h>
#include <DataAnalysis.h>
#ifdef D_MPI
	#include <mpi.h>
#endif

using namespace std;


class TaskStealing : public DataAnalysis {
	// Variables to record per cut
	// Pdfs to record
	// Insert your class variables here


	void recordVariables (unsigned cut_number);
	void writeVariables (void);
	void recordPdfs (void);
	void initRecord (void);
	void writePdfs (void);
	void initialize (void);
	void finalize (void);

public:
	TaskStealing (vector<string> _root_file, string _tree_filename, string _output_root_file, string _signal, string _background, unsigned ncuts);
	TaskStealing (vector<string> _root_file, string _tree_filename, string _output_root_file, string _signal, string _background, string _tree_name, unsigned ncuts);
};

#endif
