#include "TaskStealing.h"

#define THIS_THREAD(x) thread_id * number_of_cuts + x

using namespace std;

extern vector<HEPEvent> events;
extern unsigned num_threads;
extern unsigned thread_id;
#pragma omp threadprivate(thread_id)
extern long unsigned event_counter;
#pragma omp threadprivate(event_counter)


// ***********************************************
// Method to record variables, do not edit
void TaskStealing::initRecord (void) {

}

void TaskStealing::recordVariables (unsigned cut_number) {

}

void TaskStealing::writeVariables (void) {

}

// Write here the variables and expressions to record per cut
#ifdef RecordVariables

#endif
// ***********************************************


// ***********************************************
// Method to record pdfs, do not edit
void TaskStealing::recordPdfs (void) {

}

void TaskStealing::writePdfs (void) {

}

// Write here the variables to record as pdf
#ifdef PdfVariables

#endif
// ***********************************************
