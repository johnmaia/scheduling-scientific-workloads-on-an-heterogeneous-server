#ifndef TaskStealing_Event_h
#define TaskStealing_Event_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <vector>
#include <TLorentzVector.h>

using namespace std;

// Fixed size dimensions of array or collections stored in the TTree if any.

class HEPEvent {
	TTree *fChain;

public:
	long id;

   Float_t         mcWeight;
   Float_t         pileupWeight;
   Float_t         weight_btag_77;
   Float_t         weight_lept_eff;
   UInt_t          eventNumber;
   UInt_t          runNumber;
   UInt_t          mcChannelNumber;
   Float_t         mu;
   vector<float>   *el_pt;
   vector<float>   *el_eta;
   vector<float>   *el_phi;
   vector<float>   *el_e;
   vector<float>   *el_charge;
   vector<float>   *el_miniiso;
   vector<float>   *mu_pt;
   vector<float>   *mu_eta;
   vector<float>   *mu_phi;
   vector<float>   *mu_e;
   vector<float>   *mu_charge;
   vector<float>   *mu_miniiso;
   vector<float>   *jet_pt;
   vector<float>   *jet_eta;
   vector<float>   *jet_phi;
   vector<float>   *jet_e;
   vector<float>   *jet_mv1;
   vector<float>   *jet_mvb;
   vector<float>   *jet_mv1c;
   vector<float>   *jet_mv2c00;
   vector<float>   *jet_mv2c10;
   vector<float>   *jet_mv2c20;
   vector<float>   *jet_ip3dsv1;
   vector<float>   *jet_jvt;
   Float_t         met_met;
   Float_t         met_phi;
   Int_t           ee;
   Int_t           mumu;
   Int_t           emu;

	// Add your own event variables here!



	HEPEvent (TTree *tree);
	~HEPEvent (void);
	bool init (void);
	int loadEvent (long entry);
	void write (void);
};
#endif

