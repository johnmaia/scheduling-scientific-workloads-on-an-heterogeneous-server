#ifndef TTHmic_Event17_h
#define TTHmic_Event17_h

#pragma offload_attribute (push, _Cilk_shared)
#include <vector>
#include "TLorentzVectorWFlags.h"
#include <offload.h>
#pragma offload_attribute (pop)

using namespace std;

typedef vector<TLorentzVectorWFlags, __offload::shared_allocator<TLorentzVectorWFlags> > shared_vec_TLorentzVectorWFlags;


class _Cilk_shared HEPEvent_17 {
  public:

    double L_Hz;
    double L_MissPx;
    double L_MissPy;

    shared_vec_TLorentzVectorWFlags * L_LeptonVec;
    shared_vec_TLorentzVectorWFlags * L_MyGoodJetVec;

    // std::vector<TLorentzVectorWFlags> L_LeptonVec;
    // std::vector<TLorentzVectorWFlags> L_MyGoodJetVec;

    //_____objects_____ ttDilepKinFit
    HEP_TLorentzVector L_RecT;
    HEP_TLorentzVector L_RecB;
    HEP_TLorentzVector L_RecWp;
    HEP_TLorentzVector L_RecLepP;
    HEP_TLorentzVector L_RecNeu;
    HEP_TLorentzVector L_RecTbar;
    HEP_TLorentzVector L_RecBbar;
    HEP_TLorentzVector L_RecWn;
    HEP_TLorentzVector L_RecLepN;
    HEP_TLorentzVector L_RecNeubar;
    HEP_TLorentzVector L_RecTTbar;
    HEP_TLorentzVector L_RecHiggs;
    HEP_TLorentzVector L_RecHiggsB1;
    HEP_TLorentzVector L_RecHiggsB2;
    HEP_TLorentzVector L_Neutrino;
    HEP_TLorentzVector L_Antineutrino;

    double L_RecMassHiggsJet1;
    double L_RecMassHiggsJet2;
    double L_RecProbTotal_ttH;
    //_____Boost to top_____
    HEP_TLorentzVector L_RecB_BoostedtoT;
    HEP_TLorentzVector L_RecWp_BoostedtoT;
    HEP_TLorentzVector L_RecLepP_BoostedtoT;
    HEP_TLorentzVector L_RecNeu_BoostedtoT;
    HEP_TLorentzVector L_RecBbar_BoostedtoTbar;
    HEP_TLorentzVector L_RecWn_BoostedtoTbar;
    HEP_TLorentzVector L_RecLepN_BoostedtoTbar;
    HEP_TLorentzVector L_RecNeubar_BoostedtoTbar;
     //_____Boost to ttbar_____
    HEP_TLorentzVector L_RecT_Boostedtottbar;
    HEP_TLorentzVector L_RecTbar_Boostedtottbar;
     //_____angles______
    double L_RecCos_LepP_T_BoostedtoT;
    double L_RecCos_Neu_T_BoostedtoT;
    double L_RecCos_B_T_BoostedtoT;
    double L_RecCos_LepN_Tbar_BoostedtoTbar;
    double L_RecCos_Neubar_Tbar_BoostedtoTbar;
    double L_RecCos_Bbar_Tbar_BoostedtoTbar;
    double L_RecCos_LepP_B_BoostedtoWp;
    double L_RecCos_LepN_Bbar_BoostedtoWn;
     //_____Boost to W+-_____
    HEP_TLorentzVector L_RecB_BoostedtoWp;
    HEP_TLorentzVector L_RecLepP_BoostedtoWp;
    HEP_TLorentzVector L_RecNeu_BoostedtoWp;
    HEP_TLorentzVector L_RecBbar_BoostedtoWn;
    HEP_TLorentzVector L_RecLepN_BoostedtoWn;
    HEP_TLorentzVector L_RecNeubar_BoostedtoWn;

    HEPEvent_17 ();
    ~HEPEvent_17 ();

    // Function to allocate shared vectors
    void allocSharedVectors(void){
      L_LeptonVec = new (_Offload_shared_malloc(sizeof(vector<TLorentzVectorWFlags>))) _Cilk_shared vector<TLorentzVectorWFlags, __offload::shared_allocator<TLorentzVectorWFlags> >(0);

      L_MyGoodJetVec = new (_Offload_shared_malloc(sizeof(vector<TLorentzVectorWFlags>))) _Cilk_shared vector<TLorentzVectorWFlags, __offload::shared_allocator<TLorentzVectorWFlags> >(0);
  	}

    // Functions to free shared vectors
    void freeSharedVectors(void){
      _Offload_shared_free(L_LeptonVec);
      _Offload_shared_free(L_MyGoodJetVec);
    }

    // Functions access elements allocated vectors
    TLorentzVectorWFlags getLeptonFromVec(int i){ return (*L_LeptonVec)[i]; }
    TLorentzVectorWFlags getMyGoodJetFromVec(int i){ return (*L_MyGoodJetVec)[i]; }

    // Auxiliar functions on allocated vectors
    int getLeptonVecSize(void){ return (*L_LeptonVec).size(); }
    void pushLeptonToVec(TLorentzVectorWFlags lep){ (*L_LeptonVec).push_back(lep); }

    int getMyGoodJetVecSize(void){ return (*L_MyGoodJetVec).size(); }
    void pushMyGoodJetToVec(TLorentzVectorWFlags mgj){ (*L_MyGoodJetVec).push_back(mgj); }
};
#endif
