#ifndef TTHmic_EventBRANCHES_h
#define TTHmic_EventBRANCHES_h


   TBranch        *b_runNumber;   //!
   TBranch        *b_eventNumber;   //!
   TBranch        *b_channelNumber;   //!
   TBranch        *b_rndRunNumber;   //!
   TBranch        *b_dataPeriod;   //!
   TBranch        *b_mu;   //!
   TBranch        *b_mcWeight;   //!
   TBranch        *b_eventWeight_PRETAG;   //!
   TBranch        *b_eventWeight_BTAG;   //!
   TBranch        *b_pvxp_n;   //!
   TBranch        *b_hfor;   //!
   TBranch        *b_vxp_z;   //!
   TBranch        *b_mc_vxp_z;   //!
   TBranch        *b_mcevt_pdf1;   //!
   TBranch        *b_mcevt_pdf2;   //!
   TBranch        *b_mcevt_pdf_id1;   //!
   TBranch        *b_mcevt_pdf_id2;   //!
   TBranch        *b_mcevt_pdf_scale;   //!
   TBranch        *b_mcevt_pdf_x1;   //!
   TBranch        *b_mcevt_pdf_x2;   //!
   TBranch        *b_m_scaleFactor_PILEUP;   //!
   TBranch        *b_m_scaleFactor_ELE;   //!
   TBranch        *b_m_scaleFactor_MUON;   //!
   TBranch        *b_m_scaleFactor_DILEP;   //!
   TBranch        *b_m_scaleFactor_BTAG;   //!
   TBranch        *b_m_scaleFactor_WJETSNORM;   //!
   TBranch        *b_m_scaleFactor_WJETSSHAPE;   //!
   TBranch        *b_m_scaleFactor_JVFSF;   //!
   TBranch        *b_m_scaleFactor_ZVERTEX;   //!
   TBranch        *b_m_scaleFactor_ALLPRETAG;   //!
   TBranch        *b_m_scaleFactor_ALLBTAG;   //!
   TBranch        *b_truE;   //!
   TBranch        *b_truM;   //!
   TBranch        *b_trigE;   //!
   TBranch        *b_trigM;   //!
   TBranch        *b_passGRL;   //!
   TBranch        *b_cosmicEvent;   //!
   TBranch        *b_isOS;   //!
   TBranch        *b_hasGoodVertex;   //!
   TBranch        *b_ht;   //!
   TBranch        *b_mass;   //!
   TBranch        *b_massInv_LL;   //!
   TBranch        *b_massInv_BQQ;   //!
   TBranch        *b_massTrans_BLMet;   //!
   TBranch        *b_massTrans_LMet;   //!
   TBranch        *b_flag_DL;   //!
   TBranch        *b_flag_TTZ;   //!
   TBranch        *b_channel_DL;   //!
   TBranch        *b_channel_TTZ;   //!
   TBranch        *b_scaledWeight;   //!
   TBranch        *b_lep_n;   //!
   TBranch        *b_lep_truthMatched;   //!
   TBranch        *b_lep_trigMatched;   //!
   TBranch        *b_lep_pt;   //!
   TBranch        *b_lep_eta;   //!
   TBranch        *b_lep_phi;   //!
   TBranch        *b_lep_E;   //!
   TBranch        *b_lep_z0;   //!
   TBranch        *b_lep_charge;   //!
   TBranch        *b_lep_isTight;   //!
   TBranch        *b_lep_type;   //!
   TBranch        *b_lep_flag;   //!
   TBranch        *b_el_cl_eta;   //!
   TBranch        *b_lep_ptcone30;   //!
   TBranch        *b_lep_etcone20;   //!
   TBranch        *b_massTrans_LMet_Vec;   //!
   TBranch        *b_lepPair_n;   //!
   TBranch        *b_isSameFlavor_LL_Vec;   //!
   TBranch        *b_isOppSign_LL_Vec;   //!
   TBranch        *b_massInv_LL_Vec;   //!
   TBranch        *b_met_sumet;   //!
   TBranch        *b_met_et;   //!
   TBranch        *b_met_phi;   //!
   TBranch        *b_jet_n;   //!
   TBranch        *b_alljet_n;   //!
   TBranch        *b_jet_pt;   //!
   TBranch        *b_jet_eta;   //!
   TBranch        *b_jet_phi;   //!
   TBranch        *b_jet_E;   //!
   TBranch        *b_jet_jvf;   //!
   TBranch        *b_jet_trueflav;   //!
   TBranch        *b_jet_truthMatched;   //!
   TBranch        *b_jet_SV0;   //!
   TBranch        *b_jet_MV1;   //!

#endif
