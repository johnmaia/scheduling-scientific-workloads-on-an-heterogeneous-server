#ifndef TTHmic_Event_h
#define TTHmic_Event_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <vector>
#include "TLorentzVectorWFlags.h"

using namespace std;

// Fixed size dimensions of array or collections stored in the TTree if any.

class HEPEvent {
	TTree *fChain;

public:
	long id;

   Int_t           runNumber;
   Int_t           eventNumber;
   Int_t           channelNumber;
   Int_t           rndRunNumber;
   Int_t           dataPeriod;
   Float_t         mu;
   Float_t         mcWeight;
   Float_t         eventWeight_PRETAG;
   Float_t         eventWeight_BTAG;
   Int_t           pvxp_n;
   Int_t           hfor;
   Float_t         vxp_z;
   Float_t         mc_vxp_z;
   Double_t        mcevt_pdf1;
   Double_t        mcevt_pdf2;
   Int_t           mcevt_pdf_id1;
   Int_t           mcevt_pdf_id2;
   Double_t        mcevt_pdf_scale;
   Double_t        mcevt_pdf_x1;
   Double_t        mcevt_pdf_x2;
   Float_t         scaleFactor_PILEUP;
   Float_t         scaleFactor_ELE;
   Float_t         scaleFactor_MUON;
   Float_t         scaleFactor_DILEP;
   Float_t         scaleFactor_BTAG;
   Float_t         scaleFactor_WJETSNORM;
   Float_t         scaleFactor_WJETSSHAPE;
   Float_t         scaleFactor_JVFSF;
   Float_t         scaleFactor_ZVERTEX;
   Float_t         scaleFactor_ALLPRETAG;
   Float_t         scaleFactor_ALLBTAG;
   Int_t           truE;
   Int_t           truM;
   Bool_t          trigE;
   Bool_t          trigM;
   Bool_t          passGRL;
   Bool_t          cosmicEvent;
   Bool_t          isOS;
   Bool_t          hasGoodVertex;
   Float_t         ht;
   Float_t         mass;
   Float_t         massInv_LL;
   Float_t         massInv_BQQ;
   Float_t         massTrans_BLMet;
   Float_t         massTrans_LMet;
   UInt_t          flag_DL;
   UInt_t          flag_TTZ;
   UInt_t          channel_DL;
   UInt_t          channel_TTZ;
   Float_t         scaledWeight;
   UInt_t          lep_n;
   Bool_t          lep_truthMatched[3];   //[lep_n]
   Bool_t          lep_trigMatched[3];   //[lep_n]
   Float_t         lep_pt[3];   //[lep_n]
   Float_t         lep_eta[3];   //[lep_n]
   Float_t         lep_phi[3];   //[lep_n]
   Float_t         lep_E[3];   //[lep_n]
   Float_t         lep_z0[3];   //[lep_n]
   Float_t         lep_charge[3];   //[lep_n]
   Bool_t          lep_isTight[3];   //[lep_n]
   UInt_t          lep_type[3];   //[lep_n]
   UInt_t          lep_flag[3];   //[lep_n]
   Float_t         el_cl_eta[3];   //[lep_n]
   Float_t         lep_ptcone30[3];   //[lep_n]
   Float_t         lep_etcone20[3];   //[lep_n]
   Float_t         massTrans_LMet_Vec[3];   //[lep_n]
   UInt_t          lepPair_n;
   Bool_t          isSameFlavor_LL_Vec[3];   //[lepPair_n]
   Bool_t          isOppSign_LL_Vec[3];   //[lepPair_n]
   Float_t         massInv_LL_Vec[3];   //[lepPair_n]
   Float_t         met_sumet;
   Float_t         met_et;
   Float_t         met_phi;
   UInt_t          jet_n;
   UInt_t          alljet_n;
   Float_t         jet_pt[14];   //[alljet_n]
   Float_t         jet_eta[14];   //[alljet_n]
   Float_t         jet_phi[14];   //[alljet_n]
   Float_t         jet_E[14];   //[alljet_n]
   Float_t         jet_jvf[14];   //[alljet_n]
   Int_t           jet_trueflav[14];   //[alljet_n]
   Int_t           jet_truthMatched[14];   //[alljet_n]
   Float_t         jet_SV0[14];   //[alljet_n]
   Float_t         jet_MV1[14];   //[alljet_n]

	// Add your own event variables here!
   double Weight;
   double Ht;
   double Hz;
   double Ht_Mini;
   double BTagCut;
   double PtCutJet;
   double MissPx;
   double MissPy;
   double MissPt;
   double massInv_LL_Mini;
   double Sphericity;
   double Aplanarity;
   double Planarity;

   int ElectronTrigger;
   int MuonTrigger;
   int Cosmic;
   int EleMuoOverlap;
   int JetCleanning;
   int jet_n_Mini;
   int NbtagJet;
   int ntruthlep;
   int ntruthele;
   int ntruthmu;
   int ntrutheletau;
   int ntruthmutau;
   int ntruthtau;
   int ntruthleptau;
   int TruthEleNumber;
   int TruthMuonNumber;
   int Isub;
   int LumiBlock;
   int RunNumber;
   int EveNumber;
   int HforFlag;
   int GoodRL;

   HEP_TLorentzVector ll;
   HEP_TLorentzVector llmiss;

   vector<HEP_TVector3> Vtx;
   vector<TLorentzVectorWFlags> LeptonVec;
   vector<TLorentzVectorWFlags> MyGoodJetVec;
   vector<TLorentzVectorWFlags> MyGoodBtaggedJetVec;
   vector<TLorentzVectorWFlags> MyGoodNonBtaggedJetVec;
   vector<TLorentzVectorWFlags> JetVec;
   //_____objects_____ ttDilepKinFit
   HEP_TLorentzVector RecT;
   HEP_TLorentzVector RecB;
   HEP_TLorentzVector RecWp;
   HEP_TLorentzVector RecLepP;
   HEP_TLorentzVector RecNeu;
   HEP_TLorentzVector RecTbar;
   HEP_TLorentzVector RecBbar;
   HEP_TLorentzVector RecWn;
   HEP_TLorentzVector RecLepN;
   HEP_TLorentzVector RecNeubar;
   HEP_TLorentzVector RecTTbar;
   HEP_TLorentzVector RecHiggs;
   HEP_TLorentzVector RecHiggsB1;
   HEP_TLorentzVector RecHiggsB2;
   HEP_TLorentzVector Neutrino;
   HEP_TLorentzVector Antineutrino;

   double RecMassHiggsJet1;
   double RecMassHiggsJet2;
   double RecProbTotal_ttH;
   //_____Boost to top_____
   HEP_TLorentzVector RecB_BoostedtoT;
   HEP_TLorentzVector RecWp_BoostedtoT;
   HEP_TLorentzVector RecLepP_BoostedtoT;
   HEP_TLorentzVector RecNeu_BoostedtoT;
   HEP_TLorentzVector RecBbar_BoostedtoTbar;
   HEP_TLorentzVector RecWn_BoostedtoTbar;
   HEP_TLorentzVector RecLepN_BoostedtoTbar;
   HEP_TLorentzVector RecNeubar_BoostedtoTbar;
     //_____Boost to ttbar_____
   HEP_TLorentzVector RecT_Boostedtottbar;
   HEP_TLorentzVector RecTbar_Boostedtottbar;
     //_____angles______
   double RecCos_LepP_T_BoostedtoT;
   double RecCos_Neu_T_BoostedtoT;
   double RecCos_B_T_BoostedtoT;
   double RecCos_LepN_Tbar_BoostedtoTbar;
   double RecCos_Neubar_Tbar_BoostedtoTbar;
   double RecCos_Bbar_Tbar_BoostedtoTbar;
   double RecCos_LepP_B_BoostedtoWp;
   double RecCos_LepN_Bbar_BoostedtoWn;
     //_____Boost to W+-_____
   HEP_TLorentzVector RecB_BoostedtoWp;
   HEP_TLorentzVector RecLepP_BoostedtoWp;
   HEP_TLorentzVector RecNeu_BoostedtoWp;
   HEP_TLorentzVector RecBbar_BoostedtoWn;
   HEP_TLorentzVector RecLepN_BoostedtoWn;
   HEP_TLorentzVector RecNeubar_BoostedtoWn;

   //bool _cut17;



	HEPEvent (TTree *tree);
	~HEPEvent (void);
	bool init (void);
	int loadEvent (long entry);
	void write (void);
};
#endif
