#ifndef TLorentzVectorWFlags_h
#define TLorentzVectorWFlags_h

#include <vector>
#include "HEP_TLorentzVector.h"

// #############################################################################
class TLorentzVectorWFlags : public HEP_TLorentzVector {

 public:
  //
  TLorentzVectorWFlags() ;
  TLorentzVectorWFlags(double px, double py, double pz, double E, int idx, int isb, double IsoDeltaR, int itruthMatch, int itrigMatch);
  TLorentzVectorWFlags(HEP_TLorentzVector v, int idx, int isb, double IsoDeltaR, int itruthMatch, int itrigMatch);
  TLorentzVectorWFlags(const TLorentzVectorWFlags& other);
  //
  ~TLorentzVectorWFlags();
  TLorentzVectorWFlags& operator=(const TLorentzVectorWFlags& other) ;

  inline void SetIsoDeltaR(double p_IsoDeltaR) {IsoDeltaR = p_IsoDeltaR;};

  int idx ;
  int isb ;
  double IsoDeltaR;
  int itruthMatch;
  int itrigMatch;

 private:

  //  ClassDef(TLorentzVectorWFlags,0)
};
#endif
