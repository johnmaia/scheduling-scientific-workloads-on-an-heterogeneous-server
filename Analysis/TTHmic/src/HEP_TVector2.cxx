#include "HEP_TVector2.h"

double const  kPI        = std::atan(1)*4;
double const  kTWOPI     = 2.*kPI;


//______________________________________________________________________________
HEP_TVector2::HEP_TVector2()
{
   //constructor
   fX = 0.;
   fY = 0.;
}

//______________________________________________________________________________
HEP_TVector2::HEP_TVector2(double *v)
{
   //constructor
   fX = v[0];
   fY = v[1];
}

//______________________________________________________________________________
HEP_TVector2::HEP_TVector2(double x0, double y0)
{
   //constructor
   fX = x0;
   fY = y0;
}

//______________________________________________________________________________
HEP_TVector2::~HEP_TVector2()
{
}

//______________________________________________________________________________
double HEP_TVector2::Mod() const
{
   // return modulo of this vector
   return std::sqrt(fX*fX+fY*fY);
}

//______________________________________________________________________________
HEP_TVector2 HEP_TVector2::Unit() const
{
   // return module normalized to 1
   return (Mod2()) ? *this/Mod() : HEP_TVector2();
}

//______________________________________________________________________________
double HEP_TVector2::Phi() const
{
   // return vector phi
   return (std::atan(1)*4)+std::atan2(-fY,-fX);
}

//______________________________________________________________________________
double HEP_TVector2::Phi_0_2pi(double x) {
   // (static function) returns phi angle in the interval [0,2*PI)
   if(std::isnan(x)){
      //gROOT->Error("HEP_TVector2::Phi_0_2pi","function called with NaN");
      return x;
   }
   while (x >= kTWOPI) x -= kTWOPI;
   while (x <     0.)  x += kTWOPI;
   return x;
}

//______________________________________________________________________________
double HEP_TVector2::Phi_mpi_pi(double x) {
   // (static function) returns phi angle in the interval [-PI,PI)m
   if(std::isnan(x)){
      //gROOT->Error("HEP_TVector2::Phi_mpi_pi","function called with NaN");
      return x;
   }
   while (x >= kPI) x -= kTWOPI;
   while (x < -kPI) x += kTWOPI;
   return x;
}

//______________________________________________________________________________
HEP_TVector2 HEP_TVector2::Rotate (double phi) const
{
   //rotation by phi
   return HEP_TVector2( fX*std::cos(phi)-fY*std::sin(phi), fX*std::sin(phi)+fY*std::cos(phi) );
}

//______________________________________________________________________________
void HEP_TVector2::SetMagPhi(double mag, double phi)
{
   //set vector using mag and phi
   double amag = std::abs(mag);
   fX = amag * std::cos(phi);
   fY = amag * std::sin(phi);
}
//______________________________________________________________________________
// void HEP_TVector2::Streamer(TBuffer &R__b)
// {
//    // Stream an object of class HEP_TVector2.
//
//    if (R__b.IsReading()) {
//       UInt_t R__s, R__c;
//       Version_t R__v = R__b.ReadVersion(&R__s, &R__c);
//       if (R__v > 2) {
//          R__b.ReadClassBuffer(HEP_TVector2::Class(), this, R__v, R__s, R__c);
//          return;
//       }
//       //====process old versions before automatic schema evolution
//       if (R__v < 2) TObject::Streamer(R__b);
//       R__b >> fX;
//       R__b >> fY;
//       R__b.CheckByteCount(R__s, R__c, HEP_TVector2::IsA());
//       //====end of old versions
//
//    } else {
//       R__b.WriteClassBuffer(HEP_TVector2::Class(),this);
//    }
// }

// void HEP_TVector2::Print(Option_t*)const
// {
//    //print vector parameters
//    Printf("%s %s (x,y)=(%f,%f) (rho,phi)=(%f,%f)",GetName(),GetTitle(),X(),Y(),
//                                           Mod(),Phi()*TMath::RadToDeg());
// }
