#ifndef HEP_TVector2_h
#define HEP_TVector2_h

#include <cmath>

class HEP_TVector2
{
//------------------------------------------------------------------------------
//  data members
//------------------------------------------------------------------------------
protected:

   double    fX;    // components of the vector
   double    fY;
//------------------------------------------------------------------------------
//  function members
//------------------------------------------------------------------------------
public:

   typedef double Scalar;   // to be able to use it with the ROOT::Math::VectorUtil functions

   HEP_TVector2 ();
   HEP_TVector2 (double *s);
   HEP_TVector2 (double x0, double y0);
   virtual ~HEP_TVector2();
                                        // ****** unary operators

   HEP_TVector2&       operator  = (HEP_TVector2 const & v);
   HEP_TVector2&       operator += (HEP_TVector2 const & v);
   HEP_TVector2&       operator -= (HEP_TVector2 const & v);
   double        operator *= (HEP_TVector2 const & v);
   HEP_TVector2&       operator *= (double s);
   HEP_TVector2&       operator /= (double s);

                                        // ****** binary operators

   friend HEP_TVector2       operator + (const HEP_TVector2&, const HEP_TVector2&);
   friend HEP_TVector2       operator + (const HEP_TVector2&, double  );
   friend HEP_TVector2       operator + (double  , const HEP_TVector2&);
   friend HEP_TVector2       operator - (const HEP_TVector2&, const HEP_TVector2&);
   friend HEP_TVector2       operator - (const HEP_TVector2&, double  );
   friend double       operator * (const HEP_TVector2&, const HEP_TVector2&);
   friend HEP_TVector2       operator * (const HEP_TVector2&, double  );
   friend HEP_TVector2       operator * (double  , const HEP_TVector2&);
   friend HEP_TVector2       operator / (const HEP_TVector2&, double  );
   friend double       operator ^ (const HEP_TVector2&, const HEP_TVector2&);

                                        // ****** setters
   void Set(const HEP_TVector2& v);
   void Set(double x0, double y0);
   void Set(float  x0, float  y0);

                                        // ****** other member functions

   double Mod2() const { return fX*fX+fY*fY; };
   double Mod () const;

   double Px()   const { return fX; };
   double Py()   const { return fY; };
   double X ()   const { return fX; };
   double Y ()   const { return fY; };

                                        // phi() is defined in [0,TWOPI]

   double Phi           () const;
   double DeltaPhi(const HEP_TVector2& v) const;
   void     SetMagPhi(double mag, double phi);

                                        // unit vector in the direction of *this

   HEP_TVector2 Unit() const;
   HEP_TVector2 Ort () const;

                                        // projection of *this to the direction
                                        // of HEP_TVector2 vector `v'

   HEP_TVector2 Proj(const HEP_TVector2& v) const;

                                        // component of *this normal to `v'

   HEP_TVector2 Norm(const HEP_TVector2& v) const;

                                        // rotates 2-vector by phi radians
   HEP_TVector2 Rotate (double phi) const;

                                        // returns phi angle in the interval [0,2*PI)
   static double Phi_0_2pi(double x);                                                                               // returns phi angle in the interval
                                        // returns phi angle in the interval [-PI,PI)
   static double Phi_mpi_pi(double x);


  //  void Print(Option_t* option="") const;
   //
  //  ClassDef(HEP_TVector2,3)  // A 2D physics vector

};

                                        // ****** unary operators

inline HEP_TVector2& HEP_TVector2::operator  = (HEP_TVector2 const& v) {fX  = v.fX; fY  = v.fY; return *this;}
inline HEP_TVector2& HEP_TVector2::operator += (HEP_TVector2 const& v) {fX += v.fX; fY += v.fY; return *this;}
inline HEP_TVector2& HEP_TVector2::operator -= (HEP_TVector2 const& v) {fX -= v.fX; fY -= v.fY; return *this;}

                                        // scalar product of 2 2-vectors

inline double   HEP_TVector2::operator *= (const HEP_TVector2& v) { return(fX*v.fX+fY*v.fY); }

inline HEP_TVector2& HEP_TVector2::operator *= (double s) { fX *=s; fY *=s; return *this; }
inline HEP_TVector2& HEP_TVector2::operator /= (double s) { fX /=s; fY /=s; return *this; }

                                        // ****** binary operators

inline HEP_TVector2  operator + (const HEP_TVector2& v1, const HEP_TVector2& v2) {
   return HEP_TVector2(v1.fX+v2.fX,v1.fY+v2.fY);
}

inline HEP_TVector2  operator + (const HEP_TVector2& v1, double bias) {
   return HEP_TVector2 (v1.fX+bias,v1.fY+bias);
}

inline HEP_TVector2  operator + (double bias, const HEP_TVector2& v1) {
   return HEP_TVector2 (v1.fX+bias,v1.fY+bias);
}

inline HEP_TVector2  operator - (const HEP_TVector2& v1, const HEP_TVector2& v2) {
   return HEP_TVector2(v1.fX-v2.fX,v1.fY-v2.fY);
}

inline HEP_TVector2  operator - (const HEP_TVector2& v1, double bias) {
   return HEP_TVector2 (v1.fX-bias,v1.fY-bias);
}

inline HEP_TVector2  operator * (const HEP_TVector2& v, double s) {
   return HEP_TVector2 (v.fX*s,v.fY*s);
}

inline HEP_TVector2    operator * (double s, const HEP_TVector2& v) {
   return HEP_TVector2 (v.fX*s,v.fY*s);
}

inline double operator * (const HEP_TVector2& v1, const HEP_TVector2& v2) {
   return  v1.fX*v2.fX+v1.fY*v2.fY;
}

inline HEP_TVector2     operator / (const HEP_TVector2& v, double s) {
   return HEP_TVector2 (v.fX/s,v.fY/s);
}

inline double   operator ^ (const HEP_TVector2& v1, const HEP_TVector2& v2) {
   return  v1.fX*v2.fY-v1.fY*v2.fX;
}

inline  double HEP_TVector2::DeltaPhi(const HEP_TVector2& v) const { return Phi_mpi_pi(Phi()-v.Phi()); }

inline  HEP_TVector2 HEP_TVector2::Ort () const { return Unit(); }

inline  HEP_TVector2 HEP_TVector2::Proj(const HEP_TVector2& v) const { return v*(((*this)*v)/v.Mod2()); }

inline  HEP_TVector2 HEP_TVector2::Norm(const HEP_TVector2& v) const {return *this-Proj(v); }

                                     // ****** setters

inline void HEP_TVector2::Set(const HEP_TVector2& v   )     { fX = v.fX; fY = v.fY; }
inline void HEP_TVector2::Set(double x0, double y0) { fX = x0  ; fY = y0 ;  }
inline void HEP_TVector2::Set(float  x0, float  y0)     { fX = x0  ; fY = y0 ;  }

#endif
