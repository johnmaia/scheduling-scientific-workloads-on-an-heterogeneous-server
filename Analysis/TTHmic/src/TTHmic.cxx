#include <TCanvas.h>
#include <TRandom3.h>
#include <TFile.h>

#include "TTHmic.h"
#include "TLorentzVectorWFlags.h"
#include "EventInterface.h"

#include "myvector.h"
#include "TTHmic_aux.h"


using namespace std;
using namespace boost::interprocess;

// Allocate memory to store information from the event
//extern int NBins[100];
//extern long unsigned event_counter;
static TRandom3 rnd;

// pdfs
TH1D *pdfKinFit[100];

TCanvas *mydummycanvas;

// int num_threads_aux 	= 1;
// int rArraySize				= 0;
// int rArraySizeLimit   = 0;
// int rArrayStackSize		= 0;
//
// float rArraySizeFactor 	= 0.6;
// int rArrayMultFactor		= 300;
// int observerFlag 	      = 1;
// int* rArrayOfThread;
//
// double* rArray[3];
// boost::lockfree::queue<int, boost::lockfree::capacity<10000>> rArrayStacks[3];

TTHmic::~TTHmic (void) {
}

// Use this constructor if you don't specify a ttree in the input file
TTHmic::TTHmic (vector<string> _root_file, string _tree_filename, string _output_root_file, string _signal, string _background, unsigned ncuts) : DataAnalysis (_root_file, _tree_filename, _output_root_file, _signal, _background, ncuts) {
	// Set to true if you want to save events that pass all cuts
	save_events = false;
	mydummycanvas=new TCanvas();
	// Initialise your class variables here
	// Set to true if you want to save events that pass all cuts
	save_events = false;
	// Initialise your class variables here
	TFile *fTruth = new TFile("main_ttH_8TeV.root");
	double IntPDF;

	for (unsigned n = 0; n < 2; n++) {
		if ( n == 0 ) pdfKinFit[n] = (TH1D*)fTruth->Get("sel00_pdf_pt__n1"); // 1st pdf: pT neutrino 1
		if ( n == 1 ) pdfKinFit[n] = (TH1D*)fTruth->Get("sel00_pdf_pt__n2"); // 2nd pdf: pT neutrino 2

		// normalize to unity and save histos
		IntPDF = pdfKinFit[n]->Integral();
		if ( IntPDF != 0 ) pdfKinFit[n]->Scale(1./IntPDF);

		// smooth histos and save them
  		pdfKinFit[n]->Smooth(3);

		NBins[n] = pdfKinFit[n]->GetNbinsX();
		pdfKinFitVec[n] = new _Cilk_shared double[NBins[n]+2];
		LowerEdge[n] = pdfKinFit[n]->GetBinLowEdge(1);
		UpperEdge[n] = pdfKinFit[n]->GetBinLowEdge(NBins[n]+1);
		Scale[n] = double(NBins[n])/(UpperEdge[n]-LowerEdge[n]);

		// Input pdfKinFitVec
		for (int bin = 0; bin < NBins[n]+2; bin++){
			pdfKinFitVec[n][bin] = pdfKinFit[n]->GetBinContent(bin);
		}
	}
	delete fTruth;
}

// Use this constructor if you specify a ttree in the input file
TTHmic::TTHmic (vector<string> _root_file, string _tree_filename, string _output_root_file, string _signal, string _background, string _tree_name, unsigned ncuts) : DataAnalysis (_root_file, _tree_filename, _output_root_file, _signal, _background, _tree_name, ncuts) {
	// Set to true if you want to save events that pass all cuts
	save_events = false;
	mydummycanvas=new TCanvas();
	// Initialise your class variables here
	// Set to true if you want to save events that pass all cuts
	save_events = false;
	// Initialise your class variables here
	TFile *fTruth = new TFile("main_ttH_8TeV.root");
	double IntPDF;

	for (unsigned n = 0; n < 2; n++) {
		if ( n == 0 ) pdfKinFit[n] = (TH1D*)fTruth->Get("sel00_pdf_pt__n1"); // 1st pdf: pT neutrino 1
		if ( n == 1 ) pdfKinFit[n] = (TH1D*)fTruth->Get("sel00_pdf_pt__n2"); // 2nd pdf: pT neutrino 2

		// normalize to unity and save histos
		IntPDF = pdfKinFit[n]->Integral();
		if ( IntPDF != 0 ) pdfKinFit[n]->Scale(1./IntPDF);

		// smooth histos and save them
  		pdfKinFit[n]->Smooth(3);

		NBins[n] = pdfKinFit[n]->GetNbinsX();
		pdfKinFitVec[n] = new _Cilk_shared double[NBins[n]+2];
		LowerEdge[n] = pdfKinFit[n]->GetBinLowEdge(1);
		UpperEdge[n] = pdfKinFit[n]->GetBinLowEdge(NBins[n]+1);
		Scale[n] = double(NBins[n])/(UpperEdge[n]-LowerEdge[n]);

		// Input pdfKinFitVec
		for (int bin = 0; bin < NBins[n]+2; bin++){
			pdfKinFitVec[n][bin] = pdfKinFit[n]->GetBinContent(bin);
		}
	}
	delete fTruth;
}

// Initialize any variables of the event
void TTHmic::initialize (void) {

}

// Finalize any calculations with variables of the event after the cut processing
void TTHmic::finalize (void) {

}

// void checkEnvironmentVariables(){
// 	char* MFenv = getenv("MF");
// 	char* SFenv = getenv("SF");
// 	char* NTenv = getenv("OMP_NUM_THREADS");
//
//   if(MFenv != NULL)
//     rArrayMultFactor = atoi(MFenv);
//
// 	if(SFenv != NULL)
// 		if((atof(SFenv) > 0) && (atof(SFenv) <= 1))
// 			rArraySizeFactor = atof(SFenv);
//
// 	 if(NTenv != NULL)
// 	 	num_threads_aux = atoi(NTenv);
// }
//
// int calcNumberOfIterations(int ttDKF_njets_val){
//   int niter = 0;
//   for (int j1=0; j1 < ttDKF_njets_val ; j1++){
//     for (int j2=0; j2 < ttDKF_njets_val ; j2++){
//       if (j1!=j2){
//         for (int j3=0; j3 < ttDKF_njets_val-1 ; j3++){
//           if ((j3!=j1) && (j3!=j2)){
//             for (int j4=j3+1; j4 < ttDKF_njets_val ; j4++){
//               if ((j4!=j1) && (j4!=j2)){
//                 niter++;
//               }
//             }
//           }
//         }
//       }
//     }
//   }
// return niter;
// }
//
// void initrArrayOfThread(){
// 	rArrayOfThread = (int*) malloc(sizeof(int*) * num_threads_aux);
// 	for(unsigned int i = 0; i < num_threads_aux; i++){
// 		rArrayOfThread[i] = -1;
// 	}
// }
//
// int getNextnArray(int nArray){
// 	if(nArray >= 2){
// 		return 0;
// 	} else {
// 		return nArray+1;
// 	}
// }
//
// int getPreviousnArray(int nArray){
// 	if(nArray == 0){
// 		return 2;
// 	} else {
// 		return nArray-1;
// 	}
// }
//
// int checkIfThreadinArray(int nArray){
// 	int tnr = 0;
// 	if(rArrayStacks[nArray].empty()){
// 		for(unsigned int i = 0; i < num_threads_aux; i++){
// 			if((rArrayOfThread[i] == nArray)) return 1;
// 		}
// 	} else return 1;
// 	// If no thread is currently running then return a false positive
// 	//std::cout << "[CITIA] tnr:" << tnr << std::endl;
// 	return 0;
// }
//
// void randomArrayObserver(){
// 	int nArray = 1;
// 	int nextStack = 0;
// 	double * auxArray;
//
// 	for(unsigned int i = 0; i < 3; ++i){
// 		rArray[i] = NULL;
// 	}
//
// 	// set PRN seed
// 	rnd.SetSeed(0);
//
// 	// Compute sizes
// 	rArraySize =	20 * rArrayMultFactor * dilep_iterations * calcNumberOfIterations(4);
// 	rArraySizeLimit = rArraySize  * rArraySizeFactor;
// 	rArrayStackSize = 20 * dilep_iterations * calcNumberOfIterations(4);
//
// 	// Allocate space for first array
// 	while(!rArray[0]){
// 		rArray[0] = (double *)malloc(sizeof(double*) * rArraySize);
// 		//if(!rArray[0]) cout << "[OBS] Failed to allocate space for first Array" << endl;
// 	}
// 	//cout << "[OBS] num_threads: " << num_threads_aux << endl;
// 	//cout << "[OBS] First array allocated" << endl;
//
// 	// Fill initial array
// 	for(unsigned int i = 0; i < rArraySize; ++i){
// 		rArray[0][i] = 1 + rnd.Gaus(0., 0.02);
// 		if(i == nextStack){
// 			rArrayStacks[0].push(i);
// 			nextStack+=rArrayStackSize;
// 		}
// 	}
//
// 	//cout << "[OBS] First array filled" << endl;
//
// 	// Allocate space for second array
// 	while(!rArray[1]){
// 		rArray[1] = (double *)malloc(sizeof(double*) * rArraySize);
// 		//if(!rArray[1]) cout << "[OBS] Failed to allocate space for second Array" << endl;
// 	}
//
// 	//cout << "[OBS] Second array allocated" << endl;
//
// 	nextStack = 0;
// 	// Fill second array
// 	for(unsigned int i = 0; i < rArraySize; ++i){
// 		rArray[1][i] = 1 + rnd.Gaus(0., 0.02);
// 		if(i == nextStack){
// 			rArrayStacks[1].push(i);
// 			nextStack+=rArrayStackSize;
// 		}
// 	}
//
// 	//cout << "[OBS] Second array filled" << endl;
//
// 	while(observerFlag){
// 		// Prepare a new array
// 		//cout << "[OBS] Current Array: " << nArray << " | Previous array: " << getPreviousnArray(nArray) << endl;
// 		if(!checkIfThreadinArray(getPreviousnArray(nArray))){
//
// 			//cout << "[OBS] No thread on array: " << getPreviousnArray(nArray) << ". Going to free it." << endl;
// 			free(rArray[getPreviousnArray(nArray)]);
//
// 			rArray[getPreviousnArray(nArray)] = NULL;
//
// 			//cout << "[OBS] Previous array: " << getPreviousnArray(nArray) << " free" << endl;
//
// 			nArray = getNextnArray(nArray);
//
// 			// Allocate next array
// 			while(!rArray[nArray]){
// 				rArray[nArray] = (double *)malloc(sizeof(double*) * rArraySize);
// 				// if(!rArray[nArray]) cout << "[OBS] Failed to allocate space for the next Array" << endl;
// 			}
//
// 			//cout << "[OBS] Next array allocated : " << nArray << endl;
//
// 			nextStack = 0;
//
// 			// Fill next array
// 			for(unsigned int i = 0; i < rArraySize; ++i){
// 				rArray[nArray][i] = 1 + rnd.Gaus(0., 0.02);
// 				if(i == nextStack){
// 					rArrayStacks[nArray].push(i);
// 					nextStack+=rArrayStackSize;
// 				}
// 			}
// 			// cout << "[OBS] Next array filled : " << nArray << endl;
// 			// cout << "[OBS] Next array filled : " << nArray << endl;
// 		}
// 	}
// }


double rand_normal(double mean, double stddev)
{//Box muller method
    double n2 = 0.0;
    int n2_cached = 0;
    if (!n2_cached)
    {
        double x, y, r;
        do
        {
            x = 2.0*rand()/RAND_MAX - 1;
            y = 2.0*rand()/RAND_MAX - 1;

            r = x*x + y*y;
        }
        while (r == 0.0 || r > 1.0);
        {
            double d = sqrt(-2.0*log(r)/r);
            double n1 = x*d;
            n2 = y*d;
            double result = n1*stddev + mean;
            n2_cached = 1;
            return result;
        }
    }
    else
    {
        n2_cached = 0;
        return n2*stddev + mean;
    }
}

//myrunnumber != 105200
void Calculations(void){
	NbtagJet=0;

	MyGoodJetVec.clear();
	// __AO 18 Outubro_______________________________
	MyGoodBtaggedJetVec.clear();
	MyGoodNonBtaggedJetVec.clear();
	// __AO 18 Outubro_______________________________

	for( unsigned i =0; i<JetVec.size(); i++){
		if(JetVec[i].Pt()> PtCutJet && fabs(JetVec[i].Eta())<EtaCutJet ){
			MyGoodJetVec.push_back(JetVec[i]);
			if(abs(JetVec[i].isb) == 5) {
				NbtagJet++;
				MyGoodBtaggedJetVec.push_back(JetVec[i]);
			}
			if(abs(JetVec[i].isb) != 5) {
				MyGoodNonBtaggedJetVec.push_back(JetVec[i]);
			}
		}
	}


	// get number of truth leptons
	ntruthlep    = 0;
	ntruthele    = 0;
	ntruthmu     = 0;
	ntruthtau    = 0;
	ntrutheletau = 0;
	ntruthmutau  = 0;
	ntruthleptau = 0;


	ntruthele    = TruthEleNumber; 	// nTuple Variable
	ntruthmu     = TruthMuonNumber; // nTuple Variable

	ntruthlep = ntruthele + ntruthmu + ntruthleptau;

	// Ht from Minintuple
	Ht = Ht_Mini;

	// Lepton Lorentz vectors reconstruction
	if(LeptonVec.size() > 1)
	{
		ll = LeptonVec[0] + LeptonVec[1];

		llmiss.SetPxPyPzE(ll.Px() + MissPx, ll.Py() + MissPy, 0., ll.E() + MissPt);
	}
	else
	{
		ll.SetPxPyPzE(0., 0., 0., 0.);

		llmiss.SetPxPyPzE(0., 0., 0., 0.);
	}


	// Hz calculation
	Hz = 0.;
	for(unsigned i = 0; i<LeptonVec.size(); i++)     Hz = Hz+LeptonVec[i].Pz();
	for (unsigned i = 0; i<MyGoodJetVec.size(); i++) Hz = Hz+MyGoodJetVec[i].Pz();
}

bool LorentzVecComp (TLorentzVectorWFlags a, TLorentzVectorWFlags b) {
	return a.Pt() > b.Pt();
}

void LeptonIsolationDeltaR(void){
	double IsoDeltaR;
	double tmp;

	for(unsigned i = 0; i < LeptonVec.size(); ++i){
		IsoDeltaR = 999.;
		tmp = 999.;

		for(unsigned j = 0; j < JetVec.size(); j++) {
			tmp = JetVec[j].DeltaR(LeptonVec[i]);
			if( tmp < IsoDeltaR )
				IsoDeltaR = tmp;
		}

		for(unsigned j = 0; j < LeptonVec.size(); j++) {
			tmp = LeptonVec[j].DeltaR(LeptonVec[i]);
			if( tmp < IsoDeltaR && j != i )
				IsoDeltaR = tmp;
		}
	}
}

void JetIsolationDeltaR(void){
	double IsoDeltaR;
	double tmp;

	for (unsigned i = 0; i < JetVec.size(); ++i){
		IsoDeltaR = 999.;
		tmp = 999.;

		for (unsigned j = 0; j < JetVec.size(); j++) {
			tmp = JetVec[j].DeltaR(JetVec[i]);
			if (tmp < IsoDeltaR && j != i)
				IsoDeltaR = tmp;
		}

		for (unsigned j = 0; j < LeptonVec.size(); j++) {
			tmp = LeptonVec[j].DeltaR(JetVec[i]);
			if (tmp < IsoDeltaR)
				IsoDeltaR = tmp;
		}

		JetVec[i].SetIsoDeltaR(IsoDeltaR);
	}
}

void FillObjects(void) {
	bool FillJets      = true;
	bool FillMuons     = true;
	bool FillElectrons = true;


  // reset output
  std::vector<TLorentzVectorWFlags> use_Photon_Vec;
  std::vector<TLorentzVectorWFlags> use_Electron_Vec;
  std::vector<TLorentzVectorWFlags> use_Muon_Vec;
  std::vector<TLorentzVectorWFlags> use_Jet_Vec;

  // usefull variables
  int jets25inevent=0;

  // auxiliar stuff
  int kf = 0;
  //TLorentzVector Good;
	HEP_TLorentzVector Good;

  // ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
  // electrons
  // ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

  //For 2011:  https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/TopCommonObjects2011#Electrons
  double elpT, elEta, elPhi, elEne;
  int elTruthMatch, elTrigMatch;

  // Loop over all electrons.
  if (FillElectrons) for (unsigned k=0; k<lep_n; ++k){
    // ------------------------------check it is an electron
    if ( fabs(lep_type[k])  != 11 )  continue;
    // ------------------------------accept electron
    elpT   = lep_pt[k];
    elEta  = lep_eta[k];
    elPhi  = lep_phi[k];
    elEne  = lep_E[k];
    Good.SetPtEtaPhiE(elpT, elEta, elPhi, elEne);
    if ( lep_charge[k]>0 ) kf = -11; else kf = 11;
    // truth and trigger match
    elTruthMatch = 0;
    if ( lep_truthMatched[k] ) elTruthMatch = 1 ;
    elTrigMatch  = 0;
    if ( lep_trigMatched[k] )  elTrigMatch  = 1 ;
    // creat TLorentzWFlags vector
    TLorentzVectorWFlags GoodWFlags(Good, k, kf, 999., elTruthMatch, elTrigMatch);
    use_Electron_Vec.push_back(GoodWFlags);
  }  // Fill Electrons done

  // ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
  // take vertex z information (assumed to be ok)
  // ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

  if( hasGoodVertex == 1 ){
    // ------------------------------accept vertex
    HEP_TVector3 v( -999., -999., vxp_z);
    Vtx.push_back(v);
  }


  // ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
  // jets
  // ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

  // for jet energy scale and resolution uncertainties:
  //double jetET;
  double jetpT, jetEta, jetPhi, jetEne;
  int jetTruthMatch;

  // Loop over all jets
  if (FillJets) for (unsigned k=0; k<jet_n; ++k)  {
    // ------------------------------if jets have MV1>0.795
    if ( BTagCut != 999  &&  jet_MV1[k] > BTagCut  ) kf = 5; else kf = 1;

    // ------------------------------accept jet
    jetpT   = jet_pt[k];
    jetEta  = jet_eta[k];
    jetPhi  = jet_phi[k];
    jetEne  = jet_E[k];
    Good.SetPtEtaPhiE(jetpT, jetEta, jetPhi, jetEne );
    // truth and trigger match
    jetTruthMatch = 0;
    if ( jet_truthMatched[k] == 1 ) jetTruthMatch = 1 ;
    // creat TLorentzWFlags vector
    TLorentzVectorWFlags GoodWFlags(Good, k, kf, 999., jetTruthMatch, -1);
    use_Jet_Vec.push_back(GoodWFlags);
  }  // Fill jets done


  // ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
  // muons
  // ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

  double mupT, muEta, muPhi, muEne;
  int muTruthMatch, muTrigMatch;

  // loop over all muons
  if (FillMuons) for (unsigned k=0; k<lep_n; ++k){

    // ------------------------------check it is an electron
    if ( fabs(lep_type[k])  != 13 )  continue;

    // ------------------------------accept muon
    mupT   = lep_pt[k];
    muEta  = lep_eta[k];
    muPhi  = lep_phi[k];
    muEne  = lep_E[k];
    if ( lep_charge[k]>0 ) kf = -13; else kf = 13;
    Good.SetPtEtaPhiE(mupT, muEta, muPhi, muEne);
    // truth and trigger match
    muTruthMatch = 0;
    if ( lep_truthMatched[k] ) muTruthMatch = 1 ;
    muTrigMatch  = 0;
    if ( lep_trigMatched[k] )  muTrigMatch  = 1 ;
    // creat TLorentzWFlags vector
    TLorentzVectorWFlags GoodWFlags(Good, k, kf, 999., muTruthMatch, muTrigMatch);
    use_Muon_Vec.push_back(GoodWFlags);

  }  // Fill muons done


  // ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
  // Fill objects
  // ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

   // Fill jets
   for (unsigned k=0; k< use_Jet_Vec.size(); ++k) {
        JetVec.push_back(use_Jet_Vec[k]);
        if ( use_Jet_Vec[k].Pt() > 25*GeV ) jets25inevent++;
   }

   // Fill electrons
   for (unsigned k=0; k< use_Electron_Vec.size(); ++k) {
    	LeptonVec.push_back(use_Electron_Vec[k]);
   }
   // Fill muons
   for (unsigned k=0; k< use_Muon_Vec.size();     ++k) {
	LeptonVec.push_back(use_Muon_Vec[k]);
   }
}

void FillAllVectors (void) {

	// fill vectors with particles

	FillObjects();

	// sort particles inside the vectors
	sort(LeptonVec.begin(), LeptonVec.end(), LorentzVecComp);
	sort(JetVec.begin(), JetVec.end(), LorentzVecComp);

	// isolation angles
	LeptonIsolationDeltaR();
	JetIsolationDeltaR();

	// other stuff to be filled only after nTuple->FillVectors is called
	Isub = channelNumber;
	LumiBlock = -999;
	RunNumber = runNumber;
	EveNumber = eventNumber;
	TruthEleNumber = truE;
	TruthMuonNumber = truM;
	if (cosmicEvent)
		Cosmic = 1;
	else
		Cosmic = 0;
	HforFlag = 0;
	massInv_LL_Mini = massInv_LL;
	jet_n_Mini = jet_n;
	EleMuoOverlap = 0;
	JetCleanning = 0;
	if ( passGRL )
		GoodRL = 1;
	else
		GoodRL = 0;
	// MissPt() must be called before MissPx() and MissPy()
	MissPt = met_et;
	MissPx = met_et * cos(met_phi) / GeV;
	MissPy = met_et * sin(met_phi) / GeV;
	Sphericity = 0;
	Aplanarity = 0;
	Planarity  = 0;

}


void initEvent (void) {
	FillAllVectors();
	PtCutJet  = -999.*GeV;
	TruthEleNumber = truE;
	TruthMuonNumber = truM;
	Ht_Mini = ht;
	Weight = mcWeight * Luminosity / mc_lum;
	BTagCut = 0.8119;

	if ( trigE )
		ElectronTrigger = 1;

	if ( trigM )
		MuonTrigger = 1;


	/*
	TMonteCarlo mc_tot(0, -1, 1, 1, "Total MC");
	MonteCarlo.push_back(mc_tot);

	// coisas montecarlo

	THisto histos;
	MonteCarlo[0].histo.push_back(histos);
	BookHistograms(MonteCarlo[0].histo[i_syst].histo);
	// prepare counters of events (all MC/data samples)
	for (int mc=0; mc<MonteCarlo.size(); ++mc) {
		MonteCarlo[mc].p_nSelEvt.push_back(std::vector<double>());
		MonteCarlo[mc].p_nSelWeightedEvt.push_back(std::vector<double>());
		MonteCarlo[mc].p_nSelWeightedEvtErr.push_back(std::vector<double>());
		for (int i=0; i <= MaxCuts; ++i) {
			MonteCarlo[mc].p_nSelEvt[i_syst].push_back(0.);
			MonteCarlo[mc].p_nSelWeightedEvt[i_syst].push_back(0.);
			MonteCarlo[mc].p_nSelWeightedEvtErr[i_syst].push_back(0.);
		}
	}*/
}

bool cut0 (void) {
	initEvent();
	Calculations();

	// _cut17 = false;

	// nao faz o calc2 porque lepsample == 23 e leptonsep == 0
	if(Weight == 0)
		return false;
	else
		return true;
}

bool cut1 (void) {
	// isdata == 0 por isso nao se considera o cut 0
	if (ElectronTrigger != 1 && MuonTrigger != 1)
		return false;
	else
		return true;
}

bool cut2 (void) {
	if(Cosmic)
		return false;
	else
		return true;
}

bool cut3 (void) {
	if (Vtx.size() == 0)
		return false;
	else
		return true;
}

bool cut4 (void) {
	if (LeptonVec.size() < 2)
		return false;

	int nele = 0;
	int nmuo = 0;
	for(unsigned i=0; i < LeptonVec.size(); i++){
		if (abs(LeptonVec[i].isb) == 11) nele = nele + 1;
		if (abs(LeptonVec[i].isb) == 13) nmuo = nmuo + 1;
	}
	if (nele < 1) return false;
	if (nmuo < 1) return false;

	return true;
}

bool cut5 (void) {
	if (LeptonVec.size() != 2)
		return false;

	if((abs(LeptonVec[0].isb) != 11 || abs(LeptonVec[1].isb) != 13) && (abs(LeptonVec[0].isb) != 13 || abs(LeptonVec[1].isb) != 11) )
		return false;

	return true;
}

bool cut6 (void) {
	if (LeptonVec[0].Pt() < 25.*GeV)
		return false;
	else
		return true;
}

bool cut7 (void) {
	int HasElectronMatchingTrigger = 0;
	int HasMuonMatchingTrigger = 0;

	for (unsigned i=0; i < LeptonVec.size(); ++i) {
		int ii = LeptonVec[i].idx;
		// check leptons are trigger matched
		if (abs(LeptonVec[ii].isb) == 11 && LeptonVec[ii].itrigMatch == 1 ) HasElectronMatchingTrigger = 1;
		if (abs(LeptonVec[ii].isb) == 13 && LeptonVec[ii].itrigMatch == 1 ) HasMuonMatchingTrigger = 1;
	}

	bool ElectronTriggerOK = ( ElectronTrigger != 0 ) && ( HasElectronMatchingTrigger != 0 );
	bool     MuonTriggerOK = (     MuonTrigger != 0 ) && (     HasMuonMatchingTrigger != 0 );

	if ( !(ElectronTriggerOK || MuonTriggerOK) )
		return false;

	return true;

}

bool cut8 (void) {
	if (EleMuoOverlap !=0)
		return false;
	else
		return true;
}

bool cut9 (void) {
	if (JetCleanning != 0)
		return false;
	else
		return true;

}

bool cut10 (void) {
	if(Ht <= 130.*GeV)
		return false;
	else
		return true;
}

bool cut11 (void) {
	if (jet_n_Mini <= 2 )
		return false;
	else
		return true;

}

bool cut12 (void) {
	if (LeptonVec[0].isb * LeptonVec[1].isb >= 0.)
		return false;
	else
		return true;
}

bool cut13 (void) {
	if(ll.M()/GeV <= 15.)
		return false;
	else
		return true;
}

bool cut14 (void) {
	if (LeptonVec[0].itruthMatch != 1)
		return false;
	if (LeptonVec[1].itruthMatch != 1)
		return false;

	return true;
}

bool cut15 (void) {
	// cut 18 nao testa nada porque lepsample == 23
	if (NbtagJet < 1)
		return false;
	else
		return true;
}

bool cut16 (void) {
	if (NbtagJet != 2 && rnd.Rndm() < 0.4)
		return false;
	else
		return true;

}

bool cut17 (void) {

  ttDilepKinFit();

	return true;
}

int main (int argc, char *argv[]) {
	map<string, string> options;
	ProgramOptions opts;
	vector<string> file_list;

	// checkEnvironmentVariables();
	// initrArrayOfThread();
	// boost::thread t{randomArrayObserver};

	#ifdef D_MPI
		MPI_Init(&argc, &argv);
	#endif

	if (!opts.getOptions(argc, argv, options, file_list))
		return 0;

	size_t const max_msg_size = 0x100;

	// try{
		//Erase previous message queue
		message_queue::remove("filelist_queue");

		//Create a message_queue.
		message_queue mq
										 (create_only               //only create
										 ,"filelist_queue"          //name
										 ,file_list.size()     //max message number
										 ,max_msg_size              //max message size
										 );


		// reads a specific amount of files allocated for each process
		for (unsigned i = 0; i < file_list.size(); i++) {
			std::string s(file_list[i]);
			mq.send(s.data(), s.size(), 0);
		}
	//}
	// catch(interprocess_exception &ex){
	// 	std::cout << ex.what() << std::endl;
	// }

	unsigned number_of_cuts = 18;

	TTHmic anl (file_list, options["record"], options["output"], options["signal"], options["background"], "mini", number_of_cuts);

	// Add the cuts using the addCut method
	anl.addCut("cut0",  cut0);
	anl.addCut("cut1",  cut1);
	anl.addCut("cut2",  cut2);
	anl.addCut("cut3",  cut3);
	anl.addCut("cut4",  cut4);
	anl.addCut("cut5",  cut5);
	anl.addCut("cut6",  cut6);
	anl.addCut("cut7",  cut7);
	anl.addCut("cut8",  cut8);
	anl.addCut("cut9",  cut9);
	anl.addCut("cut10", cut10);
	anl.addCut("cut11", cut11);
	anl.addCut("cut12", cut12);
	anl.addCut("cut13", cut13);
	anl.addCut("cut14", cut14);
	anl.addCut("cut15", cut15);
	anl.addCut("cut16", cut16);
	anl.addCut("cut17", cut17);

	anl.addCutDependency("cut4", "cut5");
	anl.addCutDependency("cut4", "cut6");
	anl.addCutDependency("cut4", "cut7");
	anl.addCutDependency("cut4", "cut12");
	anl.addCutDependency("cut4", "cut14");

	anl.addCutDependency("cut0", "cut17");
	anl.addCutDependency("cut1", "cut17");
	anl.addCutDependency("cut2", "cut17");
	anl.addCutDependency("cut3", "cut17");
	anl.addCutDependency("cut4", "cut17");
	anl.addCutDependency("cut5", "cut17");
	anl.addCutDependency("cut6", "cut17");
	anl.addCutDependency("cut7", "cut17");
	anl.addCutDependency("cut8", "cut17");
	anl.addCutDependency("cut9", "cut17");
	anl.addCutDependency("cut10", "cut17");
	anl.addCutDependency("cut11", "cut17");
	anl.addCutDependency("cut12", "cut17");
	anl.addCutDependency("cut13", "cut17");
	anl.addCutDependency("cut14", "cut17");
	anl.addCutDependency("cut15", "cut17");
	anl.addCutDependency("cut16", "cut17");


	anl.addCutDependency("cut0", "cut1");
	anl.addCutDependency("cut0", "cut2");
	anl.addCutDependency("cut0", "cut3");
	anl.addCutDependency("cut0", "cut4");
	anl.addCutDependency("cut0", "cut8");
	anl.addCutDependency("cut0", "cut9");
	anl.addCutDependency("cut0", "cut10");
	anl.addCutDependency("cut0", "cut11");
	anl.addCutDependency("cut0", "cut13");
	anl.addCutDependency("cut0", "cut15");
	anl.addCutDependency("cut0", "cut16");


/*	anl.addCutDependency("cut0", "cut1");
	anl.addCutDependency("cut1", "cut2");
	anl.addCutDependency("cut2", "cut3");
	anl.addCutDependency("cut3", "cut4");
	anl.addCutDependency("cut4", "cut5");
	anl.addCutDependency("cut5", "cut6");
	anl.addCutDependency("cut6", "cut7");
	anl.addCutDependency("cut7", "cut8");
	anl.addCutDependency("cut8", "cut9");
	anl.addCutDependency("cut9", "cut10");
	anl.addCutDependency("cut10", "cut11");
	anl.addCutDependency("cut11", "cut12");
	anl.addCutDependency("cut12", "cut13");
	anl.addCutDependency("cut13", "cut14");
	anl.addCutDependency("cut14", "cut15");
	anl.addCutDependency("cut15", "cut16");
	anl.addCutDependency("cut16", "cut17");*/



	// Processes the analysis
	anl.run();
	#ifdef D_MPI
		MPI_Finalize();
	#endif
	// observerFlag = 0;
	boost::interprocess::message_queue::remove("filelist_queue");
	return 0;
}
