#include "HEP_TLorentzVector.h"
//ClassImp(HEP_TLorentzVector)

HEP_TLorentzVector::HEP_TLorentzVector()
               : fP(), fE(0.0) {}

HEP_TLorentzVector::HEP_TLorentzVector(double x, double y, double z, double t)
               : fP(x,y,z), fE(t) {}

HEP_TLorentzVector::HEP_TLorentzVector(const double * x0)
               : fP(x0), fE(x0[3]) {}

HEP_TLorentzVector::HEP_TLorentzVector(const float * x0)
               : fP(x0), fE(x0[3]) {}

HEP_TLorentzVector::HEP_TLorentzVector(const HEP_TVector3 & p, double e)
               : fP(p), fE(e) {}

HEP_TLorentzVector::HEP_TLorentzVector(const HEP_TLorentzVector & p)
               : fP(p.Vect()), fE(p.T()) {}

HEP_TLorentzVector::~HEP_TLorentzVector()  {}

double HEP_TLorentzVector::operator () (int i) const
{
   //dereferencing operator const
   switch(i) {
      case kX:
      case kY:
      case kZ:
         return fP(i);
      case kT:
         return fE;
      default:
         return fE;
         //Error("operator()()", "bad index (%d) returning 0",i);
   }
   return 0.;
}

double & HEP_TLorentzVector::operator () (int i)
{
   //dereferencing operator
   switch(i) {
      case kX:
      case kY:
      case kZ:
         return fP(i);
      case kT:
         return fE;
      default:
         return fE;
         //Error("operator()()", "bad index (%d) returning &fE",i);
   }
   return fE;
}

void HEP_TLorentzVector::Boost(double bx, double by, double bz)
{
   //Boost this Lorentz vector
   double b2 = bx*bx + by*by + bz*bz;
   double gamma = 1.0 / std::sqrt(1.0 - b2);
   double bp = bx*X() + by*Y() + bz*Z();
   double gamma2 = b2 > 0 ? (gamma - 1.0)/b2 : 0.0;

   SetX(X() + gamma2*bp*bx + gamma*bx*T());
   SetY(Y() + gamma2*bp*by + gamma*by*T());
   SetZ(Z() + gamma2*bp*bz + gamma*bz*T());
   SetT(gamma*(T() + bp));
}

double HEP_TLorentzVector::Rapidity() const
{
   //return rapidity
   return 0.5*std::log( (E()+Pz()) / (E()-Pz()) );
}

// HEP_TLorentzVector &HEP_TLorentzVector::operator *= (const TLorentzRotation & m)
// {
//    //multiply this Lorentzvector by m
//    return *this = m.VectorMultiplication(*this);
// }
//
// HEP_TLorentzVector &HEP_TLorentzVector::Transform(const TLorentzRotation & m)
// {
//    //Transform this Lorentzvector
//    return *this = m.VectorMultiplication(*this);
// }

// void HEP_TLorentzVector::Streamer(TBuffer &R__b)
// {
//    // Stream an object of class HEP_TLorentzVector.
//    double x, y, z;
//    UInt_t R__s, R__c;
//    if (R__b.IsReading()) {
//       Version_t R__v = R__b.ReadVersion(&R__s, &R__c);
//       if (R__v > 3) {
//          R__b.ReadClassBuffer(HEP_TLorentzVector::Class(), this, R__v, R__s, R__c);
//          return;
//       }
//       //====process old versions before automatic schema evolution
//       if (R__v != 2) TObject::Streamer(R__b);
//       R__b >> x;
//       R__b >> y;
//       R__b >> z;
//       fP.SetXYZ(x,y,z);
//       R__b >> fE;
//       R__b.CheckByteCount(R__s, R__c, HEP_TLorentzVector::IsA());
//    } else {
//       R__b.WriteClassBuffer(HEP_TLorentzVector::Class(),this);
//    }
// }


// //______________________________________________________________________________
// void HEP_TLorentzVector::Print(Option_t *) const
// {
//   // Print the TLorentz vector components as (x,y,z,t) and (P,eta,phi,E) representations
//   Printf("(x,y,z,t)=(%f,%f,%f,%f) (P,eta,phi,E)=(%f,%f,%f,%f)",
//     fP.x(),fP.y(),fP.z(),fE,
//     P(),Eta(),Phi(),fE);
// }
