#ifndef HEP_TLorentzVector_h
#define HEP_TLorentzVector_h

#include <vector>
#include <cmath>
#include "HEP_TVector2.h"
#include "HEP_TVector3.h"

class HEP_TLorentzVector{

private:

   HEP_TVector3 fP;  // 3 vector component
   double fE;  // time or energy of (x,y,z,t) or (px,py,pz,e)

public:

   typedef double Scalar;   // to be able to use it with the ROOT::Math::VectorUtil functions

   enum { kX=0, kY=1, kZ=2, kT=3, kNUM_COORDINATES=4, kSIZE=kNUM_COORDINATES };
   // Safe indexing of the coordinates when using with matrices, arrays, etc.

   HEP_TLorentzVector();

   HEP_TLorentzVector(double x, double y, double z, double t);
   // Constructor giving the components x, y, z, t.

   HEP_TLorentzVector(const double * carray);
   HEP_TLorentzVector(const float * carray);
   // Constructor from an array, not checked!

   HEP_TLorentzVector(const HEP_TVector3 & vector3, double t);
   // Constructor giving a 3-Vector and a time component.

   HEP_TLorentzVector(const HEP_TLorentzVector & lorentzvector);
   // Copy constructor.

   virtual ~HEP_TLorentzVector();
   // Destructor

   // inline operator HEP_TVector3 () const;
   // inline operator HEP_TVector3 & ();
   // Conversion (cast) to HEP_TVector3.

   inline double X() const;
   inline double Y() const;
   inline double Z() const;
   inline double T() const;
   // Get position and time.

   inline void SetX(double a);
   inline void SetY(double a);
   inline void SetZ(double a);
   inline void SetT(double a);
   // Set position and time.

   inline double Px() const;
   inline double Py() const;
   inline double Pz() const;
   inline double P()  const;
   inline double E()  const;
   inline double Energy() const;
   // Get momentum and energy.

   inline void SetPx(double a);
   inline void SetPy(double a);
   inline void SetPz(double a);
   inline void SetE(double a);
   // Set momentum and energy.

   inline HEP_TVector3 Vect() const ;
   // Get spatial component.

   inline void SetVect(const HEP_TVector3 & vect3);
   // Set spatial component.

   inline double Theta() const;
   inline double CosTheta() const;
   inline double Phi() const; //returns phi from -pi to pi
   inline double Rho() const;
   // Get spatial vector components in spherical coordinate system.

   inline void SetTheta(double theta);
   inline void SetPhi(double phi);
   inline void SetRho(double rho);
   // Set spatial vector components in spherical coordinate system.

   inline void SetPxPyPzE(double px, double py, double pz, double e);
   inline void SetXYZT(double  x, double  y, double  z, double t);
   inline void SetXYZM(double  x, double  y, double  z, double m);
   inline void SetPtEtaPhiM(double pt, double eta, double phi, double m);
   inline void SetPtEtaPhiE(double pt, double eta, double phi, double e);
   // Setters to provide the functionality (but a more meanigful name) of
   // the previous version eg SetV4... PsetV4...

   inline void GetXYZT(double *carray) const;
   inline void GetXYZT(float *carray) const;
   // Getters into an arry
   // no checking!

   double operator () (int i) const;
   inline double operator [] (int i) const;
   // Get components by index.

   double & operator () (int i);
   inline double & operator [] (int i);
   // Set components by index.

   inline HEP_TLorentzVector & operator = (const HEP_TLorentzVector &);
   // Assignment.

   inline HEP_TLorentzVector   operator +  (const HEP_TLorentzVector &) const;
   inline HEP_TLorentzVector & operator += (const HEP_TLorentzVector &);
   // Additions.

   inline HEP_TLorentzVector   operator -  (const HEP_TLorentzVector &) const;
   inline HEP_TLorentzVector & operator -= (const HEP_TLorentzVector &);
   // Subtractions.

   inline HEP_TLorentzVector operator - () const;
   // Unary minus.

   inline HEP_TLorentzVector operator * (double a) const;
   inline HEP_TLorentzVector & operator *= (double a);
   // Scaling with real numbers.

   inline bool operator == (const HEP_TLorentzVector &) const;
   inline bool operator != (const HEP_TLorentzVector &) const;
   // Comparisons.

   inline double Perp2() const;
   // Transverse component of the spatial vector squared.

   inline double Pt() const;
   inline double Perp() const;
   // Transverse component of the spatial vector (R in cylindrical system).

   inline void SetPerp(double);
   // Set the transverse component of the spatial vector.

   inline double Perp2(const HEP_TVector3 & v) const;
   // Transverse component of the spatial vector w.r.t. given axis squared.

   inline double Pt(const HEP_TVector3 & v) const;
   inline double Perp(const HEP_TVector3 & v) const;
   // Transverse component of the spatial vector w.r.t. given axis.

   inline double Et2() const;
   // Transverse energy squared.

   inline double Et() const;
   // Transverse energy.

   inline double Et2(const HEP_TVector3 &) const;
   // Transverse energy w.r.t. given axis squared.

   inline double Et(const HEP_TVector3 &) const;
   // Transverse energy w.r.t. given axis.

   inline double DeltaPhi(const HEP_TLorentzVector &) const;
   inline double DeltaR(const HEP_TLorentzVector &) const;
   inline double DrEtaPhi(const HEP_TLorentzVector &) const;
   inline HEP_TVector2 EtaPhiVector();

   inline double Angle(const HEP_TVector3 & v) const;
   // Angle wrt. another vector.

   inline double Mag2() const;
   inline double M2() const;
   // Invariant mass squared.

   inline double Mag() const;
   inline double M() const;
   // Invariant mass. If mag2() is negative then -sqrt(-mag2()) is returned.

   inline double Mt2() const;
   // Transverse mass squared.

   inline double Mt() const;
   // Transverse mass.

   inline double Beta() const;
   inline double Gamma() const;

   inline double Dot(const HEP_TLorentzVector &) const;
   inline double operator * (const HEP_TLorentzVector &) const;
   // Scalar product.

   inline void SetVectMag(const HEP_TVector3 & spatial, double magnitude);
   inline void SetVectM(const HEP_TVector3 & spatial, double mass);
   // Copy spatial coordinates, and set energy = sqrt(mass^2 + spatial^2)

   inline double Plus() const;
   inline double Minus() const;
   // Returns t +/- z.
   // Related to the positive/negative light-cone component,
   // which some define this way and others define as (t +/- z)/sqrt(2)

   inline HEP_TVector3 BoostVector() const ;
   // Returns the spatial components divided by the time component.

   void Boost(double, double, double);
   inline void Boost(const HEP_TVector3 &);
   // Lorentz boost.

   double Rapidity() const;
   // Returns the rapidity, i.e. 0.5*ln((E+pz)/(E-pz))

   inline double Eta() const;
   inline double PseudoRapidity() const;
   // Returns the pseudo-rapidity, i.e. -ln(tan(theta/2))

   inline void RotateX(double angle);
   // Rotate the spatial component around the x-axis.

   inline void RotateY(double angle);
   // Rotate the spatial component around the y-axis.

   inline void RotateZ(double angle);
   // Rotate the spatial component around the z-axis.

   inline void RotateUz(HEP_TVector3 & newUzVector);
   // Rotates the reference frame from Uz to newUz (unit vector).

   inline void Rotate(double, const HEP_TVector3 &);
   // Rotate the spatial component around specified axis.

  //  inline HEP_TLorentzVector & operator *= (const TRotation &);
  //  inline HEP_TLorentzVector & Transform(const TRotation &);
  //  // Transformation with HepRotation.
	 //
  //  HEP_TLorentzVector & operator *= (const TLorentzRotation &);
  //  HEP_TLorentzVector & Transform(const TLorentzRotation &);
   // Transformation with HepLorenzRotation.

  //  virtual void        Print(Option_t *option="") const;

   //ClassDef(HEP_TLorentzVector,4) // A four vector with (-,-,-,+) metric
};


//inline HEP_TLorentzVector operator * (const HEP_TLorentzVector &, double a);
// moved to HEP_TLorentzVector::operator * (double a)
inline HEP_TLorentzVector operator * (double a, const HEP_TLorentzVector &);
// Scaling LorentzVector with a real number


inline double HEP_TLorentzVector::X() const { return fP.X(); }
inline double HEP_TLorentzVector::Y() const { return fP.Y(); }
inline double HEP_TLorentzVector::Z() const { return fP.Z(); }
inline double HEP_TLorentzVector::T() const { return fE; }

inline void HEP_TLorentzVector::SetX(double a) { fP.SetX(a); }
inline void HEP_TLorentzVector::SetY(double a) { fP.SetY(a); }
inline void HEP_TLorentzVector::SetZ(double a) { fP.SetZ(a); }
inline void HEP_TLorentzVector::SetT(double a) { fE = a; }

inline double HEP_TLorentzVector::Px() const { return X(); }
inline double HEP_TLorentzVector::Py() const { return Y(); }
inline double HEP_TLorentzVector::Pz() const { return Z(); }
inline double HEP_TLorentzVector::P()  const { return fP.Mag(); }
inline double HEP_TLorentzVector::E()  const { return T(); }
inline double HEP_TLorentzVector::Energy()  const { return T(); }

inline void HEP_TLorentzVector::SetPx(double a) { SetX(a); }
inline void HEP_TLorentzVector::SetPy(double a) { SetY(a); }
inline void HEP_TLorentzVector::SetPz(double a) { SetZ(a); }
inline void HEP_TLorentzVector::SetE(double a)  { SetT(a); }

inline HEP_TVector3 HEP_TLorentzVector::Vect() const { return fP; }

inline void HEP_TLorentzVector::SetVect(const HEP_TVector3 &p) { fP = p; }

inline double HEP_TLorentzVector::Phi() const {
   return fP.Phi();
}

inline double HEP_TLorentzVector::Theta() const {
   return fP.Theta();
}

inline double HEP_TLorentzVector::CosTheta() const {
   return fP.CosTheta();
}


inline double HEP_TLorentzVector::Rho() const {
   return fP.Mag();
}

inline void HEP_TLorentzVector::SetTheta(double th) {
   fP.SetTheta(th);
}

inline void HEP_TLorentzVector::SetPhi(double phi) {
   fP.SetPhi(phi);
}

inline void HEP_TLorentzVector::SetRho(double rho) {
   fP.SetMag(rho);
}

inline void HEP_TLorentzVector::SetXYZT(double  x, double  y, double  z, double t) {
   fP.SetXYZ(x, y, z);
   SetT(t);
}

inline void HEP_TLorentzVector::SetPxPyPzE(double px, double py, double pz, double e) {
   SetXYZT(px, py, pz, e);
}

inline void HEP_TLorentzVector::SetXYZM(double  x, double  y, double  z, double m) {
   if ( m  >= 0 )
      SetXYZT( x, y, z, std::sqrt(x*x+y*y+z*z+m*m) );
   else
      SetXYZT( x, y, z, std::sqrt(fmax((x*x+y*y+z*z-m*m), 0. ) ) );
}

inline void HEP_TLorentzVector::SetPtEtaPhiM(double pt, double eta, double phi, double m) {
   pt = std::abs(pt);
   SetXYZM(pt*std::cos(phi), pt*std::sin(phi), pt*sinh(eta) ,m);
}

inline void HEP_TLorentzVector::SetPtEtaPhiE(double pt, double eta, double phi, double e) {
   pt = std::abs(pt);
   SetXYZT(pt*std::cos(phi), pt*std::sin(phi), pt*sinh(eta) ,e);
}

inline void HEP_TLorentzVector::GetXYZT(double *carray) const {
   fP.GetXYZ(carray);
   carray[3] = fE;
}

inline void HEP_TLorentzVector::GetXYZT(float *carray) const{
   fP.GetXYZ(carray);
   carray[3] = fE;
}

double & HEP_TLorentzVector::operator [] (int i)       { return (*this)(i); }
double   HEP_TLorentzVector::operator [] (int i) const { return (*this)(i); }

inline HEP_TLorentzVector &HEP_TLorentzVector::operator = (const HEP_TLorentzVector & q) {
   fP = q.Vect();
   fE = q.T();
   return *this;
}

inline HEP_TLorentzVector HEP_TLorentzVector::operator + (const HEP_TLorentzVector & q) const {
   return HEP_TLorentzVector(fP+q.Vect(), fE+q.T());
}

inline HEP_TLorentzVector &HEP_TLorentzVector::operator += (const HEP_TLorentzVector & q) {
   fP += q.Vect();
   fE += q.T();
   return *this;
}

inline HEP_TLorentzVector HEP_TLorentzVector::operator - (const HEP_TLorentzVector & q) const {
   return HEP_TLorentzVector(fP-q.Vect(), fE-q.T());
}

inline HEP_TLorentzVector &HEP_TLorentzVector::operator -= (const HEP_TLorentzVector & q) {
   fP -= q.Vect();
   fE -= q.T();
   return *this;
}

inline HEP_TLorentzVector HEP_TLorentzVector::operator - () const {
   return HEP_TLorentzVector(-X(), -Y(), -Z(), -T());
}

inline HEP_TLorentzVector& HEP_TLorentzVector::operator *= (double a) {
   fP *= a;
   fE *= a;
   return *this;
}

inline HEP_TLorentzVector HEP_TLorentzVector::operator * (double a) const {
   return HEP_TLorentzVector(a*X(), a*Y(), a*Z(), a*T());
}

inline bool HEP_TLorentzVector::operator == (const HEP_TLorentzVector & q) const {
   return (Vect() == q.Vect() && T() == q.T());
}

inline bool HEP_TLorentzVector::operator != (const HEP_TLorentzVector & q) const {
   return (Vect() != q.Vect() || T() != q.T());
}

inline double HEP_TLorentzVector::Perp2() const  { return fP.Perp2(); }

inline double HEP_TLorentzVector::Perp()  const  { return fP.Perp(); }

inline double HEP_TLorentzVector::Pt() const { return Perp(); }

inline void HEP_TLorentzVector::SetPerp(double r) {
   fP.SetPerp(r);
}

inline double HEP_TLorentzVector::Perp2(const HEP_TVector3 &v) const {
   return fP.Perp2(v);
}

inline double HEP_TLorentzVector::Perp(const HEP_TVector3 &v) const {
   return fP.Perp(v);
}

inline double HEP_TLorentzVector::Pt(const HEP_TVector3 &v) const {
   return Perp(v);
}

inline double HEP_TLorentzVector::Et2() const {
   double pt2 = fP.Perp2();
   return pt2 == 0 ? 0 : E()*E() * pt2/(pt2+Z()*Z());
}

inline double HEP_TLorentzVector::Et() const {
   double etet = Et2();
   return E() < 0.0 ? -std::sqrt(etet) : std::sqrt(etet);
}

inline double HEP_TLorentzVector::Et2(const HEP_TVector3 & v) const {
   double pt2 = fP.Perp2(v);
   double pv = fP.Dot(v.Unit());
   return pt2 == 0 ? 0 : E()*E() * pt2/(pt2+pv*pv);
}

inline double HEP_TLorentzVector::Et(const HEP_TVector3 & v) const {
   double etet = Et2(v);
   return E() < 0.0 ? -std::sqrt(etet) : std::sqrt(etet);
}

inline double HEP_TLorentzVector::DeltaPhi(const HEP_TLorentzVector & v) const {
   return HEP_TVector2::Phi_mpi_pi(Phi()-v.Phi());
}

inline double HEP_TLorentzVector::Eta() const {
   return PseudoRapidity();
}
inline double HEP_TLorentzVector::DeltaR(const HEP_TLorentzVector & v) const {
   double deta = Eta()-v.Eta();
   double dphi = HEP_TVector2::Phi_mpi_pi(Phi()-v.Phi());
   return std::sqrt( deta*deta+dphi*dphi );
}

inline double HEP_TLorentzVector::DrEtaPhi(const HEP_TLorentzVector & v) const{
   return DeltaR(v);
}

inline HEP_TVector2 HEP_TLorentzVector::EtaPhiVector() {
   return HEP_TVector2 (Eta(),Phi());
}


inline double HEP_TLorentzVector::Angle(const HEP_TVector3 &v) const {
   return fP.Angle(v);
}

inline double HEP_TLorentzVector::Mag2() const {
   return T()*T() - fP.Mag2();
}

inline double HEP_TLorentzVector::Mag() const {
   double mm = Mag2();
   return mm < 0.0 ? -std::sqrt(-mm) : std::sqrt(mm);
}

inline double HEP_TLorentzVector::M2() const { return Mag2(); }
inline double HEP_TLorentzVector::M() const { return Mag(); }

inline double HEP_TLorentzVector::Mt2() const {
   return E()*E() - Z()*Z();
}

inline double HEP_TLorentzVector::Mt() const {
   double mm = Mt2();
   return mm < 0.0 ? -std::sqrt(-mm) : std::sqrt(mm);
}

inline double HEP_TLorentzVector::Beta() const {
   return fP.Mag() / fE;
}

inline double HEP_TLorentzVector::Gamma() const {
   double b = Beta();
   return 1.0/std::sqrt(1- b*b);
}

inline void HEP_TLorentzVector::SetVectMag(const HEP_TVector3 & spatial, double magnitude) {
   SetXYZM(spatial.X(), spatial.Y(), spatial.Z(), magnitude);
}

inline void HEP_TLorentzVector::SetVectM(const HEP_TVector3 & spatial, double mass) {
   SetVectMag(spatial, mass);
}

inline double HEP_TLorentzVector::Dot(const HEP_TLorentzVector & q) const {
   return T()*q.T() - Z()*q.Z() - Y()*q.Y() - X()*q.X();
}

inline double HEP_TLorentzVector::operator * (const HEP_TLorentzVector & q) const {
   return Dot(q);
}

//Member functions Plus() and Minus() return the positive and negative
//light-cone components:
//
//  double pcone = v.Plus();
//  double mcone = v.Minus();
//
//CAVEAT: The values returned are T{+,-}Z. It is known that some authors
//find it easier to define these components as (T{+,-}Z)/sqrt(2). Thus
//check what definition is used in the physics you're working in and adapt
//your code accordingly.

inline double HEP_TLorentzVector::Plus() const {
   return T() + Z();
}

inline double HEP_TLorentzVector::Minus() const {
   return T() - Z();
}

inline HEP_TVector3 HEP_TLorentzVector::BoostVector() const {
   return HEP_TVector3(X()/T(), Y()/T(), Z()/T());
}

inline void HEP_TLorentzVector::Boost(const HEP_TVector3 & b) {
   Boost(b.X(), b.Y(), b.Z());
}

inline double HEP_TLorentzVector::PseudoRapidity() const {
   return fP.PseudoRapidity();
}

inline void HEP_TLorentzVector::RotateX(double angle) {
   fP.RotateX(angle);
}

inline void HEP_TLorentzVector::RotateY(double angle) {
   fP.RotateY(angle);
}

inline void HEP_TLorentzVector::RotateZ(double angle) {
   fP.RotateZ(angle);
}

inline void HEP_TLorentzVector::RotateUz(HEP_TVector3 &newUzVector) {
   fP.RotateUz(newUzVector);
}

// inline void HEP_TLorentzVector::Rotate(double a, const HEP_TVector3 &v) {
//    fP.Rotate(a,v);
// }

// inline HEP_TLorentzVector &HEP_TLorentzVector::operator *= (const TRotation & m) {
//    fP *= m;
//    return *this;
// }
//
// inline HEP_TLorentzVector &HEP_TLorentzVector::Transform(const TRotation & m) {
//    fP.Transform(m);
//    return *this;
// }

inline HEP_TLorentzVector operator * (double a, const HEP_TLorentzVector & p) {
   return HEP_TLorentzVector(a*p.X(), a*p.Y(), a*p.Z(), a*p.T());
}

#endif
