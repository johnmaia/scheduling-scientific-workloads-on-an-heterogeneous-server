#include "HEP_TVector3.h"

//ClassImp(HEP_TVector3)

//______________________________________________________________________________
HEP_TVector3::HEP_TVector3()
: fX(0.0), fY(0.0), fZ(0.0) {}

HEP_TVector3::HEP_TVector3(const HEP_TVector3 & p)
: fX(p.fX), fY(p.fY), fZ(p.fZ) {}

HEP_TVector3::HEP_TVector3(double xx, double yy, double zz)
: fX(xx), fY(yy), fZ(zz) {}

HEP_TVector3::HEP_TVector3(const double * x0)
: fX(x0[0]), fY(x0[1]), fZ(x0[2]) {}

HEP_TVector3::HEP_TVector3(const float * x0)
: fX(x0[0]), fY(x0[1]), fZ(x0[2]) {}

HEP_TVector3::~HEP_TVector3() {}

//______________________________________________________________________________
double HEP_TVector3::operator () (int i) const {
   //dereferencing operator const
   switch(i) {
      case 0:
         return fX;
      case 1:
         return fY;
      case 2:
         return fZ;
      default:
         return fX;
         //Error("operator()(i)", "bad index (%d) returning 0",i);
   }
   return 0.;
}

//______________________________________________________________________________
double & HEP_TVector3::operator () (int i) {
   //dereferencing operator
   switch(i) {
      case 0:
         return fX;
      case 1:
         return fY;
      case 2:
         return fZ;
      default:
         return fX;
         //Error("operator()(i)", "bad index (%d) returning &fX",i);
   }
   return fX;
}

//______________________________________________________________________________
// HEP_TVector3 & HEP_TVector3::operator *= (const TRotation & m){
//    //multiplication operator
//    return *this = m * (*this);
// }

//______________________________________________________________________________
// TVector3 & TVector3::Transform(const TRotation & m) {
//    //transform this vector with a TRotation
//    return *this = m * (*this);
// }
//
//______________________________________________________________________________
double HEP_TVector3::Angle(const HEP_TVector3 & q) const
{
   // return the angle w.r.t. another 3-vector
   double ptot2 = Mag2()*q.Mag2();
   if(ptot2 <= 0) {
      return 0.0;
   } else {
      double arg = Dot(q)/std::sqrt(ptot2);
      if(arg >  1.0) arg =  1.0;
      if(arg < -1.0) arg = -1.0;
      return std::acos(arg);
   }
}

//______________________________________________________________________________
double HEP_TVector3::Mag() const
{
   // return the magnitude (rho in spherical coordinate system)

   return std::sqrt(Mag2());
}

//______________________________________________________________________________
double HEP_TVector3::Perp() const
{
   //return the transverse component  (R in cylindrical coordinate system)

   return std::sqrt(Perp2());
}


//______________________________________________________________________________
double HEP_TVector3::Perp(const HEP_TVector3 & p) const
{
   //return the transverse component (R in cylindrical coordinate system)

   return std::sqrt(Perp2(p));
}

//______________________________________________________________________________
double HEP_TVector3::Phi() const
{
   //return the  azimuth angle. returns phi from -pi to pi
   return fX == 0.0 && fY == 0.0 ? 0.0 : std::atan2(fY,fX);
}

//______________________________________________________________________________
double HEP_TVector3::Theta() const
{
   //return the polar angle
   return fX == 0.0 && fY == 0.0 && fZ == 0.0 ? 0.0 : std::atan2(Perp(),fZ);
}

//______________________________________________________________________________
HEP_TVector3 HEP_TVector3::Unit() const
{
   // return unit vector parallel to this.
   double  tot2 = Mag2();
   double tot = (tot2 > 0) ?  1.0/std::sqrt(tot2) : 1.0;
   HEP_TVector3 p(fX*tot,fY*tot,fZ*tot);
   return p;
}

//______________________________________________________________________________
void HEP_TVector3::RotateX(double angle) {
   //rotate vector around X
   double s = std::sin(angle);
   double c = std::cos(angle);
   double yy = fY;
   fY = c*yy - s*fZ;
   fZ = s*yy + c*fZ;
}

//______________________________________________________________________________
void HEP_TVector3::RotateY(double angle) {
   //rotate vector around Y
   double s = std::sin(angle);
   double c = std::cos(angle);
   double zz = fZ;
   fZ = c*zz - s*fX;
   fX = s*zz + c*fX;
}

//______________________________________________________________________________
void HEP_TVector3::RotateZ(double angle) {
   //rotate vector around Z
   double s = std::sin(angle);
   double c = std::cos(angle);
   double xx = fX;
   fX = c*xx - s*fY;
   fY = s*xx + c*fY;
}

//______________________________________________________________________________
// void HEP_TVector3::Rotate(double angle, const HEP_TVector3 & axis){
//    //rotate vector
//    TRotation trans;
//    trans.Rotate(angle, axis);
//    operator*=(trans);
// }

//______________________________________________________________________________
void HEP_TVector3::RotateUz(const HEP_TVector3& NewUzVector) {
   // NewUzVector must be normalized !

   double u1 = NewUzVector.fX;
   double u2 = NewUzVector.fY;
   double u3 = NewUzVector.fZ;
   double up = u1*u1 + u2*u2;

   if (up) {
      up = std::sqrt(up);
      double px = fX,  py = fY,  pz = fZ;
      fX = (u1*u3*px - u2*py + u1*up*pz)/up;
      fY = (u2*u3*px + u1*py + u2*up*pz)/up;
      fZ = (u3*u3*px -    px + u3*up*pz)/up;
   } else if (u3 < 0.) { fX = -fX; fZ = -fZ; }      // phi=0  teta=pi
   else {};
}

//______________________________________________________________________________
double HEP_TVector3::PseudoRapidity() const {
   //double m = Mag();
   //return 0.5*log( (m+fZ)/(m-fZ) );
   // guard against Pt=0
   double cosTheta = CosTheta();
   if (cosTheta*cosTheta < 1) return -0.5* std::log( (1.0-cosTheta)/(1.0+cosTheta) );
   if (fZ == 0) return 0;
   //Warning("PseudoRapidity","transvers momentum = 0! return +/- 10e10");
   if (fZ > 0) return 10e10;
   else        return -10e10;
}

//______________________________________________________________________________
void HEP_TVector3::SetPtEtaPhi(double pt, double eta, double phi) {
   //set Pt, Eta and Phi
   double apt = std::abs(pt);
   SetXYZ(apt*std::cos(phi), apt*std::sin(phi), apt/std::tan(2.0*std::atan(std::exp(-eta))) );
}

//______________________________________________________________________________
void HEP_TVector3::SetPtThetaPhi(double pt, double theta, double phi) {
   //set Pt, Theta and Phi
   fX = pt * std::cos(phi);
   fY = pt * std::sin(phi);
   double tanTheta = std::tan(theta);
   fZ = tanTheta ? pt / tanTheta : 0;
}

//______________________________________________________________________________
void HEP_TVector3::SetTheta(double th)
{
   // Set theta keeping mag and phi constant (BaBar).
   double ma   = Mag();
   double ph   = Phi();
   SetX(ma*std::sin(th)*std::cos(ph));
   SetY(ma*std::sin(th)*std::sin(ph));
   SetZ(ma*std::cos(th));
}

//______________________________________________________________________________
void HEP_TVector3::SetPhi(double ph)
{
   // Set phi keeping mag and theta constant (BaBar).
   double xy   = Perp();
   SetX(xy*std::cos(ph));
   SetY(xy*std::sin(ph));
}

//______________________________________________________________________________
// double HEP_TVector3::DeltaR(const HEP_TVector3 & v) const
// {
//    //return deltaR with respect to v
//    double deta = Eta()-v.Eta();
//    double dphi = HEP_TVector2::Phi_mpi_pi(Phi()-v.Phi());
//    return std::sqrt( deta*deta+dphi*dphi );
// }

//______________________________________________________________________________
void HEP_TVector3::SetMagThetaPhi(double mag, double theta, double phi)
{
   //setter with mag, theta, phi
   double amag = std::abs(mag);
   fX = amag * std::sin(theta) * std::cos(phi);
   fY = amag * std::sin(theta) * std::sin(phi);
   fZ = amag * std::cos(theta);
}

//______________________________________________________________________________
// void HEP_TVector3::Streamer(TBuffer &R__b)
// {
//    // Stream an object of class HEP_TVector3.
//
//    if (R__b.IsReading()) {
//       Uint R__s, R__c;
//       Version_t R__v = R__b.ReadVersion(&R__s, &R__c);
//       if (R__v > 2) {
//          R__b.ReadClassBuffer(HEP_TVector3::Class(), this, R__v, R__s, R__c);
//          return;
//       }
//       //====process old versions before automatic schema evolution
//       if (R__v < 2) TObject::Streamer(R__b);
//       R__b >> fX;
//       R__b >> fY;
//       R__b >> fZ;
//       R__b.CheckByteCount(R__s, R__c, HEP_TVector3::IsA());
//       //====end of old versions
//
//    } else {
//       R__b.WriteClassBuffer(HEP_TVector3::Class(),this);
//    }
//}

HEP_TVector3 operator + (const HEP_TVector3 & a, const HEP_TVector3 & b) {
   return HEP_TVector3(a.X() + b.X(), a.Y() + b.Y(), a.Z() + b.Z());
}

HEP_TVector3 operator - (const HEP_TVector3 & a, const HEP_TVector3 & b) {
   return HEP_TVector3(a.X() - b.X(), a.Y() - b.Y(), a.Z() - b.Z());
}

HEP_TVector3 operator * (const HEP_TVector3 & p, double a) {
   return HEP_TVector3(a*p.X(), a*p.Y(), a*p.Z());
}

HEP_TVector3 operator * (double a, const HEP_TVector3 & p) {
   return HEP_TVector3(a*p.X(), a*p.Y(), a*p.Z());
}

double operator * (const HEP_TVector3 & a, const HEP_TVector3 & b) {
   return a.Dot(b);
}

// HEP_TVector3 operator * (const TMatrix & m, const HEP_TVector3 & v ) {
//    return HEP_TVector3( m(0,0)*v.X()+m(0,1)*v.Y()+m(0,2)*v.Z(),
//                     m(1,0)*v.X()+m(1,1)*v.Y()+m(1,2)*v.Z(),
//                     m(2,0)*v.X()+m(2,1)*v.Y()+m(2,2)*v.Z());
// }


//const HEP_TVector3 kXHat(1.0, 0.0, 0.0);
//const HEP_TVector3 kYHat(0.0, 1.0, 0.0);
//const HEP_TVector3 kZHat(0.0, 0.0, 1.0);

// void HEP_TVector3::Print(Option_t*)const
// {
//    //print vector parameters
//    Printf("%s %s (x,y,z)=(%f,%f,%f) (rho,theta,phi)=(%f,%f,%f)",GetName(),GetTitle(),X(),Y(),Z(),
//                                           Mag(),Theta()*TMath::RadToDeg(),Phi()*TMath::RadToDeg());
// }
