#include "TLorentzVectorWFlags.h"

// #############################################################################
TLorentzVectorWFlags::TLorentzVectorWFlags(): HEP_TLorentzVector(), idx(-1), isb(-1), IsoDeltaR(999), itruthMatch(-1), itrigMatch(-1){
// #############################################################################

}

// #############################################################################
TLorentzVectorWFlags::TLorentzVectorWFlags(double px, double py, double pz, double E, int index, int index2, double p_IsoDeltaR, int index3, int index4) :
  HEP_TLorentzVector(px,py,pz,E), idx(index), isb(index2), IsoDeltaR(p_IsoDeltaR), itruthMatch(index3), itrigMatch(index4) {
// #############################################################################

}

// #############################################################################
TLorentzVectorWFlags::TLorentzVectorWFlags(HEP_TLorentzVector v, int index, int index2, double p_IsoDeltaR, int index3, int index4) :
  HEP_TLorentzVector(v), idx(index), isb(index2), IsoDeltaR(p_IsoDeltaR), itruthMatch(index3), itrigMatch(index4) {
// #############################################################################

}

// #############################################################################
TLorentzVectorWFlags::TLorentzVectorWFlags(const TLorentzVectorWFlags& other) :
  HEP_TLorentzVector(other), idx(other.idx), isb(other.isb), IsoDeltaR(other.IsoDeltaR), itruthMatch(other.itruthMatch), itrigMatch(other.itrigMatch) {
// #############################################################################

}

// #############################################################################
TLorentzVectorWFlags& TLorentzVectorWFlags::operator=(const TLorentzVectorWFlags& other) {
// #############################################################################

  if (&other==this) {
    return *this ;
  }
  HEP_TLorentzVector::operator=(other) ;
  idx = other.idx ;
  isb = other.isb ;
  IsoDeltaR = other.IsoDeltaR;
  itruthMatch = other.itruthMatch ;
  itrigMatch  = other.itrigMatch ;
  return *this ;
}

// #############################################################################
TLorentzVectorWFlags::~TLorentzVectorWFlags() {
// #############################################################################

}
