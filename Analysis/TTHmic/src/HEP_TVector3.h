#ifndef HEP_TVector3_h
#define HEP_TVector3_h

#include <cmath>

class HEP_TVector3 {

public:

   typedef double Scalar;   // to be able to use it with the ROOT::Math::VectorUtil functions

   HEP_TVector3();

   HEP_TVector3(double x, double y, double z);
   // The constructor.

   HEP_TVector3(const double *);
   HEP_TVector3(const float *);
   // Constructors from an array

   HEP_TVector3(const HEP_TVector3 &);
   // The copy constructor.

   virtual ~HEP_TVector3();
   // Destructor

   double operator () (int) const;
   inline double operator [] (int) const;
   // Get components by index (Geant4).

   double & operator () (int);
   inline double & operator [] (int);
   // Set components by index.

   inline double x()  const;
   inline double y()  const;
   inline double z()  const;
   inline double X()  const;
   inline double Y()  const;
   inline double Z()  const;
   inline double Px() const;
   inline double Py() const;
   inline double Pz() const;
   // The components in cartesian coordinate system.

   inline void SetX(double);
   inline void SetY(double);
   inline void SetZ(double);
   inline void SetXYZ(double x, double y, double z);
   void        SetPtEtaPhi(double pt, double eta, double phi);
   void        SetPtThetaPhi(double pt, double theta, double phi);

   inline void GetXYZ(double *carray) const;
   inline void GetXYZ(float *carray) const;
   // Get the components into an array
   // not checked!

   double Phi() const;
   // The azimuth angle. returns phi from -pi to pi

   double Theta() const;
   // The polar angle.

   inline double CosTheta() const;
   // Cosine of the polar angle.

   inline double Mag2() const;
   // The magnitude squared (rho^2 in spherical coordinate system).

   double Mag() const;
   // The magnitude (rho in spherical coordinate system).

   void SetPhi(double);
   // Set phi keeping mag and theta constant (BaBar).

   void SetTheta(double);
   // Set theta keeping mag and phi constant (BaBar).

   inline void SetMag(double);
   // Set magnitude keeping theta and phi constant (BaBar).

   inline double Perp2() const;
   // The transverse component squared (R^2 in cylindrical coordinate system).

   inline double Pt() const;
   double Perp() const;
   // The transverse component (R in cylindrical coordinate system).

   inline void SetPerp(double);
   // Set the transverse component keeping phi and z constant.

   inline double Perp2(const HEP_TVector3 &) const;
   // The transverse component w.r.t. given axis squared.

   inline double Pt(const HEP_TVector3 &) const;
   double Perp(const HEP_TVector3 &) const;
   // The transverse component w.r.t. given axis.

   inline double DeltaPhi(const HEP_TVector3 &) const;
   //double DeltaR(const HEP_TVector3 &) const;
   //inline double DrEtaPhi(const HEP_TVector3 &) const;
   // inline TVector2 EtaPhiVector() const;
   void SetMagThetaPhi(double mag, double theta, double phi);

   inline HEP_TVector3 & operator = (const HEP_TVector3 &);
   // Assignment.

   inline bool operator == (const HEP_TVector3 &) const;
   inline bool operator != (const HEP_TVector3 &) const;
   // Comparisons (Geant4).

   inline HEP_TVector3 & operator += (const HEP_TVector3 &);
   // Addition.

   inline HEP_TVector3 & operator -= (const HEP_TVector3 &);
   // Subtraction.

   inline HEP_TVector3 operator - () const;
   // Unary minus.

   inline HEP_TVector3 & operator *= (double);
   // Scaling with real numbers.

   HEP_TVector3 Unit() const;
   // Unit vector parallel to this.

  //  inline HEP_TVector3 Orthogonal() const;
   // Vector orthogonal to this (Geant4).

   inline double Dot(const HEP_TVector3 &) const;
   // Scalar product.

   inline HEP_TVector3 Cross(const HEP_TVector3 &) const;
   // Cross product.

   double Angle(const HEP_TVector3 &) const;
   // The angle w.r.t. another 3-vector.

   double PseudoRapidity() const;
   // Returns the pseudo-rapidity, i.e. -ln(tan(theta/2))

   inline double Eta() const;

   void RotateX(double);
   // Rotates the Hep3Vector around the x-axis.

   void RotateY(double);
   // Rotates the Hep3Vector around the y-axis.

   void RotateZ(double);
   // Rotates the Hep3Vector around the z-axis.

   void RotateUz(const HEP_TVector3&);
   // Rotates reference frame from Uz to newUz (unit vector) (Geant4).

   void Rotate(double, const HEP_TVector3 &);
   // Rotates around the axis specified by another Hep3Vector.

  //  HEP_TVector3 & operator *= (const TRotation &);
  //  HEP_TVector3 & Transform(const TRotation &);
   // Transformation with a Rotation matrix.

  // inline TVector2 XYvector() const;

  // void Print(Option_t* option="") const;


private:
  double fX, fY, fZ;

   // The components.

   //ClassDef(HEP_TVector3,3) // A 3D physics vector

};


HEP_TVector3 operator + (const HEP_TVector3 &, const HEP_TVector3 &);
// Addition of 3-vectors.

HEP_TVector3 operator - (const HEP_TVector3 &, const HEP_TVector3 &);
// Subtraction of 3-vectors.

double operator * (const HEP_TVector3 &, const HEP_TVector3 &);
// Scalar product of 3-vectors.

HEP_TVector3 operator * (const HEP_TVector3 &, double a);
HEP_TVector3 operator * (double a, const HEP_TVector3 &);
// Scaling of 3-vectors with a real number

//HEP_TVector3 operator * (const TMatrix &, const TVector3 &);


double & HEP_TVector3::operator[] (int i)       { return operator()(i); }
double   HEP_TVector3::operator[] (int i) const { return operator()(i); }

inline double HEP_TVector3::x()  const { return fX; }
inline double HEP_TVector3::y()  const { return fY; }
inline double HEP_TVector3::z()  const { return fZ; }
inline double HEP_TVector3::X()  const { return fX; }
inline double HEP_TVector3::Y()  const { return fY; }
inline double HEP_TVector3::Z()  const { return fZ; }
inline double HEP_TVector3::Px() const { return fX; }
inline double HEP_TVector3::Py() const { return fY; }
inline double HEP_TVector3::Pz() const { return fZ; }

inline void HEP_TVector3::SetX(double xx) { fX = xx; }
inline void HEP_TVector3::SetY(double yy) { fY = yy; }
inline void HEP_TVector3::SetZ(double zz) { fZ = zz; }

inline void HEP_TVector3::SetXYZ(double xx, double yy, double zz) {
   fX = xx;
   fY = yy;
   fZ = zz;
}

inline void HEP_TVector3::GetXYZ(double *carray) const {
   carray[0] = fX;
   carray[1] = fY;
   carray[2] = fZ;
}

inline void HEP_TVector3::GetXYZ(float *carray) const {
   carray[0] = fX;
   carray[1] = fY;
   carray[2] = fZ;
}


inline HEP_TVector3 & HEP_TVector3::operator = (const HEP_TVector3 & p) {
   fX = p.fX;
   fY = p.fY;
   fZ = p.fZ;
   return *this;
}

inline bool HEP_TVector3::operator == (const HEP_TVector3& v) const {
   return (v.fX==fX && v.fY==fY && v.fZ==fZ) ? true : false;
}

inline bool HEP_TVector3::operator != (const HEP_TVector3& v) const {
   return (v.fX!=fX || v.fY!=fY || v.fZ!=fZ) ? true : false;
}

inline HEP_TVector3& HEP_TVector3::operator += (const HEP_TVector3 & p) {
   fX += p.fX;
   fY += p.fY;
   fZ += p.fZ;
   return *this;
}

inline HEP_TVector3& HEP_TVector3::operator -= (const HEP_TVector3 & p) {
   fX -= p.fX;
   fY -= p.fY;
   fZ -= p.fZ;
   return *this;
}

inline HEP_TVector3 HEP_TVector3::operator - () const {
   return HEP_TVector3(-fX, -fY, -fZ);
}

inline HEP_TVector3& HEP_TVector3::operator *= (double a) {
   fX *= a;
   fY *= a;
   fZ *= a;
   return *this;
}

inline double HEP_TVector3::Dot(const HEP_TVector3 & p) const {
   return fX*p.fX + fY*p.fY + fZ*p.fZ;
}

inline HEP_TVector3 HEP_TVector3::Cross(const HEP_TVector3 & p) const {
   return HEP_TVector3(fY*p.fZ-p.fY*fZ, fZ*p.fX-p.fZ*fX, fX*p.fY-p.fX*fY);
}

inline double HEP_TVector3::Mag2() const { return fX*fX + fY*fY + fZ*fZ; }


// inline TVector3 HEP_TVector3::Orthogonal() const {
//    double xx = fX < 0.0 ? -fX : fX;
//    double yy = fY < 0.0 ? -fY : fY;
//    double zz = fZ < 0.0 ? -fZ : fZ;
//    if (xx < yy) {
//       return xx < zz ? HEP_TVector3(0,fZ,-fY) : HEP_TVector3(fY,-fX,0);
//    } else {
//       return yy < zz ? HEP_TVector3(-fZ,0,fX) : HEP_TVector3(fY,-fX,0);
//    }
// }

inline double HEP_TVector3::Perp2() const { return fX*fX + fY*fY; }


inline double HEP_TVector3::Pt() const { return Perp(); }

inline double HEP_TVector3::Perp2(const HEP_TVector3 & p)  const {
   double tot = p.Mag2();
   double ss  = Dot(p);
   double per = Mag2();
   if (tot > 0.0) per -= ss*ss/tot;
   if (per < 0)   per = 0;
   return per;
}

inline double HEP_TVector3::Pt(const HEP_TVector3 & p) const {
   return Perp(p);
}

inline double HEP_TVector3::CosTheta() const {
   double ptot = Mag();
   return ptot == 0.0 ? 1.0 : fZ/ptot;
}

inline void HEP_TVector3::SetMag(double ma) {
   double factor = Mag();
   if (factor == 0) {
      //Warning("SetMag","zero vector can't be stretched");
   } else {
      factor = ma/factor;
      SetX(fX*factor);
      SetY(fY*factor);
      SetZ(fZ*factor);
   }
}

inline void HEP_TVector3::SetPerp(double r) {
   double p = Perp();
   if (p != 0.0) {
      fX *= r/p;
      fY *= r/p;
   }
}

// inline double TVector3::DeltaPhi(const TVector3 & v) const {
//    return TVector2::Phi_mpi_pi(Phi()-v.Phi());
// }

inline double HEP_TVector3::Eta() const {
   return PseudoRapidity();
}

// inline double HEP_TVector3::DrEtaPhi(const HEP_TVector3 & v) const{
//    return DeltaR(v);
// }


// inline HEP_TVector3 HEP_TVector3::EtaPhiVector() const {
//    return TVector2 (Eta(),Phi());
// }

// inline TVector2 TVector3::XYvector() const {
//    return TVector2(fX,fY);
// }

#endif
