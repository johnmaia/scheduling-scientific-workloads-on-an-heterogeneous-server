// ###################################################################################
//
//							!!Disclaimer!!
//
// ###################################################################################
//
// Add variables in the EventData class, between the public and constructor statements
//
// ###################################################################################

#include <vector>
#include "TTHmic_Event.h"

extern std::vector<HEPEvent> events;
extern long unsigned event_counter;

/*
 *	 defines below
 */


#define id events[event_counter].id
#define runNumber events[event_counter].runNumber
#define eventNumber events[event_counter].eventNumber
#define channelNumber events[event_counter].channelNumber
#define rndRunNumber events[event_counter].rndRunNumber
#define dataPeriod events[event_counter].dataPeriod
#define mu events[event_counter].mu
#define mcWeight events[event_counter].mcWeight
#define eventWeight_PRETAG events[event_counter].eventWeight_PRETAG
#define eventWeight_BTAG events[event_counter].eventWeight_BTAG
#define pvxp_n events[event_counter].pvxp_n
#define hfor events[event_counter].hfor
#define vxp_z events[event_counter].vxp_z
#define mc_vxp_z events[event_counter].mc_vxp_z
#define mcevt_pdf1 events[event_counter].mcevt_pdf1
#define mcevt_pdf2 events[event_counter].mcevt_pdf2
#define mcevt_pdf_id1 events[event_counter].mcevt_pdf_id1
#define mcevt_pdf_id2 events[event_counter].mcevt_pdf_id2
#define mcevt_pdf_scale events[event_counter].mcevt_pdf_scale
#define mcevt_pdf_x1 events[event_counter].mcevt_pdf_x1
#define mcevt_pdf_x2 events[event_counter].mcevt_pdf_x2
#define scaleFactor_PILEUP events[event_counter].scaleFactor_PILEUP
#define scaleFactor_ELE events[event_counter].scaleFactor_ELE
#define scaleFactor_MUON events[event_counter].scaleFactor_MUON
#define scaleFactor_DILEP events[event_counter].scaleFactor_DILEP
#define scaleFactor_BTAG events[event_counter].scaleFactor_BTAG
#define scaleFactor_WJETSNORM events[event_counter].scaleFactor_WJETSNORM
#define scaleFactor_WJETSSHAPE events[event_counter].scaleFactor_WJETSSHAPE
#define scaleFactor_JVFSF events[event_counter].scaleFactor_JVFSF
#define scaleFactor_ZVERTEX events[event_counter].scaleFactor_ZVERTEX
#define scaleFactor_ALLPRETAG events[event_counter].scaleFactor_ALLPRETAG
#define scaleFactor_ALLBTAG events[event_counter].scaleFactor_ALLBTAG
#define truE events[event_counter].truE
#define truM events[event_counter].truM
#define trigE events[event_counter].trigE
#define trigM events[event_counter].trigM
#define passGRL events[event_counter].passGRL
#define cosmicEvent events[event_counter].cosmicEvent
#define isOS events[event_counter].isOS
#define hasGoodVertex events[event_counter].hasGoodVertex
#define ht events[event_counter].ht
#define mass events[event_counter].mass
#define massInv_LL events[event_counter].massInv_LL
#define massInv_BQQ events[event_counter].massInv_BQQ
#define massTrans_BLMet events[event_counter].massTrans_BLMet
#define massTrans_LMet events[event_counter].massTrans_LMet
#define flag_DL events[event_counter].flag_DL
#define flag_TTZ events[event_counter].flag_TTZ
#define channel_DL events[event_counter].channel_DL
#define channel_TTZ events[event_counter].channel_TTZ
#define scaledWeight events[event_counter].scaledWeight
#define lep_n events[event_counter].lep_n
#define lep_truthMatched events[event_counter].lep_truthMatched
#define lep_trigMatched events[event_counter].lep_trigMatched
#define lep_pt events[event_counter].lep_pt
#define lep_eta events[event_counter].lep_eta
#define lep_phi events[event_counter].lep_phi
#define lep_E events[event_counter].lep_E
#define lep_z0 events[event_counter].lep_z0
#define lep_charge events[event_counter].lep_charge
#define lep_isTight events[event_counter].lep_isTight
#define lep_type events[event_counter].lep_type
#define lep_flag events[event_counter].lep_flag
#define el_cl_eta events[event_counter].el_cl_eta
#define lep_ptcone30 events[event_counter].lep_ptcone30
#define lep_etcone20 events[event_counter].lep_etcone20
#define massTrans_LMet_Vec events[event_counter].massTrans_LMet_Vec
#define lepPair_n events[event_counter].lepPair_n
#define isSameFlavor_LL_Vec events[event_counter].isSameFlavor_LL_Vec
#define isOppSign_LL_Vec events[event_counter].isOppSign_LL_Vec
#define massInv_LL_Vec events[event_counter].massInv_LL_Vec
#define met_sumet events[event_counter].met_sumet
#define met_et events[event_counter].met_et
#define met_phi events[event_counter].met_phi
#define jet_n events[event_counter].jet_n
#define alljet_n events[event_counter].alljet_n
#define jet_pt events[event_counter].jet_pt
#define jet_eta events[event_counter].jet_eta
#define jet_phi events[event_counter].jet_phi
#define jet_E events[event_counter].jet_E
#define jet_jvf events[event_counter].jet_jvf
#define jet_trueflav events[event_counter].jet_trueflav
#define jet_truthMatched events[event_counter].jet_truthMatched
#define jet_SV0 events[event_counter].jet_SV0
#define jet_MV1 events[event_counter].jet_MV1
#define Weight events[event_counter].Weight
#define Ht events[event_counter].Ht
#define Hz events[event_counter].Hz
#define Ht_Mini events[event_counter].Ht_Mini
#define BTagCut events[event_counter].BTagCut
#define PtCutJet events[event_counter].PtCutJet
#define MissPx events[event_counter].MissPx
#define MissPy events[event_counter].MissPy
#define MissPt events[event_counter].MissPt
#define massInv_LL_Mini events[event_counter].massInv_LL_Mini
#define Sphericity events[event_counter].Sphericity
#define Aplanarity events[event_counter].Aplanarity
#define Planarity events[event_counter].Planarity
#define ElectronTrigger events[event_counter].ElectronTrigger
#define MuonTrigger events[event_counter].MuonTrigger
#define Cosmic events[event_counter].Cosmic
#define EleMuoOverlap events[event_counter].EleMuoOverlap
#define JetCleanning events[event_counter].JetCleanning
#define jet_n_Mini events[event_counter].jet_n_Mini
#define NbtagJet events[event_counter].NbtagJet
#define ntruthlep events[event_counter].ntruthlep
#define ntruthele events[event_counter].ntruthele
#define ntruthmu events[event_counter].ntruthmu
#define ntrutheletau events[event_counter].ntrutheletau
#define ntruthmutau events[event_counter].ntruthmutau
#define ntruthtau events[event_counter].ntruthtau
#define ntruthleptau events[event_counter].ntruthleptau
#define TruthEleNumber events[event_counter].TruthEleNumber
#define TruthMuonNumber events[event_counter].TruthMuonNumber
#define Isub events[event_counter].Isub
#define LumiBlock events[event_counter].LumiBlock
#define RunNumber events[event_counter].RunNumber
#define EveNumber events[event_counter].EveNumber
#define HforFlag events[event_counter].HforFlag
#define GoodRL events[event_counter].GoodRL
#define ll events[event_counter].ll
#define llmiss events[event_counter].llmiss
#define Vtx events[event_counter].Vtx
#define LeptonVec events[event_counter].LeptonVec
#define MyGoodJetVec events[event_counter].MyGoodJetVec
#define MyGoodBtaggedJetVec events[event_counter].MyGoodBtaggedJetVec
#define MyGoodNonBtaggedJetVec events[event_counter].MyGoodNonBtaggedJetVec
#define JetVec events[event_counter].JetVec
#define RecT events[event_counter].RecT
#define RecB events[event_counter].RecB
#define RecWp events[event_counter].RecWp
#define RecLepP events[event_counter].RecLepP
#define RecNeu events[event_counter].RecNeu
#define RecTbar events[event_counter].RecTbar
#define RecBbar events[event_counter].RecBbar
#define RecWn events[event_counter].RecWn
#define RecLepN events[event_counter].RecLepN
#define RecNeubar events[event_counter].RecNeubar
#define RecTTbar events[event_counter].RecTTbar
#define RecHiggs events[event_counter].RecHiggs
#define RecHiggsB1 events[event_counter].RecHiggsB1
#define RecHiggsB2 events[event_counter].RecHiggsB2
#define Neutrino events[event_counter].Neutrino
#define Antineutrino events[event_counter].Antineutrino
#define RecMassHiggsJet1 events[event_counter].RecMassHiggsJet1
#define RecMassHiggsJet2 events[event_counter].RecMassHiggsJet2
#define RecProbTotal_ttH events[event_counter].RecProbTotal_ttH
#define RecB_BoostedtoT events[event_counter].RecB_BoostedtoT
#define RecWp_BoostedtoT events[event_counter].RecWp_BoostedtoT
#define RecLepP_BoostedtoT events[event_counter].RecLepP_BoostedtoT
#define RecNeu_BoostedtoT events[event_counter].RecNeu_BoostedtoT
#define RecBbar_BoostedtoTbar events[event_counter].RecBbar_BoostedtoTbar
#define RecWn_BoostedtoTbar events[event_counter].RecWn_BoostedtoTbar
#define RecLepN_BoostedtoTbar events[event_counter].RecLepN_BoostedtoTbar
#define RecNeubar_BoostedtoTbar events[event_counter].RecNeubar_BoostedtoTbar
#define RecT_Boostedtottbar events[event_counter].RecT_Boostedtottbar
#define RecTbar_Boostedtottbar events[event_counter].RecTbar_Boostedtottbar
#define RecCos_LepP_T_BoostedtoT events[event_counter].RecCos_LepP_T_BoostedtoT
#define RecCos_Neu_T_BoostedtoT events[event_counter].RecCos_Neu_T_BoostedtoT
#define RecCos_B_T_BoostedtoT events[event_counter].RecCos_B_T_BoostedtoT
#define RecCos_LepN_Tbar_BoostedtoTbar events[event_counter].RecCos_LepN_Tbar_BoostedtoTbar
#define RecCos_Neubar_Tbar_BoostedtoTbar events[event_counter].RecCos_Neubar_Tbar_BoostedtoTbar
#define RecCos_Bbar_Tbar_BoostedtoTbar events[event_counter].RecCos_Bbar_Tbar_BoostedtoTbar
#define RecCos_LepP_B_BoostedtoWp events[event_counter].RecCos_LepP_B_BoostedtoWp
#define RecCos_LepN_Bbar_BoostedtoWn events[event_counter].RecCos_LepN_Bbar_BoostedtoWn
#define RecB_BoostedtoWp events[event_counter].RecB_BoostedtoWp
#define RecLepP_BoostedtoWp events[event_counter].RecLepP_BoostedtoWp
#define RecNeu_BoostedtoWp events[event_counter].RecNeu_BoostedtoWp
#define RecBbar_BoostedtoWn events[event_counter].RecBbar_BoostedtoWn
#define RecLepN_BoostedtoWn events[event_counter].RecLepN_BoostedtoWn
#define RecNeubar_BoostedtoWn events[event_counter].RecNeubar_BoostedtoWn
#define _cut17 events[event_counter]._cut17
