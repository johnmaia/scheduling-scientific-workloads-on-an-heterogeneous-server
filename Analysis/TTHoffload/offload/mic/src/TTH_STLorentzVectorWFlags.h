#ifndef stlorentzwflags_h
#define stlorentzwflags_h

#include <cmath>

double const  kPI        = atan(1)*4;
double const  kTWOPI     = 2.*kPI;

typedef struct stlorentzwflags {
  double Fx;
  double Fy;
  double Fz;
  double Fe;

  int idx = -1;
  int isb = -1;
  double IsoDeltaR = 999;
  int itruthMatch = -1;
  int itrigMatch = -1;

  double Px();
  double Py();
  double Pz();
  double Pt();
  double E();
  double T();

  double M();

  double P();

  double Perp();
  double Perp2();

  double Mag();
  double Mag2();

  void SetPx(double x);
  void SetPy(double y);
  void SetPz(double z);
  void SetE(double e);
  void SetPxPyPzE(double px, double py, double pz, double e);
  void SetRest(int pidx, int pisb, double pIsoDeltaR, int pitruthMatch, int pitrigMatch);
  void SetIsoDeltaR(double p_IsoDeltaR);

  double CosTheta();
  double Phi();
  double Phi_mpi_pi(double x);
  double PseudoRapidity();
  double Eta();
  double DeltaR(const stlorentzwflags & v);
} STLorentzVectorWFlags;

inline double STLorentzVectorWFlags::Px(){ return Fx; }
inline double STLorentzVectorWFlags::Py(){ return Fy; }
inline double STLorentzVectorWFlags::Pz(){ return Fz; }
inline double STLorentzVectorWFlags::Pt(){ return Perp(); }
inline double STLorentzVectorWFlags::E(){ return Fe; }
inline double STLorentzVectorWFlags::T(){ return Fe; }

inline double STLorentzVectorWFlags::M(){ return Mag(); }

inline double STLorentzVectorWFlags::P(){ return Mag(); }

inline double STLorentzVectorWFlags::CosTheta(){
   double ptot = Mag();
   return ptot == 0.0 ? 1.0 : Fz/ptot;
}

inline double STLorentzVectorWFlags::Phi()
{
   // return vector phi
   return (atan(1)*4)+atan2(-Fy,-Fx);
}


inline double STLorentzVectorWFlags::Phi_mpi_pi(double x) {
   if(std::isnan(x)){
      return x;
   }
   while (x >= kPI) x -= kTWOPI;
   while (x < -kPI) x += kTWOPI;
   return x;
}

inline double STLorentzVectorWFlags::PseudoRapidity(){
   double cosTheta = CosTheta();
   if (cosTheta*cosTheta < 1) return -0.5* log( (1.0-cosTheta)/(1.0+cosTheta) );
   if (Fz == 0) return 0;
   //Warning("PseudoRapidity","transvers momentum = 0! return +/- 10e10");
   if (Fz > 0) return 10e10;
   else        return -10e10;
}

inline double STLorentzVectorWFlags::Eta(){
   return PseudoRapidity();
}

inline double STLorentzVectorWFlags::DeltaR(const stlorentzwflags& v) {
   STLorentzVectorWFlags a;
   a = v;
   double deta = Eta()-a.Eta();
   double dphi = Phi_mpi_pi(Phi()-a.Phi());
   return sqrt( deta*deta+dphi*dphi );
}

inline double STLorentzVectorWFlags::Perp(){ return sqrt(Perp2()); }
inline double STLorentzVectorWFlags::Perp2(){ return Fx*Fx + Fy*Fy;; }

inline double STLorentzVectorWFlags::Mag(){
   double mm = Mag2();
   return mm < 0.0 ? -sqrt(-mm) : sqrt(mm);
}

inline double STLorentzVectorWFlags::Mag2(){
  return Fx*Fx + Fy*Fy + Fz*Fz;
}

inline void STLorentzVectorWFlags::SetPx(double x){ Fx = x; }
inline void STLorentzVectorWFlags::SetPy(double y){ Fy = y; }
inline void STLorentzVectorWFlags::SetPz(double z){ Fz = z; }
inline void STLorentzVectorWFlags::SetE(double e){ Fe = e; }
inline void STLorentzVectorWFlags::SetPxPyPzE(double px, double py, double pz, double e){
  Fx = px; Fy =py; Fz = pz; Fe = e;
}
inline void STLorentzVectorWFlags::SetRest(int pidx, int pisb, double pIsoDeltaR, int pitruthMatch, int pitrigMatch){
  idx = pidx;
  isb = pisb;
  IsoDeltaR = pIsoDeltaR;
  itruthMatch = pitruthMatch;
  itrigMatch = pitrigMatch;
}

inline void STLorentzVectorWFlags::SetIsoDeltaR(double p_IsoDeltaR){ IsoDeltaR = p_IsoDeltaR; };

#endif
