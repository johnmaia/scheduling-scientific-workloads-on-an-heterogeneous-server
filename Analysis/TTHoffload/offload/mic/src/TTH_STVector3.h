#ifndef stvector3_h
#define stvector3_h

#include <math.h>

typedef struct stvector3 {
  double Fx;
  double Fy;
  double Fz;

  double Px();
  double Py();
  double Pz();

  double X();
  double Y();
  double Z();

  void SetPx(double x);
  void SetPy(double y);
  void SetPz(double z);
  void SetPxPyPz(double px, double py, double pz);

  double Mag2();
  double Dot(const stvector3 & p);
  double Angle(const stvector3 & q);

  inline stvector3 operator - () const;

} STVector3;

inline double STVector3::Px(){ return Fx; }
inline double STVector3::Py(){ return Fy; }
inline double STVector3::Pz(){ return Fz; }

inline double STVector3::X(){ return Fx; }
inline double STVector3::Y(){ return Fy; }
inline double STVector3::Z(){ return Fz; }

inline void STVector3::SetPx(double x){ Fx = x; }
inline void STVector3::SetPy(double y){ Fy = y; }
inline void STVector3::SetPz(double z){ Fz = z; }
inline void STVector3::SetPxPyPz(double px, double py, double pz){
  Fx = px; Fy =py; Fz = pz;
}

inline double STVector3::Mag2(){
  return Fx*Fx + Fy*Fy + Fz*Fz;
}

inline double STVector3::Dot(const stvector3 & p) {
   return Fx*p.Fx + Fy*p.Fy + Fz*p.Fz;
}

inline double STVector3::Angle(const stvector3 & q)
{
   double ptot2 = (Fx*Fx + Fy*Fy + Fz*Fz)*(q.Fx*q.Fx + q.Fy*q.Fy + q.Fz*q.Fz);
   if(ptot2 <= 0) {
      return 0.0;
   } else {
      double arg = Dot(q)/sqrt(ptot2);
      if(arg >  1.0) arg =  1.0;
      if(arg < -1.0) arg = -1.0;
      return acos(arg);
   }
}

inline stvector3 STVector3::operator - () const {
   stvector3 vect;
   vect.SetPxPyPz(-Fx, -Fy, -Fz);
   return vect;
}
#endif
