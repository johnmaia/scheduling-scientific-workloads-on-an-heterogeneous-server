#ifndef stlorentz_h
#define stlorentz_h

#include <stdlib.h>
#include <cmath>
#include "TTH_STVector3.h"
#include "TTH_STLorentzVectorWFlags.h"

typedef struct stlorentz {
  double Fx;
  double Fy;
  double Fz;
  double Fe;

  double Px();
  double Py();
  double Pz();
  double Pt();

  double X();
  double Y();
  double Z();
  double E();
  double T();

  double M();

  double P();

  double Perp();
  double Perp2();

  double Mag();
  double Mag2();

  void SetPx(double x);
  void SetPy(double y);
  void SetPz(double z);
  void SetE(double e);
  void SetPxPyPzE(double px, double py, double pz, double e);
  void SetPtEtaPhiE(double pt, double eta, double phi, double e);

  STVector3 Vect();

  double Angle(const STVector3 & q);

  STVector3 BoostVector();

  void Boost(double, double, double);

  inline void Boost(const STVector3 & b);

  inline stlorentz& operator =(const STLorentzVectorWFlags& a);

  inline stlorentz& operator +(const stlorentz& a);

  inline stlorentz operator - () const;

} STLorentzVector;

inline double STLorentzVector::Px(){ return Fx; }
inline double STLorentzVector::Py(){ return Fy; }
inline double STLorentzVector::Pz(){ return Fz; }
inline double STLorentzVector::Pt(){ return Perp(); }

inline double STLorentzVector::X(){ return Fx; }
inline double STLorentzVector::Y(){ return Fy; }
inline double STLorentzVector::Z(){ return Fz; }
inline double STLorentzVector::E(){ return Fe; }
inline double STLorentzVector::T(){ return Fe; }

inline double STLorentzVector::M(){ return Mag(); }

inline double STLorentzVector::P(){ return Mag(); }

inline STVector3 STLorentzVector::Vect()
{
   STVector3 vect;
   vect.Fx = Fx;
   vect.Fy = Fy;
   vect.Fz = Fz;
   return vect;
}

inline double STLorentzVector::Angle(const STVector3 & q)
{
   STVector3 vect;
   vect.Fx = Fx;
   vect.Fy = Fy;
   vect.Fz = Fz;
   return vect.Angle(q);
}


inline double STLorentzVector::Perp(){ return sqrt(Perp2()); }
inline double STLorentzVector::Perp2(){ return Fx*Fx + Fy*Fy;; }

inline double STLorentzVector::Mag(){
   double mm = Mag2();
   return mm < 0.0 ? -sqrt(-mm) : sqrt(mm);
}

inline double STLorentzVector::Mag2(){
  return T()*T() - (Fx*Fx + Fy*Fy + Fz*Fz);
}

inline STVector3 STLorentzVector::BoostVector(){
  STVector3 vect;
  vect.SetPxPyPz(X()/T(), Y()/T(), Z()/T());
  return vect;
}

inline void STLorentzVector::Boost(double bx, double by, double bz)
{
   //Boost this Lorentz vector
   double b2 = bx*bx + by*by + bz*bz;
   double gamma = 1.0 / sqrt(1.0 - b2);
   double bp = bx*X() + by*Y() + bz*Z();
   double gamma2 = b2 > 0 ? (gamma - 1.0)/b2 : 0.0;

   SetPx(X() + gamma2*bp*bx + gamma*bx*T());
   SetPy(Y() + gamma2*bp*by + gamma*by*T());
   SetPz(Z() + gamma2*bp*bz + gamma*bz*T());
   SetE(gamma*(T() + bp));
}

inline void STLorentzVector::Boost(const STVector3 & b) {
   Boost(b.Fx, b.Fy, b.Fz);
}

inline void STLorentzVector::SetPx(double x){ Fx = x; }
inline void STLorentzVector::SetPy(double y){ Fy = y; }
inline void STLorentzVector::SetPz(double z){ Fz = z; }
inline void STLorentzVector::SetE(double e){ Fe = e; }
inline void STLorentzVector::SetPxPyPzE(double px, double py, double pz, double e){
  Fx = px; Fy =py; Fz = pz; Fe = e;
}

inline void STLorentzVector::SetPtEtaPhiE(double pt, double eta, double phi, double e) {
   pt = abs(pt);
   SetPxPyPzE(pt*cos(phi), pt*sin(phi), pt*sinh(eta) ,e);
}

inline stlorentz& STLorentzVector::operator =(const STLorentzVectorWFlags& a)
{
    Fx = a.Fx;
    Fy = a.Fy;
    Fz = a.Fz;
    Fe = a.Fe;
    return *this;
}

inline stlorentz& STLorentzVector::operator +(const stlorentz& a)
{
    Fx += a.Fx;
    Fy += a.Fy;
    Fz += a.Fz;
    Fe += a.Fe;
    return *this;
}

inline stlorentz STLorentzVector::operator - () const {
   STLorentzVector tlvect;
   tlvect.SetPxPyPzE(-Fx, -Fy, -Fz, -Fe);
   return tlvect;
}
#endif
