#include "TTH_STVector3.h"
#include "TTH_STLorentzVector.h"
// #include "TTH_STLorentzVectorWFlags.h"

// #include "TTH_offload_aux.h"
#include <vector>
#include "myvector.h"

#define GeV 1000
#define Luminosity 2.034
#define TPI 3.14159265358979312
#define mc_lum 2.034	//mc_aux e sempre 2

// int getWorkStatus();
void processCutsMIC0(EventPack pack[], int cutReport[], int nevents);
void processCutsMIC1(EventPack pack[], int cutReport[], int nevents);
