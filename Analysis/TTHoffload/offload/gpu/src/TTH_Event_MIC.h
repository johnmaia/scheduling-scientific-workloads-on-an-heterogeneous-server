#ifndef hepevmic_h
#define hepevmic

#include "TTH_STLorentzVectorWFlags.h"
#include "TTH_STVector3.h"

typedef struct eventMIC {
  int eventID;

  HEPEvent_Offload ev;
  std::vector<STLorentzVectorWFlags> Off_JetVec;
  std::vector<STLorentzVectorWFlags> Off_LeptonVec;
  std::vector<STLorentzVectorWFlags> Off_MyGoodJetVec;
  std::vector<STLorentzVectorWFlags> Off_MyGoodBtaggedJetVec;
  std::vector<STLorentzVectorWFlags> Off_MyGoodNonBtaggedJetVec;
} EventMIC;

#endif
