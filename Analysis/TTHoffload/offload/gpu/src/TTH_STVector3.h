#ifndef stvector3_h
#define stvector3_h

#include <cuda.h>

typedef struct stvector3 {
  double Fx;
  double Fy;
  double Fz;

  __device__ double Px();
  __device__ double Py();
  __device__ double Pz();

  __device__ double X();
  __device__ double Y();
  __device__ double Z();

  __device__ void SetPx(double x);
  __device__ void SetPy(double y);
  __device__ void SetPz(double z);
  __device__ void SetPxPyPz(double px, double py, double pz);

  __device__ double Mag2();
  __device__ double Dot(const stvector3 & p);
  __device__ double Angle(const stvector3 & q);

  __device__ inline stvector3 operator - () const;

} STVector3;

inline __device__ double STVector3::Px(){ return Fx; }
inline __device__ double STVector3::Py(){ return Fy; }
inline __device__ double STVector3::Pz(){ return Fz; }

inline __device__ double STVector3::X(){ return Fx; }
inline __device__ double STVector3::Y(){ return Fy; }
inline __device__ double STVector3::Z(){ return Fz; }

inline __device__ void STVector3::SetPx(double x){ Fx = x; }
inline __device__ void STVector3::SetPy(double y){ Fy = y; }
inline __device__ void STVector3::SetPz(double z){ Fz = z; }
inline __device__ void STVector3::SetPxPyPz(double px, double py, double pz){
  Fx = px; Fy =py; Fz = pz;
}

inline __device__ double STVector3::Mag2(){
  return Fx*Fx + Fy*Fy + Fz*Fz;
}

inline __device__ double STVector3::Dot(const stvector3 & p) {
   return Fx*p.Fx + Fy*p.Fy + Fz*p.Fz;
}

inline __device__ double STVector3::Angle(const stvector3 & q)
{
   double ptot2 = (Fx*Fx + Fy*Fy + Fz*Fz)*(q.Fx*q.Fx + q.Fy*q.Fy + q.Fz*q.Fz);
   if(ptot2 <= 0) {
      return 0.0;
   } else {
      double arg = Dot(q)/sqrt(ptot2);
      if(arg >  1.0) arg =  1.0;
      if(arg < -1.0) arg = -1.0;
      return acos(arg);
   }
}

inline __device__ stvector3 STVector3::operator - () const {
   stvector3 vect;
   vect.SetPxPyPz(-Fx, -Fy, -Fz);
   return vect;
}
#endif
