#ifndef stlorentz_h
#define stlorentz_h

#include <stdlib.h>
#include <cuda.h>
#include "TTH_STVector3.h"
#include "TTH_STLorentzVectorWFlags.h"

typedef struct stlorentz {
  double Fx;
  double Fy;
  double Fz;
  double Fe;

  __device__ double Px();
  __device__ double Py();
  __device__ double Pz();
  __device__ double Pt();

  __device__ double X();
  __device__ double Y();
  __device__ double Z();
  __device__ double E();
  __device__ double T();

  __device__ double M();

  __device__ double P();

  __device__ double Perp();
  __device__ double Perp2();

  __device__ double Mag();
  __device__ double Mag2();

  __device__ void SetPx(double x);
  __device__ void SetPy(double y);
  __device__ void SetPz(double z);
  __device__ void SetE(double e);
  __device__ void SetPxPyPzE(double px, double py, double pz, double e);
  __device__ void SetPtEtaPhiE(double pt, double eta, double phi, double e);

  __device__ STVector3 Vect();

  __device__ double Angle(const STVector3 & q);

  __device__ STVector3 BoostVector();

  __device__ void Boost(double, double, double);

  inline __device__ void Boost(const STVector3 & b);

  inline __device__ stlorentz& operator =(const STLorentzVectorWFlags& a);

  inline __device__ stlorentz& operator +(const stlorentz& a);

  inline __device__ stlorentz operator - () const;

} STLorentzVector;

inline __device__ double STLorentzVector::Px(){ return Fx; }
inline __device__ double STLorentzVector::Py(){ return Fy; }
inline __device__ double STLorentzVector::Pz(){ return Fz; }
inline __device__ double STLorentzVector::Pt(){ return Perp(); }

inline __device__ double STLorentzVector::X(){ return Fx; }
inline __device__ double STLorentzVector::Y(){ return Fy; }
inline __device__ double STLorentzVector::Z(){ return Fz; }
inline __device__ double STLorentzVector::E(){ return Fe; }
inline __device__ double STLorentzVector::T(){ return Fe; }

inline __device__ double STLorentzVector::M(){ return Mag(); }

inline __device__ double STLorentzVector::P(){ return Mag(); }

inline __device__ STVector3 STLorentzVector::Vect()
{
   STVector3 vect;
   vect.Fx = Fx;
   vect.Fy = Fy;
   vect.Fz = Fz;
   return vect;
}

inline __device__ double STLorentzVector::Angle(const STVector3 & q)
{
   STVector3 vect;
   vect.Fx = Fx;
   vect.Fy = Fy;
   vect.Fz = Fz;
   return vect.Angle(q);
}


inline __device__ double STLorentzVector::Perp(){ return sqrt(Perp2()); }
inline __device__ double STLorentzVector::Perp2(){ return Fx*Fx + Fy*Fy;; }

inline __device__ double STLorentzVector::Mag(){
   double mm = Mag2();
   return mm < 0.0 ? -sqrt(-mm) : sqrt(mm);
}

inline __device__ double STLorentzVector::Mag2(){
  return T()*T() - (Fx*Fx + Fy*Fy + Fz*Fz);
}

inline __device__ STVector3 STLorentzVector::BoostVector(){
  STVector3 vect;
  vect.SetPxPyPz(X()/T(), Y()/T(), Z()/T());
  return vect;
}

inline __device__ void STLorentzVector::Boost(double bx, double by, double bz)
{
   //Boost this Lorentz vector
   double b2 = bx*bx + by*by + bz*bz;
   double gamma = 1.0 / sqrt(1.0 - b2);
   double bp = bx*X() + by*Y() + bz*Z();
   double gamma2 = b2 > 0 ? (gamma - 1.0)/b2 : 0.0;

   SetPx(X() + gamma2*bp*bx + gamma*bx*T());
   SetPy(Y() + gamma2*bp*by + gamma*by*T());
   SetPz(Z() + gamma2*bp*bz + gamma*bz*T());
   SetE(gamma*(T() + bp));
}

inline __device__ void STLorentzVector::Boost(const STVector3 & b) {
   Boost(b.Fx, b.Fy, b.Fz);
}

inline __device__ void STLorentzVector::SetPx(double x){ Fx = x; }
inline __device__ void STLorentzVector::SetPy(double y){ Fy = y; }
inline __device__ void STLorentzVector::SetPz(double z){ Fz = z; }
inline __device__ void STLorentzVector::SetE(double e){ Fe = e; }
inline __device__ void STLorentzVector::SetPxPyPzE(double px, double py, double pz, double e){
  Fx = px; Fy =py; Fz = pz; Fe = e;
}

inline __device__ void STLorentzVector::SetPtEtaPhiE(double pt, double eta, double phi, double e) {
   pt = abs(pt);
   SetPxPyPzE(pt*cos(phi), pt*sin(phi), pt*sinh(eta) ,e);
}

inline __device__ stlorentz& STLorentzVector::operator =(const STLorentzVectorWFlags& a)
{
    Fx = a.Fx;
    Fy = a.Fy;
    Fz = a.Fz;
    Fe = a.Fe;
    return *this;
}

inline __device__ stlorentz& STLorentzVector::operator +(const stlorentz& a)
{
    Fx += a.Fx;
    Fy += a.Fy;
    Fz += a.Fz;
    Fe += a.Fe;
    return *this;
}

inline __device__ stlorentz STLorentzVector::operator - () const {
   STLorentzVector tlvect;
   tlvect.SetPxPyPzE(-Fx, -Fy, -Fz, -Fe);
   return tlvect;
}
#endif
