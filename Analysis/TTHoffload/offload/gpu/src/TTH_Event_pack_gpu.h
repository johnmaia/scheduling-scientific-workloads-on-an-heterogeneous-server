#ifndef hepevpack_gpu_h
#define hepevpack_gpu

#include "TTH_STLorentzVectorWFlags.h"
#include "TTH_STVector3.h"

typedef struct eventPack_gpu {
  int eventID;
  HEPEvent_Offload_GPU ev;
} EventPackGPU;

#endif
