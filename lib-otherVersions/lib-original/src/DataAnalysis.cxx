#include "DataAnalysis.h"

using namespace std;
using namespace boost::interprocess;

extern vector<HEPEvent> events;
long unsigned event_counter;
long unsigned event_counter_process;
#pragma omp threadprivate(event_counter)
unsigned thread_id;
#pragma omp threadprivate(thread_id)
unsigned num_threads;

boost::thread *filereader_thread;

/*
 * DISCLAIMER => cuts begin with id 1 to number_of_cuts (including this value)
 */

DataAnalysis::DataAnalysis (vector<string> _rootfile_list, string _tree_filename, string _output_root_file, string _signal_file, string _background_file, string _tree_name, unsigned ncuts) {
	#ifdef D_MPI
		MPI_Comm_rank(MPI_COMM_WORLD, &process_id);
		MPI_Comm_size(MPI_COMM_WORLD, &number_of_processes);
	#else
		process_id = 0;
		number_of_processes = 1;
	#endif

	event_counter_process = 0;
	event_counter = 0;
	tree_name = _tree_name;
	root_file_list = _rootfile_list;
	stringstream ts1;
	ts1 << _output_root_file;
	ts1 << "_" << process_id << ".root";
	output_root_file = ts1.str();
	stringstream ts2;
	ts2 << _tree_filename;
	ts2 << "_" << process_id << ".root";
	tree_filename = ts2.str();
	signal_file = _signal_file;
	background_file = _background_file;
	pdfs = false;
	number_of_cuts = ncuts;

	initCuts();
}

DataAnalysis::DataAnalysis (vector<string> _rootfile_list, string _tree_filename, string _output_root_file, string _signal_file, string _background_file, unsigned ncuts) {
	#ifdef D_MPI
		MPI_Comm_rank(MPI_COMM_WORLD, &process_id);
		MPI_Comm_size(MPI_COMM_WORLD, &number_of_processes);
	#else
		process_id = 0;
		number_of_processes = 1;
	#endif

	event_counter_process = 0;
	event_counter = 0;
	tree_name = "unset";
	root_file_list = _rootfile_list;
	stringstream ts1;
	ts1 << _output_root_file;
	ts1 << "_" << process_id << ".root";
	output_root_file = ts1.str();
	stringstream ts2;
	ts2 << _tree_filename;
	ts2 << "_" << process_id << ".root";
	tree_filename = ts2.str();
	signal_file = _signal_file;
	background_file = _background_file;
	pdfs = false;
	number_of_cuts = ncuts;

	initCuts();
}

DataAnalysis::~DataAnalysis (void) {
	delete[] tree_rec_vars;
}

void DataAnalysis::initCuts (void) {
	cut_counter   = 1;

	cut_organiser.initialize(number_of_cuts);

	tree_rec_vars = new TTree*[number_of_cuts];

	for (unsigned i = 0; i < number_of_cuts; i++){
		stringstream s1, s2;
		s1 << "Recorded_Vars_";
		s1 << i;
		s2 << "Variables recorded for cut ";
		s2 << i;

		tree_rec_vars[i] = new TTree (s1.str().c_str(), s2.str().c_str());
	}
}

void DataAnalysis::loop (void) {

	size_t const max_msg_size = 0x100;

	// receiving
	std::string fileName;
	fileName.resize(max_msg_size);
	size_t msg_size;
	unsigned msg_prio;

	//Open a message queue.
	message_queue mq (open_only ,"filelist_queue");

	//cout << "Dataset size: " << number_events * sizeof(HEPEvent)/1048576 << " MBytes" << endl;
	while(true){
		 fileName.clear();
		 fileName.resize(max_msg_size);

		 bool rcv = mq.try_receive(&fileName[0], fileName.size(), msg_size, msg_prio);

		 if(!rcv){
			 break;
		 }
		 else{
			fileName.resize(msg_size);
			events.clear();

			//cout << "[Process] " << process_id << " | File: " << fileName << endl;
			if (tree_name == "unset") {
				DataReader root_reader (fileName);
				root_reader.loadEvents();
			} else {
				DataReader root_reader (fileName, tree_name);
				root_reader.loadEvents();
			}

		 	//cout << "File: " << fileName << endl;

			long unsigned number_events = events.size();
			event_counter_process += events.size();

			#pragma omp parallel
			{
				#ifdef D_MULTICORE
				thread_id = omp_get_thread_num();
				num_threads = omp_get_num_threads();
				#else
				thread_id = 0;
				num_threads = 1;
				#endif

				#pragma omp single
				initRecord();

				#pragma omp for schedule(dynamic)
				for (unsigned ev = 0; ev < number_events; ev++) {
					event_counter = ev;

					processCuts();

					/*
					if (ev % CUT_CHECKPOINT == 0) {
						printCuts();	// debug purposes
						cut_organiser.computeCutOrder();
						cut_organiser.reorderCuts();
						printCuts();	// debug purposes
					}
					*/
				}
			}

		event_counter++;
		writeVariables();
		writePdfs();
		}
	}
}

void DataAnalysis::loopNoRecord (void) {

	size_t const max_msg_size = 0x100;

	// receiving
	std::string fileName;
	fileName.resize(max_msg_size);
	size_t msg_size;
	unsigned msg_prio;

	//Open a message queue.
	message_queue mq (open_only ,"filelist_queue");

	while(true){
		 fileName.clear();
		 fileName.resize(max_msg_size);

		 bool rcv = mq.try_receive(&fileName[0], fileName.size(), msg_size, msg_prio);

		 if(!rcv){
			 break;
		 }
		 else{

			fileName.resize(msg_size);
			events.clear();
			if (tree_name == "unset") {
				DataReader root_reader (fileName);
				root_reader.loadEvents();
			} else {
				DataReader root_reader (fileName, tree_name);
				root_reader.loadEvents();
			}

		 	//cout << "File: " << fileName << endl;

			long unsigned number_events = events.size();
			event_counter_process += events.size();

			#pragma omp parallel
			{
				#ifdef D_MULTICORE
				thread_id = omp_get_thread_num();
				num_threads = omp_get_num_threads();
				#else
				thread_id = 0;
				num_threads = 1;
				#endif

				#pragma omp single
				initRecord();

				#pragma omp for schedule(dynamic)
				for (unsigned ev = 0; ev < number_events; ev++) {
					event_counter = ev;

					processCutsNoRecord();

					/*
					if (ev % CUT_CHECKPOINT == 0) {
						printCuts();	// debug purposes
						cut_organiser.computeCutOrder();
						cut_organiser.reorderCuts();
						printCuts();	// debug purposes
					}
					*/
					}
				}
			event_counter++;
			}
		}
}


void DataAnalysis::smFileListQ (void) {

	// size_t const max_msg_size = 0x100;
	//
	// //Erase previous message queue
	// message_queue::remove("filelist_queue");
	//
	// //Create a message_queue.
	// message_queue mq
	// 								 (create_only               //only create
	// 								 ,"filelist_queue"          //name
	// 								 ,root_file_list.size()     //max message number
	// 								 ,max_msg_size              //max message size
	// 								 );
	//
	//
	// // reads a specific amount of files allocated for each process
	// for (int i = 0; i < root_file_list.size(); i++) {
	// 	std::string s(root_file_list[i]);
	// 	mq.send(s.data(), s.size(), 0);
	// }
}

void DataAnalysis::run (void) {

	t.start();

	// Start filereader thread
	//filereader_thread = new boost::thread(boost::bind(&DataAnalysis::smFileListQ, this));
	//filereader_thread->join();

	// Start shared memory filelist
	//smFileListQ();

	initialize();

	stringstream err;
	err << "unset_" << process_id << ".root";

	if (tree_filename != err.str())
		loop();
	else
		loopNoRecord();

	finalize();

	if (save_events)
		writeEvents();

	#ifdef D_MPI
		MPI_Barrier(MPI_COMM_WORLD);
	#endif

	t.stop();

	#ifdef D_REPORT
		report();
	#endif

	t.report(Report::Verbose);
}

//inline
void DataAnalysis::processCuts (void) {
	bool pass = true;
	unsigned cut_number = 0;
	unsigned cut_id;
	Cut c;

	for (unsigned i = 1; i <= number_of_cuts && pass; i++) {
		cut_id = cut_organiser.getCutOrder(i);
		c = cuts[cut_id];

	//	cut_organiser.cutInitialTime();

		pass = c();

	//	cut_organiser.cutEndTime(cut_id);

		if (pass) {

			if (i == number_of_cuts)
				recordPdfs();

			#pragma omp critical
			recordVariables(cut_number);

			#ifdef D_REPORT
				cut_organiser.incPassedEvent(cut_id);
			#endif
		} else {
			#ifdef D_REPORT
				#pragma omp critical
				cut_organiser.incFailedEvent(cut_id);
			#endif
		}
		cut_number++;
	}
}

//inline
void DataAnalysis::processCutsNoRecord (void) {
	bool pass = true;
	unsigned cut_number = 0;
	unsigned cut_id;
	Cut c;

	for (unsigned i = 1; i <= number_of_cuts && pass; i++) {
		cut_id = cut_organiser.getCutOrder(i);
		c = cuts[cut_id];

	//	cut_organiser.cutInitialTime();

		pass = c();

	//	cut_organiser.cutEndTime(cut_id);

		if (pass) {

			#ifdef D_REPORT
				#pragma omp critical
				cut_organiser.incPassedEvent(cut_id);
			#endif
		} else {
			#ifdef D_REPORT
				#pragma omp critical
				cut_organiser.incFailedEvent(cut_id);
			#endif
		}
		cut_number++;
	}
}

void DataAnalysis::initialize (void) {
}

void DataAnalysis::finalize (void) {
}

void DataAnalysis::initRecord (void) {
}

void DataAnalysis::report (void) {

	#ifdef D_MPI
	long unsigned local_events = events.size();
	long unsigned global_events;

	MPI_Reduce(&event_counter_process, &global_events, 1, MPI_UNSIGNED_LONG, MPI_SUM, 0, MPI_COMM_WORLD);

	if (process_id == 0)
		cout << event_counter_process << " events analysed by " << number_of_processes << " processes with " << num_threads << " threads each" << endl << endl;
	#else
	cout << event_counter_process << " events analysed by process " << process_id << " with " << num_threads << " threads" << endl << endl;
	#endif

	cout << "Number of events that passed each cut: " << endl;

	for (unsigned i = 1; i <= number_of_cuts; i++) {
	//	for (unsigned j  cut_table)
				#ifdef D_MPI
				local_events = cut_organiser.getPassedEvents(cut_organiser.getCutOrder(i) - 1);

				MPI_Reduce(&local_events, &global_events, 1, MPI_UNSIGNED_LONG, MPI_SUM, 0, MPI_COMM_WORLD);

				if(process_id == 0)
					cout << "\t=> " << i << ": " << global_events << endl;
				#else
				cout << "\t=> " << i << ": " << cut_organiser.getPassedEvents(cut_organiser.getCutOrder(i) - 1) << endl;
				#endif
			}

	cout << endl;
	//printCuts();
}

unsigned DataAnalysis::addCut (string name, Cut c) {
	cuts.insert(make_pair(cut_counter, c));
	cut_table.insert(make_pair(name, cut_counter));

	cut_counter++;

	return cuts.size();
}

void DataAnalysis::printCuts (void) {
/*	for (unsigned i = 0; i < number_of_cuts + 1; i++) {
		if (!i) {
			cout << "\tstart\t";

			for (auto &ct : cut_table)
				cout << ct.first << "\t";

			cout << endl;
		}

		for (unsigned j = 0; j < number_of_cuts + 1; j++) {
			if (!j) {
				if(!i)
					cout << "start\t";

				unsigned b = 0;
				for (auto &ct : cut_table) {
					if (b == (i - 1)) {
						cout << ct.first << "\t";
						break;
					}

					b++;
				}
			}

			cout << cut_organiser.getCutDependency(i, j) << "\t";
		}
		cout << endl;
	}
	cout << endl << endl;

	printCutsOrder();*/
}

void DataAnalysis::printCutsOrder (void) {

/*	cout << "== Cut order ==" << endl;
	for (unsigned i = 1; i < number_of_cuts + 1; i++)
		for (auto &a : cut_table)
			if (a.second == cut_organiser.getCutOrder(i))
				cout << "\t" << a.first << endl;

	cout << endl;*/
}

bool DataAnalysis::addCutDependency (string cut1, string cut2) {
	// check if the cut exists on the lookup table
	if (cut_table.find(cut1) == cut_table.end() || cut_table.find(cut2) == cut_table.end() )
		return false;

	// checks if it's a duplicate
	if (cut1 == cut2)
		return false;

	// add the dependency
	cut_organiser.addCutDependency(cut_table.find(cut1)->second, cut_table.find(cut2)->second);

	return true;
}

void DataAnalysis::writeEvents (void) {
	TFile out_file (output_root_file.c_str(), "recreate");
	TTree *to_write = new TTree ("Final_Events", "Events that passed all cuts");

	for (long unsigned i = 0; i < cut_organiser.getPassedEvents(number_of_cuts - 1); i++) {
	//	events[events_passed_per_cut[number_of_cuts - 1].at(i)].write(to_write);
	}
}

void DataAnalysis::recordVariables (unsigned cut_number) {
}

void DataAnalysis::writeVariables (void) {
}

void DataAnalysis::recordPdfs (void) {
}

void DataAnalysis::writePdfs (void) {
}

void DataAnalysis::readPdf (vector<string> signals, vector<string> backgrounds) {
	// Read signal pdf
	if (signal_file != "unset") {
		TFile file (signal_file.c_str());

		for (unsigned i = 0; i < signals.size(); i++) {
			TH1D *p = (TH1D*) file.Get(signals[i].c_str());
			signal_pdf.push_back(*p);
		}
	}

	// Read background pdf
	if (background_file != "unset") {
		TFile file (background_file.c_str());

		for (unsigned i = 0; i < backgrounds.size(); i++) {
			TH1D *p = (TH1D*) file.Get(backgrounds[i].c_str());
			background_pdf.push_back(*p);
		}
	}

	pdfs = true;
}

void DataAnalysis::writeAllHistograms (void) {
}

long unsigned DataAnalysis::currentEvent (void) {
	return event_counter;
}
