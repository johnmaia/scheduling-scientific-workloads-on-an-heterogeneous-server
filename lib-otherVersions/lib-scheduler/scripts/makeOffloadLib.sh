BOOST_DIR=/home/a51752/tools/boost/1.59-icc
HEMI_DIR=/home/a51752/tools/hemi/2.0

# Remove old files
ORIGINAL_FOLDER="$(pwd)"

cd ../Analysis/TTHoffload/offload/mic
rm -f build/*
rm -f lib/*

mkdir -p build/
mkdir -p lib/

# Compile new lib
icpc -fopenmp -c -Wall -std=c++11 src/TTH_offload.cxx -o build/TTH_offload.o
ar -r lib/libOffload.a build/TTH_offload.o

# Build GPU offload
cd ../gpu

rm -f build/*
rm -f lib/*

mkdir -p build/
mkdir -p lib/

# Compile new lib
nvcc -c -std=c++11 --relaxed-constexpr -I$HEMI_DIR src/TTH_offload_gpu.cu -o build/TTH_offload.o
ar -r lib/libOffload.a build/TTH_offload.o

cd $ORIGINAL_FOLDER
