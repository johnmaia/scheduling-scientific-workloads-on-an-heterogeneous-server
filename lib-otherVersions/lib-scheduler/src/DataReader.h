#ifndef DATAREADER_h
#define DATAREADER_h

#include <string>
#include <iostream>
#include <vector>
#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <TKey.h>
#include "Event.h"

using namespace std;

class DataReader {
	TTree *fChain;	//!pointer to the analyzed TTree or TChain
	int fCurrent;	//!current Tree number in a TChain
	long number_events;

	void treeInit (void);
	int getEntry (long entry);
	long loadTree (long entry);

public:
	DataReader (string filepath, string tree_name);
	DataReader (string filepath);
	~DataReader (void);
	void loadEvents (void);
};

#endif
