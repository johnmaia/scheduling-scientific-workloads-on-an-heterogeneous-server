#ifndef CUTORGANISER_h
#define CUTORGANISER_h

#include <cstdlib>
#include <iostream>
#include <fstream>
#include <vector>
#include <omp.h>
#include <chrono>
#include <string>
#include <sstream>

#define DEFAULT_CUT_WEIGHT 1
#define INF_CUT_WEIGHT 999999
#define MAX_CUT_WEIGHT 10000
#define MAX_CUT_TIME 1000

using namespace std;

class CutOrganiser {
	unsigned number_of_cuts;
	long unsigned longest_cut;

	unsigned **cut_dependencies;	// cut code and and cut code "x depends on y"
	double *cut_weight;
	unsigned *cut_order;

	long unsigned *cut_execution_time;
	long unsigned *events_passed_per_cut;
	long unsigned *events_failed_per_cut;

	chrono::time_point<chrono::high_resolution_clock> initial_time;

	void updateCutWeight (unsigned cut_number);

	bool exists (unsigned vertex, vector<unsigned> current_path);
	bool existsDep (unsigned dep_vertex, unsigned source_vertex, vector<unsigned> current_path);
	bool checkDependencies (unsigned vertex, vector<unsigned> current_path);
	unsigned pathCost (vector<unsigned> current_path);
	vector<unsigned> route (unsigned start, vector<unsigned> current_path, unsigned level);

public:
	CutOrganiser (void);
	~CutOrganiser (void);
	CutOrganiser (unsigned _number_of_cuts);
	void initialize (unsigned _number_of_cuts);
	void addCutDependency (unsigned cut1, unsigned cut2);
	void computeCutOrder (void);
	void initCutDependencies (void);

	inline void incPassedEvent (unsigned cut) { events_passed_per_cut[cut - 1]++; }
	inline void incFailedEvent (unsigned cut) { events_failed_per_cut[cut - 1]++; }
	inline long unsigned getPassedEvents (unsigned cut) { return events_passed_per_cut[cut]; }
	inline long unsigned getFailedEvents (unsigned cut) { return events_failed_per_cut[cut]; }
	inline unsigned getCutDependency (unsigned cut1, unsigned cut2) { return cut_dependencies[cut1][cut2]; }
	inline unsigned getCutOrder (unsigned index) { return cut_order[index]; }
	void cutInitialTime (void);
	void cutEndTime (unsigned cut_number);
	void reorderCuts (void);
	void readOrder (string filename);
	void writeOrder (string filename);

	// for debugging purposes
	inline void setWeight (unsigned cut1, unsigned cut2, unsigned weight) { cut_dependencies[cut1][cut2] = weight; }
};

#endif
