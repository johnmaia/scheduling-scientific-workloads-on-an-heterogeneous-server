#include "DataReader.h"

#ifdef D_DELPHES
	#include <ExRootAnalysis/ExRootAnalysis/ExRootTreeReader.h>

	extern ExRootTreeReader *ex_tree;
	extern TTree *tree;

	extern TClonesArray *branchJet;
	extern TClonesArray *branchElectron;
	extern TClonesArray *branchPhoton;
	extern TClonesArray *branchMuon;
	extern TClonesArray *branchMissingET;
	extern TClonesArray *branchScalarHT;
	extern TClonesArray *branchGenParticle;
	extern TClonesArray *branchTrack;
	extern TClonesArray *branchTower;
	extern TClonesArray *branchLHCOEvent;
	extern TClonesArray *branchLHEFEvent;
	extern TClonesArray *branchHepMCEvent;
	extern TClonesArray *branchEvent;
#endif

using namespace std;

vector<HEPEvent> events;

// DISCLAIMER: ROOT file needs a TTree
DataReader::DataReader (string filepath, string tree_name) : fChain (0) {

	TFile *file = (TFile*)gROOT->GetListOfFiles()->FindObject(filepath.c_str());

	if (!file || !file->IsOpen()) {
		file = new TFile(filepath.c_str());
	} else {
		cerr << "Error: " << filepath << " does not exist or is not accessible!" << endl;
		exit(-1);
	}

	file->GetObject(tree_name.c_str(), fChain);

	if (!fChain) {
		cerr << "Error: Problem in the root file structure" << endl;
		cerr << "Error: TTree " << fChain->GetName() << endl;
		exit (-1);
	}

	fCurrent = -1;
	fChain->SetMakeClass(1);
	number_events = fChain->GetEntriesFast();
}

DataReader::DataReader (string filepath) : fChain (0) {

	TFile *file = (TFile*)gROOT->GetListOfFiles()->FindObject(filepath.c_str());

	if (!file || !file->IsOpen()) {
		file = new TFile(filepath.c_str());
	} else {
		cerr << "Error: " << filepath << " does not exist or is not accessible!" << endl;
		exit(-1);
	}

	TIter nextkey (file->GetListOfKeys());
	TKey *key;
	bool tree_found = false;

	while ((key = (TKey*) nextkey()) && !tree_found) {
		TObject *obj = key->ReadObj();
		if (obj->IsA()->InheritsFrom(TTree::Class())) {
			fChain = (TTree*)obj;
			tree_found = true;
		}
	}

	if (!fChain) {
		cerr << "Error: Problem in the root file structure" << endl;
		cerr << "Error: TTree " << fChain->GetName() << endl;
		exit (-1);
	}

#ifndef D_DELPHES
	fCurrent = -1;
	fChain->SetMakeClass(1);
	number_events = fChain->GetEntriesFast();
#endif

	delete key;
}

DataReader::~DataReader (void) {

	if (!fChain)
		return;

//	delete fChain->GetCurrentFile();	//comentado so para o TTH
}

// It depends on the event data...
void DataReader::treeInit (void) {

}

// Read contents of entry
int DataReader::getEntry (long entry) {

	if (!fChain)
		return 0;

	return fChain->GetEntry(entry);
}

// Set the environment to read one entry
long DataReader::loadTree (long entry) {

	if (!fChain)
		return -5;

	long centry = fChain->LoadTree(entry);

	if (centry < 0)
		return centry;

	if (fChain->GetTreeNumber() != fCurrent)
		fCurrent = fChain->GetTreeNumber();

	return centry;
}

// Load all events into memory
#ifdef D_DELPHES
void loadBranches (void) {

	// Get pointers to branches used in this analysis
	branchJet       = ex_tree->UseBranch("Jet");
	if (branchJet==NULL) cout << "Jet collection branch is not found" << endl;

	branchElectron  = ex_tree->UseBranch("Electron");
	if (branchElectron==NULL) cout << "Electron collection branch is not found" << endl;

	branchPhoton    = ex_tree->UseBranch("Photon");
	if (branchPhoton==NULL) cout << "Photon collection branch is not found" << endl;

	branchMuon      = ex_tree->UseBranch("Muon");
	if (branchMuon==NULL) cout << "Muon collection branch is not found" << endl;

	branchMissingET = ex_tree->UseBranch("MissingET");
	if (branchMissingET==NULL) cout << "MissingEt branch is not found" << endl;

	branchScalarHT = ex_tree->UseBranch("ScalarHT");
	if (branchScalarHT==NULL) cout << "ScalarHT branch is not found" << endl;

	branchGenParticle = ex_tree->UseBranch("Particle");
	if (branchGenParticle==NULL) cout << "GenParticle branch is not found" << endl;

	branchTrack = ex_tree->UseBranch("Track");
	if (branchTrack==NULL) cout << "Track branch is not found" << endl;

	branchTower = ex_tree->UseBranch("Tower");
	if (branchTower==NULL) cout << "Tower branch is not found" << endl;
}

void DataReader::loadEvents (void) {

	ex_tree = new ExRootTreeReader(fChain);

	loadBranches();

	long unsigned nevents = ex_tree->GetEntries();
	number_events = nevents;


	for (long unsigned i = 0; i < nevents; i++) {
		HEPEvent myEvent;

		ex_tree->ReadEntry(i);

		myEvent.loadEvent();

		events.push_back(myEvent);
	}
}

#else

void DataReader::loadEvents (void) {
	bool loading = true;

	for (long event_id = 0; event_id < number_events && loading; event_id++) {
		long tentry = loadTree(event_id);

		if (tentry < 0) {
			cerr << "Error: Could not load all entries on the input file" << endl;
			cerr << "Error: Entry " << tentry << " in the " << fChain->GetName() << " TTree" << endl;

			loading = false;
		} else {
			// Creates and loads an event into memory
			HEPEvent ev (fChain);
			ev.init();
			ev.loadEvent(event_id);
			events.push_back(ev);

			/* Load extra info like MC */

		}
	}

	loading = false;
}
#endif
