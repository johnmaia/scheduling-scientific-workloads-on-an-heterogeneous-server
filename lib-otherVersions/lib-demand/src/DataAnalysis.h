#ifndef DATAANALYSIS_h
#define DATAANALYSIS_h

#include <string>
#include <sstream>
#include <map>
#include <vector>
#include <TH1D.h>
#include <TFile.h>
#include <TTree.h>
#include <TChain.h>
#include <omp.h>
#include "DataReader.h"
#include "Timer.h"
#include "TimeCounter.h"
#include "ProgramOptions.h"
#include "Event.h"
#include "CutOrganiser.h"
//#include <boost/thread.hpp>
#include <boost/interprocess/ipc/message_queue.hpp>
#include <boost/thread/thread.hpp>
#include <boost/ref.hpp>



#ifdef D_MPI
	#include <mpi.h>
#endif

#define NUM_THREADS 1
#define CUT_CHECKPOINT 100

typedef bool (*Cut) (void);

using namespace std;

class DataAnalysis {
protected:
	int process_id;
	int number_of_processes;

	vector<string> root_file_list;
	string output_root_file;
	string tree_filename;
	string tree_name;
	string signal_file;
	string background_file;
	TTree **tree_rec_vars;
	Timer t;

	vector<vector<unsigned>> areaPerThread;

	map<unsigned, Cut> cuts;	// map with name and function pointers for all cuts
	map<string, unsigned> cut_table;	// cut name to code
	unsigned cut_counter;	// different from number of cuts, it's used for the matrix and starts at 1
	CutOrganiser cut_organiser;

	bool save_events;
	// pdf input here
	vector<TH1D> signal_pdf;
	vector<TH1D> background_pdf;
	bool pdfs;

	// Counters
	unsigned number_of_cuts;

	void processCutsCPU(int setSize, int& evComputed, double &timeCPU);

	void loop (void);
	void processCuts (void);
	void loopNoRecord (void);
	void processCutsNoRecord (void);
	void writeAllHistograms (void);
	void report (void);
	void writeEvents (void);
	void initCuts (void);
	void initCutDependencies (void);
	void updateCutWeight (unsigned cut_number);
	void computeCutOrder (void);
	void reorderCuts (void);



	virtual void recordVariables (unsigned cut_number);
	virtual void writeVariables (void);
	virtual void initRecord (void);

	virtual void recordPdfs (void);
	virtual void writePdfs (void);

	virtual void initialize (void);
	virtual void finalize (void);

public:
	DataAnalysis (vector<string> _rootfile_list, string _tree_filename, string _output_root_file, string _signal_file, string _background_file, string _tree_name, unsigned ncuts);
	DataAnalysis (vector<string> _rootfile_list, string _tree_filename, string _output_root_file, string _signal_file, string _background_file, unsigned ncuts);
	~DataAnalysis (void);
	void smFileListQ (void);
	void run (void);
	unsigned addCut (string name, Cut c);
	bool addCutDependency (string cut1, string cut2);
	long unsigned currentEvent (void);
	void readPdf (vector<string> signals, vector<string> backgrounds);


	// debug
	void printCuts (void);
	void printCutsOrder (void);
};

#endif
