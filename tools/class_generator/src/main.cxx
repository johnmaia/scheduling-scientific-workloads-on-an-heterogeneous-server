#include <cstdlib>
#include <iostream>
#include <string>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <boost/program_options.hpp>

#include "EventClassGenerator.h"

using namespace std;
namespace po = boost::program_options;

string file;
string tree_name;
string classname;
string outdir;
bool delphes;
bool specific_tree;

bool programOptions (int argc, char *argv[]) {
	// creates the program options
	po::options_description desc ("Valid options");
	desc.add_options()
		("help,h", "produces the help message")
		("file,f", po::value<string>(), "input root file")
		("class,c", po::value<string>(), "name for the event class file (optional)")
		("dir,d", po::value<string>(), "directory for the generated class file")
		("type,t", po::value<string>(), "input file type (delphes/else) (optional)")
		("atree,a", po::value<string>(), "specific tree (optional)")
	;
	po::variables_map vm;
	po::store(po::parse_command_line(argc, argv, desc), vm);
	po::notify(vm);

	// get the options
	if (vm.count("help")) {
		cout << desc << endl;
		return false;
	}

	if (vm.count("file")) {
		file = vm["file"].as<string>();
		struct stat buffer;


		if (stat (file.c_str(), &buffer) != 0) {
			cerr << "Input root file does not exist" << endl;
			return false;
		}
	} else {
		cerr << "Input root file not set" << endl;
		return false;
	}

	if (vm.count("class")) {
		classname = vm["class"].as<string>();
	} else {
		classname = "unset";
	}

	if (vm.count("type")) {
		delphes = true;
	} else {
		delphes = false;
	}

	if (vm.count("dir")) {
		outdir = vm["dir"].as<string>();
		struct stat info;

		if(stat(outdir.c_str(), &info) != 0) {
			cerr << "Output directory does not exist" << endl;
			return false;
		}
	} else {
		cerr << "Output directory not set" << endl;
		return false;
	}

	if (vm.count("atree")) {
		tree_name = vm["atree"].as<string>();
		specific_tree = true;
	} else {
		specific_tree = false;
	}

	return true;
}

int main (int argc, char *argv[]) {
	EventClassGenerator *gen;

	if (!programOptions(argc, argv))
		return 0;

	cout << "Processing " << file << " root file" << endl;
	
	if (classname.compare("unset") == 0)
		if (specific_tree)
			gen = new EventClassGenerator (file, outdir, tree_name, true);
		else
			gen = new EventClassGenerator (file, outdir);
	else
		if (specific_tree)
			gen = new EventClassGenerator (file, classname, outdir, tree_name);
		else
			gen = new EventClassGenerator (file, classname, outdir);

	if (!delphes) {
		gen->makeClassCommon();
		gen->parseClassCommon();
	} else {
		gen->parseClassDelphes();
	}

	delete gen;

	return 1;
}