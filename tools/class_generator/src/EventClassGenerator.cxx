#include "EventClassGenerator.h"

using namespace std;

EventClassGenerator::EventClassGenerator (void) {
}

EventClassGenerator::EventClassGenerator (string _filename, string _dir) {
	filename = _filename;
	npa_dir = _dir;
	data_creator = new TFile (filename.c_str());
	number_events = 0;
	tree = NULL;
	vector<string> tokens;
	string s;
	istringstream f (filename);
	stringstream dir;

	while (getline(f, s, '/'))
		tokens.push_back(s);

	for (unsigned i = 0; i < tokens.size() - 1; i++)
		dir << "/" << tokens[i];

	char dd[256];
	getcwd(dd, 256);

	directory = dd;

	char *temp = strdup (tokens.back().c_str());
	char *token = strtok (temp, ".");

	classname = string(token);
	classname[0] = toupper(classname[0]);
}

EventClassGenerator::EventClassGenerator (string _filename, string _dir, string _tree_name, bool sd) {
	filename = _filename;
	npa_dir = _dir;
	tree_name = _tree_name;
	data_creator = new TFile (filename.c_str());
	number_events = 0;
	tree = NULL;
	vector<string> tokens;
	string s;
	istringstream f (filename);
	stringstream dir;

	while (getline(f, s, '/'))
		tokens.push_back(s);

	for (unsigned i = 0; i < tokens.size() - 1; i++)
		dir << "/" << tokens[i];

	char dd[256];
	getcwd(dd, 256);

	directory = dd;

	char *temp = strdup (tokens.back().c_str());
	char *token = strtok (temp, ".");

	classname = string(token);
	classname[0] = toupper(classname[0]);
}

EventClassGenerator::EventClassGenerator (string _filename, string _classname, string _dir) {
	filename = _filename;
	npa_dir = _dir;
	data_creator = new TFile (filename.c_str());
	number_events = 0;
	tree = NULL;
	classname = _classname;

	vector<string> tokens;
	string s;
	istringstream f (filename);
	stringstream dir;

	while (getline(f, s, '/'))
		tokens.push_back(s);

	for (unsigned i = 0; i < tokens.size() - 1; i++)
		dir << "/" << tokens[i];


	directory = dir.str();
	directory.erase(0, 1);
}

EventClassGenerator::EventClassGenerator (string _filename, string _classname, string _dir, string _tree_name) {
	filename = _filename;
	npa_dir = _dir;
	tree_name = _tree_name;
	data_creator = new TFile (filename.c_str());
	number_events = 0;
	tree = NULL;
	classname = _classname;

	vector<string> tokens;
	string s;
	istringstream f (filename);
	stringstream dir;

	while (getline(f, s, '/'))
		tokens.push_back(s);

	for (unsigned i = 0; i < tokens.size() - 1; i++)
		dir << "/" << tokens[i];


	directory = dir.str();
	directory.erase(0, 1);
}

EventClassGenerator::~EventClassGenerator (void) {

}

bool EventClassGenerator::makeClassCommon (void) {
	stringstream cmd;

	if (!data_creator->IsOpen())
		return false;

	TIter nextkey( data_creator->GetListOfKeys() );
	TKey *key;
	while ( (key = (TKey*)nextkey())) {
		TObject *obj = key->ReadObj();
		if ( obj->IsA()->InheritsFrom( TTree::Class() ) ) {
			tree = (TTree*)obj;
			if (tree->GetName() == tree_name)
				break;
		}
	}

	if (!tree)
		return false;

	// get the number of events
	number_events = tree->GetEntriesFast();

	tree->MakeClass(classname.c_str());

	cmd << "rm " << classname << ".C";

	system(cmd.str().c_str());

	return true;
}

void EventClassGenerator::parseClassCommon (void) {
	stringstream cl, ol, br, bd, cmd;

	cl << /*directory << "/" <<*/ classname << ".h";
	ifstream input (cl.str().c_str());
	ol << npa_dir << "/" << classname << ".h";
	ofstream out_header (ol.str().c_str());
	br << npa_dir << "/" << classname << "Branches.h";
	ofstream out_branches (br.str().c_str());
	bd << npa_dir << "/" << classname << ".cxx";
	ofstream out_body (bd.str().c_str());


	string line;
	bool parse_header = false;
	bool parse_const_header = false;
	bool parse_branches = false;
	bool parse_body = false;
	bool stop = false;
	stringstream class_header, class_branches, class_body;

	// creates the class header
	class_header << "#ifndef " << classname << "_h" << endl;
	class_header << "#define " << classname << "_h" << endl << endl;
	// class_header << "#include \"Event.h\"" << endl;
	class_header << "#include <TROOT.h>\n#include <TChain.h>\n#include <TFile.h>\n#include <vector>\n#include <TLorentzVector.h>\n\nusing namespace std;" << endl << endl;

	out_header << class_header.str();

	// creates the class branches
	class_branches << "#ifndef " << classname << "BRANCHES_h" << endl;
	class_branches << "#define " << classname << "BRANCHES_h\n" << endl;

	// creates the class body cxx
	class_body << "#include \"" << classname << ".h\"" << endl;
	class_body << "#include \"" << classname << "Branches.h\"\n" << endl;
	class_body << "extern bool tree_filled;\n" << endl;

	class_body << "HEPEvent::HEPEvent (TTree *tree) {\n\tfChain = tree;\n}\n" << endl;
	class_body << "HEPEvent::~HEPEvent (void) {\n}\n" << endl;
	class_body << "bool HEPEvent::init (void) {" << endl;

	out_body << class_body.str();

	class_body.clear();
	class_body.str(string());


	while (getline(input, line) && !stop) {
		// checks for const values in the header
		if (line.find(PARSE_CLASS_HEADER_CONST_START) != string::npos) {
			parse_const_header = true;
		}
		// checks for const values in the header
		if (line.find(PARSE_CLASS_HEADER_CONST_STOP) != string::npos) {
			parse_const_header = false;
			class_header.clear();
			class_header.str(string());
		}

		// checks to start parsing
		if (line.find(PARSE_CLASS_HEADER_START) != string::npos) {
			parse_header = true;
			class_header << "class HEPEvent {" << endl;
			class_header << "\tTTree *fChain;\n" << endl; 
			class_header << "public:" << endl;
			class_header << "\tlong id;" << endl;
			line = class_header.str();
		}

		// checks to stop parsing
		if (line.find(PARSE_CLASS_HEADER_STOP) != string::npos) {
			parse_branches = true;
			parse_header = false;
			class_header.clear();
			class_header.str(string());

			class_header << "\t// Add your own event variables here!" << endl << endl << endl << endl;
			class_header << "\tHEPEvent (TTree *tree);" << endl;
			class_header << "\t~HEPEvent (void);" << endl;
			class_header << "\tbool init (void);" << endl;
			class_header << "\tint loadEvent (long entry);" << endl;
			class_header << "\tvoid write (void);" << endl;
			class_header << "};\n#endif\n";

			out_header << class_header.str() << endl;

			line = class_branches.str();
		}

		// checks for branches
		if (line.find(PARSE_CLASS_BRANCHES_STOP) != string::npos) {
			parse_branches = false;
			class_branches.clear();
			class_branches.str(string());

			class_branches << "#endif" << endl;

			out_branches << class_branches.str() << endl;
		}

		// checks for body start (if there are pointers) and stop
		if (line.find(PARSE_CLASS_BODY_POINTER_START) != string::npos) {
			parse_body = true;
		}

		// checks for body start (if there are pointers) and stop
		if (line.find(PARSE_CLASS_BODY_POINTER_STOP) != string::npos) {
			parse_body = false;
		}

		// checks for body start and stop
		if (line.find(PARSE_CLASS_BODY_START) != string::npos) {
			parse_body = true;

			line = class_body.str();
		} else 
			if (line.find(PARSE_CLASS_BODY_STOP) != string::npos && parse_body) {
				parse_body = false;
				class_body.clear();
				class_body.str(string());
				
				class_body << "\treturn true;\n}\n" << endl;
				class_body << "int HEPEvent::loadEvent (long entry) {" << endl;
				class_body << "\tif (!fChain)\n\t\treturn 0;\n" << endl;
				class_body << "\tid = entry;\n" << endl;
				class_body << "\treturn fChain->GetEntry(entry);\n}\n" << endl;

				class_body << "void HEPEvent::write (void) {" << endl;
				class_body << "\tfChain->Write();\n}" << endl;

				out_body << class_body.str() << endl;
			}

		if (parse_header || parse_const_header)
			out_header << line << endl;

		if (parse_branches)
			out_branches << line << endl;

		if (parse_body)
			out_body << line << endl;
	}

	// cleanup
	input.close();
	out_header.close();
	out_branches.close();
	out_body.close();
}

void EventClassGenerator::parseClassDelphes (void) {
	stringstream cl_header, cl_body;
	ofstream of_header, of_body;

	cl_header << "#ifndef " << classname << "_h" << endl;
	cl_header << "#define " << classname << "_h" << endl << endl;

	cl_header << "#include <TROOT.h>" << endl;
	cl_header << "#include <TKey.h>" << endl;
	cl_header << "#include <TClonesArray.h>" << endl;
	cl_header << "#include <TLorentzVector.h>" << endl;
	cl_header << "#include <iostream>" << endl;
	cl_header << "#include <vector>" << endl;
	cl_header << "#include <ExRootAnalysis/ExRootAnalysis/ExRootTreeReader.h>" << endl;
	cl_header << "#include <Delphes-3.2.0/classes/DelphesClasses.h>" << endl << endl;


	cl_header << "using namespace std;" << endl << endl;

	cl_header << "class HEPParticle {" << endl;
	cl_header << "public:" << endl;
	cl_header << "\tTLorentzVector momentum;" << endl;
	cl_header << "\tfloat EhadOverEem;" << endl << endl;

	cl_header << "\tHEPParticle (double pt, double eta, double phi, double m, float eh) {" << endl;
	cl_header << "\t\tmomentum.SetPtEtaPhiM(pt, eta, phi, m);" << endl;
	cl_header << "\t\tEhadOverEem = eh;" << endl;
	cl_header << "\t}" << endl << endl;

	cl_header << "\tHEPParticle (double px, double py, double e) {" << endl;
	cl_header << "\t\tmomentum.SetPx(px);" << endl;
	cl_header << "\t\tmomentum.SetPy(py);" << endl;
	cl_header << "\t\tmomentum.SetE(e);" << endl;
	cl_header << "\t\tEhadOverEem = 0;" << endl;
	cl_header << "\t}" << endl << endl;

	cl_header << "\tHEPParticle (void) {" << endl;
	cl_header << "\t}" << endl;
	cl_header << "};" << endl << endl;

	cl_header << "class HEPLepton : public HEPParticle {" << endl;
	cl_header << "public:" << endl;
	cl_header << "\tshort int charge;\t// +1 or -1" << endl << endl;

	cl_header << "\tHEPLepton (double pt, double eta, double phi, double m, bool ch, float eh) : HEPParticle (pt, eta, phi, m, eh){" << endl;
	cl_header << "\t\tcharge = (ch) ? 1 : -1;" << endl;
	cl_header << "\t}" << endl;
	cl_header << "};" << endl << endl;

	cl_header << "class HEPPhoton : public HEPParticle {" << endl;
	cl_header << "public:" << endl;
	cl_header << "\tHEPPhoton (double pt, double eta, double phi, double m, float eh) : HEPParticle (pt, eta, phi, m, eh){" << endl;
	cl_header << "\t}" << endl;
	cl_header << "};" << endl << endl;

	cl_header << "// tau" << endl;
	cl_header << "class HEPJet : public HEPParticle {" << endl;
	cl_header << "public:" << endl;
	cl_header << "\tshort unsigned ntracks;" << endl;
	cl_header << "\tbool btag;" << endl << endl;

	cl_header << "\tHEPJet (double pt, double eta, double phi, double m, float eh, bool _btag, short unsigned _ntracks) : HEPParticle (pt, eta, phi, m, eh){" << endl;
	cl_header << "\t\tbtag = _btag;" << endl;
	cl_header << "\t\tntracks = _ntracks;" << endl;
	cl_header << "\t}" << endl;
	cl_header << "};" << endl << endl;

	cl_header << "class HEPTau : public HEPParticle {" << endl;
	cl_header << "public:" << endl;
	cl_header << "\tshort int charge;\t// +1 or -1" << endl;
	cl_header << "\tshort unsigned ntracks;" << endl << endl;

	cl_header << "\tHEPTau (double pt, double eta, double phi, double m, bool ch, short unsigned _ntracks, float eh) : HEPParticle (pt, eta, phi, m, eh){" << endl;
	cl_header << "\t\tcharge = (ch) ? 1 : -1;" << endl;
	cl_header << "\t\tntracks = _ntracks;" << endl;
	cl_header << "\t}" << endl;
	cl_header << "};" << endl << endl;

	cl_header << "class HEPTrack : public HEPParticle {" << endl;
	cl_header << "public:" << endl;
	cl_header << "\tshort int charge;\t// +1 or -1" << endl;
	cl_header << "\tshort int pid;" << endl;
	cl_header << "\tdouble etaCalo;\t/// eta @ first layer of calo" << endl;
	cl_header << "\tdouble phiCalo;\t/// phi @ first layer of calo" << endl << endl;

	cl_header << "\tHEPTrack (double pt, double eta, double phi, bool ch, short int _pid, double _etaCalo, double _phiCalo) {" << endl;
	cl_header << "\t\tmomentum.SetPtEtaPhiE(pt, eta, phi, pt);" << endl;
	cl_header << "\t\tcharge = (ch) ? 1 : -1;" << endl;
	cl_header << "\t\tpid = _pid;" << endl;
	cl_header << "\t\tetaCalo = _etaCalo;" << endl;
	cl_header << "\t\tphiCalo = _phiCalo;" << endl;
	cl_header << "\t}" << endl;
	cl_header << "};" << endl << endl;

	cl_header << "class HEPMCParticle {" << endl;
	cl_header << "public:" << endl;
	cl_header << "\tTLorentzVector momentum;" << endl;
	cl_header << "\tint pid;\t\t\t// PDG numbering of the particle" << endl;
	cl_header << "\tshort int status;\t// status code (-1 for initial state, 2 intermediate state, 1 final state)" << endl;
	cl_header << "\tunsigned m1;\t\t/// first mother index\t// para que???" << endl;
	cl_header << "\tunsigned m2;\t\t/// second mother index\t// para que???" << endl << endl;

	cl_header << "\tHEPMCParticle (double px, double py, double pz, double e, int _pid, short int _status, unsigned _m1, unsigned _m2) {" << endl;
	cl_header << "\t\tmomentum.SetPxPyPzE(px, py, pz, e);" << endl;
	cl_header << "\t\tpid = _pid;" << endl;
	cl_header << "\t\tstatus = _status;" << endl;
	cl_header << "\t\tm1 = _m1;" << endl;
	cl_header << "\t\tm2 = _m2;" << endl;
	cl_header << "\t}" << endl;
	cl_header << "};" << endl << endl;

	cl_header << "class HEPEvent {" << endl;
	cl_header << "public:" << endl;
	cl_header << "\t// Reconstructed stuff" << endl;
	cl_header << "\tvector<HEPPhoton> photons;\t/// Collection of reconstructed photons" << endl;
	cl_header << "\tvector<HEPLepton> electrons;\t/// Collection of reconstructed electrons" << endl;
	cl_header << "\tvector<HEPLepton> muons;\t\t/// Collection of reconstructed muons" << endl;
	cl_header << "\tvector<HEPTau> taus;\t\t\t/// Collection of reconstructed taus" << endl;
	cl_header << "\tvector<HEPJet> jets;\t\t\t/// Collection of reconstructed jets" << endl;
	cl_header << "//\tvector<HEPJet> genjets;\t\t/// Collection of generated jets" << endl;
	cl_header << "\tvector<HEPTrack> tracks;\t\t/// Collection of reconstructed tracks" << endl;
	cl_header << "\tHEPParticle MET;\t\t\t\t/// Reconstructed Missing Transverse Energy" << endl;
	cl_header << "\tHEPParticle MHT;\t\t\t\t/// Reconstructed Missing Hadronic Transverse Energy" << endl;
	cl_header << "\tdouble TET;\t\t\t\t\t\t\t/// Reconstructed Scalar sum of transverse Energy" << endl;
	cl_header << "\tdouble THT;\t\t\t\t\t\t\t/// Reconstructed Scalar sum of hadronic transverse energy" << endl << endl;

	cl_header << "\t// Reconstructed MonteCarlo particles" << endl;
	cl_header << "//\tvector<MCParticleFormat> MCHadronicTaus;\t\t/// taus decaying hadronically" << endl;
	cl_header << "//\tvector<MCParticleFormat> MCMuonicTaus;\t\t/// taus decaying into muon" << endl;
	cl_header << "//\tvector<MCParticleFormat> MCElectronicTaus;\t/// taus decaying into electron" << endl;
	cl_header << "//\tvector<MCParticleFormat> MCBquarks;\t\t\t/// b-quarks" << endl;
	cl_header << "//\tvector<MCParticleFormat> MCCquarks;\t\t\t/// c-quarks" << endl << endl;

	cl_header << "\t// MonteCarlo stuff" << endl;
	cl_header << "\tunsigned nparts;       /// number of particles in the event" << endl;
	cl_header << "\tunsigned processId;    /// identity of the current process" << endl;
	cl_header << "\tdouble weight;      /// event weight" << endl;
	cl_header << "\tdouble scale;       /// scale Q of the event" << endl;
	cl_header << "\tdouble alphaQED;    /// ALPHA_em value used" << endl;
	cl_header << "\tdouble alphaQCD;    /// ALPHA_s value used" << endl;
	cl_header << "\tdouble PDFscale;" << endl << endl;

	cl_header << "\t/// List of generated particles" << endl;
	cl_header << "\tvector<HEPMCParticle> particles;" << endl << endl;

	cl_header << "\tvoid loadEvent (void);" << endl;
	cl_header << "};" << endl;

	cl_header << "#endif" << endl;



	cl_body << "#include \"" << classname << ".h\"" << endl << endl;
	cl_body << "ExRootTreeReader *ex_tree;" << endl;
	cl_body << "TTree *tree;" << endl << endl;

	cl_body << "TClonesArray *branchJet;" << endl;
	cl_body << "TClonesArray *branchElectron;" << endl;
	cl_body << "TClonesArray *branchPhoton;" << endl;
	cl_body << "TClonesArray *branchMuon;" << endl;
	cl_body << "TClonesArray *branchMissingET;" << endl;
	cl_body << "TClonesArray *branchScalarHT;" << endl;
	cl_body << "TClonesArray *branchGenParticle;" << endl;
	cl_body << "TClonesArray *branchTrack;" << endl;
	cl_body << "TClonesArray *branchTower;" << endl;
	cl_body << "TClonesArray *branchLHCOEvent;" << endl;
	cl_body << "TClonesArray *branchLHEFEvent;" << endl;
	cl_body << "TClonesArray *branchHepMCEvent;" << endl;
	cl_body << "TClonesArray *branchEvent;" << endl << endl;

	cl_body << "void HEPEvent::loadEvent (void) {" << endl;
	cl_body << "\t// Fill electrons" << endl;
	cl_body << "\tif (branchElectron != NULL) {" << endl;
	cl_body << "\t\tfor (unsigned i=0;i<static_cast<unsigned>(branchElectron->GetEntries());i++) {" << endl;
	cl_body << "\t\t\tElectron* electron = dynamic_cast<Electron*>(branchElectron->At(i));" << endl;
	cl_body << "\t\t\tHEPLepton elec (electron->PT, electron->Eta, electron->Phi, 0, electron->Charge, electron->EhadOverEem);" << endl;
	cl_body << "\t\t\telectrons.push_back(elec);" << endl;
	cl_body << "\t\t}" << endl;
	cl_body << "\t}" << endl << endl;

	cl_body << "\t// Fill muons" << endl;
	cl_body << "\tif (branchMuon != NULL) {" << endl;
	cl_body << "\t\tfor (unsigned i=0;i<static_cast<unsigned>(branchMuon->GetEntries());i++) {" << endl;
	cl_body << "\t\t\tMuon* muon = dynamic_cast<Muon*>(branchMuon->At(i));" << endl;
	cl_body << "\t\t\tHEPLepton mu (muon->PT, muon->Eta, muon->Phi, 0, muon->Charge, 0);" << endl;
	cl_body << "\t\t\tmuons.push_back(mu);" << endl;
	cl_body << "\t\t}" << endl;
	cl_body << "\t}" << endl << endl;

	cl_body << "\t// Fill photons" << endl;
	cl_body << "\tif (branchPhoton != NULL) {" << endl;
	cl_body << "\t\tfor (unsigned i=0;i<static_cast<unsigned>(branchPhoton->GetEntries());i++) {" << endl;
	cl_body << "\t\t\tPhoton* photon = dynamic_cast<Photon*>(branchPhoton->At(i));" << endl;
	cl_body << "\t\t\tHEPPhoton phot (photon->PT, photon->Eta, photon->Phi, 0, photon->EhadOverEem);" << endl;
	cl_body << "\t\t\tphotons.push_back(phot);" << endl;
	cl_body << "\t\t}" << endl;
	cl_body << "\t}" << endl << endl;

	cl_body << "\t// Fill jets and taus" << endl;
	cl_body << "\tif (branchJet != NULL) {" << endl;
	cl_body << "\t\tfor (unsigned i=0;i<static_cast<unsigned>(branchJet->GetEntries());i++) {" << endl;
	cl_body << "\t\t\tJet* part = dynamic_cast<Jet*>(branchJet->At(i));" << endl << endl;

	cl_body << "\t\tif (part->TauTag == 1) {" << endl;
	cl_body << "\t\t\tHEPTau t (part->PT, part->Eta, part->Phi, 0, part->Charge, 0, part->EhadOverEem);" << endl;
	cl_body << "\t\t\ttaus.push_back(t);" << endl;
	cl_body << "\t\t} else {" << endl;
	cl_body << "\t\t\tHEPJet jt (part->PT, part->Eta, part->Phi, 0, part->EhadOverEem, part->BTag, 0);" << endl;
	cl_body << "\t\t\tjets.push_back(jt);" << endl;
	cl_body << "\t\t}" << endl;
	cl_body << "\t\t}" << endl;
	cl_body << "\t}" << endl << endl;

	cl_body << "\t// Track collection" << endl;
	cl_body << "\tif (branchTrack != NULL) {" << endl;
	cl_body << "\t\tfor (unsigned i=0;i<static_cast<unsigned>(branchTrack->GetEntries());i++) {" << endl;
	cl_body << "\t\t\tTrack* track = dynamic_cast<Track*>(branchTrack->At(i));" << endl;
	cl_body << "\t\t\tHEPTrack tr (track->PT, track->Eta, track->Phi, track->Charge, track->PID, track->EtaOuter, track->PhiOuter);" << endl;
	cl_body << "\t\t\ttracks.push_back(tr);" << endl;
	cl_body << "\t\t}" << endl;
	cl_body << "\t}" << endl << endl;

	cl_body << "\t// THT" << endl;
	cl_body << "\tif (branchScalarHT != NULL) {" << endl;
	cl_body << "\t\tif (branchScalarHT->GetEntries()>0) {" << endl;
	cl_body << "\t\t\tScalarHT* part = dynamic_cast<ScalarHT*>(branchScalarHT->At(0));" << endl;
	cl_body << "\t\t\tTHT = part->HT;" << endl;
	cl_body << "\t\t}" << endl;
	cl_body << "\t}" << endl << endl;

	cl_body << "\t// MET" << endl;
	cl_body << "\tif (branchMissingET != NULL) {" << endl;
	cl_body << "\t\tif (branchMissingET->GetEntries()>0) {" << endl;
	cl_body << "\t\t\tMissingET* part = dynamic_cast<MissingET*>(branchMissingET->At(0));" << endl;
	cl_body << "\t\t\tHEPParticle recpart (part->MET*cos(part->Phi), part->MET*sin(part->Phi), part->MET);" << endl;
	cl_body << "\t\t\tMET = recpart;" << endl;
	cl_body << "\t\t}" << endl;
	cl_body << "\t}" << endl << endl;

	cl_body << "\t// GenParticle collection" << endl;
	cl_body << "\tif (branchGenParticle != NULL) {" << endl;
	cl_body << "\t\tfor (unsigned i=0;i<static_cast<unsigned>(branchGenParticle->GetEntries());i++) {" << endl;
	cl_body << "\t\t\tGenParticle* part = dynamic_cast<GenParticle*>(branchGenParticle->At(i));" << endl;
	cl_body << "\t\t\tHEPMCParticle mcpart (part->Px, part->Py, part->Pz, part->E, part->PID, part->Status, part->M1, part->M2);" << endl;
	cl_body << "\t\t\tparticles.push_back(mcpart);" << endl;
	cl_body << "\t\t}" << endl;
	cl_body << "\t}\n}" << endl << endl;

	of_header.open(classname + ".h");
	of_header << cl_header.str();
	of_header.close();

	of_body.open(classname + ".cxx");
	of_body << cl_body.str();
	of_body.close();
}
