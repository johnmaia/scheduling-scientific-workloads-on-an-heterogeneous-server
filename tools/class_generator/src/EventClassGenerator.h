#ifndef EventClassGenerator_h
#define EventClassGenerator_h

#include <cstdlib>
#include <cstring>
#include <string>
#include <sstream>
#include <vector>
#include <fstream>
#include <TFile.h>
#include <TTree.h>
#include <TIterator.h>
#include <TKey.h>
#include <unistd.h>


#include <iostream>

#define PARSE_CLASS_HEADER_STOP "// List of branches"
#define PARSE_CLASS_HEADER_START "// Declaration of leaf types"
#define PARSE_CLASS_BRANCHES_STOP "TTree *tree=0"
#define PARSE_CLASS_BODY_START "fChain->SetMakeClass(1);"
#define PARSE_CLASS_BODY_STOP "Notify"
#define PARSE_CLASS_BODY_POINTER_START "// Set object pointer"
#define PARSE_CLASS_BODY_POINTER_STOP "// Set branch addresses and branch pointers"
#define PARSE_CLASS_HEADER_CONST_START "// Fixed size dimensions of array or collections stored in the TTree if any."
#define PARSE_CLASS_HEADER_CONST_STOP "class "
using namespace std;

// All variables are public for debugging
class EventClassGenerator {
	string tree_name;
	string filename;
	string npa_dir;
	string classname;
	string directory;
	TFile *data_creator;
	TTree *tree;
	long unsigned number_events;

public:

	EventClassGenerator (void);
	EventClassGenerator (string _filename, string _dir);
	EventClassGenerator (string _filename, string _dir, string _tree_name, bool);
	EventClassGenerator (string _filename, string _classname, string _dir);
	EventClassGenerator (string _filename, string _classname, string _dir, string _tree_name);
	~EventClassGenerator (void);

	bool makeClassCommon (void);
	void parseClassCommon (void);
	void parseClassDelphes (void);
};

#endif
