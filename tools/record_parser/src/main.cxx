#include <cstdlib>
#include <iostream>
#include <string>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <boost/program_options.hpp>
#include "RecordVariables.h"

using namespace std;
namespace po = boost::program_options;

string file;
string event;
string analysis;
unsigned cuts;
bool type;


bool programOptions (int argc, char *argv[]) {
	// creates the program options
	po::options_description desc ("Valid options");
	desc.add_options()
		("help,h", "produces the help message")
		("file,f", po::value<string>(), "input file")
		("event,e", po::value<string>(), "input event file")
		("analysis,a", po::value<string>(), "analysis file/class name")
		("cuts,c", po::value<unsigned>(), "number of cuts of the analysis")
		("type,t", po::value<bool>(), "type of the root file - true for delphes")
	;
	po::variables_map vm;
	po::store(po::parse_command_line(argc, argv, desc), vm);
	po::notify(vm);

	// get the options
	if (vm.count("help")) {
		cout << desc << endl;
		return false;
	}

	if (vm.count("file")) {
		file = vm["file"].as<string>();
		struct stat buffer;
		string test_file = file;
		if (stat (test_file.c_str(), &buffer) != 0) {
			cerr << "Input file " << file << " does not exist" << endl;
			return false;
		}
	} else {
		cerr << "Input file not set" << endl;
		return false;
	}

	if (vm.count("event")) {
		event = vm["event"].as<string>();
		struct stat buffer;

		if (stat (event.c_str(), &buffer) != 0) {
			cerr << "Input event file " << event << " does not exist" << endl;
			return false;
		}
	} else {
		cerr << "Input event file not set" << endl;
	}

	if (vm.count("analysis")) {
		analysis = vm["analysis"].as<string>();
	} else {
		cerr << "Input analysis file not set" << endl;
	}

	if (vm.count("type")) {
		type = vm["type"].as<bool>();
	} else {
		cerr << "Type not set" << endl;
		return false;
	}

	if (vm.count("cuts")) {
		cuts = vm["cuts"].as<unsigned>();
	} else {
		cerr << "Number of analysis cuts not set" << endl;
		return false;
	}

	return true;
}


int main (int argc, char *argv[]) {

	if (!programOptions(argc, argv))
		return -1;

	RecordVariables *record;

	record = new RecordVariables (file, event);

	record->parse(analysis, cuts, type);

	delete record;

	return 0;
}
