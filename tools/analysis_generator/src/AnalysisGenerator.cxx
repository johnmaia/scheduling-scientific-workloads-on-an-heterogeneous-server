#include "AnalysisGenerator.h"

AnalysisGenerator::AnalysisGenerator (string _name, string _dir, string _tree_name) {
	name = _name;
	dir = _dir;
	tree_name = _tree_name;
}

void AnalysisGenerator::generate (void) {
	ofstream out_header (dir + name + ".h");
	ofstream out_body (dir + name + "_cfg.cxx");
	ofstream out_body_user (dir + name + ".cxx");

	out_header << "#ifndef " << name << "_h" << endl;
	out_header << "#define " << name << "_h" << endl << endl;
	out_header << "#include <cstdlib>" << endl;
	out_header << "#include <iostream>" << endl;
	out_header << "#include <string>" << endl;
	out_header << "#include <vector>" << endl;
	out_header << "#include <algorithm>" << endl;
	out_header << "#include <omp.h>" << endl;
	out_header << "#include <DataAnalysis.h>" << endl;
	out_header << "#ifdef D_MPI" << endl;
	out_header << "\t#include <mpi.h>" << endl;
	out_header << "#endif" << endl;
	out_header << endl << "using namespace std;" << endl << endl << endl;

	out_header << "class " << name << " : public DataAnalysis {" << endl;
	out_header << "\t// Variables to record per cut" << endl << endl << "// Pdfs to record" << endl << endl << endl;
	out_header << "\t// Insert your class variables here" << endl << endl << endl;

	out_header << "\tvoid recordVariables (unsigned cut_number);" << endl;
	out_header << "\tvoid writeVariables (void);" << endl;
	out_header << "\tvoid recordPdfs (void);" << endl;
	out_header << "\tvoid initRecord (void);" << endl;
	out_header << "\tvoid writePdfs (void);" << endl;
	out_header << "\tvoid initialize (void);" << endl;
	out_header << "\tvoid finalize (void);" << endl << endl;
	out_header << "public:" << endl;
	out_header << "\t" << name << " (vector<string> _root_file, string _tree_filename, string _output_root_file, string _signal, string _background, unsigned ncuts);" << endl;
	out_header << "\t" << name << " (vector<string> _root_file, string _tree_filename, string _output_root_file, string _signal, string _background, string _tree_name, unsigned ncuts);" << endl;
	out_header << "};\n\n#endif" << endl;


	out_body_user << "#include \"" << name << ".h\"" << endl;

	out_body_user << "#include \"EventInterface.h\"" << endl;
	/*
		BEGIN - List of files in shared memory
	*/
	out_body_user << "#include <boost/interprocess/ipc/message_queue.hpp>" << endl << endl;
	/*
		END - List of files in shared memory
	*/
	out_body_user << "using namespace std;" << endl;
	/*
		BEGIN - List of files in shared memory
	*/
	out_body_user << "using namespace boost::interprocess;" << endl << endl;
	/*
		END - List of files in shared memory
	*/
	out_body_user << "extern vector<HEPEvent> events;" << endl;
	out_body_user << "extern long unsigned event_counter;" << endl;
	out_body_user << "#pragma omp threadprivate(event_counter)" << endl << endl << endl;

	out_body_user << "// Use this constructor if you don't specify a ttree in the input file" << endl;
	out_body_user << name << "::" << name << " (vector<string> _root_file, string _tree_filename, string _output_root_file, string _signal, string _background, unsigned ncuts) : DataAnalysis (_root_file, _tree_filename, _output_root_file, _signal, _background, ncuts) {" << endl;
	out_body_user << "\t// Set to true if you want to save events that pass all cuts" << endl;
	out_body_user << "\tsave_events = false;" << endl << endl;
	out_body_user << "\t// Initialise your class variables here" << endl << endl;
	out_body_user << "}" << endl << endl;

	out_body_user << "// Use this constructor if you specify a ttree in the input file" << endl;
	out_body_user << name << "::" << name << " (vector<string> _root_file, string _tree_filename, string _output_root_file, string _signal, string _background, string _tree_name, unsigned ncuts) : DataAnalysis (_root_file, _tree_filename, _output_root_file, _signal, _background, _tree_name, ncuts) {" << endl;
	out_body_user << "\t// Set to true if you want to save events that pass all cuts" << endl;
	out_body_user << "\tsave_events = false;" << endl << endl;
	out_body_user << "\t// Initialise your class variables here" << endl << endl;
	out_body_user << "}" << endl << endl;

	out_body_user << "// Initialize any information before processing the events" << endl;
	out_body_user << "void " << name << "::initialize (void) {" << endl << endl;
	out_body_user << "}" << endl << endl;

	out_body_user << "// Finalize any calculations after processing all events" << endl;
	out_body_user << "void " << name << "::finalize (void) {" << endl << endl;
	out_body_user << "}" << endl << endl;

	out_body_user << "// Sample cut" << endl;
	out_body_user << "bool cut (void) {" << endl;
	out_body_user << "\treturn true;" << endl;
	out_body_user << "}" << endl << endl;


	out_body << "#include \"" << name << ".h\"" << endl << endl;
	out_body << "#define THIS_THREAD(x) thread_id * number_of_cuts + x" << endl << endl;
	out_body << "using namespace std;" << endl << endl;
	out_body << "extern vector<HEPEvent> events;" << endl;
	out_body << "extern unsigned num_threads;" << endl;
	out_body << "extern unsigned thread_id;" << endl;
	out_body << "#pragma omp threadprivate(thread_id)" << endl;
	out_body << "extern long unsigned event_counter;" << endl;
	out_body << "#pragma omp threadprivate(event_counter)" << endl << endl << endl;
	out_body << "// ***********************************************" << endl;
	out_body << "// Method to record variables, do not edit" << endl;
	out_body << endl;


	out_body << "// Write here the variables and expressions to record per cut" << endl;
	out_body << "#ifdef RecordVariables" << endl << endl;
	out_body << "#endif" << endl;
	out_body << "// ***********************************************" << endl << endl << endl;

	out_body << "// ***********************************************" << endl;
	out_body << "// Method to record pdfs, do not edit" << endl;
	out_body << endl;


	out_body << "// Write here the variables to record as pdf" << endl;
	out_body << "#ifdef PdfVariables" << endl << endl;
	out_body << "#endif" << endl;
	out_body << "// ***********************************************" << endl << endl << endl;

	out_body_user << "int main (int argc, char *argv[]) {" << endl;
	out_body_user << "\tmap<string, string> options;\n\tProgramOptions opts;" << endl;
	out_body_user << "\tvector<string> file_list;" << endl << endl;
	/*
		BEGIN - List of files in shared memory
	*/
	out_body_user << "\tsize_t const max_msg_size = 0x100;" << endl << endl;
	out_body_user << "\ttry{" << endl;
	out_body_user << "\t\t//Erase previous message queue" << endl;
	out_body_user << "\t\tmessage_queue::remove(\"filelist_queue\");" << endl << endl;
	out_body_user << "\t\t//Create a message_queue" << endl;
	out_body_user << "\t\tmessage_queue mq(create_only, \"filelist_queue\", file_list.size(), max_msg_size);" << endl << endl;
	out_body_user << "\t\t//Read a specific amount of files allocated for each process" << endl;
	out_body_user << "\t\tfor (int i = 0; i < file_list.size(); i++) {" << endl;
	out_body_user << "\t\t\tstd::string s(file_list[i]);" << endl;
	out_body_user << "\t\t\tmq.send(s.data(), s.size(), 0);" << endl;
	out_body_user << "\t\t}" << endl;
	out_body_user << "\t}" << endl;
	out_body_user << "\tcatch(interprocess_exception &ex){" << endl;
	out_body_user << "\t\tstd::cout << ex.what() << std::endl;" << endl;
	out_body_user << "\t}" << endl;
	/*
		END - List of files in shared memory
	*/
	out_body_user << "\t#ifdef D_MPI" << endl;
	out_body_user << "\t\tMPI_Init(&argc, &argv);" << endl;
	out_body_user << "\t#endif" << endl << endl;
	out_body_user << "\tif (!opts.getOptions(argc, argv, options, file_list))\n\t\treturn 0;" << endl << endl;
	out_body_user << "\tunsigned number_of_cuts = 1;" << endl << endl;

	if (tree_name == "unset")
		out_body_user << "\t" << name << " anl (file_list, options[\"record\"], options[\"output\"], options[\"signal\"], options[\"background\"], number_of_cuts);" << endl;
	else
		out_body_user << "\t" << name << " anl (file_list, options[\"record\"], options[\"output\"], options[\"signal\"], options[\"background\"], \"" << tree_name << "\", number_of_cuts);" << endl << endl;

	out_body_user << "\t// Add the cuts using the addCut method" << endl;
	out_body_user << "\tanl.addCut(\"cut\", cut);" << endl << endl;
	out_body_user << "\t// Processes the analysis" << endl;
	out_body_user << "\tanl.run();" << endl << endl;
	out_body_user << "\t#ifdef D_MPI" << endl;
	out_body_user << "\t\tMPI_Finalize();" << endl;
	out_body_user << "\t#endif" << endl << endl;
	/*
		BEGIN - List of files in shared memory
	*/
	out_body_user << "\tboost::interprocess::message_queue::remove(\"filelist_queue\");" << endl << endl;
	/*
		END - List of files in shared memory
	*/
	out_body_user << "\treturn 0;\n}" << endl;

	out_header.close();
	out_body.close();
}
