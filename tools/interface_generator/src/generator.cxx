#include "generator.h"

using namespace std;

bool parse = false;
vector<string> elements;
string classname;

void tokenize (string line) {
	stringstream current;
	bool first = false;

	if (line.find("public:") != string::npos) {
		parse = true;
		return;
	}

	if (line.find("HEPEvent (TTree *tree);") != string::npos)
		parse = false;

	if (parse)
		for (unsigned i = 0; i < line.size(); i++) {
			// trim spaces
			if (!(line[i] == ' ') && !(line[i] == '\t')) {
				first = true;
				// stop condition
				if (line[i] == '[' || line[i] == ';') {
					if(current.str().at(0) == '*') {
						string aux = current.str();
						aux.erase(0,1);
						elements.push_back(aux);
					}
					else {
						elements.push_back(current.str());
					}

					return;
				}

				current << line[i];
			} else {
				if (first)
					current.str(string());
			}
		}
}

// Reads a line from the input file
void parseLines (string file) {
	ifstream input (file.c_str(), ifstream::in);
	string line;

	while (getline(input, line)) {
		type_changed = false;
		// 1: trim spaces and tabs
		tokenize(line);
		// 2: check if the first element is a type
//		if (elements.size())
//			parseElements(elements);
	}
}

void writeDefines (void) {
	of << endl << "/*" << endl << " *\t defines below" << endl << " */" << endl << endl << endl;

	for (unsigned a = 0; a < elements.size(); a++) {
		//if (a != "mu")
			of << "#define " << elements[a] << " events[event_counter]." << elements[a] << endl;
	}
}

void writeInterface (string output) {
	of.open(output.c_str(), fstream::out);

	of << "// ###################################################################################" << endl;
	of << "//" << endl;
	of << "//							!!Disclaimer!!" << endl;
	of << "//" << endl;
	of << "// ###################################################################################" << endl;
	of << "//" << endl;
	of << "// Add variables in the EventData class, between the public and constructor statements" << endl;
	of << "//" << endl;
	of << "// ###################################################################################" << endl;
	of << endl;
	of << "#include <vector>" << endl;
	of << "#include \"" << classname << "\"" << endl << endl;
	of << "extern std::vector<HEPEvent> events;" << endl;
	of << "extern long unsigned event_counter;" << endl;

	writeDefines();
}

int main (int argc, char* argv[]) {

	if (argc < 3) {
		cout << "EventData file name and/or source directory not passed" << endl;
		cout << "Pass it in the \"./EDGen dir file\" format" << endl;
		return -1;
	}

	stringstream ss1, ss2;
	string file1, file2;

	ss1 << argv[1] << "/" << argv[2];
	ss1 >> file1;

	ss2 << argv[1] << "/EventInterface.h";
	ss2 >> file2;

	classname = argv[2];

	parseLines(file1);

	writeInterface(file2);

	return 0;
}
